import React from "react"

import ReactDOM from "react-dom"
import * as serviceWorker from "./serviceWorker"

import { Route, Switch, Redirect } from "react-router"
import { BrowserRouter } from "react-router-dom"

import { Provider } from "react-redux"
import { applyMiddleware, createStore, compose } from "redux"
import {
  LoginActionWithTickets,
  GetCurrentUserWithTickets
} from "./redux/actions/myExperience/authActions"
import setAuthToken from "./utils/setAuthToken"

import reducers from "./redux/reducers"
import promise from "redux-promise"
import thunk from "redux-thunk"

// Header - Footer
import Navbar from "./components/commons/Navbar"
import Footer from "./components/commons/Footer"
//  Components
import HomeComponent from "./components/homepage/index"
import Startups from "./components/crowds/startups/index"
import PitchComp from "./components/crowds/pitchComp/index"
import StartupStation from "./components/crowds/StartupStation/index"
import menaHipos from "./components/crowds/menaHipos/index"
import Volunteers from "./components/crowds/volunteers/index"
import VolunteersForm from "./components/crowds/volunteers/volunteers-form"
import Media from "./components/crowds/media/index"
import Speakers from "./components/speakers/index"
import TechTrack from "./components/programs/TechTrack"
import Summit101 from "./components/summit101/index"
import SatelliteEvents from "./components/summit/satelliteEvents/index"
import Activities from "./components/activities/Activities"
import Partnerships from "./components/crowds/partnerships/index"
import Workshops from "./components/crowds/workshops/index"
import MySessionsList from "./components/profile/my-sessions"
import LoginWithTicket from "./components/login/index"
import Launchpad from "./components/crowds/launchpad/index"
import Agenda from "./components/agenda/index"

import NotFound from "./components/404/index"

import withTracker from "./withTracker"
import "./styles/sass/App.scss"

const store = createStore(
  reducers,
  compose(
    applyMiddleware(promise, thunk)
    // DEVTOOl
  )
)

if (process.env.NODE_ENV !== "production") {
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
}
if (window.localStorage.token) {
  setAuthToken(window.localStorage.token)
  store.dispatch(LoginActionWithTickets(window.localStorage.token))
  store.dispatch(GetCurrentUserWithTickets(window.localStorage.token))
}

ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <div>
        <Navbar />
        <div id="main-fold">
          <Switch>
            <Route
              exact
              path="/"
              render={() => {
                return <Redirect to="/home" />
              }}
            />
            
            <Route
              exact
              path="/summit_tickets"
              render={() => {
                return <Redirect path="/home#tickets" />
              }}
            />
            <Route exact path="/home" component={withTracker(HomeComponent)} />
            <Route exact path="/startups" component={withTracker(Startups)} />
            <Route
              exact
              path="/pitch-competition"
              component={withTracker(PitchComp)}
            />
            <Route
              exact
              path="/startup-station"
              component={withTracker(StartupStation)}
            />
            <Route
              exact
              path="/mena-hipos"
              component={withTracker(menaHipos)}
            />
            <Route
              exact
              path="/volunteers"
              component={withTracker(Volunteers)}
            />
            <Route
              exact
              path="/volunteers/form"
              component={withTracker(VolunteersForm)}
            />
            <Route exact path="/media" component={withTracker(Media)} />
            <Route exact path="/speakers" component={withTracker(Speakers)} />

            <Route exact path="/programs" component={withTracker(TechTrack)} />
            <Route exact path="/summit101" component={withTracker(Summit101)} />
            <Route
              exact
              path="/satelliteEvents"
              component={withTracker(SatelliteEvents)}
            />

            <Route
              exact
              path="/activities"
              component={withTracker(Activities)}
            />
            <Route
              exact
              path="/partners"
              component={withTracker(Partnerships)}
            />
            <Route exact path="/launchpad" component={withTracker(Launchpad)} />

            <Route exact path="/workshops" component={withTracker(Workshops)} />
            <Route
              exact
              path="/my-sessions"
              component={withTracker(MySessionsList)}
            />
            <Route
              exact
              path="/login-with-tickets"
              component={withTracker(LoginWithTicket)}
            />
            <Route exact path="/agenda" component={withTracker(Agenda)} />
            <Route path="*" component={NotFound} />
          </Switch>
        </div>
        <Footer />
      </div>
    </BrowserRouter>
  </Provider>,
  document.getElementById("root")
)

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister()
