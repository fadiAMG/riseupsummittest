import React from "react"
import DocumentTitle from "react-document-title"
import classNames from "classnames"
import FeaturePost from "../commons/FeaturePost"
import HeaderSlider from "../commons/HeaderSlider"

// Data
import { strings } from "../strings"

class TechTrack extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      active: false,
      selected: "TECHNOLOGY"
    }
  }
  render() {
    let _self = this
    function RenderTabsContent() {
      if (_self.state.active) {
        return (
          <div>
            <div className="tracks-wrap__v2">
              <div className="tracks-list">
                <div className="item">
                  <h2 className="header">CREATIVE</h2>
                  <div className="content">
                    <p className="slog">THE ULTIMATE SUMMIT EXPERIENCE</p>
                    <h4>CREATIVITY IS THE CORNERSTONE...</h4>
                    <ul>
                      <li>
                        <p>
                          Creativity and innovation are cornerstones for the
                          sustenance, advancement, and inspiration of the Human
                          Experience. Our creative track examines the most
                          prominent innovations in specific creative industries,
                          and cutting-edge aspects of the creative process in
                          its stages of ideation to production. We also tackle
                          how creatives have grounded themselves at the
                          forefront of business, and we value stories of how
                          creatives monetize, sustain themselves, and provide
                          cornerstone support for other industries and
                          businesses.
                        </p>
                      </li>
                      <li>
                        <h5>POTENTIAL AREAS OF INTEREST:</h5>
                        <p>
                          Film, Design, Marketing, Branding, Fashion, Writing
                          and Content Creation, Gaming, Heritage and Cultural
                          Capital, Design Thinking, UI/UX.
                        </p>
                      </li>
                    </ul>
                    <h6>{""}</h6>
                  </div>
                </div>
                <div className="item">
                  <h2 className="header">TECH</h2>

                  <div className="content">
                    <p className="slog">THE ULTIMATE SUMMIT EXPERIENCE</p>
                    <h4>TECH IS ABSOLUTE...</h4>
                    <ul>
                      <li>
                        <p>
                          Technology is by no means separate from our grounded
                          experiences; it shapes and drives them. In our Tech
                          track, we showcase cutting edge technology, address
                          industry challenges and opportunities, and demonstrate
                          how businesses scale through technology. Importantly,
                          we also want to stop and reflect on the social
                          implications and everyday impact tech has on human
                          life. We aim to cover topics that demystify
                          technologies like AI, IoT, and Big Data while
                          expanding on the myriad ways tech can simplify
                          business-customer interaction, increase growth and
                          optimize efficiency.
                        </p>
                      </li>
                      <li>
                        <h5>POTENTIAL AREAS OF INTEREST:</h5>
                        <p>
                          AI & Big Data, Coding, Cloud Services, Blockchain &
                          Cryptocurrencies, Cybersecurity, Robotics, AR & VR,
                          Tech industries like AgriTech, Assistive Tech, EdTech,
                          FinTech, MedTech/Health Tech, Creating human-centric
                          products, Including engineers in product design,
                          Future of work, Digital skills gap.
                        </p>
                      </li>
                    </ul>
                    <h6>{""}</h6>
                  </div>
                </div>
                <div className="item">
                  <h2 className="header">CAPITAL</h2>

                  <div className="content">
                    <p className="slog">THE ULTIMATE SUMMIT EXPERIENCE</p>
                    <h4>
                      THE BEST CAPITAL IS GROUNDED IN REALISTIC EXPECTATIONS AND
                      GOALS
                    </h4>
                    <ul>
                      <li>
                        <p>
                          Capital is key. As always, our capital track hosts a
                          varied range of the trials and tribulations of
                          funding, investment, and raising capital. With this
                          year’s track, we also want to demonstrate this year’s
                          slogan of “aim high, stay grounded.” While aims for
                          raising capital should have the sky as limit, we
                          emphasize the importance grounding expectations and
                          making sure the foundations of our decisions are
                          solidly rooted. Our sessions and talks cover topics of
                          interest for both entrepreneurs and investors.
                        </p>
                      </li>
                      <li>
                        <h5>POTENTIAL AREAS OF INTEREST:</h5>
                        <p>
                          Know-hows of investment, Economics of venture capital,
                          Boot-strapping, Valuation, Debt financing, Equity
                          financing, Scalability, Future prospect evaluation,
                          Raising capital through alternatives, Funding through
                          technology (ICOs, Fintech, etc.)
                        </p>
                      </li>
                    </ul>
                    <h6>{""}</h6>
                  </div>
                </div>
                {/* <div className="item">
                  <h2 className="header">GROUNDWORK</h2>
                  <div className="content">
                    <p className="slog">THE ULTIMATE SUMMIT EXPERIENCE</p>
                    <h4>STRONG FOUNDATIONS LAST LONGER...</h4>
                    <ul>
                      <li>
                        <p>
                          Because we understand the importance of foundations,
                          this year’s Summit sheds light upon the building
                          blocks needed by entrepreneurs in each stage of their
                          journey: Seed & Development, Growth & Expansion and
                          Maturity. Our new Groundwork project targets these
                          startup lifecycle stages through our three primary
                          tracks; Capital, Tech and Creative.
                        </p>
                      </li>
                      <li>
                        <h5>CONTENT TYPES:</h5>
                        <p>
                          Early Stage content focusing on the basics of capital
                          funding, the creative process and the integration of
                          tech. Growth stage content focusing on correct
                          expansions methods, means of positioning oneself in
                          the fields of Creative/ Tech/Capital, management
                          techniques, and assessing the competition.
                        </p>
                        <p>
                          Late Stage content focusing on the intricacies of
                          raising capital, sustaining the creative workflow and
                          expanding the Tech horizons, and succeeding to exit!
                        </p>
                      </li>
                    </ul>
                    <h6>Creatives #DriveHX</h6>
                  </div>
                </div> */}
              </div>
            </div>
          </div>
        )
      } else {
        return (
          <div>
            <FeaturePost
              title={strings.GroundedExperiences.feature.title}
              details={strings.GroundedExperiences.feature.details}
              img={require("../../assets/images/techTrack.jpg")}
              ImgMaxHeight={"600px"}
              strength={250}
              noLink={true}
              seprator={false}
              // products={strings.GroundedExperiences.feature.products}
              style={{
                titleFontSize: "35px",
                detailsFontSize: "17px",
                slogColor: "#2a428c"
              }}
              BtnTitle={"Excited"}
              noArrows={true}
            />
          </div>
        )
      }
    }
    return (
      <div className="about--wrap">
        <DocumentTitle title="RiseUp Summit - Tracks" />
        <HeaderSlider slider={false} />
        {/* <div className="fullwidth--seprator" /> */}

        <ul className="nav--wrap">
          <li>
            <div
              onClick={() => this.setState({ active: false })}
              className={classNames({ active: !this.state.active })}
            >
              GROUNDED EXPERIENCES
            </div>
          </li>
          <li>
            <div
              onClick={() => this.setState({ active: true })}
              className={classNames({ active: this.state.active })}
            >
              TRACKS
            </div>
          </li>
        </ul>
        {RenderTabsContent()}
      </div>
    )
  }
}

export default TechTrack
