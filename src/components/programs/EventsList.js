import React from 'react'

const EventsList = (props) => {
	function renderList(){
		return props.list.map(event => {
			return(
				<div className="event">
					<div className="event--image">
						<img src={event.image} alt=""/>
					</div>
					<div className="event--details">
						<h3>{event.title}</h3>
						<p>{event.details}</p>
						<div className="date">{event.date}</div>
					</div>
				</div>
			)
		})
	}
	return(
		<div className="satellite--events-list">
			{renderList()}
		</div>
	)
}

export default EventsList