import React from "react"
import DocumentTitle from "react-document-title"
import classNames from "classnames"
import FeaturePost from "../commons/FeaturePost"
import HeaderSlider from "../commons/HeaderSlider"

import $ from "jquery"

import NetworkingEvents from "./NetworkingEvents"
// Data
import { strings } from "../strings"
import { data } from "../data"

const Tabs = [
  "NETWORKING EVENTS",
  "TALENT MATCHMAKING",
  "THE STARTUP MANIFESTO",
  "COWORKING & MEETING SPACES",
  "INVESTOR OFFICE HOURS",
  "UX Clinic Consultancy",
  "UBER Pitch n’Ride",
  "F&B AND ENTERTAINMENT",
  "RiseUp Xperience",
  "3-Day-Accelerator Program",
  "100 BEST ARABIC POSTERS",
  "Fast Track",
  "Career Coaching",
  "Impact Investment  Roundtable"
]
class Activities extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      active: 0
    }
    this.renderTabs = this.renderTabs.bind(this)
  }
  componentDidMount() {
    $(window).scrollTop($(".Activities").offset().top - 100)

    // let ele = document.querySelector(".fullwidth--seprator")
    // let windowHeight = window.innerHeight - 37
    // ele.style.paddingTop = windowHeight + "px"
  }
  renderTabs() {
    return Tabs.map((tab, i) => {
      return (
        <li key={i}>
          <div
            onClick={() => this.setState({ active: i })}
            className={classNames({ active: this.state.active === i })}
          >
            {tab}
          </div>
        </li>
      )
    })
  }
  render() {
    let string = strings.Activities
    let _self = this
    function RenderTabsContent() {
      // - NETWORKING
      if (_self.state.active === 0) {
        return (
          <div>
            <ul className="nav--wrap Activities ">{_self.renderTabs()}</ul>
            <FeaturePost
              title={string.networking.feature.title}
              details={string.networking.feature.details}
              img={require("../../assets/images/networking.jpeg")}
              Url="/"
              strength={300}
              slog="THE ULTIMATE SUMMIT EXPERIENCE"
              noArrows
              noLink
              products={string.networking.feature.products}
              seprator={false}
              ImgMaxHeight={"600px"}
              style={{
                slogFontSize: "18px",
                titleFontSize: "30px",
                detailsFontSize: "17px",
                // slogFontWeight: "400",
                slogColor: "#2a428c"
              }}
            />
            <NetworkingEvents list={data.Activities} />
          </div>
        )
      }
      // - MATCHMAKING
      if (_self.state.active === 1) {
        return (
          <div>
            <ul className="nav--wrap Activities ">{_self.renderTabs()}</ul>
            {/* products={string.matchmaking.feature.products} */}
            <FeaturePost
              caption="POWERED BY"
              captionImage={require("../../assets/images/Wuzzuf-logo-blue.png")}
              title={string.matchmaking.feature.title}
              details={string.matchmaking.feature.details}
              products={string.matchmaking.feature.products}
              img={require("../../assets/images/Wuzzuftalentmatchmaking.jpeg")}
              Url="https://goo.gl/1FNvwa"
              strength={300}
              noArrows
              BtnTitle="APPLY NOW"
              seprator={false}
              SelfLink={true}
              noLink={true}
              openText=""
              ImgMaxHeight={"500px"}
              style={{
                slogFontSize: "18px",
                titleFontSize: "30px",
                detailsFontSize: "16px",
                // productsFontSize:"17px",
                slogColor: "#2a428c"
              }}
            />
          </div>
        )
      }
      // - CONNECT SUPERMARKET
      if (_self.state.active === 2) {
        return (
          <div>
            <ul className="nav--wrap Activities ">{_self.renderTabs()}</ul>
            <FeaturePost
              title={string.contentSupermarket.feature.title}
              details={string.contentSupermarket.feature.details}
              img={require("../../assets/images/Manifesto.jpeg")}
              Url={`/partners/NeBMDb/riseup_connect_perks_providers`}
              strength={300}
              slog="THE ULTIMATE SUMMIT EXPERIENCE"
              noArrows
              BtnTitle="Apply to provide perks"
              seprator={false}
              SelfLink={false}
              openText=""
              ImgMaxHeight={"600px"}
              style={{
                slogFontSize: "18px",
                titleFontSize: "30px",
                detailsFontSize: "17px",
                // slogFontWeight: "400",
                slogColor: "#2a428c"
              }}
              noLink
            />
          </div>
        )
      }
      // - COWORKING & MEETING SPACES
      if (_self.state.active === 3) {
        return (
          <div>
            <ul className="nav--wrap Activities ">{_self.renderTabs()}</ul>
            <FeaturePost
              title={string.coworking.feature.title}
              details={string.coworking.feature.details}
              img={require("../../assets/images/coworking.jpg")}
              Url="/"
              strength={300}
              slog="THE ULTIMATE SUMMIT EXPERIENCE"
              noArrows
              noLink
              ImgMaxHeight={"600px"}
              style={{
                slogFontSize: "18px",
                titleFontSize: "30px",
                detailsFontSize: "17px",
                // slogFontWeight: "400",
                slogColor: "#2a428c"
              }}
              seprator={false}
            />
          </div>
        )
      }

      // INVESTOR OFFICE HOURS
      if (_self.state.active === 4) {
        return (
          <div>
            <ul className="nav--wrap Activities ">{_self.renderTabs()}</ul>
            <FeaturePost
              caption="POWERED BY"
              captionImage={require("../../assets/images/algebra-logo-grey.png")}
              title={string.investorOH.feature.title}
              details={string.investorOH.feature.details}
              img={require("../../assets/images/INVESTOROFFICEHOURS.jpeg")}
              Url="https://riseupco.typeform.com/to/x5BDyb"
              products={string.investorOH.feature.products}
              strength={300}
              SelfLink={true}
              noLink={true}
              openTextBlue={"APPLICATION IS NOW CLOSED"}
              noArrows
              BtnTitle="APPLY NOW"
              ImgMaxHeight={"600px"}
              style={{
                slogFontSize: "18px",
                titleFontSize: "30px",
                detailsFontSize: "17px",
                // slogFontWeight: "400",
                slogColor: "#2a428c"
              }}
              seprator={false}
            />
          </div>
        )
      }
      if (_self.state.active === 5) {
        return (
          <div>
            <ul className="nav--wrap Activities ">{_self.renderTabs()}</ul>
            <FeaturePost
              caption="POWERED BY"
              captionImage={require("../../assets/images/Webkeyz_logo.jpeg")}
              title={string.UXClinicConsultancy.feature.title}
              details={string.UXClinicConsultancy.feature.details}
              img={require("../../assets/images/UXClinicConsultancy.jpeg")}
              Url={string.UXClinicConsultancy.feature.link}
              strength={300}
              SelfLink={true}
              noLink={true}
              noArrows
              BtnTitle="APPLY NOW"
              ImgMaxHeight={"600px"}
              style={{
                slogFontSize: "18px",
                titleFontSize: "30px",
                detailsFontSize: "17px",
                // slogFontWeight: "400",
                slogColor: "#2a428c"
              }}
              seprator={false}
            />
          </div>
        )
      }

      if (_self.state.active === 6) {
        return (
          <div>
            <ul className="nav--wrap Activities ">{_self.renderTabs()}</ul>
            <FeaturePost
              caption="POWERED BY"
              captionImage={require("../../assets/images/uber-logo-png-1581.png")}
              title={string.ubuer.feature.title}
              details={string.ubuer.feature.details}
              img={require("../../assets/images/UBERPitchN’Ride.png")}
              Url={string.ubuer.feature.link}
              products={string.ubuer.feature.products}
              strength={300}
              SelfLink={true}
              noLink={true}
              noArrows
              BtnTitle="APPLY NOW"
              openTextBlue={"APPLICATION IS NOW CLOSED"}
              ImgMaxHeight={"570px"}
              style={{
                slogFontSize: "18px",
                titleFontSize: "30px",
                detailsFontSize: "17px",
                // slogFontWeight: "400",
                slogColor: "#2a428c"
              }}
              seprator={false}
            />
          </div>
        )
      }
      // - ENTERTAINMENT
      if (_self.state.active === 7) {
        return (
          <div>
            <ul className="nav--wrap Activities ">{_self.renderTabs()}</ul>
            <FeaturePost
              title={string.entertainment.feature.title}
              details={string.entertainment.feature.details}
              img={require("../../assets/images/entertainment.jpg")}
              Url="/"
              strength={300}
              slog="THE ULTIMATE SUMMIT EXPERIENCE"
              noArrows
              noLink
              ImgMaxHeight={"600px"}
              style={{
                slogFontSize: "18px",
                titleFontSize: "30px",
                detailsFontSize: "17px",
                // slogFontWeight: "400",
                slogColor: "#2a428c"
              }}
              seprator={false}
            />
            {/* <div
              style={{
                backgroundColor: "#fff",
                padding: 60,
                position: "relative"
              }}
            /> */}
            <FeaturePost
              title={string.entertainment.feature1.title}
              details={string.entertainment.feature1.details}
              caption="POWERED BY"
              captionImage={require("../../assets/images/logo-uber-eats.png")}
              img={require("../../assets/images/food.jpg")}
              Url="/"
              strength={300}
              slog="THE ULTIMATE SUMMIT EXPERIENCE"
              noArrows
              noLink
              ImgMaxHeight={"500px"}
              style={{
                slogFontSize: "18px",
                titleFontSize: "30px",
                detailsFontSize: "17px",
                // slogFontWeight: "400",
                slogColor: "#2a428c"
              }}
              seprator={false}
            />
          </div>
        )
      }

      if (_self.state.active === 8) {
        return (
          <div>
            <ul className="nav--wrap Activities ">{_self.renderTabs()}</ul>
            <FeaturePost
              title={string.AllSummitLong.feature.title}
              details={string.AllSummitLong.feature.details}
              Url={string.AllSummitLong.feature.link}
              caption="POWERED BY"
              captionImage={require("../../assets/images/BrainySquad.png")}
              products={string.AllSummitLong.feature.products}
              strength={300}
              SelfLink={true}
              BtnTitle="CLICK HERE FOR THE RISEUP XPERIENCE MANUAL"
              noLink={true}
              noArrows
              ImgMaxHeight={"570px"}
              style={{
                slogFontSize: "18px",
                titleFontSize: "30px",
                detailsFontSize: "17px",
                // slogFontWeight: "400",
                slogColor: "#2a428c"
              }}
              seprator={false}
            />
          </div>
        )
      }

      if (_self.state.active === 9) {
        return (
          <div>
            <ul className="nav--wrap Activities ">{_self.renderTabs()}</ul>
            <FeaturePost
              title={string.threeDayAcceleratorProgram.feature.title}
              details={string.threeDayAcceleratorProgram.feature.details}
              Url={string.threeDayAcceleratorProgram.feature.link}
              //caption="POWERED BY"
              //captionImage={require("../../assets/images/BrainySquad.png")}
              products={string.threeDayAcceleratorProgram.feature.products}
              strength={300}
              SelfLink={true}
              noLink={true}
              noArrows
              img={require("../../assets/images/RiseUpSummit2017.jpg")}
              BtnTitle="APPLY NOW"
              ImgMaxHeight={"570px"}
              style={{
                slogFontSize: "18px",
                titleFontSize: "30px",
                detailsFontSize: "17px",
                // slogFontWeight: "400",
                slogColor: "#2a428c"
              }}
              seprator={false}
            />
          </div>
        )
      }

      if (_self.state.active === 10) {
        return (
          <div>
            <ul className="nav--wrap Activities ">{_self.renderTabs()}</ul>
            <FeaturePost
              caption="Brought to you by"
              captionImage={require("../../assets/images/100100logo.jpg")}
              title={string.BESTARABICPOSTERS.feature.title}
              details={string.BESTARABICPOSTERS.feature.details}
              Url={string.BESTARABICPOSTERS.feature.link}
              strength={300}
              SelfLink={true}
              noLink={true}
              noArrows
              BtnTitle="APPLY NOW"
              ImgMaxHeight={"570px"}
              style={{
                slogFontSize: "18px",
                titleFontSize: "30px",
                detailsFontSize: "17px",
                // slogFontWeight: "400",
                slogColor: "#2a428c"
              }}
              seprator={false}
            />
          </div>
        )
      }
      if (_self.state.active === 11) {
        return (
          <div>
            <ul className="nav--wrap Activities ">{_self.renderTabs()}</ul>
            <FeaturePost
              caption="Brought to you by"
              captionImage={require("../../assets/images/SBCPrideAbbreviated.png")}
              title={string.FastTrack.feature.title}
              details={string.FastTrack.feature.details}
              Url={string.FastTrack.feature.link}
              products={string.FastTrack.feature.products}
              openText={string.FastTrack.openText}
              strength={300}
              SelfLink={true}
              noLink={true}
              noArrows
              BtnTitle="APPLY NOW"
              ImgMaxHeight={"570px"}
              style={{
                slogFontSize: "18px",
                titleFontSize: "30px",
                detailsFontSize: "17px",
                // slogFontWeight: "400",
                slogColor: "#2a428c"
              }}
              seprator={false}
            />
          </div>
        )
      }
      if (_self.state.active === 12) {
        return (
          <div>
            <ul className="nav--wrap Activities ">{_self.renderTabs()}</ul>
            <FeaturePost
              caption="Powered by"
              captionImage={require("../../assets/images/Wuzzuf-logo-blue.png")}
              title={string.CareerCoaching.feature.title}
              details={string.CareerCoaching.feature.details}
              Url={string.CareerCoaching.feature.link}
              products={string.CareerCoaching.feature.products}
              openText={string.CareerCoaching.openText}
              strength={300}
              SelfLink={true}
              noLink={true}
              noArrows
              BtnTitle="APPLY NOW"
              ImgMaxHeight={"570px"}
              style={{
                slogFontSize: "18px",
                titleFontSize: "30px",
                detailsFontSize: "17px",
                // slogFontWeight: "400",
                slogColor: "#2a428c"
              }}
              seprator={false}
            />
          </div>
        )
      }
      if (_self.state.active === 13) {
        return (
          <div>
            <ul className="nav--wrap Activities ">{_self.renderTabs()}</ul>
            <FeaturePost
              title={string.ImpactInvestmentRoundtable.feature.title}
              details={string.ImpactInvestmentRoundtable.feature.details}
              Url={string.ImpactInvestmentRoundtable.feature.link}
              products={string.ImpactInvestmentRoundtable.feature.products}
              openText={string.ImpactInvestmentRoundtable.openText}
              strength={300}
              SelfLink={true}
              noLink={true}
              noArrows
              BtnTitle="APPLY NOW"
              ImgMaxHeight={"570px"}
              style={{
                slogFontSize: "18px",
                titleFontSize: "30px",
                detailsFontSize: "17px",
                // slogFontWeight: "400",
                slogColor: "#2a428c"
              }}
              seprator={false}
            />
          </div>
        )
      }
    }
    return (
      <div>
        <DocumentTitle title="RiseUp Summit - Activities" />
        <HeaderSlider slider={false} />
        {RenderTabsContent()}
      </div>
    )
  }
}

export default Activities
