import React from "react"

const NetworkingEvents = props => {
  function renderList() {
    return props.list.map((event, i) => {
      return (
        <div key={i} className="event">
          <div className="featured">FEATURED</div>
          <div className="event--image">
            <img src={require(`../../assets/images/${event.image}`)} alt="" />
          </div>
          <div className="event--details">
            <h3>{event.title}</h3>
            <p>{event.details}</p>
            <div className="date">{event.date}</div>
          </div>
        </div>
      )
    })
  }
  return (
    <div className="parllex-fix">
      <div className="satellite--events-list">{renderList()}</div>
    </div>
  )
}

export default NetworkingEvents
