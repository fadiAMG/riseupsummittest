import React from "react"
import classnames from "classnames"
import PropTypes from "prop-types"

const InputField = ({
  field,
  value,
  label,
  error,
  type,
  onChange,
  placeholder,
  disabled,
  required
}) => {
  return (
    <div className={classnames("form-group", { "has-error": error })}>
      <label className="control-label">{label}</label>
      <input
        required={required}
        onChange={onChange}
        type={type}
        name={field}
        value={value}
        className="form-control"
        placeholder={placeholder}
        disabled={disabled}
      />
      {error && <div className="error-popover">{error}</div>}
    </div>
  )
}

InputField.proptypes = {
  field: PropTypes.string.isRequired,
  // value: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  placeholder: PropTypes.string,
  error: PropTypes.string,
  disabled: PropTypes.bool,
  type: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired
}

InputField.defaultProps = {
  type: "text",
  required: false
}
export default InputField
