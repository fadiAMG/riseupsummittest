import React from "react"
import classNames from "classnames"
import DocumentTitle from "react-document-title"
import HeaderSlider from "../../commons/HeaderSlider"
import FeaturePost from "../../commons/FeaturePost"
import Sessions from "./sessions"
// Data

class Workshops extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      active: true
    }
  }

  render() {
    return (
      <div>
        <DocumentTitle title="RiseUp Summit - Workshops" />
        <HeaderSlider slider={false} />

        <ul className="nav--wrap ">
          <li>
            <div
              onClick={() => this.setState({ active: false })}
              className={classNames({ active: !this.state.active })}
            >
              101s
            </div>
          </li>
          <li>
            <div
              onClick={() => this.setState({ active: true })}
              className={classNames({ active: this.state.active })}
            >
              DEEP DIVES
            </div>
          </li>
        </ul>
        {!this.state.active ? (
          <div>
            <FeaturePost
              title="101s"
              details="Take a break from our main stage content and immerse yourself in a RiseUp workshop. We’ve split our workshops into 101s and Deep Dives. Is there a topic you’re interested in learning more about? For explorers, we’ve created special lectures to give you the 101 on different topics, allowing you to explore and expand."
              hashTag="101s will take 1 hour, and registration will be on a first come, first served basis. You can only register to four 101 Sessions. Make sure you don’t register for two sessions that are going on simultaneously!"
              // warnMsg="Missed the registration? There will be a wait list at the gate for no-shows. Get there early to put your name down"
              // openTextBlue={"REGISTRATION OPENING SOON!"}
              openText = "If you have any questions, please send us an email to workshops@riseup.co"
              img={require("../../../assets/images/v2/101.png")}
              Url="/my-sessions"
              BtnTitle="MY WORKSHOPS"
              noLink={false}
              slog="THE ULTIMATE SUMMIT EXPERIENCE"
              seprator={false}
            />
            <div className="section--header-wrap">
              <h3>
                THIS YEAR’S 101<span>s</span> SESSIONS
              </h3>

              <h4>
                You will only be able to attend the workshop if you have signed
                up for it!{" "}
              </h4>
            </div>
            <div style={{ position: "relative", backgroundColor: "#fff" }}>
              <div className="sessions-list__wrap">
                <Sessions type={["101s"]} session_101={true} />
              </div>
            </div>
          </div>
        ) : (
          <div>
            <FeaturePost
              title="DEEP DIVES"
              details="Take a break from our main stage content and immerse yourself in a RiseUp workshop. We’ve split our workshops into 101s and Deep Dives. For the more advanced, this year we’re offering interactive workshops to really dive into a subject and give you hands on experience. With a capacity of only 20 attendees, Deep Dives are filtered workshops for which you have to apply so make sure to do that as soon as possible to not miss out on that in-depth and detailed knowledge."
              hashTag="Deep Dives will take 2 - 4 hours and application is necessary. Selection will be filtration-based."
              img={require("../../../assets/images/v2/deepdive.png")}
              // warnMsg="Keep an eye out on our workshop page here, some spots might free up!"
              // openTextBlue={"REGISTRATION OPENING SOON!"}
              openText = "If you have any questions, please send us an email to workshops@riseup.co"
              Url="/my-sessions"
              BtnTitle="MY WORKSHOPS"
              slog="THE ULTIMATE SUMMIT EXPERIENCE"
              seprator={false}
              noLink={false}
            />
            <div className="section--header-wrap">
              <h3>THIS YEAR’S DEEP DIVES SESSIONS</h3>
              <h4>
                You must fill in all the application questions to apply. Any
                incomplete applications will not be selected.{" "}
              </h4>
            </div>
            <div style={{ position: "relative", backgroundColor: "#fff" }}>
              <div className="sessions-list__wrap">
                <div className="sessions--list">
                  <Sessions type={["Deep Dives"]} session_deep_dive={true} />
                </div>
              </div>
            </div>
          </div>
        )}
      </div>
    )
  }
}

// function mapStateToProps(state) {
//   return {
//     sessions: state.sessions.sessions,
//     speakers17: state.Speakers.speakers17
//   }
// }
// export default connect(
//   mapStateToProps,
//   { fetchSessions, fetchSpeakers17 }
// )(Workshops)

export default Workshops
