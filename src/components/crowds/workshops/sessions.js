// import React from "react"
// import axios from "axios"
// import { NavLink, Link } from "react-router-dom"
// import { connect } from "react-redux"
// import Modal from "react-modal"
// import 'bootstrap/dist/css/bootstrap.min.css';
// import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
// import AppBar from 'material-ui/AppBar';
// import TextField from 'material-ui/TextField';
// import RaisedButton from 'material-ui/RaisedButton';
// import ValidateLogin from "../../../validation/LoginValidation"

// // import OwlCarousel from 'react-owl-carousel2'
// import { fetchSessions } from "../../../redux/actions/sessions/SessionsActions"
// import {
//   LoginActionWithTickets,
//   GetCurrentUserWithTickets
// } from "../../../redux/actions/myExperience/authActions"

// import InputField from "./commons/inputField"

// const BOOK_SESSION_API =
//   "https://bdev.riseupsummit.com/api/v2/deepdives"
// const RESND_TICKETS =
//   "https://bdev.riseupsummit.com/api/ticketholders/resend"



// class Sessions extends React.Component {
//   constructor(props) {
//     super(props)
//     this.state = {
//       sessions: [],
//       deepdives: [],
//       modalIsOpen: false,
//       loginModal: false,
//       type: "",
//       item: {},
//       success101: false,
//       successMsg: "",
//       successDeep: false,
//       iframeUrl: "",
//       successLoad: false,
//       noNotificationLink: false,
//       LoginerorrExist: false,
//       LoginErr: "",
//       username: "",
//       password: "",
//       emailId: "",
//       errors: {},
//       loginCase: false,
//       errorSendEmail: {},
//       disabled: false,
//       sendEmailErr: "",
//       sendEmailSuccess: "",
//       notificationModalIsOpen: true,
//       notificationModalMessage: "",
//       StepsModal: false,
//       StepsCase: false,
//       deepSessionQuestions: '',
//       deepSessionAns: []
//     }
//     this.onSubmitLogin = this.onSubmitLogin.bind(this)
//     this.onChange = this.onChange.bind(this)
//   }

//   componentDidMount() {
//     // this.fetchSessions()
//     this.getSessionDeepQuestion();
//   }
  

//   fetchSessions() {
//     this.props.fetchSessions().then(res => {
//       this.setState({
//         sessions: res.payload.data.sessions,
//         successLoad: res.payload.data.success
//       })
//     })
//   }

//   // Login Modal
//   openLoginModal() {
//     this.setState({ loginModal: true, loginCase: true })
//   }
//   closeLoginModal() {
//     this.setState({
//       loginModal: true,
//       username: "",
//       password: "",
//       errors: {},
//       LoginerorrExist: true
//     })
//   }
//   // Content Modal
//   openModal(id, type) {
//     this.setState({ modalIsOpen: true, id, type: type })
//   }
//   openNotificationModal() {
//     this.setState({ notificationModalIsOpen: true })
//   }
//   closeNotificationModal() {
//     this.setState({
//       notificationModalIsOpen: false
//     })
//     this.fetchSessions()
//   }


//   afterOpenModal() {
//     this.state.sessions.map(item => {
//       if (item._id === this.state.id) {
//         this.setState({ item: item })
//       }
//       return null
//     })
//   }

//   closeModal() {
//     this.setState({
//       modalIsOpen: false,
//       item: {},
//       type: "",
//       id: null,
//       successDeep: true,
//       iframeUrl: "",
//       loading: true,
//       success101: true,
//       successMsg: "",
//       isLoading: true,
//       error: "",
//       erorrExist: true,
//       LoginerorrExist: true
//     })
//     this.fetchSessions()
//   }
//   onSubmitLogin(e) {
//     let state = this.state
//     e.preventDefault()
//     if (this.isValid()) {
//       this.setState({ isLoading: true })
//       this.props
//         .LoginActionWithTickets({
//           username: state.username,
//           password: state.password
//         })
//         .then(res => {
//           if (res.payload.data.session) {
//             const token = res.payload.data.access_token
//             localStorage.setItem("token", token)
//             this.setState({ isLoading: true, loginModal: true })
//             this.props.GetCurrentUserWithTickets(token)
//           } else {
//             this.setState({
//               errors: {},
//               LoginerorrExist: true,
//               isLoading: true,
//               authErorr: res.payload.data.message,
//               username: "",
//               password: ""
//             })
//           }
//         })
//     }
//   }
  

//   bookSession101(e, id) {
//     e.preventDefault()
//     this.setState({
//       success101: true,
//       successMsg: "",
//       disabled: true,
//       modalIsOpen: true,
//       loading: true,
//       successLoad: true
//     })
//     let _self = this
//     let token = localStorage.token
//     if (_self.props.isAuth) {
//       axios.defaults.headers.common["Authorization"] = token
//       axios
//         .post(`${BOOK_SESSION_API}/${id}/book`)
//         .then(res => {
//           this.setState({
//             success101: res.data.success,
//             successMsg: res.data.message
//           })
//           let title = _self.state.item.title
//           if (res.data.success)
//             setTimeout(function() {
//               _self.setState({
//                 modalIsOpen: true,
//                 item: {},
//                 type: "",
//                 id: null,
//                 successDeep: true,
//                 iframeUrl: "",
//                 loading: true,
//                 success101: true,
//                 successMsg: "",
//                 disabled: false,
//                 notificationModalIsOpen: true,
//                 notificationModalMessage: `You have successfully registered for ${title}`,
//                 noNotificationLink: true,
//                 successLoad: true
//               })
//               _self.fetchSessions()
//             }, 5500)
//           else if (!res.data.success)
//             setTimeout(function() {
//               _self.setState({
//                 modalIsOpen: true,
//                 notificationModalIsOpen: true,
//                 noNotificationLink: true,
//                 notificationModalMessage: res.data.message,
//                 disabled: true,
//                 loading: true,
//                 successLoad: true
//               })
//               _self.fetchSessions()
//             }, 5500)
//         })
//         .catch(err => {
//           this.setState({
//             loading: true,
//             LoginerorrExist: true,
//             LoginErr: err.response.data.error,
//             notificationModalIsOpen: true,
//             noNotificationLink: true,
//             notificationModalMessage: err.response.data.error,
//             disabled: true,
//             successLoad: true
//           })
//         })
//     }
//   }



//   /////////////////////////////////////////////////////////////////////////////////
//   /* WORKING AREA */

//   getSessionDeepQuestion() {
//     let token = localStorage.token
//     axios.get('https://bdev.riseupsummit.com/api/v2/deepdives', {headers: {Authorization:token}})
//     .then( response =>{
//       this.setState({deepSessionQuestions: response.data.workshops[0].questions, deepdives: response.data.workshops,successLoad:true})
//     })
//     .catch((error) => {
//       console.log('Error ' + error);
//    });
//   }

//   bookSessionDeep(id) {

//     this.setState({ disabled: true })
//     let _self = this
//     let token = localStorage.token
//     console.log(token);
//     if (_self.props.isAuth) {
//       this.setState({ loading: true })
//       axios
//         .post('https://bdev.riseupsummit.com/api/v2/deepdive/register', 
//         {


//             workshop_id: id,
//             answers: this.state.deepSessionAns

          
//         }, {headers : {
//           Authorization: token
//         }})
//         .then(res => {
//           this.setState({
//             successDeep: res.data.success,
//             // iframeUrl: res.data.typeFormUrl,
//             loading: true,

//             disabled: true
//           })
//           // this.fetchSessions()
//         })
//         .catch(err => {
//           this.setState({
//             loading: true,
//             erorrExist: true,
//             error: err.response.data.error
//           })
//         })
//     }
//   }
//   isValid() {
//     let data = {
//       username: this.state.username,
//       password: this.state.password
//     }
//     const { errors, isValid } = ValidateLogin(data, "loginWithTicket")
//     if (!isValid) {
//       this.setState({ errors, erorrsExit: true })
//     }
//     return isValid
//   }
//   RenderErorrs() {
//     if (this.state.erorrsExit) {
//       return (
//         <ul className="errors login">
//           <li>{this.state.errors.email}</li>
//           <li>{this.state.errors.password}</li>
//         </ul>
//       )
//     } else if (this.state.authErr) {
//       return (
//         <ul className="errors login">
//           <li>{this.state.authErorr}</li>
//         </ul>
//       )
//     } else {
//       return null
//     }
//   }
//   onChange(e) {
//     this.setState({ [e.target.name]: e.target.value })
//   }
//   openStepsModal(e) {
//     e.preventDefault()
//     this.setState({ StepsModal: true, StepsCase: true })
//   }

//   closeStepsModal() {
//     this.setState({ StepsModal: true, StepsCase: true })
//   }
//   onSubmitSendEmail(e) {
//     let _self = this
//     e.preventDefault()
//     let token = localStorage.token
//     axios.defaults.headers.common["Authorization"] = token
//     axios
//       .post(RESND_TICKETS, { email: this.state.emailId })
//       .then(res => {
//         this.setState({ sendEmailErr: "", sendEmailSuccess: res.data.message })
//         setTimeout(function() {
//           _self.setState({ sendEmailSuccess: "", loginModal: true })
//         }, 3000)
//       })
//       .catch(err => {
//         this.setState({
//           sendEmailSuccess: "",
//           sendEmailErr: err.response.data.error
//         })
//         setTimeout(function() {
//           _self.setState({ sendEmailSuccess: "" })
//         }, 2000)
//       })
//   }
//   render() {
//     let windowHeight = window.innerHeight - 120

//     let _self = this
//     function RenderNotificationModalContent() {
//       return (
//         <div className="speaker-modal--preview session--preview session--details">
//           <div
//             className="speaker--details profile--session"
//             style={{
//               height: "200px",
//               overflow: "hidden"
//             }}
//           >
//             <div
//               className="content"
//               style={{
//                 height: windowHeight,
//                 padding: _self.state.noNotificationLink
//                   ? "35px 40px"
//                   : "5px 40px"
//               }}
//             >
//               {_self.state.erorrExist ? (
//                 <div className="alert alert-danger">{_self.state.error}</div>
//               ) : null}
//               <div
//                 style={{ alignItems: "center" }}
//                 className="success--session--content"
//               >
//                 <div>
//                   <h6 style={{ paddingBottom: "20px" }}>
//                     {_self.state.notificationModalMessage}
//                   </h6>

//                   {!_self.state.noNotificationLink ? (
//                     <React.Fragment>
//                       <br />
//                       <h6>
//                         Please check{" "}
//                         <Link
//                           to={"/my-sessions"}
//                           className="link"
//                           style={{ fontSize: "14px" }}
//                         >
//                           {"MY WORKSHOPS"}
//                         </Link>{" "}
//                         to know your schedule
//                       </h6>
//                     </React.Fragment>
//                   ) : null}

//                   <div style={{ float: "right" }}>
//                     <button
//                       className="btn btn-default right"
//                       onClick={() => _self.closeNotificationModal()}
//                       style={{ padding: "6px 15px" }}
//                     >
//                       OK
//                     </button>
//                   </div>
//                 </div>
//               </div>
//             </div>
//           </div>
//         </div>
//       )
//     }
//     function RenderCorpModalContent() {
//       // 101s
//       if (_self.state.type === "101s") {
//         return (
//           <div className="speaker-modal--preview session--preview session--details ">
//             <div className="speaker--details profile--session">
//               <button className="close" onClick={() => _self.closeModal()} />
//               <div className="content" style={{ overflowY: "scroll" }}>
//                 {_self.state.erorrExist ? (
//                   <div className="alert alert-danger">{_self.state.error}</div>
//                 ) : null}
//                 {_self.state.LoginerorrExist ? (
//                   <div className="alert alert-danger">
//                     {_self.state.LoginErr}
//                   </div>
//                 ) : null}
//                 {!_self.state.success101 ? (
//                   <div>
//                     <h3 style={{ maxWidth: "90%" }}>
//                       {_self.state.item.title} <br />
//                     </h3>
//                     <h5>{_self.state.item.subTitle}</h5>
//                     <ul className="main--list">
//                       <li>
//                         <i className="fa fa-calendar" /> {_self.state.item.date}{" "}
//                         DAY
//                       </li>
//                       <li>
//                         <i className="fa fa-clock-o" />{" "}
//                         {_self.state.item.start_time} -{" "}
//                         {_self.state.item.end_time}
//                       </li>
//                       <li>
//                         <i className="fa fa-map-marker" />{" "}
//                         {_self.state.item.venue}{" "}
//                         {_self.state.item.subVenue ? "-" : ""}{" "}
//                         {_self.state.item.subVenue}
//                       </li>
//                     </ul>
//                     {_self.state.item.speakersData ? (
//                       <div>
//                         {_self.state.item.speakersData.length > 0 ? (
//                           <h6 className="session--speakers--list">
//                             {_self.state.item.speakersData.length === 1
//                               ? "Workshop Speaker"
//                               : "Workshop Speakers"}
//                           </h6>
//                         ) : null}
//                         <ul className="speakers--list--wrap">
//                           {_self.state.item.speakersData.map(item => {
//                             return (
//                               <li>
//                                 <div className="speaker--image">
//                                   <img
//                                     src={item.imgUrl ? item.imgUrl : null}
//                                     alt=""
//                                   />
//                                 </div>
//                                 <div className="speaker--details--wrap">
//                                   <h4>
//                                     {item.fname} {item.lname}
//                                   </h4>
//                                   <p>
//                                     {item.job_title}{" "}
//                                     {item.entity_name ? "AT" : null}{" "}
//                                     {item.entity_name}
//                                   </p>
//                                 </div>
//                               </li>
//                             )
//                           })}
//                         </ul>
//                       </div>
//                     ) : null}
//                     <p className="details">{_self.state.item.description}</p>
//                   </div>
//                 ) : (
//                   <div className="success--session--content">
//                     <span className="fa fa-check-circle-o" />
//                     <h3>{_self.state.successMsg} SESSION</h3>
//                   </div>
//                 )}
//               </div>
//             </div>
//           </div>
//         )
//       } else if (_self.state.type === "deep") {
//         // DEEP DIVE
//         return (
//           <div className="speaker-modal--preview session--preview session--details">
//             <div className="speaker--details profile--session">
//               <button className="close" onClick={() => _self.closeModal()} />
//               <div className="content" style={{ overflowY: "scroll" }}>
//                 {_self.state.loading ? (
//                   <div className="LoadingComponent">
//                     <img
//                       width="80"
//                       src={require("../../../assets/loading.svg")}
//                       alt="Loading..."
//                     />
//                   </div>
//                 ) : null}
//                 {!_self.state.successDeep ? (
//                   <div>
//                     <h3 style={{ maxWidth: "90%" }}>
//                       {_self.state.item.title} <br />
//                     </h3>
//                     <h5>{_self.state.item.subTitle}</h5>
//                     <ul className="main--list">
//                       <li>
//                         <i className="fa fa-calendar" /> {_self.state.item.date}{" "}
//                         DAY
//                       </li>
//                       <li>
//                         <i className="fa fa-clock-o" />{" "}
//                         {_self.state.item.start_time} -{" "}
//                         {_self.state.item.end_time}
//                       </li>
//                       <li>
//                         <i className="fa fa-map-marker" />{" "}
//                         {_self.state.item.venue}{" "}
//                         {_self.state.item.subVenue ? "-" : ""}{" "}
//                         {_self.state.item.subVenue}
//                       </li>
//                     </ul>
//                     {_self.state.item.speakersData ? (
//                       <div>
//                         {_self.state.item.speakersData.length > 0 ? (
//                           <h6 className="session--speakers--list">
//                             {_self.state.item.speakersData.length === 1
//                               ? "Workshop Speaker"
//                               : "Workshop Speakers"}
//                           </h6>
//                         ) : null}
//                         <ul className="speakers--list--wrap">
//                           {_self.state.item.speakersData.map((item, i) => {
//                             if (item)
//                               return (
//                                 <li key={i}>
//                                   <div className="speaker--image">
//                                     <img src={item.imgUrl} alt="" />
//                                   </div>
//                                   <div className="speaker--details--wrap">
//                                     <h4>
//                                       {item.fname} {item.lname}
//                                     </h4>
//                                     <p>
//                                       {item.job_title}{" "}
//                                       {item.entity_name ? "AT" : null}{" "}
//                                       {item.entity_name}
//                                     </p>
//                                   </div>
//                                 </li>
//                               )
//                             else return null
//                           })}
//                         </ul>
//                       </div>
//                     ) : null}
//                     <p className="details">{_self.state.item.description}</p>
//                     {_self.props.isAuth ? (
//                       _self.state.item.bookedByMe ===
//                         "Waiting for user response" ||
//                       (!_self.state.item.bookedByMe &&
//                         _self.state.item.bookingStatus) ? (
//                         _self.state.applicationFormId !== "" ? (
//                           <button
//                             onClick={() =>
//                               _self.bookSessionDeep(_self.state.item._id)
//                             }
//                             className="apply--session-button"
//                           >
//                             APPLY
//                           </button>
//                         ) : (
//                           <button className="apply--session-button">
//                             PLEASE CONTACT SUPPORT
//                           </button>
//                         )
//                       ) : null
//                     ) : null}
//                   </div>
//                 ) : (
//                   <div className="success--session--content">
//                     <div>
//                       <h6>
//                         If the form doesn't appear right away, don't worry! We
//                         just need a moment of your time until it loads.
//                       </h6>
//                       <iframe
//                         title="workshops"
//                         wmode="Opaque"
//                         id="typeform-full"
//                         width="100%"
//                         height="400px"
//                         frameBorder="0"
//                         src={_self.state.item.applicationFormId}
//                       />
//                     </div>
//                   </div>
//                 )}
//               </div>
//             </div>
//           </div>
//         )
//       } else if (_self.state.type === "confirmation101") {
//         return (
//           <div className="speaker-modal--preview session--preview session--details">
//             <div
//               className="speaker--details profile--session"
//               style={{
//                 height: "200px",
//                 overflow: "hidden"
//               }}
//             >
//               <button className="close" onClick={() => _self.closeModal()} />
//               <div
//                 className="content"
//                 style={{ height: windowHeight, padding: "35px 40px" }}
//               >
//                 {_self.state.erorrExist ? (
//                   <div className="alert alert-danger">{_self.state.error}</div>
//                 ) : null}
//                 <div
//                   style={{ alignItems: "center" }}
//                   className="success--session--content"
//                 >
//                   <div style={{ paddingTop: "20px" }}>
//                     <h6 style={{ paddingBottom: "20px" }}>
//                       Are you sure you want to register for{" "}
//                       {" " + _self.state.item.title + "?"}
//                     </h6>
//                     <div style={{ paddingLeft: "30%" }}>
//                       <button
//                         style={{ float: "left" }}
//                         className="btn btn-default left"
//                         onClick={e =>
//                           _self.bookSession101(e, _self.state.item._id)
//                         }
//                         disabled={_self.state.disabled}
//                       >
//                         YES
//                       </button>
//                       <button
//                         className="btn btn-default right"
//                         onClick={() => _self.closeModal()}
//                         disabled={_self.state.disabled}
//                       >
//                         NO
//                       </button>
//                     </div>
//                   </div>
//                 </div>
//               </div>
//             </div>
//           </div>
//         )
//       } else if (_self.state.type === "confirmationDeep") {
//         return (
//           <div className="speaker-modal--preview session--preview session--details">
//             <div
//               className="speaker--details profile--session"
//               style={{ height: "100%" }}
//             >
//               {" "}
//               <button className="close" onClick={() => _self.closeModal()} />
//               <div
//                 className="content"
//                 style={{ height: windowHeight, overflowY: "scroll" }}
//               >
//                 <div className="success--session--content">
//                   <div>
//                     <h6>
//                       If the form doesn't appear right away, don't worry! We
//                       just need a moment of your time until it loads.
//                     </h6>
//                     <MuiThemeProvider>
//                       <div >
//                         <AppBar title="Please Answer These Questions" />
//                         <TextField
//                           style= {{width: '-webkit-fill-available'}}
//                           hintText= "Write Your Answer Here"
//                           floatingLabelText={_self.state.deepSessionQuestions[0]}
//                           onChange={e => _self.state.deepSessionAns[0] = e.target.value}
//                           // defaultValue={values.bio}
//                         /> <br/>
//                         <TextField
//                           style= {{width: '-webkit-fill-available'}}
//                           hintText= "Write Your Answer Here"
//                           floatingLabelText={_self.state.deepSessionQuestions[1]}
//                           onChange={e => _self.state.deepSessionAns[1] = e.target.value}
//                           // defaultValue={values.bio}
//                         /> <br/>
//                         <TextField
//                           style= {{width: '-webkit-fill-available'}}
//                           hintText= "Write Your Answer Here"
//                           floatingLabelText={_self.state.deepSessionQuestions[2]}
//                           onChange={e => _self.state.deepSessionAns[2] = e.target.value}
//                           // defaultValue={values.bio}
//                         />
//                         <br/>
//                         <RaisedButton
//                           label="Submit"
//                           primary={true}
//                           onClick={() =>
//                             _self.bookSessionDeep(_self.state.id)
//                           }
//                         />
//                       </div>
//                     </MuiThemeProvider>
            
//                     {/* <img style={{position:'absolute', bottom:80, left:'40%', zIndex:1}} width="80" src={require('../../../assets/loading.svg')} alt='Loading...'/> */}
                    
//                   </div>
//                 </div>
//               </div>
//             </div>
//           </div>
//         )
//       }
//     }
//     function renderSessionItem() {
//       // debugger;
//       return _self.state.deepdives.map((item, i) => {
//         if (item.status) {
//           if (
//             (_self.props.session_101 && item.session_101) ||
//             (_self.props.session_deep_dive && item.session_deep_dive) ||
//             true
//           )
//             return (
//               <div key={i} className="item">
//                 <div
//                   className="banner clickable"
//                   onClick={() =>
//                     _self.openModal(
//                       item._id,
//                       item.session_101 ? "101s" : "deep"
//                     )
//                   }
//                 >
//                   <div className="content">
//                     <p className="date">
//                       {item.date} Day at {item.start_time}{" "}
//                       <i style={{ fontSize: 14 }}>to</i> {item.end_time}
//                     </p>
//                     <h4>
//                       {item.title.substring(0, 50)}
//                       {item.title.length > 50 ? "..." : null}
//                     </h4>
//                   </div>
//                 </div>
//                 <div className="content--info">
//                   <ul>
//                     {item.user_tags
//                       ? item.user_tags
//                           .split(",")
//                           .map((item, i) => <li key={i}>{item}</li>)
//                       : null}
//                   </ul>

//                   {/* <ul className="left-seats">
//                     <li>{item.applicantLimit - item.currentApplicants} LEFT</li>
//                   </ul> */}

//                   {!_self.props.loading && _self.props.isAuth ? (
//                     !item.bookedByMe ? (
//                       // !item.fully_booked ? (
//                       true ? (
//                         item.session_101 ? (
//                           <button
//                             onClick={() =>
//                               _self.openModal(item._id, "confirmation101")
//                             }
//                             className="btn btn-default"
//                           >
//                             Register FOR 101
//                           </button>
//                         ) : (
//                           <button
//                             onClick={() =>
//                               _self.openModal(item._id, "confirmationDeep")
//                             }
//                             className="btn btn-default"
//                           >
//                             APPLY NOW
//                           </button>
//                         )
//                       ) : (
//                         null
//                         // <button className="btn btn-default center" disabled={true}>
//                         //   CLOSED
//                         // </button>
//                       )
//                     ) : item.session_101 ? (
//                       <button
//                         style={{ left: "-20px" }}
//                         onClick={() => _self.openModal(item._id, "101s")}
//                         className="btn btn-default center "
//                       >
//                         {item.bookedByMe}
//                       </button>
//                     ) : (
//                       <button
//                         style={{ bottom: "-35px", left: "-20px" }}
//                         onClick={() => _self.openModal(item._id, "deep")}
//                         className="btn btn-default center "
//                       >
//                         {item.bookedByMe}
//                       </button>
//                     )
//                   ) : (
//                     <div>
//                       {item.session_101 ? (
//                         <div
//                           onClick={() => _self.openLoginModal()}
//                           className="btn btn-default left"
//                         >
//                           LOGIN TO REGISTER
//                         </div>
//                       ) : (
//                         <div
//                           onClick={() => _self.openLoginModal()}
//                           className="btn btn-default left"
//                         >
//                           LOGIN TO APPLY
//                         </div>
//                       )}
//                       {item.session_101 ? (
//                         <div
//                           onClick={() => _self.openModal(item._id, "101s")}
//                           className="btn btn-default right"
//                         >
//                           SHOW DETAILS
//                         </div>
//                       ) : (
//                         <div
//                           onClick={() => _self.openModal(item._id, "deep")}
//                           className="btn btn-default right"
//                         >
//                           SHOW DETAILS
//                         </div>
//                       )}
//                     </div>
//                   )}
//                 </div>
//               </div>
//             )
//           else return null
//         } else return null
//       })
//     }

//     return (
//       <div className="sessions--list my-experience">
//         {!this.state.successLoad ? (
//           <div
//             className="LoadingComponent"
//             style={{ textAlign: "center", width: "100%", marginBottom: 50 }}
//           >
//             <img
//               width="80"
//               src={require("../../../assets/loading.svg")}
//               alt="Loading..."
//             />
//           </div>
//         ) : (
//           renderSessionItem()
//         )}

//         <Modal
//           isOpen={this.state.notificationModalIsOpen}
//           onRequestClose={this.closeModal.bind(this)}
//           contentLabel=""
//           className={"notification-popup"}
//         >
//           {RenderNotificationModalContent()}
//         </Modal>

//         <Modal
//           isOpen={this.state.modalIsOpen}
//           onAfterOpen={this.afterOpenModal.bind(this)}
//           onRequestClose={this.closeModal.bind(this)}
//           contentLabel=""
//           className={
//             _self.state.type === "confirmation101"
//               ? "notification-popup"
//               : "workshops-popup"
//           }
//         >
//           {RenderCorpModalContent()}
//         </Modal>

//         <Modal
//           isOpen={this.state.loginModal}
//           onRequestClose={this.closeLoginModal.bind(this)}
//           contentLabel=""
//           className={"LoginPopup"}
//         >
//           <div className="speaker-modal--preview session--preview session--details">
//             <div
//               style={{ overflow: "hidden" }}
//               className="speaker--details profile--session"
//             >
//               <button
//                 className="close"
//                 onClick={() => this.closeLoginModal()}
//               />
//               <div className="content">
//                 {this.state.LoginerorrExist ? (
//                   <div
//                     style={{ overflow: "hidden" }}
//                     className="alert alert-danger"
//                   >
//                     {this.state.authErorr}
//                   </div>
//                 ) : null}
//                 <h3>LOGIN</h3>

//                 {this.state.loginCase ? (
//                   <form onSubmit={e => this.onSubmitLogin(e)}>
//                     <div className="row">
//                       <div className="col-md-12">
//                         <InputField
//                           error={this.state.errors.email}
//                           label="Email Address"
//                           onChange={this.onChange}
//                           value={this.state.username}
//                           field="username"
//                           placeholder="Email Address"
//                         />
//                       </div>
//                       <div className="col-md-12">
//                         <input
//                           error={this.state.errors.password}
//                           type="text"
//                           name="password"
//                           label="Ticket Confirmation Code "
//                           value={this.state.password}
//                           className="form-control"
//                           placeholder="Ticket Confirmation Code "
//                           onChange={this.onChange}
//                           style={{ textTransform: "uppercase" }}
//                         />
//                       </div>

//                       {this.state.isLoading ? (
//                         <div className="col-md-12">
//                           <img
//                             alt=""
//                             width="60"
//                             className="loading"
//                             src={require("../../../assets/loading.gif")}
//                           />
//                         </div>
//                       ) : null}
//                       <div className="col-md-12">
//                         <p style={{ marginTop: 15 }}>
//                           Please use your email and ticket confirmation code
//                           <h3
//                             onClick={e => {
//                               this.openStepsModal(e)
//                             }}
//                             style={{
//                               fontSize: "14px",
//                               fontWeight: "bold",
//                               cursor: "pointer"
//                             }}
//                           >
//                             here is how you can find your Confirmation code
//                           </h3>
//                           Or buy your ticket if you haven’t yet
//                           <NavLink
//                             className="btn btn-primary"
//                             to="/home#tickets"
//                           >
//                             Tickets
//                           </NavLink>
//                         </p>
//                         <button
//                           style={{ margin: "0" }}
//                           className="btn btn-default"
//                         >
//                           Login
//                         </button>
//                       </div>
//                     </div>
//                   </form>
//                 ) : null}
//               </div>
//             </div>
//           </div>
//         </Modal>
//         <Modal
//           isOpen={this.state.StepsModal}
//           onRequestClose={this.closeStepsModal.bind(this)}
//           contentLabel=""
//           className={"workshops-popup"}
//         >
//           <div className="speaker-modal--preview session--preview session--details">
//             <div
//               className="speaker--details profile--session loginIMG"
//               style={{
//                 height: "100%",
//                 overflowY: "scroll",
//                 maxHeight: "500px"
//               }}
//             >
//               <img
//                 alt=""
//                 src={require("../../../assets/images/ConfirmationCodeSteps.jpeg")}
//                 onClick={e => {
//                   this.closeStepsModal()
//                 }}
//               />
//             </div>
//           </div>
//         </Modal>
//       </div>
//     )
//   }
// }

// function mapStateToProps(state) {
//   return {
//     isAuth: state.authTicket.isAuth,
//     isLoading: state.authTicket.isLoading
//   }
// }

// Sessions.defaultProps = {
//   session_101: false,
//   session_deep_dive: false
// }

// export default connect(
//   mapStateToProps,
//   { fetchSessions, LoginActionWithTickets, GetCurrentUserWithTickets }
// )(Sessions)


import React from "react"
import axios from "axios"
import { NavLink, Link } from "react-router-dom"
import { connect } from "react-redux"
import Modal from "react-modal"
import ValidateLogin from "../../../validation/LoginValidation"

// import OwlCarousel from 'react-owl-carousel2'
import { fetchSessions } from "../../../redux/actions/sessions/SessionsActions"
import {
  LoginActionWithTickets,
  GetCurrentUserWithTickets
} from "../../../redux/actions/myExperience/authActions"

import InputField from "./commons/inputField"

const BOOK_SESSION_API =
  "https://bdev.riseupsummit.com/api/newsessionsdata"
const RESND_TICKETS =
  "https://bdev.riseupsummit.com/api/ticketholders/resend"
class Sessions extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      sessions: [],
      modalIsOpen: false,
      loginModal: false,
      type: "",
      item: {},
      success101: true,
      successMsg: "",
      successDeep: true,
      iframeUrl: "",
      successLoad: true,
      noNotificationLink: false,
      LoginerorrExist: false,
      LoginErr: "",
      username: "",
      password: "",
      emailId: "",
      errors: {},
      loginCase: false,
      errorSendEmail: {},
      disabled: false,
      sendEmailErr: "",
      sendEmailSuccess: "",
      notificationModalIsOpen: true,
      notificationModalMessage: "",
      StepsModal: false,
      StepsCase: false
    }
    this.onSubmitLogin = this.onSubmitLogin.bind(this)
    this.onChange = this.onChange.bind(this)
  }

  componentDidMount() {
    this.fetchSessions()
  }

  fetchSessions() {
    this.props.fetchSessions().then(res => {
      this.setState({
        sessions: res.payload.data.sessions,
        successLoad: res.payload.data.success
      })
    })
  }

  // Login Modal
  openLoginModal() {
    this.setState({ loginModal: true, loginCase: true })
  }
  closeLoginModal() {
    this.setState({
      loginModal: true,
      username: "",
      password: "",
      errors: {},
      LoginerorrExist: true
    })
  }
  // Content Modal
  openModal(id, type) {
    this.setState({ modalIsOpen: true, id, type: type })
  }
  openNotificationModal() {
    this.setState({ notificationModalIsOpen: true })
  }
  closeNotificationModal() {
    this.setState({
      notificationModalIsOpen: false
    })
    this.fetchSessions()
  }
  afterOpenModal() {
    this.state.sessions.map(item => {
      if (item._id === this.state.id) {
        this.setState({ item: item })
      }
      return null
    })
  }

  closeModal() {
    this.setState({
      modalIsOpen: false,
      item: {},
      type: "",
      id: null,
      successDeep: true,
      iframeUrl: "",
      loading: true,
      success101: true,
      successMsg: "",
      isLoading: true,
      error: "",
      erorrExist: true,
      LoginerorrExist: true
    })
    this.fetchSessions()
  }
  onSubmitLogin(e) {
    let state = this.state
    e.preventDefault()
    if (this.isValid()) {
      this.setState({ isLoading: true })
      this.props
        .LoginActionWithTickets({
          username: state.username,
          password: state.password
        })
        .then(res => {
          if (res.payload.data.session) {
            const token = res.payload.data.access_token
            localStorage.setItem("token", token)
            this.setState({ isLoading: true, loginModal: true })
            this.props.GetCurrentUserWithTickets(token)
          } else {
            this.setState({
              errors: {},
              LoginerorrExist: true,
              isLoading: true,
              authErorr: res.payload.data.message,
              username: "",
              password: ""
            })
          }
        })
    }
  }
  bookSession101(e, id) {
    e.preventDefault()
    this.setState({
      success101: true,
      successMsg: "",
      disabled: true,
      modalIsOpen: true,
      loading: true,
      successLoad: true
    })
    let _self = this
    let token = localStorage.token
    if (true) {
      // axios.defaults.headers.common["Authorization"] = token
      axios
        .post(`${BOOK_SESSION_API}/${id}/book`, {headers:{Authorization: token}})
        .then(res => {
          this.setState({
            success101: res.data.success,
            successMsg: res.data.message
          })
          let title = _self.state.item.title
          if (res.data.success)
            setTimeout(function() {
              _self.setState({
                modalIsOpen: true,
                item: {},
                type: "",
                id: null,
                successDeep: true,
                iframeUrl: "",
                loading: true,
                success101: true,
                successMsg: "",
                disabled: false,
                notificationModalIsOpen: true,
                notificationModalMessage: `You have successfully registered for ${title}`,
                noNotificationLink: true,
                successLoad: true
              })
              _self.fetchSessions()
            }, 5500)
          else if (!res.data.success)
            setTimeout(function() {
              _self.setState({
                modalIsOpen: true,
                notificationModalIsOpen: true,
                noNotificationLink: true,
                notificationModalMessage: res.data.message,
                disabled: true,
                loading: true,
                successLoad: true
              })
              _self.fetchSessions()
            }, 5500)
        })
        .catch(err => {
          this.setState({
            loading: true,
            LoginerorrExist: true,
            LoginErr: err.response.data.error,
            notificationModalIsOpen: true,
            noNotificationLink: true,
            notificationModalMessage: err.response.data.error,
            disabled: true,
            successLoad: true
          })
        })
    }
  }
  bookSessionDeep(id) {
    this.setState({ disabled: true })
    let _self = this
    let token = localStorage.token
    if (_self.props.isAuth) {
      axios.defaults.headers.common["Authorization"] = token
      this.setState({ loading: true })
      axios
        .post(`${BOOK_SESSION_API}/${id}/book`)
        .then(res => {
          this.setState({
            successDeep: res.data.success,
            iframeUrl: res.data.typeFormUrl,
            loading: true,
            disabled: true
          })
          this.fetchSessions()
        })
        .catch(err => {
          this.setState({
            loading: true,
            erorrExist: true,
            error: err.response.data.error
          })
        })
    }
  }
  isValid() {
    let data = {
      username: this.state.username,
      password: this.state.password
    }
    const { errors, isValid } = ValidateLogin(data, "loginWithTicket")
    if (!isValid) {
      this.setState({ errors, erorrsExit: true })
    }
    return isValid
  }
  RenderErorrs() {
    if (this.state.erorrsExit) {
      return (
        <ul className="errors login">
          <li>{this.state.errors.email}</li>
          <li>{this.state.errors.password}</li>
        </ul>
      )
    } else if (this.state.authErr) {
      return (
        <ul className="errors login">
          <li>{this.state.authErorr}</li>
        </ul>
      )
    } else {
      return null
    }
  }
  onChange(e) {
    this.setState({ [e.target.name]: e.target.value })
  }
  openStepsModal(e) {
    e.preventDefault()
    this.setState({ StepsModal: true, StepsCase: true })
  }

  closeStepsModal() {
    this.setState({ StepsModal: true, StepsCase: true })
  }
  onSubmitSendEmail(e) {
    let _self = this
    e.preventDefault()
    let token = localStorage.token
    axios.defaults.headers.common["Authorization"] = token
    axios
      .post(RESND_TICKETS, { email: this.state.emailId })
      .then(res => {
        this.setState({ sendEmailErr: "", sendEmailSuccess: res.data.message })
        setTimeout(function() {
          _self.setState({ sendEmailSuccess: "", loginModal: true })
        }, 3000)
      })
      .catch(err => {
        this.setState({
          sendEmailSuccess: "",
          sendEmailErr: err.response.data.error
        })
        setTimeout(function() {
          _self.setState({ sendEmailSuccess: "" })
        }, 2000)
      })
  }
  render() {
    let windowHeight = window.innerHeight - 120

    let _self = this
    function RenderNotificationModalContent() {
      return (
        <div className="speaker-modal--preview session--preview session--details">
          <div
            className="speaker--details profile--session"
            style={{
              height: "200px",
              overflow: "hidden"
            }}
          >
            <div
              className="content"
              style={{
                height: windowHeight,
                padding: _self.state.noNotificationLink
                  ? "35px 40px"
                  : "5px 40px"
              }}
            >
              {_self.state.erorrExist ? (
                <div className="alert alert-danger">{_self.state.error}</div>
              ) : null}
              <div
                style={{ alignItems: "center" }}
                className="success--session--content"
              >
                <div>
                  <h6 style={{ paddingBottom: "20px" }}>
                    {_self.state.notificationModalMessage}
                  </h6>

                  {!_self.state.noNotificationLink ? (
                    <React.Fragment>
                      <br />
                      <h6>
                        Please check{" "}
                        <Link
                          to={"/my-sessions"}
                          className="link"
                          style={{ fontSize: "14px" }}
                        >
                          {"MY WORKSHOPS"}
                        </Link>{" "}
                        to know your schedule
                      </h6>
                    </React.Fragment>
                  ) : null}

                  <div style={{ float: "right" }}>
                    <button
                      className="btn btn-default right"
                      onClick={() => _self.closeNotificationModal()}
                      style={{ padding: "6px 15px" }}
                    >
                      OK
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      )
    }
    function RenderCorpModalContent() {
      // 101s
      if (_self.state.type === "101s") {
        return (
          <div className="speaker-modal--preview session--preview session--details ">
            <div className="speaker--details profile--session">
              <button className="close" onClick={() => _self.closeModal()} />
              <div className="content" style={{ overflowY: "scroll" }}>
                {_self.state.erorrExist ? (
                  <div className="alert alert-danger">{_self.state.error}</div>
                ) : null}
                {_self.state.LoginerorrExist ? (
                  <div className="alert alert-danger">
                    {_self.state.LoginErr}
                  </div>
                ) : null}
                {!_self.state.success101 ? (
                  <div>
                    <h3 style={{ maxWidth: "90%" }}>
                      {_self.state.item.title} <br />
                    </h3>
                    <h5>{_self.state.item.subTitle}</h5>
                    <ul className="main--list">
                      <li>
                        <i className="fa fa-calendar" /> {_self.state.item.date}{" "}
                        DAY
                      </li>
                      <li>
                        <i className="fa fa-clock-o" />{" "}
                        {_self.state.item.start_time} -{" "}
                        {_self.state.item.end_time}
                      </li>
                      <li>
                        <i className="fa fa-map-marker" />{" "}
                        {_self.state.item.venue}{" "}
                        {_self.state.item.subVenue ? "-" : ""}{" "}
                        {_self.state.item.subVenue}
                      </li>
                    </ul>
                    {_self.state.item.speakersData ? (
                      <div>
                        {_self.state.item.speakersData.length > 0 ? (
                          <h6 className="session--speakers--list">
                            {_self.state.item.speakersData.length === 1
                              ? "Workshop Speaker"
                              : "Workshop Speakers"}
                          </h6>
                        ) : null}
                        <ul className="speakers--list--wrap">
                          {_self.state.item.speakersData.map(item => {
                            return (
                              <li>
                                <div className="speaker--image">
                                  <img
                                    src={item.imgUrl ? item.imgUrl : null}
                                    alt=""
                                  />
                                </div>
                                <div className="speaker--details--wrap">
                                  <h4>
                                    {item.fname} {item.lname}
                                  </h4>
                                  <p>
                                    {item.job_title}{" "}
                                    {item.entity_name ? "AT" : null}{" "}
                                    {item.entity_name}
                                  </p>
                                </div>
                              </li>
                            )
                          })}
                        </ul>
                      </div>
                    ) : null}
                    <p className="details">{_self.state.item.description}</p>
                  </div>
                ) : (
                  <div className="success--session--content">
                    <span className="fa fa-check-circle-o" />
                    <h3>{_self.state.successMsg} SESSION</h3>
                  </div>
                )}
              </div>
            </div>
          </div>
        )
      } else if (_self.state.type === "deep") {
        // DEEP DIVE
        return (
          <div className="speaker-modal--preview session--preview session--details">
            <div className="speaker--details profile--session">
              <button className="close" onClick={() => _self.closeModal()} />
              <div className="content" style={{ overflowY: "scroll" }}>
                {_self.state.loading ? (
                  <div className="LoadingComponent">
                    <img
                      width="80"
                      src={require("../../../assets/loading.svg")}
                      alt="Loading..."
                    />
                  </div>
                ) : null}
                {!_self.state.successDeep ? (
                  <div>
                    <h3 style={{ maxWidth: "90%" }}>
                      {_self.state.item.title} <br />
                    </h3>
                    <h5>{_self.state.item.subTitle}</h5>
                    <ul className="main--list">
                      <li>
                        <i className="fa fa-calendar" /> {_self.state.item.date}{" "}
                        DAY
                      </li>
                      <li>
                        <i className="fa fa-clock-o" />{" "}
                        {_self.state.item.start_time} -{" "}
                        {_self.state.item.end_time}
                      </li>
                      <li>
                        <i className="fa fa-map-marker" />{" "}
                        {_self.state.item.venue}{" "}
                        {_self.state.item.subVenue ? "-" : ""}{" "}
                        {_self.state.item.subVenue}
                      </li>
                    </ul>
                    {_self.state.item.speakersData ? (
                      <div>
                        {_self.state.item.speakersData.length > 0 ? (
                          <h6 className="session--speakers--list">
                            {_self.state.item.speakersData.length === 1
                              ? "Workshop Speaker"
                              : "Workshop Speakers"}
                          </h6>
                        ) : null}
                        <ul className="speakers--list--wrap">
                          {_self.state.item.speakersData.map((item, i) => {
                            if (item)
                              return (
                                <li key={i}>
                                  <div className="speaker--image">
                                    <img src={item.imgUrl} alt="" />
                                  </div>
                                  <div className="speaker--details--wrap">
                                    <h4>
                                      {item.fname} {item.lname}
                                    </h4>
                                    <p>
                                      {item.job_title}{" "}
                                      {item.entity_name ? "AT" : null}{" "}
                                      {item.entity_name}
                                    </p>
                                  </div>
                                </li>
                              )
                            else return null
                          })}
                        </ul>
                      </div>
                    ) : null}
                    <p className="details">{_self.state.item.description}</p>
                    {_self.props.isAuth ? (
                      _self.state.item.bookedByMe ===
                        "Waiting for user response" ||
                      (!_self.state.item.bookedByMe &&
                        _self.state.item.bookingStatus) ? (
                        _self.state.applicationFormId !== "" ? (
                          <button
                            onClick={() =>
                              _self.bookSessionDeep(_self.state.item._id)
                            }
                            className="apply--session-button"
                          >
                            APPLY
                          </button>
                        ) : (
                          <button className="apply--session-button">
                            PLEASE CONTACT SUPPORT
                          </button>
                        )
                      ) : null
                    ) : null}
                  </div>
                ) : (
                  <div className="success--session--content">
                    <div>
                      <h6>
                        If the form doesn't appear right away, don't worry! We
                        just need a moment of your time until it loads.
                      </h6>
                      <iframe
                        title="workshops"
                        wmode="Opaque"
                        id="typeform-full"
                        width="100%"
                        height="400px"
                        frameBorder="0"
                        src={_self.state.item.applicationFormId}
                      />
                    </div>
                  </div>
                )}
              </div>
            </div>
          </div>
        )
      } else if (_self.state.type === "confirmation101") {
        return (
          <div className="speaker-modal--preview session--preview session--details">
            <div
              className="speaker--details profile--session"
              style={{
                height: "200px",
                overflow: "hidden"
              }}
            >
              <button className="close" onClick={() => _self.closeModal()} />
              <div
                className="content"
                style={{ height: windowHeight, padding: "35px 40px" }}
              >
                {_self.state.erorrExist ? (
                  <div className="alert alert-danger">{_self.state.error}</div>
                ) : null}
                <div
                  style={{ alignItems: "center" }}
                  className="success--session--content"
                >
                  <div style={{ paddingTop: "20px" }}>
                    <h6 style={{ paddingBottom: "20px" }}>
                      Are you sure you want to register for{" "}
                      {" " + _self.state.item.title + "?"}
                    </h6>
                    <div style={{ paddingLeft: "30%" }}>
                      <button
                        style={{ float: "left" }}
                        className="btn btn-default left"
                        onClick={e =>
                          _self.bookSession101(e, _self.state.item._id)
                        }
                        disabled={_self.state.disabled}
                      >
                        YES
                      </button>
                      <button
                        className="btn btn-default right"
                        onClick={() => _self.closeModal()}
                        disabled={_self.state.disabled}
                      >
                        NO
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        )
      } else if (_self.state.type === "confirmationDeep") {
        return (
          <div className="speaker-modal--preview session--preview session--details">
            <div
              className="speaker--details profile--session"
              style={{ height: "100%" }}
            >
              {" "}
              <button className="close" onClick={() => _self.closeModal()} />
              <div
                className="content"
                style={{ height: windowHeight, overflowY: "scroll" }}
              >
                <div className="success--session--content">
                  <div>
                    <h6>
                      If the form doesn't appear right away, don't worry! We
                      just need a moment of your time until it loads.
                    </h6>
                    {/* <img style={{position:'absolute', bottom:80, left:'40%', zIndex:1}} width="80" src={require('../../../assets/loading.svg')} alt='Loading...'/> */}
                    <iframe
                      title="workshops"
                      wmode="Opaque"
                      id="typeform-full"
                      width="100%"
                      height="500px"
                      frameBorder="0"
                      src={`https://riseupco.typeform.com/to/${
                        _self.state.item.applicationFormId
                      }`}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        )
      }
    }
    function renderSessionItem() {
      return _self.state.sessions.map((item, i) => {
        if (item.status) {
          if (
            (_self.props.session_101 && item.session_101) ||
            (_self.props.session_deep_dive && item.session_deep_dive)
          )
            return (
              <div key={i} className="item">
                <div
                  className="banner clickable"
                  onClick={() =>
                    _self.openModal(
                      item._id,
                      item.session_101 ? "101s" : "deep"
                    )
                  }
                >
                  <div className="content">
                    <p className="date">
                      {item.date} Day at {item.start_time}{" "}
                      <i style={{ fontSize: 14 }}>to</i> {item.end_time}
                    </p>
                    <h4>
                      {item.title.substring(0, 50)}
                      {item.title.length > 50 ? "..." : null}
                    </h4>
                  </div>
                </div>
                <div className="content--info">
                  <ul>
                    {item.user_tags
                      ? item.user_tags
                          .split(",")
                          .map((item, i) => <li key={i}>{item}</li>)
                      : null}
                  </ul>

                  {/* <ul className="left-seats">
                    <li>{item.applicantLimit - item.currentApplicants} LEFT</li>
                  </ul> */}

                  {!_self.props.loading && _self.props.isAuth ? (
                    !item.bookedByMe ? (
                      // !item.fully_booked ? (
                      true ? (
                        item.session_101 ? (
                          <button
                            onClick={() =>
                              _self.openModal(item._id, "confirmation101")
                            }
                            className="btn btn-default"
                          >
                            Register FOR 101
                          </button>
                        ) : (
                          <button
                            onClick={() =>
                              _self.openModal(item._id, "confirmationDeep")
                            }
                            className="btn btn-default"
                          >
                            APPLY NOW
                          </button>
                        )
                      ) : (
                        null
                        // <button className="btn btn-default center" disabled={true}>
                        //   CLOSED
                        // </button>
                      )
                    ) : item.session_101 ? (
                      <button
                        style={{ left: "-20px" }}
                        onClick={() => _self.openModal(item._id, "101s")}
                        className="btn btn-default center "
                      >
                        {item.bookedByMe}
                      </button>
                    ) : (
                      <button
                        style={{ bottom: "-35px", left: "-20px" }}
                        onClick={() => _self.openModal(item._id, "deep")}
                        className="btn btn-default center "
                      >
                        {item.bookedByMe}
                      </button>
                    )
                  ) : (
                    <div>
                      {item.session_101 ? (
                        <div
                          onClick={() => _self.openLoginModal()}
                          className="btn btn-default left"
                        >
                          LOGIN TO REGISTER
                        </div>
                      ) : (
                        <div
                          onClick={() => _self.openLoginModal()}
                          className="btn btn-default left"
                        >
                          LOGIN TO APPLY
                        </div>
                      )}
                      {item.session_101 ? (
                        <div
                          onClick={() => _self.openModal(item._id, "101s")}
                          className="btn btn-default right"
                        >
                          SHOW DETAILS
                        </div>
                      ) : (
                        <div
                          onClick={() => _self.openModal(item._id, "deep")}
                          className="btn btn-default right"
                        >
                          SHOW DETAILS
                        </div>
                      )}
                    </div>
                  )}
                </div>
              </div>
            )
          else return null
        } else return null
      })
    }

    return (
      <div className="sessions--list my-experience">
        {!this.state.successLoad ? (
          <div
            className="LoadingComponent"
            style={{ textAlign: "center", width: "100%", marginBottom: 50 }}
          >
            <img
              width="80"
              src={require("../../../assets/loading.svg")}
              alt="Loading..."
            />
          </div>
        ) : (
          renderSessionItem()
        )}

        <Modal
          isOpen={this.state.notificationModalIsOpen}
          onRequestClose={this.closeModal.bind(this)}
          contentLabel=""
          className={"notification-popup"}
        >
          {RenderNotificationModalContent()}
        </Modal>

        <Modal
          isOpen={this.state.modalIsOpen}
          onAfterOpen={this.afterOpenModal.bind(this)}
          onRequestClose={this.closeModal.bind(this)}
          contentLabel=""
          className={
            _self.state.type === "confirmation101"
              ? "notification-popup"
              : "workshops-popup"
          }
        >
          {RenderCorpModalContent()}
        </Modal>

        <Modal
          isOpen={this.state.loginModal}
          onRequestClose={this.closeLoginModal.bind(this)}
          contentLabel=""
          className={"LoginPopup"}
        >
          <div className="speaker-modal--preview session--preview session--details">
            <div
              style={{ overflow: "hidden" }}
              className="speaker--details profile--session"
            >
              <button
                className="close"
                onClick={() => this.closeLoginModal()}
              />
              <div className="content">
                {this.state.LoginerorrExist ? (
                  <div
                    style={{ overflow: "hidden" }}
                    className="alert alert-danger"
                  >
                    {this.state.authErorr}
                  </div>
                ) : null}
                <h3>LOGIN</h3>

                {this.state.loginCase ? (
                  <form onSubmit={e => this.onSubmitLogin(e)}>
                    <div className="row">
                      <div className="col-md-12">
                        <InputField
                          error={this.state.errors.email}
                          label="Email Address"
                          onChange={this.onChange}
                          value={this.state.username}
                          field="username"
                          placeholder="Email Address"
                        />
                      </div>
                      <div className="col-md-12">
                        <input
                          error={this.state.errors.password}
                          type="text"
                          name="password"
                          label="Ticket Confirmation Code "
                          value={this.state.password}
                          className="form-control"
                          placeholder="Ticket Confirmation Code "
                          onChange={this.onChange}
                          style={{ textTransform: "uppercase" }}
                        />
                      </div>

                      {this.state.isLoading ? (
                        <div className="col-md-12">
                          <img
                            alt=""
                            width="60"
                            className="loading"
                            src={require("../../../assets/loading.gif")}
                          />
                        </div>
                      ) : null}
                      <div className="col-md-12">
                        <p style={{ marginTop: 15 }}>
                          Please use your email and ticket confirmation code
                          <h3
                            onClick={e => {
                              this.openStepsModal(e)
                            }}
                            style={{
                              fontSize: "14px",
                              fontWeight: "bold",
                              cursor: "pointer"
                            }}
                          >
                            here is how you can find your Confirmation code
                          </h3>
                          Or buy your ticket if you haven’t yet
                          <NavLink
                            className="btn btn-primary"
                            to="/home#tickets"
                          >
                            Tickets
                          </NavLink>
                        </p>
                        <button
                          style={{ margin: "0" }}
                          className="btn btn-default"
                        >
                          Login
                        </button>
                      </div>
                    </div>
                  </form>
                ) : null}
              </div>
            </div>
          </div>
        </Modal>
        <Modal
          isOpen={this.state.StepsModal}
          onRequestClose={this.closeStepsModal.bind(this)}
          contentLabel=""
          className={"workshops-popup"}
        >
          <div className="speaker-modal--preview session--preview session--details">
            <div
              className="speaker--details profile--session loginIMG"
              style={{
                height: "100%",
                overflowY: "scroll",
                maxHeight: "500px"
              }}
            >
              <img
                alt=""
                src={require("../../../assets/images/ConfirmationCodeSteps.jpeg")}
                onClick={e => {
                  this.closeStepsModal()
                }}
              />
            </div>
          </div>
        </Modal>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    isAuth: state.authTicket.isAuth,
    isLoading: state.authTicket.isLoading
  }
}

Sessions.defaultProps = {
  session_101: false,
  session_deep_dive: false
}

export default connect(
  mapStateToProps,
  { fetchSessions, LoginActionWithTickets, GetCurrentUserWithTickets }
)(Sessions)
