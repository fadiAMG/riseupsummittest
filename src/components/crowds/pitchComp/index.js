import React from "react"
import { connect } from "react-redux"
import DocumentTitle from "react-document-title"
import FeaturePost from "../../commons/FeaturePost"
import HeaderSlider from "../../commons/HeaderSlider"
import $ from 'jquery'

import { fetchStartups } from "../../../redux/actions/startups/StartupsActions"

// Data
import { strings } from "../../strings"

class PitchComp extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      active: 0,
      modalIsOpen: false,
      startup: {}
    }
  }

  componentDidMount() {
    $(window).scrollTop($(".feature--post").offset().top - 100)
    this.props.fetchStartups()
  }

  render() {
    return (
      <div>
        <DocumentTitle title="RiseUp Summit - Pitch Competition" />
        <HeaderSlider slider={false} />
        <FeaturePost
          title={strings.PitchComp.feature.title}
          details={strings.PitchComp.feature.details}
          img={require("../../../assets/images/PitchCompetition.jpg")}
          BtnTitle="APPLY NOW"
          Url="https://riseupco.typeform.com/to/m59t7V"
          strength={300}
          products={strings.PitchComp.feature.products}
          slog="BE PART OF OUR STORY…"
          SelfLink={true}
          openTextBlue="APPLICATION IS NOW CLOSED"
          noLink={true}
          seprator={false}
          exist_list={true}
          ImgMaxHeight={"600px"}
          style={{
            slogFontSize: "18px",
            titleFontSize: "27px",
            detailsFontSize: "17px",
            // slogFontWeight: "400",
            slogColor: "#2a428c"
          }}
        />
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    startups: state.startups.startups
  }
}

export default connect(
  mapStateToProps,
  { fetchStartups }
)(PitchComp)
