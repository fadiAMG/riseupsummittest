import React from "react"
import Modal from "react-modal"
import Select from "react-select"
import "react-select/dist/react-select.css"
import { connect } from "react-redux"
import classNames from "classnames"
import DocumentTitle from "react-document-title"
import HeaderSlider from "../../commons/HeaderSlider"
import FeaturePost from "../../commons/FeaturePost"
import CorpModal from "../partnerships/CorpModal"
import Search from "../../speakers/Search"

import { fetchStartups } from "../../../redux/actions/startups/StartupsActions"
// Data
import { strings } from "../../strings"
import removeDuplicates from "../../../utils/algorithms"

class Startups extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      active: 1,
      modalIsOpen: false,
      startup: {},
      searchTerm: "",
      startups: [],
      Industries: [],
      IndustriesOptions: [],
      Dofilter: false,
      startupFilters: []
    }
  }
  componentDidMount() {
    this.props.fetchStartups().then(_ => {
      this.mapTagsToStartups()
      this.mapTagsToOptions()
    })
  }

  openModal(id) {
    this.setState({ modalIsOpen: true, id })
  }

  mapTagsToOptions() {
    let IndustriesOptions = []
    this.props.startups.forEach(item => {
      IndustriesOptions.push({ value: item.Industries, label: item.Industries })
    })
    IndustriesOptions = removeDuplicates(IndustriesOptions)
    this.setState({ IndustriesOptions })
  }

  mapTagsToStartups() {
    let startups = this.props.startups.map(item => {
      if (item.Label_Tags.length === 0)
        item.Label_Tags[0] = item.pitch_comp
          ? "PITCH COMPETITOR"
          : item.startup_exhibitor
          ? "STARTUP EXHIBITOR"
          : item.hipo
          ? "HIPO"
          : "STARTUP EXHIBITOR & PITCH COMPETITOR"
      return item
    })
    this.setState({ startups })
  }

  afterOpenModal() {
    this.state.startups.forEach(item => {
      if (item.new_status && item._id === this.state.id) {
        this.setState({ startup: item })
      }
    })
  }
  closeModal() {
    this.setState({ modalIsOpen: false })
  }
  speakerSearch(term) {
    this.setState({ searchTerm: term })
  }
  onChangeSelect(selectedOptions) {
    this.setState({ Industries: selectedOptions, Dofilter: false })
    if (selectedOptions.length) this.filterSatrtupsByTags(selectedOptions)
  }

  filterSatrtupsByTags(selectedOptions) {
    let newItems = []
    this.props.startups.map(item => {
      selectedOptions.map(tag => {
        if (item.Industries === tag.value) {
          newItems.push(item)
        }
        return tag
      })
      return item
    })

    this.setState({ startupFilters: newItems, Dofilter: true })
  }
  render() {
    let { startups } = this.props
    let { startupFilters } = this.state
    let _self = this

    let term = this.state.searchTerm

    if (this.state.searchTerm !== "") {
      startups = startups.filter(
        item =>
          item.name && item.name.toLowerCase().includes(term.toLowerCase())
      )
    }

    let navWrap = (
      <ul className="nav--wrap">
        <li>
          <div
            onClick={() => this.setState({ active: 1 })}
            className={classNames({ active: this.state.active === 1 })}
          >
            MEET THE STARTUPS
          </div>
        </li>
        <li>
          <div
            onClick={() => this.setState({ active: 0 })}
            className={classNames({ active: this.state.active === 0 })}
          >
            OPPORTUNITIES
          </div>
        </li>
      </ul>
    )

    let startupsList = startups.map((item, i) => {
      if (
        item.new_status
        // && !item.Label_Tags.includes("MENA HiPO")
      ) {
        return (
          <li key={i}>
            <div onClick={() => this.openModal(item._id)}>
              <img src={item.logo_url} alt={item.name} />
            </div>
          </li>
        )
      } else return null
    })

    let filterdStartupList = startupFilters.map((item, i) => {
      if (
        item.new_status
        // && !item.Label_Tags.includes("MENA HiPO")
      ) {
        return (
          <li key={i}>
            <div onClick={() => this.openModal(item._id)}>
              <img src={item.logo_url} alt={item.name} />
            </div>
          </li>
        )
      } else return null
    })

    return (
      <div>
        <DocumentTitle title="RiseUp Summit - Startups" />
        <HeaderSlider slider={false} />
        {navWrap}

        {this.state.active === 0 ? (
          <FeaturePost
            title={strings.Startups.feature.title}
            details={strings.Startups.feature.details}
            img={require("../../../assets/images/STARTUPS_HEADER-min.png")}
            ImgMaxHeight={"600px"}
            BtnTitle="APPLY NOW"
            Url="https://riseupco.typeform.com/to/m59t7V"
            strength={300}
            products={strings.Startups.feature.products}
            slog="BE PART OF OUR STORY…"
            SelfLink={true}
            noLink={true}
            seprator={false}
            style={{
              slogFontSize: "18px",
              titleFontSize: "27px",
              detailsFontSize: "17px",
              // slogFontWeight: "400",
              slogColor: "#2a428c"
            }}
          />
        ) : (
          <div
            className="partner--section__content startups--wrap-v2"
            style={{
              position: "relative",
              backgroundColor: "#fff",
              paddingTop: 50
            }}
          >
            <h3 className="section--header">MEET THE STARTUPS</h3>

            <div className="partners--wrapper" style={{ paddingTop: 0 }}>
              <div
                className="partner--section__content"
                style={{ paddingTop: 0 }}
              >
                {this.props.loading === 0 ? (
                  <div
                    className="LoadingComponent"
                    style={{
                      textAlign: "center",
                      width: "100%",
                      marginBottom: 50
                    }}
                  >
                    <img
                      width="80"
                      src={require("../../../assets/loading.svg")}
                      alt="Loading..."
                    />
                  </div>
                ) : (
                  <div>
                    <div>
                      <Search
                        onSearchSpeaker={term => this.speakerSearch(term)}
                        placeholder="Search Startups..."
                      />
                    </div>
                    <div className="filter--startups--tags clearfix">
                      <div className="col-md-4 col-md-offset-4">
                        <label>Filter By Industry</label>
                        <Select
                          value={this.state.Industries}
                          options={this.state.IndustriesOptions}
                          onChange={this.onChangeSelect.bind(this)}
                          className="basic-multi-select"
                          classNamePrefix="select"
                          multi={true}
                        />
                      </div>
                    </div>
                  </div>
                )}

                <div
                  className="partner--section__content"
                  style={{ paddingTop: 40 }}
                >
                  <ul>
                    {!this.state.Dofilter ? startupsList : filterdStartupList}
                  </ul>
                </div>
              </div>
            </div>
          </div>
        )}
        <Modal
          isOpen={this.state.modalIsOpen}
          onAfterOpen={this.afterOpenModal.bind(this)}
          onRequestClose={this.closeModal.bind(this)}
          contentLabel=""
          className={"PartnersModal"}
        >
          <CorpModal
            close={() => _self.setState({ modalIsOpen: false })}
            partnerData={_self.state.startup}
          />
        </Modal>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    startups: state.startups.startups
  }
}
export default connect(
  mapStateToProps,
  { fetchStartups }
)(Startups)
