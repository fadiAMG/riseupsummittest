import React from "react"
import DocumentTitle from "react-document-title"
import HeaderSlider from "../../commons/HeaderSlider"
import FeaturePost from "../../commons/FeaturePost"

// Data
import { strings } from "../../strings"

class Launchpad extends React.Component {
  render() {
    let data = strings.Launchpad
    return (
      <div>
        <DocumentTitle title="RiseUp Summit - Pitch Competition" />
        <HeaderSlider slider={false} />
        <FeaturePost
          title={data.feature.title}
          details={data.feature.details}
          img={require("../../../assets/images/LAUNCHPADHEADER-min.jpeg")}
          BtnTitle="APPLY NOW"
          Url="https://riseupco.typeform.com/to/zJJA6X"
          strength={300}
          ImgMaxHeight={"550px"}
          slog="BE PART OF OUR STORY…"
          products={data.feature.products}
          exist_list={true}
          selfTarget={true}
          noLink={false}
        />
      </div>
    )
  }
}

export default Launchpad
