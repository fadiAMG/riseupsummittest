import React from 'react'
import PropTypes from 'prop-types'
import DocumentTitle from 'react-document-title'
import Banner from '../../commons/banner'

class  LaunchpadForm extends React.Component{
  render(){
  	return(
  		<div className="apply-to-speak">
        <DocumentTitle title='RiseUp Summit - Launchpad Application'></DocumentTitle>
  			<Banner
          title="LAUNCHPAD FORM"
          opacity={.5}
          width={85}
          bgurl={require('../../../assets/images/launchpad.jpg')}
          strength={200}
        />
        <div className="fullwidth--seprator"></div>
        <div className="form--wrap">
          <iframe id="typeform-full" width="100%" height="400px" frameBorder="0" src={`https://riseupsummit2017.typeform.com/to/QOvZDI`}></iframe>
        </div>
  		</div>
  	)
  }
}


export default LaunchpadForm
