import React, { Component } from "react"
import DocumentTitle from "react-document-title"
import FeaturePost from "../../commons/FeaturePost"
import HeaderSlider from "../../commons/HeaderSlider"
import CorpModal from "../partnerships/CorpModal"
import Modal from "react-modal"
import { strings } from "../../strings"
import { connect } from "react-redux"
import {
  fetchStartups,
  fetchStartup
} from "../../../redux/actions/startups/StartupsActions"
import $ from "jquery"

class MenaHipos extends Component {
  constructor(props) {
    super(props)
    this.state = {
      StartupsHipos: [],
      modalIsOpen: false,
      startup: {}
    }
  }

  componentDidMount() {
    $(window).scrollTop($(".feature--post").offset().top - 100)
    let StartupsHipos = []
    this.props
      .fetchStartups()
      .then(res => {
        res.payload.data.collections.map(item => {
          if (item.new_status) {
            if (item.hipo) {
              StartupsHipos.push(item)
            }
          }
          return null
        })
        this.setState({ StartupsHipos: StartupsHipos })
      })
      .then(this.mapTagsToStartups.bind(this))
  }
  mapTagsToStartups() {
    let StartupsHipos = this.state.StartupsHipos.map(item => {
      if (item.Label_Tags.length === 0)
        item.Label_Tags[0] = item.pitch_comp
          ? "PITCH COMPETITOR"
          : item.startup_exhibitor
          ? "STARTUP EXHIBITOR"
          : item.hipo
          ? "HIPO"
          : "STARTUP EXHIBITOR & PITCH COMPETITOR"
      return item
    })
    this.setState({ StartupsHipos })
  }
  openModal(id) {
    this.setState({ modalIsOpen: true, id })
  }

  afterOpenModal(id) {
    this.props.fetchStartup(this.state.id).then(res => {
      this.setState({ startup: res.payload.data.collection })
    })
  }
  closeModal() {
    this.setState({ modalIsOpen: false })
  }
  render() {
    let _self = this
    function RenderCorpModalContent() {
      if (_self.state.startup) {
        return (
          <CorpModal
            close={() => _self.setState({ modalIsOpen: false })}
            partnerData={_self.state.startup}
          />
        )
      } else {
        return null
      }
    }

    return (
      <div className="">
        <DocumentTitle title="RiseUp Summit - MENA HIPOs" />
        <HeaderSlider slider={false} />
        <FeaturePost
          title={strings.MenaHipos.feature.title}
          details={strings.MenaHipos.feature.details}
          img={require("../../../assets/images/HIPOS.png")}
          ImgMaxHeight={"600px"}
          // openText="Apply to join the ranks of MENA’s next top 20 HIPO’s!"
          // openTextBlue="Fees: 5000 USD"
          BtnTitle="APPLY NOW!"
          Url="https://riseupco.typeform.com/to/yA53jQ"
          strength={250}
          products={strings.MenaHipos.feature.products}
          slog="BE PART OF OUR STORY…"
          SelfLink={true}
          noLink={true}
          seprator={false}
          exist_list={true}
          style={{
            slogFontSize: "18px",
            titleFontSize: "27px",
            detailsFontSize: "17px",
            // slogFontWeight: "400",
            slogColor: "#2a428c"
          }}
        />

        <div className="parllex-fix" style={{ paddingTop: 0 }}>
          <div className="partners--wrapper" style={{ paddingTop: 10 }}>
            <div
              className="partner--section__content big"
              style={{ paddingTop: 0 }}
            >
              <h3 className="section--header">MEET OUR HIPOs</h3>
              <ul>
                {this.state.StartupsHipos.map((item, i) => {
                  return (
                    <li key={i}>
                      <button onClick={() => _self.openModal(item._id)}>
                        <img src={item.logo_url} alt={item.name} />
                      </button>
                    </li>
                  )
                })}
              </ul>
            </div>
          </div>
        </div>
        <Modal
          isOpen={this.state.modalIsOpen}
          onAfterOpen={this.afterOpenModal.bind(this)}
          onRequestClose={this.closeModal.bind(this)}
          contentLabel=""
          className={{
            base: "PartnersModal"
          }}
        >
          {RenderCorpModalContent()}
        </Modal>
      </div>
    )
  }
}

export default connect(
  null,
  { fetchStartups, fetchStartup }
)(MenaHipos)
