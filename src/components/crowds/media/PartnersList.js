import React from 'react';
import PartnerItem from './PartnerItem'

const PartnersList = ({data}) => {
	function renderPartners(){
		return data.map(item => {
			return(
				<PartnerItem item={item}/>
			)
		})
	}
	return(
		<div className="corp--wrap">
			{renderPartners()}
		</div>
	)
}

export default PartnersList