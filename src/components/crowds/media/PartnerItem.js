import React from 'react';
import PropTypes from 'prop-types'

const PartnerItem = ({item}) => {
	return(
		<div className="item">
			<a>
				<img className="logo--wrap" src={item.logo} alt={item.name}/>
			</a>
		</div>
	)
}


PartnerItem.propTypes = {
  item: PropTypes.object.isRequired
}

export default PartnerItem