import React, { Component } from "react"
import DocumentTitle from "react-document-title"

import FeaturePost from "../../commons/FeaturePost"
import HeaderSlider from "../../commons/HeaderSlider"

import PartnersList from "./PartnersList"
import { strings } from "../../strings"

import { data } from "../../data"
import $ from "jquery"

class Media extends Component {
  componentDidMount() {
    $(window).scrollTop($(".feature--post").offset().top - 100)
  }
  render() {
    let stringsVal = strings.Media
    // function RenderCorps() {
    // if (data.partners) {
    //    return <PartnersList data={data.Media} />
    // } else {
    //  return null
    //}
    //}
    return (
      <div>
        <DocumentTitle title="RiseUp Summit - Media" />
        <HeaderSlider slider={false} />
        <FeaturePost
          slog="BE PART OF OUR STORY…"
          title={stringsVal.feature.title}
          details={stringsVal.feature.details}
          img={require("../../../assets/images/v2/media.png")}
          strength={250}
          noLink={true}
          openText="BE THE FIRST TO KNOW IT ALL, GET IN TOUCH"
          email="media@riseup.co"
          style={{
            slogFontSize: "18px",
            titleFontSize: "27px",
            detailsFontSize: "17px",
            // slogFontWeight: "400",
            slogColor: "#2a428c"
          }}
        />
        {/* <div className="parllex-fix">
          <h3 className="section--header">PREVIOUS MEDIA</h3>
          {RenderCorps()}
        </div> */}
      </div>
    )
  }
}

export default Media
