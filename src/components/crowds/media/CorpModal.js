import React from 'react';

const CorpModal = ({close}) => { 
	return(
		<div className="corp-modal--preview">
			<header>
				<h4>CREATIVE</h4>
				<button className="close" onClick={close}></button>
			</header>
      <div className="content">
      	<div className="img--wrap">
      		<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/6/62/Uber_logo.svg/2000px-Uber_logo.svg.png" alt=""/>
				</div>
				<p>Farida received her bachelor’s degree in international development studies and anthropology from McGill University in 2013 and her master’s degree in international education policy from Harvard Graduate School of Education. Farida received her bachelor’s degree in international development studies and anthropology from McGill University in 2013 and her.</p>
      </div>
    </div>
	)
}

export default CorpModal