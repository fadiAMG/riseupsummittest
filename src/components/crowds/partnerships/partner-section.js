import React from "react"

const PartnerSection = ({ data, tag, onClick }) => {
  let item = data.map((item, i) => {
    return (
      <li key={i}>
        <a onClick={id => onClick(item._id)}>
          <img src={item.logo_url} alt={item.name} />
        </a>
      </li>
    )
    // }
  })
  return (
    <div className="partner--section__content">
      <h3 className="section--header">{tag}</h3>
      <ul>{item}</ul>
    </div>
  )
}
export default PartnerSection
