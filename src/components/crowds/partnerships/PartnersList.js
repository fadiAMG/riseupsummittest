import React from 'react';
import PartnerItem from './PartnerItem'

import {data} from '../../data';

const PartnersList = ({data,type, ClickSpeaker}) => {
	function renderPartners(){
		return data.map(item => {
			return(
				<PartnerItem item={item} ClickSpeaker={(id) => ClickSpeaker(id)}/>
			)
		})
	}
	return(
		<div className="corp--wrap">
			{renderPartners()}
		</div>
	)
}

export default PartnersList