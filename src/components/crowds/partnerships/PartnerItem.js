import React from 'react';
import PropTypes from 'prop-types'

const PartnerItem = ({item, ClickSpeaker}) => {
	return(
		<div className="item">
			<a onClick={(id) => ClickSpeaker(item._id)}>
				<img className="logo--wrap" src={`https://dashboard.riseupconnect.com/partners/${item.image.filename}`} alt={item.name}/>
			</a>
		</div>
	)
}


PartnerItem.propTypes = {
  item: PropTypes.object.isRequired,
  ClickSpeaker: PropTypes.func.isRequired
}

export default PartnerItem