import React from 'react';
import {data} from '../../data';
import {NavLink} from 'react-router-dom'
import $ from 'jquery'

const PartnershipsList = () => {
  function clickButton(){
    $("html, body").animate({ scrollTop: 0 })
  }
  function renderList(){
    return data.PartnershipsList.map(item => {
      const itemText = item.title.toLowerCase().split(' ').join('_')
      return(
        <div className="item">
          <div className="img-wrap">
            <img src={item.imgUrl}  alt={item.title}/>
          </div>
          <div className="details">
            <h3>{item.title} <span>{item.slog}</span></h3>
            <p>{item.details}</p>
            <a href={item.pdfLink} download>DOWNLOAD DETAILS</a>
            <h6>APPLICATION CLOSED</h6>
          </div>
        </div>
      )
    })
  }
  return(
    <div className="partnerships--list">
      <div className="container">
        <div className="list-wrap">
          {renderList()}
          <div className="item email-item">
            <div className="email--wrap">
              <img src="https://riseupsummit.com/wp-content/uploads/2017/08/plusicon.svg" alt=""/>
              <p>CAN’T FIND WHAT YOU’RE LOOKING FOR? </p>
              <p>SHOOT US AN EMAIL On: <span>summitcrowd@riseup.co</span></p>
              {/* <a href="mailto:summitcrowd@riseup.co?subject=Partnerships">
                SHOOT US AN EMAIL
                <img src={require('../../../assets/arrow-right.svg')} alt=""/>
              </a> */}
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

{/* <div className="action--wrap">
  <NavLink 
    onClick={clickButton} 
    to={`/partners/${item.formId}/${itemText}`} 
    className="btn btn-blue">
    APPLY NOW
  </NavLink>
</div> */}
export default PartnershipsList