import React, {Component} from 'react';
import axios from 'axios';
import Clipboard from 'react-clipboard.js';
import classNames from 'classnames';
import moment from 'moment/src/moment';
const TICKERS_NUMBER_API_URL = 'https://dashboard.riseupsummit.com/api/partnerships/'
const TICKET_SEND = 'https://dashboard.riseupsummit.com/api/partnerships/'
const RESEND_PROMOCODE = 'https://dashboard.riseupsummit.com/api/partnerships/'

const BASE_URL = 'https://riseupsummit.com/#/ticketspromo?registrationcode='
class PartnerShipsDashboard extends Component{
  constructor(props){
    super(props);
    this.state = {
      email: '',
      currentTab: 1,
      disabled:false,
      hash:'',
      buttonText:'Copy To Clipboard!'
    }
    this.onSubmitEmail = this.onSubmitEmail.bind(this);
    this.RequestTickets = this.RequestTickets.bind(this)
  }
  componentDidMount () {
    this.RequestTickets();
  }
  RequestTickets(){
    let id = this.props.match.params.id;
    let url = this.props.location.search;
    let hash = url.replace('?hash=', '');
    
    axios.get(`${TICKERS_NUMBER_API_URL}${id}/tickets?hash=${hash}`).then(res => {
      this.setState({data: res.data.collection});
      let dt = res.data.collection;
      this.setState({hash: res.data.collection.hash});
      if(dt.totalFreeTickets !== '0'){
        this.setState({currentTab:1})
      }else if(dt.totalCommunityTickets !== '0'){
        this.setState({currentTab:3})
      }else if(dt.totalStudentTickets !== '0'){
        this.setState({currentTab:2})
      }
      else if(dt.totalWuzzufTickets !== '0'){
        this.setState({currentTab:4})
      }
    })
  }
  onChange(e){
    this.setState({ email: e.target.value} );
  }
  onSubmitEmail(type, e) {
    let id = this.props.match.params.id;
    let hash = this.state.hash;
    e.preventDefault();
    let data = {
      type: type,
      email: this.state.email
    }
    axios.post(`${TICKET_SEND}${id}/send?hash=${hash}`, data).then(res => {
      this.RequestTickets();
    })
    this.setState({email: ''})
  }
  ResendPromocode(id){
    let url = this.props.location.search;
    let hash = url.replace('?hash=', '');
    this.setState({disabled:true})
    axios.post(`${RESEND_PROMOCODE}${id}/resend`)
  }

  render(){
    let data = this.state.data;
    return(
      <div className="partners--dahabord">
        <div className="agenda--main-banner">
          <h1>TICKETS</h1>
        </div>
        <div className="container">
          <h3 className="title">YOUR TICKETS TO #RISEUP17</h3>
          <div className="row">
            {
              data && data.totalFreeTickets > 0 ? (
                <div className="col-md-4">
                  <div className="card">
                    <h4>Free Tickets</h4>
                    <ul>
                      <li>Total<span> {data ? data.totalFreeTickets  : '0'}</span></li>
                      <li>Available<span> {data ? data.avbFreeTickets  : '0'}</span></li>
                    </ul>
                  </div>
                </div>
              ) : null
            }
            {
              data && data.totalCommunityTickets > 0 ? (
                <div className="col-md-4">
                  <div className="card">
                    <h4>COMMUNITY Tickets</h4>
                    <ul>
                      <li>Total<span> {data ? data.totalCommunityTickets  : '0'}</span></li>
                      <li>Available<span> {data ? data.avbCommunityTickets  : '0'}</span></li>
                    </ul>
                  </div>
                </div>
              ) : null
            }
            
            {
              data && data.totalStudentTickets > 0 ? (
                <div className="col-md-4">
                  <div className="card">
                    <h4>STUDENT Tickets</h4>
                    <ul>
                      <li>Total<span> {data ? data.totalStudentTickets  : '0'}</span></li>
                      <li>Available<span> {data ? data.avbStudentTickets  : '0'}</span></li>
                    </ul>
                  </div>
                </div>
              ) : null 
            }
            {/* WUZZUF */}
            {
              data && data.totalWuzzufTickets > 0 ? (
                <div className="col-md-4">
                  <div className="card">
                    <h4>WUZZUF TICKETS</h4>
                    <ul>
                      <li>Total<span> {data ? data.totalWuzzufTickets  : '0'}</span></li>
                      <li>Available<span> {data ? data.avbWuzzufTickets  : '0'}</span></li>
                    </ul>
                  </div>
                </div>
              ) : null 
            }
            
            
          </div>
          {
            data ? (
              <div className="alert alert-warning">
                Note that any ticket registration codes sent cannot be redeemed
              </div>
            ) : null
          }
          <ul className="nav nav-pills">
            {
              data && data.totalFreeTickets !== '0' ? (
                <li className={classNames(this.state.currentTab === 1 ? 'active' : null)}>
                  <a onClick={() => this.setState({email: '', currentTab:1})}>SEND FREE TICKETS</a>
                </li>
              ) : null 
            }
            
            {
              data && data.totalCommunityTickets !== '0' ? (
                <li className={classNames(this.state.currentTab === 3 ? 'active' : null)}>
                  <a onClick={() =>this.setState({email: '', currentTab:3})}>SEND COMMUNITY TICKETS</a>
                </li>
              ) : null
            }
            {
              data && data.totalStudentTickets !== '0' ? (
                <li className={classNames(this.state.currentTab === 2 ? 'active' : null)}>
                  <a onClick={() => this.setState({email: '', currentTab:2})}>SEND STUDENT TICKETS</a>
                </li>
              ) : null
            }
            {
              data && data.totalWuzzufTickets !== '0' ? (
                <li className={classNames(this.state.currentTab === 4 ? 'active' : null)}>
                  <a onClick={() => this.setState({email: '', currentTab:4})}>SEND WUZZUF TICKETS</a>
                </li>
              ) : null
            }
            
          </ul>
          
          <div className="row">
            {
              this.state.currentTab === 1 && data && data.totalFreeTickets > 0  ? (
                <div className="item">
                  <div className="col-md-6">
                    <h4>SEND FREE TICKETS</h4>
                    
                    <div className="emails-list">
                      {
                        data && data.avbFreeTickets !== 0 ? (
                          <div>
                            <label>Please Type Email Address</label>
                            <form onSubmit={(e) => this.onSubmitEmail('free', e)}>
                              <input 
                                required 
                                value={this.state.email}
                                className="form-control" 
                                type="email" 
                                placeholder="Email Address" 
                                onChange={(event) => this.setState({email: event.target.value})}
                              />
                              <button 
                                type="submit"
                                className="btn" 
                                >SEND</button>
                            </form>
                          </div>
                        ) : null
                      }
                    </div>
                    <h5>Previous Emails</h5>
                    <table className="table table-bordered"> 
                      <thead> 
                        <tr> 
                        <th>Email</th>
                        <th>Created At</th>
                        <th>Is Redeemed</th>
                        <th>Redeemed Email</th>
                        <th>Redeemed Name</th>
                        <th>Copy Redeem Url</th>
                        <th>Resend Ticket</th>
                        </tr> 
                      </thead> 
                      <tbody> 
                        { 
                          data ? data.free_tickets.map(item => {
                            return(
                              <tr> 
                                <td>{item.email}</td> 
                                <td>{moment(item.createdAt).format('MMMM Do YYYY, h:mm a')}</td> 
                                <td style={{textAlign:'center'}}>{item.hasOwnProperty('isRedeemed') ? item.isRedeemed ? <i style={{color:'#27ae60'}} className="fa fa-check"/> : <i style={{color:'#e74c3c'}}  className="fa fa-times"/>  :' '}</td>
                                <td>{item.hasOwnProperty('remeedEmail') ? item.remeedEmail :' '}</td>
                                <td>{item.hasOwnProperty('remeedName')  ? item.remeedName : ' '}</td>
                                <td style={{display:'flex'}}>
                                  <input style={{width:'60px'}} className="form-control" type="text" value={`${BASE_URL}${item.registrationCode}`}/>
                                   <Clipboard 
                                    className="btn btn-default" 
                                    data-clipboard-text={`${BASE_URL}${item.registrationCode}`} 
                                    onClick={() => alert(`Copied To Clipboard`)}>
                                    <i className="fa fa-copy"></i>
                                  </Clipboard>
                                </td>
                                <td><a  className="btn btn-primary" onClick={() => this.ResendPromocode(item.id)}>Resend</a></td> 
                              </tr> 
                            )
                          }) : null
                        }
                      </tbody> 
                    </table>
                    
                    
                  </div>
                </div>
              ) : null
            }

            {
              this.state.currentTab === 2  && data && data.totalStudentTickets > 0  ? (
                <div className="item">
                  <div className="col-md-6">
                    <h4>SEND STUDENT TICKETS</h4>
                    
                    <div className="emails-list">
                      {
                        data && data.avbStudentTickets !== 0 ? (
                          <div>
                            <label>Please Type Email Address</label>
                            <form onSubmit={(e) => this.onSubmitEmail('student', e)}>
                              <input 
                                required 
                                value={this.state.email}
                                className="form-control" 
                                type="email" 
                                placeholder="Email Address" 
                                onChange={(event) => this.setState({email: event.target.value})}
                              />
                              <button 
                                type="submit"
                                className="btn" 
                                >SEND</button>
                            </form>
                          </div>
                        ) : null
                      }
                    </div>
                    <h5>Previous Emails</h5>
                    <table className="table table-bordered"> 
                      <thead> 
                        <tr> 
                        <th>Email</th>
                        <th>Created At</th>
                        <th>Is Redeemed</th>
                        <th>Redeemed Email</th>
                        <th>Redeemed Name</th>
                        <th>Copy Redeem Url</th>
                        <th>Resend Ticket</th>
                        </tr> 
                      </thead> 
                      <tbody> 
                        { 
                          data ? data.student_tickets.map(item => {
                            return(
                              <tr> 
                                <td>{item.email}</td> 
                                <td>{moment(item.createdAt).format('MMMM Do YYYY, h:mm a')}</td> 
                                <td style={{textAlign:'center'}}>{item.hasOwnProperty('isRedeemed') ? item.isRedeemed ? <i style={{color:'#27ae60'}} className="fa fa-check"/> : <i style={{color:'#e74c3c'}}  className="fa fa-times"/>  :' '}</td>
                                <td>{item.hasOwnProperty('remeedEmail') ? item.remeedEmail :' '}</td>
                                <td>{item.hasOwnProperty('remeedName')  ? item.remeedName : ' '}</td>
                                <td style={{display:'flex'}}>
                                  <input style={{width:'60px'}} className="form-control" type="text" value={`${BASE_URL}${item.registrationCode}`}/>
                                   <Clipboard 
                                    className="btn btn-default" 
                                    data-clipboard-text={`${BASE_URL}${item.registrationCode}`} 
                                    onClick={() => alert(`Copied To Clipboard`)}>
                                    <i className="fa fa-copy"></i>
                                  </Clipboard>
                                </td>
                                {/* <td><a className="btn btn-default" onClick={() => this.RedeemUrl(item.registrationCode)}>Copy Redeem Url</a></td> */}
                                <td><a className="btn btn-primary" onClick={() => this.ResendPromocode(item.id)}>Resend</a></td> 
                              </tr> 
                            )
                          }) : null
                        }
                      </tbody> 
                    </table>
                    
                  </div>
                </div>
              ) : null
            }
            
            {
              this.state.currentTab === 3 && data && data.totalCommunityTickets  > 0  ? (
                <div className="item">
                  <div className="col-md-4">
                    <h4>SEND COMMUNITY TICKETS</h4>
                    
                    <div className="emails-list">
                      {
                        data && data.avbCommunityTickets !== 0 ? (
                          <div>
                            <label>Please Type Email Address</label>
                            <form onSubmit={(e) => this.onSubmitEmail('community', e)}>
                              <input 
                                required 
                                value={this.state.email}
                                className="form-control" 
                                type="email" 
                                placeholder="Email Address" 
                                onChange={(event) => this.setState({email: event.target.value})}
                              />
                              <button 
                                type="submit"
                                className="btn" 
                                >SEND</button>
                            </form>
                          </div>
                        ) : null
                      }
                    </div>
                    <h5>Previous Emails</h5>
                    <table className="table table-bordered"> 
                      <thead> 
                        <tr> 
                        <th>Email</th>
                        <th>Created At</th>
                        <th>Is Redeemed</th>
                        <th>Redeemed Email</th>
                        <th>Redeemed Name</th>
                        <th>Copy Redeem Url</th>
                        <th>Resend Ticket</th>
                        </tr> 
                      </thead> 
                      <tbody> 
                        { 
                          data ? data.community_tickets.map(item => {
                            return(
                              <tr> 
                                <td>{item.email}</td> 
                                <td>{moment(item.createdAt).format('MMMM Do YYYY, h:mm a')}</td>
                                <td style={{textAlign:'center'}}>{item.hasOwnProperty('isRedeemed') ? item.isRedeemed ? <i style={{color:'#27ae60'}} className="fa fa-check"/> : <i style={{color:'#e74c3c'}}  className="fa fa-times"/>  :' '}</td>
                                <td>{item.hasOwnProperty('remeedEmail') ? item.remeedEmail :' '}</td>
                                <td>{item.hasOwnProperty('remeedName')  ? item.remeedName : ' '}</td>
                                <td style={{display:'flex'}}>
                                  <input style={{width:'60px'}} className="form-control" type="text" value={`${BASE_URL}${item.registrationCode}`}/>
                                   <Clipboard 
                                    className="btn btn-default" 
                                    data-clipboard-text={`${BASE_URL}${item.registrationCode}`} 
                                    onClick={() => alert(`Copied To Clipboard`)}>
                                    <i className="fa fa-copy"></i>
                                  </Clipboard>
                                </td>
                                <td><a className="btn btn-primary" onClick={() => this.ResendPromocode(item.id)}>Resend</a></td>  
                              </tr> 
                            )
                          }) : null
                        }
                      </tbody> 
                    </table>
                    
                  </div>
                </div>
              ) : null
            }

            {
              this.state.currentTab === 4 && data && data.totalWuzzufTickets  > 0  ? (
                <div className="item">
                  <div className="col-md-4">
                    <h4>SEND COMMUNITY TICKETS</h4>
                    
                    <div className="emails-list">
                      {
                        data && data.avbWuzzufTickets !== 0 ? (
                          <div>
                            <label>Please Type Email Address</label>
                            <form onSubmit={(e) => this.onSubmitEmail('wuzzuf', e)}>
                              <input 
                                required 
                                value={this.state.email}
                                className="form-control" 
                                type="email" 
                                placeholder="Email Address" 
                                onChange={(event) => this.setState({email: event.target.value})}
                              />
                              <button 
                                type="submit"
                                className="btn" 
                                >SEND</button>
                            </form>
                          </div>
                        ) : null
                      }
                    </div>
                    <h5>Previous Emails</h5>
                    <table className="table table-bordered"> 
                      <thead> 
                        <tr> 
                        <th>Email</th>
                        <th>Created At</th>
                        <th>Is Redeemed</th>
                        <th>Redeemed Email</th>
                        <th>Redeemed Name</th>
                        <th>Copy Redeem Url</th>
                        <th>Resend Ticket</th>
                        </tr> 
                      </thead> 
                      <tbody> 
                        { 
                          data ? data.wuzzuf_tickets.map(item => {
                            return(
                              <tr> 
                                <td>{item.email}</td> 
                                <td>{moment(item.createdAt).format('MMMM Do YYYY, h:mm a')}</td>
                                <td style={{textAlign:'center'}}>{item.hasOwnProperty('isRedeemed') ? item.isRedeemed ? <i style={{color:'#27ae60'}} className="fa fa-check"/> : <i style={{color:'#e74c3c'}}  className="fa fa-times"/>  :' '}</td>
                                <td>{item.hasOwnProperty('remeedEmail') ? item.remeedEmail :' '}</td>
                                <td>{item.hasOwnProperty('remeedName')  ? item.remeedName : ' '}</td>
                                <td style={{display:'flex'}}>
                                  <input style={{width:'60px'}} className="form-control" type="text" value={`${BASE_URL}${item.registrationCode}`}/>
                                   <Clipboard 
                                    className="btn btn-default" 
                                    data-clipboard-text={`${BASE_URL}${item.registrationCode}`} 
                                    onClick={() => alert(`Copied To Clipboard`)}>
                                    <i className="fa fa-copy"></i>
                                  </Clipboard>
                                </td>
                                <td><a className="btn btn-primary" onClick={() => this.ResendPromocode(item.id)}>Resend</a></td>  
                              </tr> 
                            )
                          }) : null
                        }
                      </tbody> 
                    </table>
                    
                  </div>
                </div>
              ) : null
            }
          </div>
        </div>
      </div>
    )
  }
}
export default PartnerShipsDashboard;