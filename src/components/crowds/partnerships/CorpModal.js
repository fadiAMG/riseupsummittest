import React from 'react';

const CorpModal = ({partnerData,close}) => { 
	return(
		<div className="corp-modal--preview">
			<header>
				<h4>{partnerData.Label_Tags === 'Other' ? 'Partner' : partnerData.Label_Tags}</h4>
				<button className="close" onClick={close}></button>
			</header>
      <div className="content">
      	<div className="img--wrap">
      		<img src={partnerData.logo_url} alt=""/>
				</div>
				<p>{partnerData.description}</p>
				<ul>
					{
						partnerData.facebook_link ? (
							<li>
								<a href={partnerData.facebook_link } target="_blank" rel="noopener noreferrer">
									<i className="fa fa-facebook"></i>
								</a>
							</li>
						) : null
					}
					{
						partnerData.linkedin_link ? (
							<li>
								<a href={partnerData.linkedin_link } target="_blank" rel="noopener noreferrer">
									<i className="fa fa-linkedin"></i>
								</a>
							</li>
						) : null
					}
					{
						partnerData.entity_website ? (
							<li>
								<a href={partnerData.entity_website } target="_blank" rel="noopener noreferrer">
									<i className="fa fa-globe"></i>
								</a>
							</li>
						) : null
					}
					{
						partnerData.messanger_link ? (
							<li>
								<a href={partnerData.messanger_link } target="_blank" rel="noopener noreferrer">
									<i className="fa fa-commenting"></i>
								</a>
							</li>
						) : null
					}
					{
						partnerData.instagram_link ? (
							<li>
								<a href={partnerData.instagram_link } target="_blank" rel="noopener noreferrer">
									<i className="fa fa-instagram"></i>
								</a>
							</li>
						) : null
					}	
					{
						partnerData.Magnitt_link ? (
							<li>
								<a href={partnerData.Magnitt_link } target="_blank" rel="noopener noreferrer">
									<img alt="" src={require('../../../assets/images/magnitt.png')}/>

								</a>
							</li>
						) : null
					}		

								
				</ul>
      </div>
    </div>
	)
}

export default CorpModal