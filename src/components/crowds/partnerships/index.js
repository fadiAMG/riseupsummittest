import React, { Component } from "react"
import DocumentTitle from "react-document-title"
import Modal from "react-modal"
import HeaderSlider from "../../commons/HeaderSlider"
import { connect } from "react-redux"
import {
  fetchPartners,
  fetchPartner
} from "../../../redux/actions/partners/PartnersAction"
import CorpModal from "./CorpModal"
import PartnerSection from "./partner-section"
import FirstBeleivers from "./first-beleviers"
import $ from "jquery"

const PartnersSectionKeys = [
  "main_partner",
  "pr_partner",
  "partner",
  "track_partner",
  "content_partner",
  "community_partner",
  "media_partner",
  "session_host",
  "launchpad_cohost",
  "sattelite_host",
  "outreach_partner",
  "hipo_partner",
  "fandb_partner",
  "coworking_partner",
  "award_partner",
  "mobile_application_partner"
]

const HomePartnersSectionKeys = ["main_partner", "content_partner"]

class Partnerships extends Component {
  constructor(props) {
    super(props)

    this.state = {
      modalIsOpen: false,
      active: false,
      partners: {
        main_partner: { array: [], tag: "MAIN PARTNERS" },
        partner: { array: [], tag: "PARTNERS" },
        content_partner: { array: [], tag: "Content PARTNERS" },
        hipo_partner: { array: [], tag: "HIPO PARTNERS" },
        launchpad_cohost: { array: [], tag: "Launchpad Co-host" },
        media_partner: { array: [], tag: "Media PARTNERS" },
        community_partner: { array: [], tag: "COMMUNITY PARTNERS" },
        session_host: { array: [], tag: "Session Host" },
        fandb_partner: { array: [], tag: "F&B PARTNER" },
        sattelite_host: { array: [], tag: "Sattelite Event Host" },
        outreach_partner: { array: [], tag: "Outreach PARTNERS" },
        track_partner: { array: [], tag: "TRACK PARTNERS" },
        coworking_partner: { array: [], tag: "COWORKING PARTNER" },
        award_partner: { array: [], tag: "AWARD PARTNERS" },
        pr_partner: { array: [], tag: "PR PARTNER" },
        mobile_application_partner: {
          array: [],
          tag: "MOBILE APPLICATION PARTNER"
        },
        other: { array: [], tag: "Other" }
      },
      mainPartners: []
    }
  }
  componentDidMount() {
    if (!this.props.homePageView)
      $(window).scrollTop($(".partnerships--wrap").offset().top - 100)

    let partners = this.state.partners

    this.props.fetchPartners().then(res => {
      res.payload.data.collections.forEach(item => {
        if (item.status) {
          if (item.main_partner) {
            partners.main_partner.array.push(item)
          }
          if (item.partner) {
            partners.partner.array.push(item)
          }
          if (item.content_partner) {
            partners.content_partner.array.push(item)
          }
          if (item.hipo_partner) {
            partners.hipo_partner.array.push(item)
          }
          if (item.launchpad_cohost) {
            partners.launchpad_cohost.array.push(item)
          }
          if (item.media_partner) {
            partners.media_partner.array.push(item)
          }
          if (item.session_host) {
            partners.session_host.array.push(item)
          }
          if (item.sattelite_host) {
            partners.sattelite_host.array.push(item)
          }
          if (item.outreach_partner) {
            partners.outreach_partner.array.push(item)
          }
          if (item.community_partner) {
            partners.community_partner.array.push(item)
          }
          if (item.fandb_partner) {
            partners.fandb_partner.array.push(item)
          }
          if (item.track_partner) {
            partners.track_partner.array.push(item)
          }
          if (item.coworking_partner) {
            partners.coworking_partner.array.push(item)
          }
          if (item.award_partner) {
            partners.award_partner.array.push(item)
          }
          if (item.pr_partner) {
            partners.pr_partner.array.push(item)
          }
          if (item.mobile_application_partner) {
            partners.mobile_application_partner.array.push(item)
          }
        }
      })

      this.setState({ partners })
    })
  }

  openModal(id) {
    this.setState({ modalIsOpen: true, id })
  }

  afterOpenModal(id) {
    // references are now sync'd and can be accessed.
    this.props.fetchPartner(this.state.id)
  }
  closeModal() {
    this.setState({ modalIsOpen: false })
  }
  RenderPartnersList(data, key) {
    return key.map((tag, i) => {
      if (data[tag]) {
        if (data[tag].array.length)
          return (
            <PartnerSection
              key={i}
              data={data[tag].array}
              tag={data[tag].tag}
              onClick={id => this.openModal(id)}
            />
          )
        else return null
      } else return null
    })
  }
  render() {
    const _self = this
    function RenderCorpModalContent() {
      if (_self.props.partner) {
        return (
          <CorpModal
            close={() => _self.setState({ modalIsOpen: false })}
            partnerData={_self.props.partner}
          />
        )
      } else {
        return null
      }
    }

    return (
      <div className="partnerships--wrap">
        {!this.props.homePageView ? (
          <React.Fragment>
            <DocumentTitle title="RiseUp Summit - Partners" />
            <HeaderSlider slider={false} />

            <div className="partners--wrapper">
              <p className="slog">
                RiseUp started when a few people came together, now hundreds of
                entities make the magic happen
              </p>
              <div>
                {this.RenderPartnersList(
                  this.state.partners,
                  PartnersSectionKeys
                )}
              </div>
            </div>
            <FirstBeleivers />
          </React.Fragment>
        ) : (
          <React.Fragment>
            <div className="partners--wrapper">
              <p className="slog">
                RiseUp started when a few people came together, now hundreds of
                entities make the magic happen
              </p>
              <div>
                {this.RenderPartnersList(
                  this.state.partners,
                  HomePartnersSectionKeys
                )}
              </div>
            </div>
            <FirstBeleivers />
          </React.Fragment>
        )}

        <Modal
          isOpen={this.state.modalIsOpen}
          onAfterOpen={this.afterOpenModal.bind(this)}
          onRequestClose={this.closeModal.bind(this)}
          contentLabel=""
          className={"PartnersModal"}
        >
          {RenderCorpModalContent()}
        </Modal>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    partners: state.Partners.partners,
    partner: state.Partner.partner
  }
}

export default connect(
  mapStateToProps,
  { fetchPartners, fetchPartner }
)(Partnerships)
