import React from 'react';

const FirstBeleivers = () => {
  return (
    <div className="First--Bellivers clearfix">
      <div className="col-md-4">
        <div className="left-side">
          <h3>OUR FIRST BELIEVERS</h3>
          <p>THE ONES WHO BELIEVED IN OUR VISION, BUILT WITH US AND SAW IT COME TO LIFE</p>
        </div>
      </div>
      <div className="col-md-8">
        <ul>
          <li><img src="http://2016.riseupsummit.com/wp-content/uploads/2017/11/Greek.png" alt=""/></li>
          <li><img src="http://2016.riseupsummit.com/wp-content/uploads/2017/12/Wamda-logo-color-01.png" alt=""/></li>
          <li><img src="http://2016.riseupsummit.com/wp-content/uploads/2017/11/injaz-1.png" alt=""/></li>
          <li><img src="http://2016.riseupsummit.com/wp-content/uploads/2017/12/Endeavor-Egypt-hires.png" alt=""/></li>
          <li><img src="http://2016.riseupsummit.com/wp-content/uploads/2017/12/AUC-logo.png" alt=""/></li>
          <li><img src="http://2016.riseupsummit.com/wp-content/uploads/2017/11/flatlab.png" alt=""/></li>
        </ul>
      </div>
    </div>
  )
}
export default FirstBeleivers