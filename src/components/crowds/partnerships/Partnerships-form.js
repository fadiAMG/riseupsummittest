import React from 'react'
import PropTypes from 'prop-types'
import DocumentTitle from 'react-document-title'
import Banner from '../../commons/banner'
import $ from 'jquery'

class  PartnershipsForm extends React.Component{
  componentDidMount () {
    $("html, body").animate({scrollTop: 0})
  }
  
  render(){
    const id = this.props.match.params.id; 
    const name = this.props.match.params.name.split('_').join(' ').toUpperCase(); 
  	return(
  		<div className="apply-to-speak">
        <DocumentTitle title='RiseUp Summit - Partnerships'></DocumentTitle>
  			<Banner
          title={name + ' FORM'}
          opacity={.5}
          width={85}
          bgurl={require('../../../assets/images/partners1.jpg')}
          strength={200}
        />
        <div className="fullwidth--seprator"></div>
        <div className="form--wrap">
          <iframe id="typeform-full" width="100%" height="400px" frameBorder="0" src={`https://riseupsummit2017.typeform.com/to/${id}`}></iframe>
        </div>
  		</div>
  	)
  }
}


export default PartnershipsForm
