import React from "react"
import DocumentTitle from "react-document-title"
import { connect } from "react-redux"
import FeaturePost from "../../commons/FeaturePost"
import { fetchStartups } from "../../../redux/actions/startups/StartupsActions"
import HeaderSlider from "../../commons/HeaderSlider"
import QuotesCard from "../../commons/QuotesCard"
import $ from "jquery"

// Data
import { strings } from "../../strings"

class StartupStation extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      active: 0,
      modalIsOpen: false,
      startup: {}
    }
  }
  componentDidMount() {
    $(window).scrollTop($(".feature--post").offset().top - 100)
    this.props.fetchStartups()
  }

  openModal(id) {
    this.setState({ modalIsOpen: true, id })
  }

  afterOpenModal() {
    this.props.startups.map(item => {
      if (item.status && item._id === this.state.id) {
        this.setState({ startup: item })
      }
      return item
    })
  }
  closeModal() {
    this.setState({ modalIsOpen: false })
  }
  render() {
    return (
      <div className="about--wrap">
        <DocumentTitle title="RiseUp Summit - Startup Station" />
        <div>
          <DocumentTitle title="RiseUp Summit - Startups" />
          <HeaderSlider slider={false} />
          <FeaturePost
            title={strings.StartupStation.feature.title}
            details={strings.StartupStation.feature.details}
            img={require("../../../assets/images/STARTUP_STATION_HEADER-min.png")}
            ImgMaxHeight={"550px"}
            BtnTitle="APPLY NOW"
            Url="https://riseupco.typeform.com/to/m59t7V"
            strength={300}
            products={strings.StartupStation.feature.products}
            slog="THE ULTIMATE SUMMIT EXPERIENCE"
            SelfLink={true}
            openTextBlue="APPLICATION IS NOW CLOSED"
            noLink={true}
            seprator={false}
            exist_list={true}
            style={{
              slogFontSize: "18px",
              titleFontSize: "27px",
              detailsFontSize: "17px",
              slogColor: "#2a428c"
            }}
          />

          <QuotesCard
            quote={
              '"I MET MY INVESTOR AT RISEUP16 AND FINALIZED THE DEAL RIGHT AFTER."'
            }
            quoteBy={"MOHAMED EZZAT, CEO OF BOSTA"}
            BackgroundColor={"#2a428c"}
          />
        </div>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    startups: state.startups.startups
  }
}

export default connect(
  mapStateToProps,
  { fetchStartups }
)(StartupStation)
