import React from 'react'
import DocumentTitle from 'react-document-title'
import BannerV2 from '../../commons/bannerv2'
import FeaturePost from '../../commons/FeaturePost'

import {Link} from 'react-router-dom'

// Data
import {strings} from '../../strings'

class Attendees extends React.Component{
  componentDidMount() {
    //  $('.feature--post .right--side .link').click(function(){
    //   $("html, body").animate({ scrollTop: 0 })
    // })
  }
  render(){
    let data = strings.Attendees;
    return(
      <div>
          <DocumentTitle title='RiseUp Summit - Attendees'></DocumentTitle>
          <BannerV2
            title={data.banner.title}
            opacity={data.banner.bgOpacity}
            width={data.banner.width}
            strength={100}
            yellow={true}
          />
          <FeaturePost 
            title   = {data.feature.title}
            details="At the Summit, we gather all the relevant stakeholders in our region’s ecosystem. Do you need customers for your product? Developers for your App? Partners for your startup? Suppliers for your product? Or are you just looking to immerse yourself in the wisdom of industry gurus and critically acclaimed entrepreneurs? Either way, join the crowd."
            img     = {require('../../../assets/images/v2/attendees.png')}
            BtnTitle = "GET YOUR TICKET"
            Url="/tickets"
            strength={300}
            noLink={true}
            slog="BE PART OF OUR STORY…"
          />       
      </div>
    );
  }
}
export default Attendees;
