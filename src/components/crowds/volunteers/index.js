import React from "react"
import DocumentTitle from "react-document-title"
import FeaturePost from "../../commons/FeaturePost"
import HeaderSlider from "../../commons/HeaderSlider"
import $ from 'jquery'

// Data
import { strings } from "../../strings"

class Volunteers extends React.Component {
  componentDidMount() {
    $(window).scrollTop($(".feature--post").offset().top - 100)
  }
  render() {
    let data = strings.Volunteers
    return (
      <div>
        <DocumentTitle title="RiseUp Summit - Volunteers" />
        <HeaderSlider slider={false} />
        <FeaturePost
          title={data.feature.title}
          details={data.feature.details}
          img={require("../../../assets/images/v2/Volunteers.png")}
          BtnTitle="APPLY NOW"
          Url={"/volunteers/form"}
          strength={300}
          slog="BE PART OF OUR STORY…"
          gray={false}
          // openText="Applications are now closed"
          openTextBlue="Applications are now closed"
          noLink={true}
          ImgMaxHeight={"650px"}
          style={{
            slogFontSize: "18px",
            titleFontSize: "27px",
            detailsFontSize: "17px",
            // slogFontWeight: "400",
            slogColor: "#2a428c"
          }}
          SelfLink={true}
          seprator={false}
        />
      </div>
    )
  }
}

export default Volunteers
