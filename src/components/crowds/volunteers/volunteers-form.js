import React from "react"
import DocumentTitle from "react-document-title"
import Banner from "../../commons/banner"

class VolunteersForm extends React.Component {
  render() {
    return (
      <div className="apply-to-speak">
        <DocumentTitle title="RiseUp Summit - Partnerships" />
        <Banner
          title="VOLUNTEERS FORM"
          opacity={0.5}
          width={85}
          bgurl={require("../../../assets/images/volunteers.jpg")}
          strength={200}
        />
        <div className="fullwidth--seprator" />
        <div className="form--wrap">
          <iframe
            title="VolunteersForm"
            id="typeform-full"
            width="100%"
            height="400px"
            frameBorder="0"
            src={`https://riseupsummit2017.typeform.com/to/EeIa0f`}
          />
        </div>
      </div>
    )
  }
}

export default VolunteersForm
