import React from "react"
import DocumentTitle from "react-document-title"
import HeaderSlider from "../commons/HeaderSlider"

import { data } from "../data"

class Summit101 extends React.Component {
  
  
  render() {
    function renderFaqs() {
      return data.faqs.map((item, i) => {
        return (
          <div key={i} className="faq--item">
            <h5>{item.title}</h5>
            <p>{item.details}</p>
          </div>
        )
      })
    }
    return (
      <div>
        <DocumentTitle title="RiseUp Summit - Summit101" />
        <HeaderSlider slider={false} />

        <div style={{ backgroundColor: "rgba(241, 241, 241, 1.286)" }}>
          <div className="container">
            <div className="faqs--wrap">
              {/* <iframe 
								src="https://xd.adobe.com/view/9a5d356a-acd0-4a93-9288-ff26d7edd855/?fullscreen" 
								frameborder="0"
								width="100%"
								height="600"></iframe> */}
              <div className="clearfix">
                <h4 className="faq--title ">FREQUENTLY ASKED QUESTIONS</h4>
              </div>
              <div className="faqs--list">{renderFaqs()}</div>
            </div>

            <div className="apps--download">
              {/*
								<h2>DOWNLOAD OUR MOBILE APP</h2>
								<div className="buttons--wrap">
								<a href="#"><i className="fa fa-apple"></i> <span>Download Iphone App</span></a>
								<a href="#"><i className="fa fa-android"></i> <span>Download Iphone App</span></a>
							</div> */}
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default Summit101
