
export const data = {
  MenaHipos: [
    {
      id: 1,
      name: "FLAT 6 LABS",
      details:
        "Farida received her bachelor’s degree in international development studies and anthropology from McGill University in 2013.",
      logo:
        "http://static4.businessinsider.com/image/56b10e64c08a80431d8bc158-326-163/uber_logobit_digital_white.png"
    }
  ],
  partners: [
    {
      id: 1,
      name: "uber",
      details:
        "Farida received her bachelor’s degree in international development studies and anthropology from McGill University in 2013 and her master’s degree in international education policy from Harvard Graduate School of Education. Farida received her bachelor’s degree in international development studies and anthropology from McGill University in 2013 and her.",
      logo:
        "http://static4.businessinsider.com/image/56b10e64c08a80431d8bc158-326-163/uber_logobit_digital_white.png"
    },
    {
      id: 2,
      name: "wamda",
      details:
        "Farida received her bachelor’s degree in international development studies and anthropology from McGill University in 2013 and her master’s degree in international education policy from Harvard Graduate School of Education. Farida received her bachelor’s degree in international development studies and anthropology from McGill University in 2013 and her.",
      logo: "https://techwadi.org/wp-content/uploads/2015/09/Wamda.jpg"
    },
    {
      id: 3,
      name: "Flat 6 Labs",
      details:
        "Farida received her bachelor’s degree in international development studies and anthropology from McGill University in 2013 and her master’s degree in international education policy from Harvard Graduate School of Education. Farida received her bachelor’s degree in international development studies and anthropology from McGill University in 2013 and her.",
      logo:
        "http://www.flat6labs.com/wp-content/themes/twentyeleven/img/auh_logo.png"
    }
  ],
  Media: [
    {
      id: 1,
      name: "wamda",
      logo:
        "https://riseupsummit.com/wp-content/uploads/2017/08/wamda-logo-01.png"
    },
    {
      logo:
        "https://riseupsummit.com/wp-content/uploads/2017/08/Forbes_logo-bluepng.png"
    },
    {
      logo:
        "https://riseupsummit.com/wp-content/uploads/2017/08/tc-techcrunch.png"
    },
    {
      logo:
        "https://riseupsummit.com/wp-content/uploads/2017/08/Entrepreneur-Middle-East-logo.png"
    },
    {
      logo:
        "https://riseupsummit.com/wp-content/uploads/2017/08/DA-logo-on-transparent-with-t.png"
    },
    {
      logo:
        "https://riseupsummit.com/wp-content/uploads/2017/08/15171329_224624691281702_1417.png"
    }
  ],
  faqs: [
    {
      id: 2,
      title: "How can I get a letter of invitation for a visa?",
      details:
        "Send an email to hello@riseup.co to ask for the letter. We require you to have paid for your tickets before we send you an invitation."
    },
    {
      id: 3,
      title: "When and where is the event taking place?",
      details:
        "RiseUp Summit is located in the heart of Downtown Cairo, Egypt. We takeover the GrEEK Campus and The American University in Cairo at Tahrir Square, Downtown Cairo. #RiseUp18 will take place on December 07,08,09."
    },
    {
      id: 4,
      title: "How can I join?",
      details:
        "You can join our crowd by becoming a speaker, partner, startup, investor, media, volunteer, or get your attendee ticket here!"
    },
    {
      id: 5,
      title: "Are there any group discounts for tickets?",
      details:
        "Yes, please check the ‘group package’ and our ‘student tickets’ on our tickets page. Students will be asked to upload their valid student ID."
    },
    {
      id: 6,
      title: "What language will the program be in?",
      details:
        "Our Summit is international with the official language of the program being English. Any session not in English will be tagged."
    },
    {
      id: 8,
      title: "How can I connect with other people at the Summit?",
      details:
        "The Summit App will make it easy for you to see who’s coming, start conversations, and tag all the sessions you’re interested in."
    },
    {
      id: 1,
      title: "What is RiseUp Summit?",
      details:
        "The summit is our three-day, one-stop-shop, entrepreneurship marathon! Over the years, we promised that the world would come to Downtown Cairo, and thanks to an incredible level of commitment from our tireless team and phenomenal partners, we deliver. Thought leaders, media mavens, tech-legends and power player investors gather to share their experience, support and invest in the Middle East & Africa’s flourishing entrepreneurial ecosystem. From workshops, inspiring talks, and panel discussions to startup stations, pitch competitions, and networking platforms with industry experts. Whoever you are, you’ll find the place for you."
    }
  ],
  Team: [
    {
      id: 1,
      name: "Mohamed Hany",
      title: "Web Developer"
    }
  ],
  SatelliteEventsList: [
    {
      id: 1,
      title: "MEET THE TALENT",
      details: "Meet and hire your company’s next leaders.",
      date: "DATE TO BE ANNOUNCED",
      image: "https://riseupsummit.com/wp-content/uploads/2017/08/1.png"
    },
    {
      id: 2,
      title: "CREATIVE PITCH COMPETITION",
      details:
        "Have a creative idea in the realms of design, fashion, music, art? We’ve made a special competition for you! ",
      date: "DATE TO BE ANNOUNCED",
      image: "https://riseupsummit.com/wp-content/uploads/2017/08/2.png"
    },
    {
      id: 3,
      title: "DOWNTOWN CAIRO BREAKFAST",
      details:
        "A little Egyptian fuul to kick-off the Summit on Friday morning! ",
      date: "DATE TO BE ANNOUNCED",
      image: "https://riseupsummit.com/wp-content/uploads/2017/08/3.png"
    },
    {
      id: 4,
      title: "NETWORKING EVENTS",
      details: "Know more…",
      date: "DATE TO BE ANNOUNCED",
      image: "https://riseupsummit.com/wp-content/uploads/2017/08/4.png"
    }
  ],
  investorsList: [
    {
      _id: "598e00dd98246bee05b59e12",
      name: "MATT SPENCE",
      job_title: "Partner at Andreessen Horowitz",
      img: "https://riseupsummit.com/wp-content/uploads/2017/08/mat.png"
    },
    {
      _id: "598e00dd98246besdsd5b59e12",
      name: "Elissa Freiha",
      job_title: "Director at WOMENA ",
      img: "https://riseupsummit.com/wp-content/uploads/2017/08/elis.png"
    },
    {
      _id: "598e00dd98246bes1212sd5b59e12",
      name: "Sharif El-Badawi",
      job_title: "Partner at 500 Startups",
      img: "https://riseupsummit.com/wp-content/uploads/2017/08/sherif.png"
    },
    {
      _id: "598e00dd98246bes1212sd5b59e12",
      name: "Fadi Ghandour",
      job_title: " Managing Partner at Wamda Capital",
      img: "https://riseupsummit.com/wp-content/uploads/2017/09/fadi.png"
    }
  ],
  Activities: [
    {
      id: 1,
      title: "Investors & HIPOs Dinner",
      details:
        "A dinner where MENA’s top high potential startups get together with the investors of the season.",
      date: "DATE TO BE ANNOUNCED",
      image: "InvestorsandHIPOsdinner.jpeg"
    },
    {
      id: 2,
      title: "Speakers & Partners Dinner",
      details:
        "Invite-only dinner in one of Cairo’s hot-spots for speakers and our partners where all the experts of the industries come together.        ",
      date: "DATE TO BE ANNOUNCED",
      image: "speakersandpartnersDinner.jpeg"
    }
    // {
    //   id: 3,
    //   title: "SPEAKERS AND EXPERTS DINNER",
    //   details:
    //     "Invite-only dinner in one of Cairo’s hot-spots for speakers, main partners, and industry experts.",
    //   date: "DATE TO BE ANNOUNCED",
    //   image: "LAUNCHPADHEADER-min.png"
    // },
    // {
    //   id: 4,
    //   title: "ECOSYSTEM FELUCCA RIDE",
    //   details:
    //     "You can’t come to Egypt and not ride a Felucca down the Nile River! We’re hosting an invite-only mixer for representatives from global startup ecosystems. ",
    //   date: "DATE TO BE ANNOUNCED",
    //   image: "https://riseupsummit.com/wp-content/uploads/2017/08/4.png"
    // }
  ],
  PartnershipsList: [
    {
      imgUrl: "https://2016.riseupsummit.com/wp-content/uploads/2017/09/1.png",
      title: "NETWORKING FURNITURE STARTUPS",
      details:
        "RiseUp Summit is the place to network, and connect. Make those spaces happen.",
      pdfLink:
        "https://2016.riseupsummit.com/wp-content/uploads/2017/10/Networking-Chill-Areas-Furniture-Startups-Partnership-Proposal-2.pdf",
      formId: "KO1v94"
    },
    {
      imgUrl: "https://2016.riseupsummit.com/wp-content/uploads/2017/09/2.png",
      title: "AUDIENCE FURNITURE STARTUPS",
      details:
        "Each stage at the Summit requires a seating area for the audience. We need that seating area.",
      pdfLink:
        "https://2016.riseupsummit.com/wp-content/uploads/2017/10/Audience-Furniture-Startups-Partnership-Proposal-2.pdf",
      formId: "WS4E2R"
    },
    {
      imgUrl: "https://2016.riseupsummit.com/wp-content/uploads/2017/09/3.png",
      title: "STAGE FURNITURE STARTUPS",
      details:
        "Stage Setup plays a huge role in setting the overall look and feel of the Summit.",
      pdfLink:
        "https://2016.riseupsummit.com/wp-content/uploads/2017/10/Stage-Furniture-Startups-Partnership-Proposal-2.pdf",
      formId: "O5Dpnp"
    },
    // {
    //   imgUrl:'https://2016.riseupsummit.com/wp-content/uploads/2017/09/4.png',
    //   title:'AUGMENTED REALITY',
    //   slog:'CONNECT SUPERMARKET',
    //   details:'Augmented reality supermarket for RiseUp Connect at the Summit.',
    //   pdfLink:'https://2016.riseupsummit.com/wp-content/uploads/2017/10/Augmented-Reality-Connect-Supermarket-Partnership-Proposal-2.pdf',
    //   formId:'ZEoFKe'
    // },
    {
      imgUrl: "https://2016.riseupsummit.com/wp-content/uploads/2017/09/5.png",
      title: "FOOD AND BEVERAGE",
      details: "Feed our attendees!",
      pdfLink:
        "https://2016.riseupsummit.com/wp-content/uploads/2017/10/Food-Beverage-Startups-Partnership-Proposal-2.pdf",
      formId: "KAZoBt"
    },
    {
      imgUrl: "https://2016.riseupsummit.com/wp-content/uploads/2017/09/6.png",
      title: "TRAVEL AND LEISURE",
      details: "Help our attendees discover Egypt!",
      pdfLink:
        "https://2016.riseupsummit.com/wp-content/uploads/2017/10/Travel-Leisure-Startups-Discover-Egypt-Program-Partnership-Proposal-2.pdf",
      formId: "idMATL"
    },
    {
      imgUrl: "https://2016.riseupsummit.com/wp-content/uploads/2017/09/7.png",
      title: "COMMUNITY PARTNERS",
      details: "You are the ambassador of RiseUp in your community.",
      pdfLink:
        "https://2016.riseupsummit.com/wp-content/uploads/2017/10/Community-Partners-Partnership-Proposal-3.pdf",
      formId: "RHYwpV"
    },
    {
      imgUrl: "https://2016.riseupsummit.com/wp-content/uploads/2017/09/8.png",
      title: "COWORKING SPACES",
      details:
        "Co-working spaces for people to work during the three days of the summit and meetings spaces for one on ones.",
      pdfLink:
        "https://2016.riseupsummit.com/wp-content/uploads/2017/10/Co-working-Space-Partnership-Proposal-4.pdf",
      formId: "u93Yik"
    },
    {
      imgUrl:
        "https://2016.riseupsummit.com/wp-content/uploads/2017/10/connect.jpg",
      title: "RiseUp Connect Perks Providers",
      details:
        "Offer perks to the community to get exposure, acquire new customers and test new features.",
      pdfLink:
        "https://2016.riseupsummit.com/wp-content/uploads/2017/10/RiseUp-Connect-Perks-Providers-Partnership-Proposal.pdf",
      formId: "NeBMDb"
    }
  ]
}

// Investors Data
export const EntityType = [
  { value: "Angel Investor", label: "Angel Investor" },
  { value: "Angel Investment Network", label: "Angel Investment Network" },
  { value: "Accelerator", label: "Accelerator" },
  { value: "Incubator", label: "Incubator" },
  { value: "Crowdfunding Platform", label: "Crowdfunding Platform" },
  { value: "Competition", label: "Competition" },
  { value: "VC", label: "VC" },
  { value: "Trade Finance", label: "Trade Finance" },
  { value: "Investment Bank", label: "Investment Bank" },
  { value: "Private Equity", label: "Private Equity" },
  { value: "Corporate VC", label: "Corporate VC" },
  { value: "Institutional Investor", label: "Institutional Investor" },
  { value: "Investment Network", label: "Investment Network" }
]
export const country_list = [
  "Afghanistan",
  "Albania",
  "Algeria",
  "Andorra",
  "Angola",
  "Anguilla",
  "Antigua and Barbuda",
  "Argentina",
  "Aruba",
  "Austria",
  "Bahamas",
  "Bahrain",
  "Bangladesh",
  "Barbados",
  "Belgium",
  "Belize",
  "Benin",
  "Bermuda",
  "Bhutan",
  "Bolivia",
  "Bonaire Saint Eustatius and Saba",
  "Bosnia & Herzegovina",
  "Botswana",
  "Brazil",
  "British Virgin Islands",
  "Brunei",
  "Bulgaria",
  "Burkina Faso",
  "Burma",
  "Burundi",
  "Cambodia",
  "Cameroon",
  "Canada",
  "Cape Verde",
  "Cayman Islands",
  "Central African Republic",
  "Chad",
  "Chile",
  "China",
  "Colombia",
  "Comoros",
  "Congo",
  "Costa Rica",
  "Côte d'Ivoire",
  "Croatia",
  "Cuba",
  "Curaçao",
  "Cyprus",
  "Czech Republic",
  "Democratic Republic of the Congo",
  "Denmark",
  "Dominica",
  "Dominican Republic",
  "East Timor",
  "Ecuador",
  "Egypt",
  "El Salvador",
  "Equatorial Guinea",
  "Eritrea",
  "Ethiopia",
  "Falkland Islands (Malvinas)",
  "Faroe Islands",
  "Finland",
  "France",
  "French",
  "Guiana",
  "Gabon",
  "Gambia",
  "Gaza Strip",
  "Germany",
  "Ghana",
  "Gibraltar",
  "Greece",
  "Greenland",
  "Grenada",
  "Guadeloupe",
  "Guatemala",
  "Guernsey",
  "Guinea",
  "Guinea-Bissau",
  "Guyana",
  "Haiti",
  "Honduras",
  "Hong Kong",
  "Hungary",
  "Iceland",
  "India",
  "Indonesia",
  "Iran",
  "Iraq",
  "Ireland",
  "Isle of Man",
  "Israel",
  "Italy",
  "Jamaica",
  "Japan",
  "Jersey",
  "Jordan",
  "Kenya",
  "Korea",
  "North",
  "Korea",
  "South",
  "Kuwait",
  "Laos",
  "Lebanon",
  "Lesotho",
  "Liberia",
  "Libya",
  "Liechtenstein",
  "Luxembourg",
  "Macau",
  "Macedonia",
  "Madagascar",
  "Malawi",
  "Malaysia",
  "Maldives",
  "Mali",
  "Malta",
  "Martinique",
  "Mauritania",
  "Mauritius",
  "Mexico",
  "Monaco",
  "Mongolia",
  "Monserrat",
  "Morocco",
  "Mozambique",
  "Namibia",
  "Nepal",
  "Netherlands",
  "Nicaragua",
  "Niger",
  "Nigeria",
  "Norway",
  "Oman",
  "Pakistan",
  "Panama",
  "Paraguay",
  "Peru",
  "Philippines",
  "Poland",
  "Portugal",
  "Puerto Rico",
  "Qatar",
  "Romania",
  "Rwanda",
  "Saint Lucia",
  "Saint Martin",
  "Saint Pierre and Miquelon",
  "Saint Vincent and the Grenadines",
  "Saint-Barthélemy",
  "San Marino",
  "Sao Tome and Principe",
  "Saudi Arabia",
  "Senegal",
  "Serbia",
  "Seychelles",
  "Sierra Leone",
  "Singapore",
  "Sint Maarten",
  "Slovakia",
  "Slovenia",
  "Somalia",
  "South Africa",
  "Spain",
  "Sri Lanka",
  "St. Kitts and Nevis",
  "Suriname",
  "Swaziland",
  "Sweden",
  "Switzerland",
  "Syria",
  "Taiwan",
  "Thailand",
  "Togo",
  "Trinidad and Tobago",
  "Tunisia",
  "Turkey",
  "Turks and Caicos Islands",
  "Uganda",
  "United Arab Emirates",
  "United Kingdom",
  "United Republic of Tanzania",
  "United States (USA)",
  "Uruguay",
  "Venezuela",
  "Vietnam",
  "Virgin Islands (US)",
  "West Bank",
  "Yemen",
  "Zambia",
  "Zimbabwe"
]
export const region_country_list = [
  "ALL",
  "Emerging Markets",
  "North Africa-all",
  "Middle East-all",
  "Gulf-all",
  "Sub-Saharan Africa-all",
  "Europe-all",
  "Asia-all",
  "North America-all",
  "South America-all",
  "Central America-all",
  "The Caribbean-all",
  "Afghanistan",
  "Albania",
  "Algeria",
  "Andorra",
  "Angola",
  "Anguilla",
  "Antigua and Barbuda",
  "Argentina",
  "Aruba",
  "Austria",
  "Bahamas",
  "Bahrain",
  "Bangladesh",
  "Barbados",
  "Belgium",
  "Belize",
  "Benin",
  "Bermuda",
  "Bhutan",
  "Bolivia",
  "Bonaire Saint Eustatius and Saba",
  "Bosnia & Herzegovina",
  "Botswana",
  "Brazil",
  "British Virgin Islands",
  "Brunei",
  "Bulgaria",
  "Burkina Faso",
  "Burma",
  "Burundi",
  "Cambodia",
  "Cameroon",
  "Canada",
  "Cape Verde",
  "Cayman Islands",
  "Central African Republic",
  "Chad",
  "Chile",
  "China",
  "Colombia",
  "Comoros",
  "Congo",
  "Costa Rica",
  "Côte d'Ivoire",
  "Croatia",
  "Cuba",
  "Curaçao",
  "Cyprus",
  "Czech Republic",
  "Democratic Republic of the Congo",
  "Denmark",
  "Dominica",
  "Dominican Republic",
  "East Timor",
  "Ecuador",
  "Egypt",
  "El Salvador",
  "Equatorial Guinea",
  "Eritrea",
  "Ethiopia",
  "Falkland Islands (Malvinas)",
  "Faroe Islands",
  "Finland",
  "France",
  "French",
  "Guiana",
  "Gabon",
  "Gambia",
  "Gaza Strip",
  "Germany",
  "Ghana",
  "Gibraltar",
  "Greece",
  "Greenland",
  "Grenada",
  "Guadeloupe",
  "Guatemala",
  "Guernsey",
  "Guinea",
  "Guinea-Bissau",
  "Guyana",
  "Haiti",
  "Honduras",
  "Hong Kong",
  "Hungary",
  "Iceland",
  "India",
  "Indonesia",
  "Iran",
  "Iraq",
  "Ireland",
  "Isle of Man",
  "Israel",
  "Italy",
  "Jamaica",
  "Japan",
  "Jersey",
  "Jordan",
  "Kenya",
  "Korea",
  "North",
  "Korea",
  "South",
  "Kuwait",
  "Laos",
  "Lebanon",
  "Lesotho",
  "Liberia",
  "Libya",
  "Liechtenstein",
  "Luxembourg",
  "Macau",
  "Macedonia",
  "Madagascar",
  "Malawi",
  "Malaysia",
  "Maldives",
  "Mali",
  "Malta",
  "Martinique",
  "Mauritania",
  "Mauritius",
  "Mexico",
  "Monaco",
  "Mongolia",
  "Monserrat",
  "Morocco",
  "Mozambique",
  "Namibia",
  "Nepal",
  "Netherlands",
  "Nicaragua",
  "Niger",
  "Nigeria",
  "Norway",
  "Oman",
  "Pakistan",
  "Panama",
  "Paraguay",
  "Peru",
  "Philippines",
  "Poland",
  "Portugal",
  "Puerto Rico",
  "Qatar",
  "Romania",
  "Rwanda",
  "Saint Lucia",
  "Saint Martin",
  "Saint Pierre and Miquelon",
  "Saint Vincent and the Grenadines",
  "Saint-Barthélemy",
  "San Marino",
  "Sao Tome and Principe",
  "Saudi Arabia",
  "Senegal",
  "Serbia",
  "Seychelles",
  "Sierra Leone",
  "Singapore",
  "Sint Maarten",
  "Slovakia",
  "Slovenia",
  "Somalia",
  "South Africa",
  "Spain",
  "Sri Lanka",
  "St. Kitts and Nevis",
  "Suriname",
  "Swaziland",
  "Sweden",
  "Switzerland",
  "Syria",
  "Taiwan",
  "Thailand",
  "Togo",
  "Trinidad and Tobago",
  "Tunisia",
  "Turkey",
  "Turks and Caicos Islands",
  "Uganda",
  "United Arab Emirates",
  "United Kingdom",
  "United Republic of Tanzania",
  "United States (USA)",
  "Uruguay",
  "Venezuela",
  "Vietnam",
  "Virgin Islands (US)",
  "West Bank",
  "Yemen",
  "Zambia",
  "Zimbabwe"
]

// export const country_list = ["Afghanistan","Albania","Algeria","Andorra","Angola","Anguilla","Antigua &amp; Barbuda","Argentina","Armenia","Aruba","Australia","Austria","Azerbaijan","Bahamas","Bahrain","Bangladesh","Barbados","Belarus","Belgium","Belize","Benin","Bermuda","Bhutan","Bolivia","Bosnia &amp; Herzegovina","Botswana","Brazil","British Virgin Islands","Brunei","Bulgaria","Burkina Faso","Burundi","Cambodia","Cameroon","Cape Verde","Cayman Islands","Chad","Chile","China","Colombia","Congo","Cook Islands","Costa Rica","Cote D Ivoire","Croatia","Cruise Ship","Cuba","Cyprus","Czech Republic","Denmark","Djibouti","Dominica","Dominican Republic","Ecuador","Egypt","El Salvador","Equatorial Guinea","Estonia","Ethiopia","Falkland Islands","Faroe Islands","Fiji","Finland","France","French Polynesia","French West Indies","Gabon","Gambia","Georgia","Germany","Ghana","Gibraltar","Greece","Greenland","Grenada","Guam","Guatemala","Guernsey","Guinea","Guinea Bissau","Guyana","Haiti","Honduras","Hong Kong","Hungary","Iceland","India","Indonesia","Iran","Iraq","Ireland","Isle of Man","Israel","Italy","Jamaica","Japan","Jersey","Jordan","Kazakhstan","Kenya","Kuwait","Kyrgyz Republic","Laos","Latvia","Lebanon","Lesotho","Liberia","Libya","Liechtenstein","Lithuania","Luxembourg","Macau","Macedonia","Madagascar","Malawi","Malaysia","Maldives","Mali","Malta","Mauritania","Mauritius","Mexico","Moldova","Monaco","Mongolia","Montenegro","Montserrat","Morocco","Mozambique","Namibia","Nepal","Netherlands","Netherlands Antilles","New Caledonia","New Zealand","Nicaragua","Niger","Nigeria","Norway","Oman","Pakistan","Palestine","Panama","Papua New Guinea","Paraguay","Peru","Philippines","Poland","Portugal","Puerto Rico","Qatar","Reunion","Romania","Russia","Rwanda","Saint Pierre &amp; Miquelon","Samoa","San Marino","Satellite","Saudi Arabia","Senegal","Serbia","Seychelles","Sierra Leone","Singapore","Slovakia","Slovenia","South Africa","South Korea","Spain","Sri Lanka","St Kitts &amp; Nevis","St Lucia","St Vincent","St. Lucia","Sudan","Suriname","Swaziland","Sweden","Switzerland","Syria","Taiwan","Tajikistan","Tanzania","Thailand","Timor L'Este","Togo","Tonga","Trinidad &amp; Tobago","Tunisia","Turkey","Turkmenistan","Turks &amp; Caicos","Uganda","Ukraine","United Arab Emirates","United Kingdom","Uruguay","Uzbekistan","Venezuela","Vietnam","Virgin Islands (US)","Yemen","Zambia","Zimbabwe"]

export const min_value = [
  "1K",
  "5K",
  "25K",
  "50K",
  "100K",
  "200K",
  "250K",
  "500K",
  "1M",
  "2.5M",
  "5M",
  "10M",
  "25M",
  "50M",
  "100M"
]
export const max_value = [
  "5K",
  "25K",
  "50K",
  "100K",
  "200K",
  "250K",
  "500K",
  "1M",
  "2.5M",
  "5M",
  "10M",
  "25M",
  "50M",
  "100M",
  "No limit"
]
export const services = [
  { value: "Accounting & Auditing", label: "Accounting & Auditing" },
  { value: "Banking services", label: "Banking services" },
  {
    value: "Business Development Services",
    label: "Business Development Services"
  },
  { value: "Business Mentorship", label: "Business Mentorship" },
  { value: "Consulting", label: "Consulting" },
  { value: "Data", label: "Data" },
  { value: "Education", label: "Education" },
  { value: "Events", label: "Events" },
  { value: "Hardware", label: "Hardware" },
  { value: "HR & Recruitment ", label: "HR & Recruitment " },
  { value: "Legal Services", label: "Legal Services" },
  { value: "Media", label: "Media" },
  { value: "Office Services", label: "Office Services" },
  { value: "Office Space", label: "Office Space" },
  {
    value: "People development & Coaching",
    label: "People development & Coaching"
  },
  { value: "Research Facilities", label: "Research Facilities" },
  {
    value: "Social Networks & Community",
    label: "Social Networks & Community"
  },
  { value: "Softlanding Services", label: "Softlanding Services" },
  { value: "Software", label: "Software" },
  { value: "Other", label: "Other" },
  { value: "None", label: "None" }
]

export const CountryRegion = [
  { value: "ALL", label: "ALL" },
  { value: "Emerging Markets", label: "Emerging Markets" },
  { value: "North Africa-all", label: "North Africa-all" },
  { value: "Middle East-all", label: "Middle East-all" },
  { value: "Gulf-all", label: "Gulf-all" },
  { value: "Sub-Saharan Africa-all", label: "Sub-Saharan Africa-all" },
  { value: "Europe-all", label: "Europe-all" },
  { value: "Asia-all", label: "Asia-all" },
  { value: "North America-all", label: "North America-all" },
  { value: "South America-all", label: "South America-all" },
  { value: "Central America-all", label: "Central America-all" },
  { value: "The Caribbean-all", label: "The Caribbean-all" }
]

export const StartupStage = [
  { value: "Early//Idea", label: "Early//Idea" },
  { value: "Seed//MVP", label: "Seed//MVP" },
  { value: "Growth//Takeoff", label: "Growth//Takeoff" },
  { value: "ScaleUp", label: "ScaleUp" },
  { value: "All", label: "All" }
]

export const Startup_Verticals = [
  { value: "3D Printing", label: "3D Printing" },
  { value: "AI & Machine learning", label: "AI & Machine learning" },
  { value: "Analytics", label: "Analytics" },
  { value: "Big Data", label: "Big Data" },
  {
    value: "Blockchain and Cryptocurrencies",
    label: "Blockchain and Cryptocurrencies"
  },
  {
    value: "Business Development Services",
    label: "Business Development Services"
  },
  { value: "Cloud Services", label: "Cloud Services" },
  { value: "Consulting", label: "Consulting" },
  { value: "Commercial Products", label: "Commercial Products" },
  { value: "Commercial Services", label: "Commercial Services" },
  { value: "E-Commerce", label: "E-Commerce" },
  { value: "Events", label: "Events" },
  { value: "Internet of Things", label: "Internet of Things" },
  { value: "Manufacturing", label: "Manufacturing" },
  { value: "Marketplace", label: "Marketplace" },
  { value: "Mobile", label: "Mobile" },
  { value: "News & Media", label: "News & Media" },
  { value: "Nanotechnology", label: "Nanotechnology" },
  { value: "Platform", label: "Platform" },
  { value: "Robotics & Drones", label: "Robotics & Drones" },
  { value: "SaaS", label: "SaaS" },
  { value: "Smart Technologies", label: "Smart Technologies" },
  { value: "Social Enterprise", label: "Social Enterprise" },
  { value: "Software", label: "Software" },
  { value: "Technology ", label: "Technology " },
  { value: "VR & Augmented reality", label: "VR & Augmented reality" },
  { value: "Wearables", label: "Wearables" },
  { value: "Other", label: "Other" },
  { value: "All", label: "All" }
]

export const years = []
for (let i = 1960; i <= 2018; i++) {
  years.push(i)
}

export const startup_Sectors = [
  { value: "Aerospace & Defense", label: "Aerospace & Defense" },
  { value: "Agribusiness", label: "Agribusiness" },
  {
    value: "Business Development Services",
    label: "Business Development Services"
  },
  { value: "Civil Society & Democracy", label: "Civil Society & Democracy" },
  { value: "Commercial Products", label: "Commercial Products" },
  { value: "Commercial Services", label: "Commercial Services" },
  {
    value: "Communication & Connectivity",
    label: "Communication & Connectivity"
  },
  { value: "Data Science", label: "Data Science" },
  {
    value: "Fintech & Financial Services",
    label: "Fintech & Financial Services"
  },
  { value: "Food & Beverage", label: "Food & Beverage" },
  { value: "Hardware", label: "Hardware" },
  { value: "Healthcare", label: "Healthcare" },
  { value: "Hospitality", label: "Hospitality" },
  { value: "HR & Recruitment", label: "HR & Recruitment" },
  { value: "Human Development", label: "Human Development" },
  { value: "IT Services", label: "IT Services" },
  {
    value: "Manufacturing & Engineering ",
    label: "Manufacturing & Engineering "
  },
  { value: "Marketplaces", label: "Marketplaces" },
  { value: "Materials", label: "Materials" },
  { value: "Media", label: "Media" },
  { value: "Real Estate", label: "Real Estate" },
  { value: "Security Services", label: "Security Services" },
  {
    value: "Smart Cities & Urban Development",
    label: "Smart Cities & Urban Development"
  },
  {
    value: "Social Networks & Community",
    label: "Social Networks & Community"
  },
  { value: "Software", label: "Software" },
  { value: "Space", label: "Space" },
  { value: "Sports & Wellness", label: "Sports & Wellness" },
  {
    value: "Systems & Information Management",
    label: "Systems & Information Management"
  },
  { value: "Transportation", label: "Transportation" },
  { value: "Travel & Tourism", label: "Travel & Tourism" },
  { value: "All", label: "All" },
  { value: "Other", label: "Other" }
]

export const MvpProduct = [
  { value: "Idea", label: "Idea" },
  { value: "MVP", label: "MVP" },
  { value: "Product launched", label: "Product launched" },
  {
    value: "More than one product launched ",
    label: "More than one product launched "
  }
]

export const BussinessModal = [
  { value: "B2C", label: "B2C" },
  { value: "B2B", label: "B2B" },
  { value: "B2B2C", label: "B2B2C" },
  { value: "B2G", label: "B2G" },
  { value: "Pltaform", label: "Pltaform" }
]

export const Industries = [
  {
    value: "Fintech & Financial Services",
    label: "Fintech & Financial Services"
  },
  { value: "Food & Beverage", label: "Food & Beverage" },
  { value: "Healthcare", label: "Healthcare" },
  { value: "Commercial Products", label: "Commercial Products" },
  { value: "Commercial Services", label: "Commercial Services" },
  { value: "Cleantech & Energy", label: "Cleantech & Energy" },
  { value: "HR & Recruitment", label: "HR & Recruitment" },
  { value: "Marketplaces", label: "Marketplaces" },
  {
    value: "Social Networks & Community",
    label: "Social Networks & Community"
  },
  { value: "Software", label: "Software" },
  { value: "Technology", label: "Technology" },
  { value: "VR & Augmented reality", label: "VR & Augmented reality" },
  { value: "SaaS", label: "SaaS" },
  { value: "Platform", label: "Platform" },
  { value: "Internet of Things", label: "Internet of Things" },
  { value: "AI & Machine learning", label: "AI & Machine learning" },
  {
    value: "Business Development Services",
    label: "Business Development Services"
  },
  { value: "Transportation", label: "Transportation" },
  { value: "Creative Industries", label: "Creative Industries" },
  { value: "Travel & Tourism", label: "Travel & Tourism" },
  { value: "E-Commerce", label: "E-Commerce" },
  { value: "Entertainment", label: "Entertainment" },
  { value: "Creative Writing & Content", label: "Creative Writing & Content" },
  { value: "Delivery & logistics", label: "Delivery & logistics" },
  { value: "Education", label: "Education" },
  { value: "Home care", label: "Home care" },
  { value: "HR & Recruitment", label: "HR & Recruitment" },
  { value: "Film", label: "Film" },
  { value: "Marketing & Advertising", label: "Marketing & Advertising" },
  { value: "Social Enterprise", label: "Social Enterprise" },
  { value: "Sports & Wellness", label: "Sports & Wellness" },
  {
    value: "Construction & Architecture",
    label: "Construction & Architecture"
  },
  { value: "Agribusiness", label: "Agribusiness" },
  { value: "Gaming", label: "Gaming" },
  { value: "Telecom", label: "Telecom" },
  {
    value: "Data analytics and Business Intelligence",
    label: "Data analytics and Business Intelligence"
  },
  { value: "Social Enterprise", label: "Social Enterprise" },
  { value: "Engineering", label: "Engineering" }
]

export let timeList = [
  "10:00",
  "10:10",
  "10:20",
  "10:30",
  "10:40",
  "10:50",
  "11:00",
  "11:10",
  "11:20",
  "11:30",
  "11:40",
  "11:50",
  "12:00",
  "12:10",
  "12:20",
  "12:30",
  "12:40",
  "12:50",
  "01:00",
  "01:10",
  "01:20",
  "01:30",
  "01:40",
  "01:50",
  "02:00",
  "02:10",
  "02:20",
  "02:30",
  "02:40",
  "02:50",
  "03:00",
  "03:10",
  "03:20",
  "03:30",
  "03:40",
  "03:50",
  "04:00",
  "04:10",
  "04:20",
  "04:30",
  "04:40",
  "04:50",
  "05:00",
  "05:10",
  "05:20",
  "05:30",
  "05:40",
  "05:50",
  "06:00",
  "06:10",
  "06:20",
  "06:30"
]
