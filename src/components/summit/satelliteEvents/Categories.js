import React from "react"
import PropTypes from "prop-types"
import { strings } from "../../strings"
import $ from "jquery"

class Categories extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      content: strings.SatelliteEvents.categoriesContent[0]
    }
  }

  componentDidMount() {
    let _self = this
    $(".satellieEvent .category-section .content-wrap ul li").hover(function() {
      $(".satellieEvent .category-section .content-wrap ul li").removeClass(
        "active"
      )
      $(this).addClass("active")
      _self.setState({
        content: strings.SatelliteEvents.categoriesContent[this.id]
      })
    })
  }
  // handleHover = id => {
    
  // }

  render() {
    return (
      <div className="category-section" style={this.props.style}>
        <div className="container">
          <h3>Satellite&apos;s Categories </h3>
          <div className="content-wrap">
            <ul>
              <li className="active" id={0}>
                <div className="item">
                  <div className="circle first"  />
                  <h5>
                    PRE <span>summit</span>
                  </h5>
                </div>
              </li>
              <li id={1}>
                <div className="item">
                  <div className="circle second"  />
                  <h5>
                    DURING <span>summit</span>
                  </h5>
                </div>
              </li>
              <li id={2}>
                <div className="item">
                  <div className="circle third" />
                  <h5>
                    POST
                    <span>summit</span>
                  </h5>
                </div>
              </li>
            </ul>
            <div className="popup">
              <p>{this.state.content}</p>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
Categories.propTypes = {
  style: PropTypes.object,
  hoverOn: PropTypes.func,
  content: PropTypes.string
}
Categories.defaultprops = {
  style: {},
  content: ""
}

export default Categories
