import React from "react"
import PropTypes from "prop-types"

const SessionsList = props => {
  return (
    <ul className="sessions--list">
      {props.sessions.map((item, i) => {
        return (
          <li key={{ i }} className="item">
            <div className="banner">
              <div className="content">
                {item.featured_session ? (
                  <div className="label--wrap">FEATURED</div>
                ) : null}
                <p className="date">
                  {item.date || item.session_satellite_date}
                  <p>{" " + item.start_time}</p>
                </p>

                <h4>{item.title}</h4>
                <h5>
                  BY
                  {/* <img style={{marginLeft:10}} width="60" src={_self.ReturnPartnerLogo(item.partner[0])} alt=""/> */}
                  <b style={{ textTransform: "uppercase" }}>
                    {" "}
                    {item.satellite_event}
                    {/* {props.ReturnPartnerLogo(item.partner[0])} */}
                  </b>
                </h5>
              </div>
            </div>
            <div className="content--info">
              <ul>
                {item.type_of_user_interaction.isArray
                  ? item.type_of_user_interaction.map((list, i) => (
                      <li key={i}>{list}</li>
                    ))
                  : null}
              </ul>
              <h6>{item.description}</h6>
              <a
                href={item.satellite_event_map}
                target="_blank"
                rel="noopener noreferrer"
              >
                <i className="fa fa-map-marker" />{" "}
                {item.session_satellite_location}
              </a>
              <p className="deadline">
                {item.deadline_text ? "Deadline To Register " : "."}{" "}
                {item.deadline_text}
              </p>
              {item.cta_text ? (
                <a
                  href={item.cta_Link}
                  target="_blank"
                  rel="noopener noreferrer"
                  className="btn btn-default"
                >
                  {item.cta_text}
                </a>
              ) : null}
            </div>
          </li>
        )
      })}
    </ul>
  )
}

SessionsList.propTypes = {
  sessions: PropTypes.array,
  ReturnPartnerLogo: PropTypes.func
}
export default SessionsList
