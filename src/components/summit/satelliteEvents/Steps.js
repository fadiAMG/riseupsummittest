import React from "react"
import PropTypes from "prop-types"
import { NavLink } from "react-router-dom"

const Steps = props => {
  return (
    <div
      className="category-section steps"
      style={props.style ? props.style : {}}
    >
      <div className="container">
        <div className={"left"}>
          <h3>STEPS TO HOST</h3>
          <a
            className={"link"}
            href={"https://riseupco.typeform.com/to/D0gtvZ"}
            target="_blank"
            rel="noopener noreferrer"
          >
            HOST YOUR OWN
            <img alt="" src={require("../../../assets/arrow-right.svg")} />
          </a>
        </div>
        <ul>
          <li>
            <h4>
              {" "}
              <span>1</span> <p>CONCEPT DESIGN</p>
            </h4>
          </li>
          <li>
            <h4>
              {" "}
              <span>2</span> <p>PLAN AND PREP</p>
            </h4>
          </li>
          <li>
            <h4>
              {" "}
              <span>3</span> <p>LAUNCH</p>
            </h4>
          </li>
        </ul>
        <div className="action-wrap" style={{ display: "none" }}>
          <NavLink to="/partners/BhmKg4/host_a_satellite">
            HOST YOUR OWN
            <img
              width="10"
              src={require("../../../assets/arrow-right.svg")}
              alt=""
            />
          </NavLink>
        </div>
      </div>
    </div>
  )
}
Steps.propTypes = {
  style: PropTypes.object
}
export default Steps
