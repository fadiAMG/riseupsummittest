import React from "react"
import Modal from "react-modal"

import DocumentTitle from "react-document-title"
import { connect } from "react-redux"
import classNames from "classnames"
import FeaturePost from "../../commons/FeaturePost"
import HeaderSlider from "../../commons/HeaderSlider"

import Categories from "./Categories"
import SessionsList from "./SessionsList"
import CorpModal from "../../crowds/partnerships/CorpModal"

// Data
import { strings } from "../../strings"
// import { data } from "../../data"
import $ from "jquery"

import { fetchSessions } from "../../../redux/actions/sessions/SessionsActions"
import { fetchPartners } from "../../../redux/actions/partners/PartnersAction"
import Steps from "./Steps"
class SatelliteEvents extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      active: false,
      sessions: [],
      partners: [],
      partner: {}
    }
    this.ReturnPartnerLogo = this.ReturnPartnerLogo.bind(this)
  }
  componentDidMount() {
    // console.log("SatelliteEvents Compnenet Mounted")

    $(window).scrollTop($(".feature--post").offset().top - 150)

    this.props
      .fetchSessions()
      .then(res => this.setState({ sessions: res.payload.data.sessions }))
    this.props
      .fetchPartners()
      .then(res => this.setState({ partners: res.payload.data.collections }))
  }
  ReturnPartnerLogo(id) {
    let logo = ""
    this.state.partners.forEach(partnerItem => {
      if (partnerItem._id === id) {
        logo = partnerItem.name
      }
    })
    return logo
  }

  openModal(id) {
    this.setState({ modalIsOpen: true, id })
  }

  afterOpenModal() {
    this.state.partners.forEach(item => {
      if (item._id === this.state.id) {
        this.setState({ partner: item })
      }
    })
  }
  closeModal() {
    this.setState({ modalIsOpen: false })
  }

  render() {
    let { sessions } = this.state
    let string = strings.SatelliteEvents
    let _self = this
    // let items = []
    sessions = sessions.filter(item => {
      return item.session_satellite && item.status
    })
    function RenderCorpModalContent() {
      if (_self.state.partner) {
        return (
          <CorpModal
            close={() => _self.setState({ modalIsOpen: false })}
            partnerData={_self.state.partner}
          />
        )
      } else {
        return null
      }
    }
    function RenderTabsContent() {
      if (_self.state.active) {
        return (
          <div>
            <FeaturePost
              slog={string.feature.slog}
              title={string.feature.title}
              img={require("../../../assets/images/satelliteHeader.png")}
              Url="/"
              strength={300}
              BtnTitle="HOST YOUR OWN"
              seprator={false}
              // openText="MORE SATELLITES TO COME"
              noLink={true}
              products={string.feature.products1}
              ImgMaxHeight={"600px"}
              style={{
                slogFontSize: "18px",
                titleFontSize: "27px",
                detailsFontSize: "17px",
                // slogFontWeight: "400",
                slogColor: "#2a428c"
              }}
            />
            <div className="sessions--wrap__content">
              <div className="container">
                <h3 className="speakers--head">THIS YEAR’S SATELLITES</h3>
                <SessionsList
                  ReturnPartnerLogo={_self.ReturnPartnerLogo}
                  sessions={sessions}
                />

                {/* <div className="partners--wrapper" style={{ paddingTop: 10 }}>
                  <div
                    className="partner--section__content"
                    style={{ paddingTop: 0 }}
                  >
                    <h3 className="section--header">
                      SATELLITES EVENTS PARTNERS
                    </h3>
                    <ul>
                      {_self.state.partners.map(item => {
                        if (
                          item.status &&
                          item.Label_Tags.includes("Satellite Event Partner")
                        ) {
                          return (
                            <li>
                              <a onClick={() => _self.openModal(item._id)}>
                                <img src={item.logo_url} alt={item.name} />
                              </a>
                            </li>
                          )
                        }
                      })}
                    </ul>
                  </div>
                </div> */}
              </div>
            </div>
          </div>
        )
      } else {
        return (
          <div>
            <FeaturePost
              slog={string.feature.slog}
              title={string.feature.title}
              details={string.feature.details}
              img={require("../../../assets/images/satelliteHeader.png")}
              Url="https://riseupco.typeform.com/to/D0gtvZ"
              strength={300}
              seprator={false}
              products={string.feature.products}
              BtnTitle="HOST YOUR OWN"
              noLink={true}
              ImgMaxHeight={"550px"}
              style={{
                slogFontSize: "18px",
                titleFontSize: "27px",
                detailsFontSize: "17px",
                // slogFontWeight: "400",
                slogColor: "#2a428c"
              }}
              SelfLink={true}
            />
            <Categories
              style={{
                backgroundColor: "#FCE100"
              }}
            />
            <div className="category-section tags">
              <div className="container">
                <h3>TAGS</h3>
                <ul>
                  <li>
                    <img
                      height="100"
                      src={require("../../../assets/tags1.svg")}
                      alt=""
                    />
                    <div className="tagText__container">
                      <h4>NETWORK</h4>
                      <p>Such as parties, mixers, cocktails</p>
                    </div>
                  </li>
                  <li>
                    <img
                      height="100"
                      src={require("../../../assets/tags2.svg")}
                      alt=""
                    />
                    <div className="tagText__container">
                      <h4>LEARN & GET INSPIRED</h4>
                      <p>Such as office tours, talks, seminars</p>
                    </div>
                  </li>
                  <li>
                    <img
                      height="100"
                      src={require("../../../assets/tags3.svg")}
                      alt=""
                    />
                    <div className="tagText__container">
                      <h4>EXPLORE</h4>
                      <p>Such as tours and trips</p>
                    </div>
                  </li>
                </ul>
              </div>
            </div>

            <Steps style={{ backgroundColor: "rgb(44, 64, 140)" }} />
          </div>
        )
      }
    }
    return (
      <div className="about--wrap satellieEvent">
        <DocumentTitle title="RiseUp Summit - Satellite Events" />
        <HeaderSlider slider={false} />
        <ul className="nav--wrap">
          <li>
            <div
              onClick={() => this.setState({ active: false })}
              className={classNames({ active: !this.state.active })}
            >
              HOST
            </div>
          </li>
          <li>
            <div
              onClick={() => this.setState({ active: true })}
              className={classNames({ active: this.state.active })}
            >
              ATTEND
            </div>
          </li>
        </ul>
        {RenderTabsContent()}

        <Modal
          isOpen={this.state.modalIsOpen}
          onAfterOpen={this.afterOpenModal.bind(this)}
          onRequestClose={this.closeModal.bind(this)}
          contentLabel=""
          className={"PartnersModal"}
        >
          {RenderCorpModalContent()}
        </Modal>
      </div>
    )
  }
}

export default connect(
  null,
  { fetchSessions, fetchPartners }
)(SatelliteEvents)
