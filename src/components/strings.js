export const strings = {
  Homepage: {
    banner: {
      title: "INNOVATION TO IMPROVE HUMAN EXPERIENCE (HX)",
      bgOpacity: 0.15,
      width: 100,
      slog: "THIS YEAR DON’T MISS…"
    },
    feature: {
      title: "#ThinkHX",
      details:
        "EVERYTHING boils down to human experience (HX) - it’s at the heart of everything we do. ‘How can we improve human experience?’ is the question of the century, and one that needs a LOT of creative and disruptive solutions. We’re highlighting innovation as a way to create and drive these solutions. Innovation happens in the spaces where creative, capital, and technology meet. #ThinkHX"
    }
  },
  Speakers: {
    banner: {
      title: "GET INSPIRED BY TODAY’S CHANGEMAKERS.",
      bgOpacity: 0.3,
      width: 60
    },
    info: {
      text1:
        "“RISEUP IS THE BIGGEST ENTREPRENEURSHIP STARTUP EVENT IN THE REGION”",
      qoute1: "KINDA IBRAHIM, PARTNERSHIPS DIRECTOR MENA, TWITTER",
      text2:
        "“THE SIZE OF THE EVENT IS A TESTAMENT BOTH TO THE STATURE THAT THE IDEA OF ENTREPRENEURSHIP HOLDS IN THE MARKET, & THE PROMISE OF MENA’S GROWTH”",
      qoute2: "FORBES"
    },
    feature: {
      title: "SUMMIT SPEAKERS: THE RISEUP LINEUP",
      details:
        "The most influential speaker line-up in the Middle East, we’re bringing all the people you need to #RiseUp18. You’ll find them on one of our 3 track stages: Capital, Tech, and Creative. The most influential investors, top CEOs, successful entrepreneurs, creatives, tech-gurus, and innovators. They’ll be there to talk to you and share their expertise!"
    }
  },
  Partners: {
    banner: {
      title: "OUR PARTNERS CREATE OUR STORY.",
      bgOpacity: 0.25,
      width: 60
    },
    feature: {
      title: "WHY PARTNER WITH RISEUP",
      details:
        "RiseUp Summit is as crowdsourced as you can get. Working with us will provide your business and activities with exposure to the thousands of startups, companies, and investors that attend the Summit and the thousands more worldwide that follow the stories."
    }
  },
  MenaHipos: {
    banner: {
      title: "MENA HIPOs",
      bgOpacity: 0.25,
      width: 60
    },
    feature: {
      title: "HIPO’s: THE FUTURE OF MENA",
      details:
        "For the 2nd year running, the Summit shines a spotlight on the region’s best. A HIPO is a unique startup that has been identified as having HIgh POtential growth, a thirst for disruption and unparalleled resilience. Last year’s lineup of future unicorns included Instabug, Vezeeta, Mumzworld, and Bayzat.",
      products: [
        {
          title: "HIPO Benefits:",
          // details:
          //   "The HiPos were chosen through a rigorous selection process by our team and a diverse range of industry leaders. The criteria used to pick them included growth rates, customer base, team size and culture, and investment size among other aspects. We’re spotlighting these 20 champions during this year’s summit. MENA HiPos will be getting stage time, exclusive exhibition spaces, tailored connections to the stakeholders they need, and much more! Keep an eye out for these rockstar startups this summit, they have a lot in store for us.",
          list: [
            "2 Summit ‘18 tickets",
            "2.44x2.44 booth in HIPOs section for 3 days",
            "Premium startup exposure at Summit ‘18",
            "Logo on website & mobile application",
            "2 tickets to exclusive Summit ‘18 networking dinners"
          ]
          // link:'startup-station'
        },
        {
          title: "Eligibility Criteria:",
          list: [
            "Post-seed",
            "Disruptive business idea",
            "Innovative",
            "Known to the market"
          ]
        }
      ]
    }
  },
  GroundedExperiences: {
    banner: {
      title: "MENA HIPOs",
      bgOpacity: 0.25,
      width: 60
    },
    feature: {
      title: "OUR THEME",
      details:
        "Grounded Experiences is about being in tune with the needs, obstacles, and opportunities within our reach. It is about shooting for the stars while remaining realistically ground-headed and rooted within our new own locales, networks, and communities. We believe that improving human experience lies in being resourceful, setting realistic goal, and accepting failures rather than becoming then next superhero! At RiseUp Summit ‘18, we invite you to share with us your means of thinking grounded in an ever-expanding ecosystem. ",
      products: [
        {
          title:
            "Excited? Good. Now check out this year's Tracks for an idea on what this all means at #RiseUp18"
          // details:
          // "The HiPos were chosen through a rigorous selection process by our team and a diverse range of industry leaders. The criteria used to pick them included growth rates, customer base, team size and culture, and investment size among other aspects. We’re spotlighting these 20 champions during this year’s summit. MENA HiPos will be getting stage time, exclusive exhibition spaces, tailored connections to the stakeholders they need, and much more! Keep an eye out for these rockstar startups this summit, they have a lot in store for us."
          // link: "startup-station"
        }
      ]
    }
  },
  Partnerships: {
    banner: {
      title: "OUR PARTNERS CREATE OUR STORY.",
      bgOpacity: 0.25,
      width: 60
    },
    feature: {
      title: "WHY BECOME A PARTNER WITH RISEUP",
      details:
        "RiseUp Summit is as crowdsourced as you can get. Working with us will provide your business and activities with exposure to the thousands of startups, companies, and investors that attend the Summit and the thousands more worldwide that follow the stories. Find our partnership opportunities below:"
    }
  },
  Tickets: {
    banner: {
      title: "YOU’RE ONE CLICK AWAY FROM JOINING US.",
      bgOpacity: 0,
      width: 85
    }
  },
  Startups: {
    banner: {
      title: "LEARN, CONNECT, AND GROW!",
      bgOpacity: 0.3,
      width: 60
    },
    feature: {
      title: "WHY STARTUPS ATTEND RISEUP SUMMIT",
      details:
        "Startups are the stars of RiseUp Summit. We’ll be ensuring you can access the best resources for your business to grow fast. Startups can exhibit at our Startup Station, battle it out in our Pitch Competition, get access to an incredible network of investors and mentors though mentor and investor matchmaking, get exposure in the most relevant media, tools, know-how, and plug into a regional marketplace. Be sure not to miss these opportunities.",
      launchText:
        "These are the activities that you can apply to participate in at the Summit.",
      openText: "Deadline to apply for these opportunities is October 28",
      products: [
        {
          title: "Exhibit at the Startup Station",
          details:
            "Showcase your startup to the ecosystem! The best of MENA’s startups exhibit at RiseUp’s Startup Station. This year, 150 startups will exhibit at RiseUp Summit, each getting ONE day to show us what they’ve got!",
          link: "startup-station"
        },
        {
          title: "Pitch Competition",
          details:
            "50 of MENA’s best pre-seed startups will go head-to-head in our startup showdown. Think you’ve got what it takes? Sign up for the RiseUp Pitch Competition and get a chance to step onto MENA’s biggest entrepreneurship stage.",
          link: "pitch-competition"
        },
        {
          title: "Mentorship",
          details:
            "Get tailored advice from leading experts in our roundtables and one-on-ones."
          // link: "activities"
        },
        {
          title: "Talent  Matchmaking",
          details:
            "If that’s your next hire, swipe right! We’re going to match you with the designer, developers, managers, and partners your startup needs to grow."
          // link: "activities"
        },
        {
          title: "Provide Perks to the Community",
          details:
            "You can provide offers, discounts, and freebies to our RiseUp Community on RiseUp Connect."
          // link: "activities"
        }
      ]
    }
  },
  StartupsWithNoLinks: {
    banner: {
      title: "LEARN, CONNECT, AND GROW!",
      bgOpacity: 0.3,
      width: 60
    },
    feature: {
      title: "WHY STARTUPS ATTEND RISEUP SUMMIT",
      details:
        "Startups are the stars of RiseUp Summit. We’ll be ensuring you can access the best resources for your business to grow fast. Startups can exhibit at our Startup Station, battle it out in our Pitch Competition, get access to an incredible network of investors and mentors though mentor and investor matchmaking, get exposure in the most relevant media, tools, know-how, and plug into a regional marketplace. Be sure not to miss these opportunities.",
      launchText:
        "These are the activities that you can apply to participate in at the Summit.",
      openText: "Deadline to apply for these opportunities is October 28",
      products: [
        {
          title: "Exhibit at the Startup Station",
          details:
            "Showcase your startup to the ecosystem! The best of MENA’s startups exhibit at RiseUp’s Startup Station. This year, 150 startups will exhibit at RiseUp Summit, each getting ONE day to show us what they’ve got!"
        },
        {
          title: "Mentorship",
          details:
            "Get tailored advice from leading experts in our roundtables and one-on-ones."
        },
        {
          title: "Investor Matchmaking",
          details:
            "If that’s your next investor, swipe right! We're going to match you with investors."
        },
        {
          title: "Provide Perks to the Community",
          details:
            "You can provide benefits, discounts, and freebies to our RiseUp Community on RiseUp Connect."
        },
        {
          title: "Pitch Competition",
          details:
            " 50 of MENA’s best startups will go head-to-head in our startup showdown. We’ve split our pitch competition into two stages: early (seed) and growth (post seed) with a grand finale in our closing ceremony."
        }
      ]
    }
  },
  Investors: {
    banner: {
      title: "MEET YOUR NEXT INVESTMENT.",
      bgOpacity: 0.2,
      width: 60
    },

    feature: {
      title: "WHY INVESTORS ATTEND RISEUP SUMMIT",
      openText:
        "To gain full access to investor opportunities, benefits, and events at RiseUp Summit and beyond, please create your investor profile! \n To unlock your exclusive investor access & opportunities",
      details:
        "The world’s next big ideas are all coming from MENA, and we’ve got them all right here waiting to meet you. We’ve tailored this year’s Summit experience to match your expertise, fund, and market interests. We’ve jam-packed your three days with countless talks, networking opportunities, and events for investors - bringing together some of the sharpest and brightest minds from emerging and established ecosystems.",
      products: [
        {
          title: "EXCLUSIVE INVESTOR OPPORTUNITIES",
          list: [
            "A full content track dedicated to Capital, including deep dives",
            "Tailored content for investors",
            "Access to startup profiles prior to your arrival",
            "Investor’s Cocktail: Invite-only cocktail event for 200 top investors from the US, Europe, Asia and MEA, in addition to our 20 HiPos!",
            "Two pitch competitions, one for the Creative Economy and one for tech-enabled startups",
            "150 filtered startups from across the MENA region - with a daily investor-only tour. ",
            "The MENA HiPo program focusing on the High Potential startups in the region with the fastest growth and the most innovation",
            "Startup investor matchmaking, which includes private meeting spaces and access to profiles of startups.",
            "Access to private meeting spaces, in addition, we will also facilitate any one-on-one meetings you want to have with any business"
          ]
        }
      ]
    }
  },
  Attendees: {
    banner: {
      title: "YOU DON’T WANT TO MISS THIS.",
      bgOpacity: 0.2,
      width: 60
    },
    feature: {
      title: "WHY ATTEND RISEUP SUMMIT",
      details:
        "At the Summit, we gather all the relevant stakeholders in our region’s ecosystem. Do you need customers for your product? Developers for your App? Partners for your startup? Suppliers for your product? Or are you just looking to immerse yourself in the wisdom of industry gurus and critically acclaimed entrepreneurs? Either way, join the crowd."
    }
  },
  Volunteers: {
    banner: {
      title: "THE ULTIMATE BEHIND THE SCENES EXPERIENCE. ",
      bgOpacity: 0.22,
      width: 85
    },
    feature: {
      title: "VOLUNTEER AT RISEUP SUMMIT",
      details:
        "With over 1,000 volunteers in our volunteer programs over the past five years, we want you to come join us because without you guys, it wouldn’t be possible. You’ll be supporting us with the whole thing, whether that means helping out the attendees, the speakers, the logistics team or the marketing one. Not only will you be gaining extensive experience in event management and get a better idea of entrepreneurship, but you will also be joining the RiseUp crowd and we promise you, you want to be a part of this.",
      openText: "APPLICATIONS OPEN IN SEPTEMBER"
    }
  },
  PitchComp: {
    banner: {
      title: "RISEUP PITCH COMPETITION",
      bgOpacity: 0.3,
      width: 85
    },
    feature: {
      title: "THE STARTUP SHOWDOWN",
      details:
        "50 of MENA’s best startups will go head-to-head on the biggest stage in the Middle East for this year’s edition of the RiseUp Pitch Competition. The Pitch Competition is only open to pre-seed Startups. With pitches under 3 minutes, expect to be wowed as each startup calls for your attention to show off their product against the clock for the chance to win some incredible prizes!",
      openText: "Deadline to apply for these opportunities is October 28",
      products: [
        {
          title: "Eligibility",
          list: [
            "Open to all sectors",
            "Startups must be tech-enabled ",
            "Pre-seed startups must have raised less than $50,000 ",
            "Startup must be less than 5 years old since first registering company."
          ]
        }
      ]
    }
  },
  Launchpad: {
    banner: {
      title: "LAUNCHPAD",
      bgOpacity: 0.3,
      width: 85
    },
    feature: {
      title: "YOUR STAGE, YOUR STORY",
      details:
        "We’re here to give you the ultimate announcement opportunity. Launchpad's purpose is to connect our stakeholders to the most relevant and effective exposure opportunities for their biggest announcements. With attendees made up of journalists, startups, investors, corporates, techies, creatives, and more, news at the Summit travels far. Launchpad announcements will be on RiseUp Summit’s main stage and will be reflected on all digital screens in #RiseUp18's venues. Whether you want to announce a new product, a fund, an investment, a partnership, opportunities, mergers & acquisitions, or any other big announcement, this is the way to do it.",
      openText: "Deadline to apply for the Pitch Competition is October 22. ",
      products: [
        {
          title: "Who should apply?",
          list: [
            "Startups launching in the MENA market",
            "Accelerators and incubators launching new programs",
            "Corporates announcing Mergers & Acquisitions",
            "Investors announcing new funds or opportunities",
            "Ecosystem players with big announcements",
            "Corporates offering deals and benefits for startups",
            "Businesses looking for customers"
          ]
        }
      ]
    }
  },
  Media: {
    banner: {
      title: "WHAT HAPPENS IN THE SUMMIT SHOULDN’T STAY IN THE SUMMIT.",
      bgOpacity: 0.22,
      width: 85
    },
    feature: {
      title: "WHY COVER RISEUP SUMMIT 18?",
      details:
        "Once a year, for three consecutive days, an action-packed itinerary filled with entrepreneurs and business stories takes place in Downtown Cairo. With an audience from across the globe coming together, the impact causes ripples even months after it’s all over.",
      openText: "APPLICATIONS OPEN IN SEPTEMBER"
    }
  },
  Workshops: {
    banner: {
      title: "WORKSHOPS",
      bgOpacity: 0.22,
      width: 85,
      slog: "SUMMIT FEATURES"
    },
    feature: {
      title: "101s AND DEEP DIVES ",
      details:
        "Take a break from the main stage content, and immerse yourself in a RiseUp workshop. This year we’re offering 20 interactive workshops to really dive into a subject. For explorers, we’ve created special lectures to give you the 101 on the topic, and for those more advanced we’re launching a series of deep dives to help you scale! "
    }
  },
  Downtown: {
    banner: {
      title: "DOWNTOWN CAIRO",
      bgOpacity: 0.22,
      width: 60
    }
  },
  Team: {
    banner: {
      title: "",
      bgOpacity: 0.22,
      width: 85
    },
    feature: {
      title: "IT TAKES A TEAM TO BUILD IT ",
      details:
        "Satellite events are an important part of our community, allowing the spirit and momentum of the Summit to reach more people. These events are planned before/after the Summit dates, or during the Summit schedule. Examples of Satellite Events: hackathons, competitions, demos & screenings, office hours, workshops, parties & mixers! We’re co-hosting special curated events that are relevant to our attendees, but need a little bit of time outside the program."
    }
  },
  About: {
    banner: {
      title: "",
      bgOpacity: 0.22,
      width: 85
    },
    feature: {
      title: "CONNECTING STARTUPS TO MOST RELEVANT RESOURCES",
      details:
        "We’ve grown over the years from a grass-root movement to a global momentum. The purpose behind RiseUp has remained the same: to connect startups to the most relevant resources worldwide. We act as the glue that fuses ecosystems together to help them realize their ultimate potential. By connecting stakeholders to local & global networks, and fusing diverse talents & resources, ecosystems are enabled to accelerate, innovate, and grow. Our mission is accomplished through our two products: Summit and Connect.",
      products: [
        {
          title: "RISEUP SUMMIT",
          details:
            "The summit is our three-day, one-stop-shop, entrepreneurship marathon! Three years ago, when the seeds of the #RISEUPSUMMIT movement were sown we never imagined that in such a short time, our movement would Quantum Leap from an exciting local event into a global phenomenon. We promise that the world comes to Downtown Cairo, and thanks to an incredible level of commitment from our tireless team and phenomenal partners, we deliver. Thought leaders, media mavens, tech-legends and power player investors gather to share their experience, support and invest in the Middle East & Africa’s flourishing entrepreneurial ecosystem. From workshops, inspiring talks, and panel discussions to startup stations, pitch competitions, and networking platforms with industry experts. Whoever you are, you’ll find a place for you."
        },
        {
          title: "RISEUP CONNECT",
          details:
            "Connect is an online digital space connecting startups with the most relevant resources through curated content and matchmaking. Its main pillars are talent, funding, perks, marketplace, community, directory and knowledge. Connect is based on a conversational tool where users can join a community of entrepreneurs, investors, brands and mentors to mutually share their experience and exchange information. Check out www.riseupconnect.com"
        },
        {
          title: "OTHER ACTIVITIES",
          details:
            "RiseUp partners with entities around the region to shed light on opportunities across the Middle East and North Africa. We hold enabling activities such as RiseUp Explore and RiseUp Meetups in cities around the world, representing MENA. These activities and programs are in partnership with startups and support organizations such as coworking spaces, accelerators, incubators and government entities. Previous events include a networking event held at the British Embassy, RiseUp Explore in Berlin and Silicon Valley, Cairo meets Berlin Meetup, Egypt’s first entrepreneurship TV content on CBC, and a showcase at London Tech Week. "
        }
      ]
    }
  },
  Summit101: {
    banner: {
      title: "SUMMIT 101",
      bgOpacity: 0.22,
      width: 30
    }
  },
  SatelliteEvents: {
    banner: {
      title: "SATELLITES",
      bgOpacity: 0.22,
      width: 85
    },
    feature: {
      title: "IT DOESN'T STOP AT THE SUMMIT",
      slog: "#RiseUp18 Satellite Events",
      details: "",
      products: [
        {
          title: "What are Satellite Events?",
          details:
            "So much to do, so little time! This ecosystem has so much more to offer than what we have for you at the Summit, that’s why we’ve got Satellite Events! Happening at innovation hotspots around Cairo and hosted by the city’s changemakers, Satellite Events are designed to show you the ecosystem in action, and to highlight the community at their own spaces."
        },
        {
          title: "Why Host?",
          details:
            "Satellites bring the Summit to your space. By harnessing the spirit and momentum of the Summit, Satellite Events are a great way to shine a spotlight on your organization while contributing to the value offered to Summit attendees. Interested in hosting but unsure what to do? Get in touch with our team and we’ll design a Satellite perfect for you!"
        }
        // {
        //   title: "PRE SUMMIT",
        //   details:
        //     "These events are all about gearing up to the Summit! Host a Pre-Summit Satellite and a lot of eyes and ears will be looking out for you at #RiseUp18."
        // },
        // {
        //   title: "DURING SUMMIT",
        //   details:
        //     "Take the Summit beyond the Greek! Happening during Summit days, these events are an extension of the features and happenings going on during the three days."
        // },
        // {
        //   title: "POST SUMMIT",
        //   details:
        //     "Are we thinking after-party? Maybe. Post Summit Satellites are fantastic for harnessing the electric momentum of the Summit and pushing the focus onto your space."
        // }
      ],
      products1: [
        {
          title: "What are Satellite Events?",
          details:
            "So much to do, so little time! This ecosystem has so much more to offer than what we have for you at the Summit, that’s why we’ve got Satellite Events! Happening at innovation hotspots around Cairo and hosted by the city’s changemakers, Satellite Events are designed to show you the ecosystem in action, and to highlight the community at their own spaces."
        }
      ]
    },
    categoriesContent: [
      "These events are all about gearing up to the Summit! Host a Pre-Summit Satellite and a lot of eyes and ears will be looking out for you at #RiseUp18.",
      "Take the Summit beyond the Greek! Happening during Summit days, these events are an extension of the features and happenings going on during the three days.",
      "Are we thinking after-party? Maybe. Post Summit Satellites are fantastic for harnessing the electric momentum of the Summit and pushing the focus onto your space."
    ]
  },
  ProgramsData: {
    banner: {
      title: "SCHEDULE AND TRACKS",
      bgOpacity: 0.22,
      width: 30
    },
    Tracks: {
      Tech: {
        title: "THE FUTURE IS HERE…",
        slog: "#HackHX",
        details:
          "Technology is at the center of how creative solutions work and spread. Tech hacks everyday problems, simplifying and streamlining complexity and improving human experience (HX) in the process. We’re bringing in technical experts to demystify the latest in Artificial Intelligence, IoT, and Big Data. We’ll show you how new technologies can be incorporated into your business to take your work where you want it to go. Industries discussed and analyzed will range from Edtech to Fintech, spanning pivotal topics such as the Digital Skills Gap and Blockchain. Faced with these disruptive advancements, we repeatedly find ourselves asking the question: In a rapidly changing tech-enabled world, how do we hold on to our humanity? ",
        hashTag: "#HackHX"
      },
      capital: {
        title: "THE BEST CAPITAL IS SMART CAPITAL...",
        slog: "#RaiseHX",
        details:
          "Smart capital drives, accelerates, and scales creative solutions. Without it, a creative solution is just a brilliant idea. No money, no honey. Entrepreneurs will get the chance to become pitch perfect, examine the different funding models, and discover when, why, and how to raise. With our brand-new investor sessions, investors will learn from other Angels and VCs, exchange experience and knowhow, and explore different investment landscapes.  Because smart capital is not just about having money in the bank, we’ll be looking at how to secure the right investment, choose the right mentors, build the right team, and create the right support system for you. ",
        hashTag: "MENA is heating up fast. #RaiseHX"
      },
      creative: {
        title: "CREATIVE IS NO LONGER AN ADD ON...",
        slog: "#DriveHX",
        details:
          "Creativity is a driving force behind innovation. We’re shining the spotlight on how creatives can take the lead in designing solutions to improve human experience (HX). This year, we’re demonstrating how creatives can take their talents to the next level by divulging the secrets behind building a business, marketing, and monetization. Top companies will share how embedding creatives and creative thinking into their DNA transformed their growth, and helped build collaborative communities that are hotbeds for discovery and disruption. ",
        hashTag: "Creatives #DriveHX",
        list: {
          title: "JOIN US TO DISCOVER THE LATEST IN",
          listItems: [
            "Design",
            "Fashion",
            "Film",
            "Music",
            "Media",
            "Marketing and Branding"
          ]
        }
      }
    },
    SpeacialEvents: {
      title: "SPOTLIGHT",
      details: "",
      hashTag: ""
    },
    products: [
      {
        title: "Ecosystems Spotlight #LinkHX ",
        details:
          "Global and MENA ecosystem players are coming together for the first time to talk setting up, scaling, support, and collaboration. The aim of the Showcase is to exchange best practices, forge connections between ecosystems, and discover how we can collaborate with a special emphasis on the MENA region. Ecosystem players will highlight starting and expansion opportunities, the latest data, and their insights on the legal and investment landscape."
      },
      {
        title: "MENA ECOSYSTEM GALLERY",
        details:
          "Walk into a gallery displaying key info and data on all the major startup cities in MENA. From key players to market sizes and opportunities, you will Get a glimpse of Amman, Beirut, Bahrain, Cairo, Casablanca, Dubai, Riyadh, Tunis",
        nested: true
      },
      {
        title: "MENA HIPO’S ",
        details:
          "20 of MENA’s most unique startups that have been identified as having HIgh POtential growth, a thirst for disruption and unparalleled resilience.",
        nested: true
      },
      {
        title: "Spotlight On: Trailblazers #YourHX",
        details:
          "We believe in the power of storytelling - so we’ve incorporated it into the agenda of our three tracks. This year, you can get up close and personal with some of the most inspiring people out there. From the initial spark that lead them to start their business, to navigating unfamiliar territories, you’ll get to hear the fears they faced and their subsequent successes. Disclosing how they built their team, designed their culture and optimized their business strategies are but a few of the things on our agenda."
      }
    ]
  },
  StartupStation: {
    banner: {
      title: "STARTUP STATION",
      bgOpacity: 0.22,
      width: 30
    },
    feature: {
      title: "MAKE YOUR MARK AND LEAVE AN IMPRESSION ",
      openText: "Deadline to apply for these opportunities is October 28",
      details:
        "The best of MENA’s startups exhibit at RiseUp’s Startup Station to capitalize on the immense exposure and marketplace opportunities and engage with our Summit attendees. This year, 150 startups will exhibit at RiseUp Summit, each getting ONE day to show us what they’ve got!",
      products: [
        {
          title: "Why Exhibit at our Startup Station ?",
          details:
            "Showcase your startup to the ecosystem! Having your product and your team present gives you the chance to engage with potential customers and investors. We’re looking for startups who are fusing creativity, technology, and capital and innovating to improve human experience (HX)."
        },
        {
          title: "Startup Station Package: (Price EGP 3850) ",
          list: [
            "Exhibit for 1 day at the largest entrepreneurship event in the MENA region",
            "2 tickets to RiseUp Summit",
            "Attend talks, panels and workshops by top speakers and industry leaders from around the world",
            "Info & Training Session: How can you get the most out of the Summit? How should you pitch to investors?",
            "Investor-only Startup Station tour every morning",
            "Visibility to our network of 200+ investors",
            "5000+ potential new customers",
            "Positioning on RiseUp Summit website, mobile application, social media and newsletter with an outreach of 100K+ followers and readers.",
            "Subscription to the RiseOpps exclusive opportunities list."
          ]
        },
        {
          title: "Why will startups exhibit for one day only?",
          list: [
            "To ensure maximum exposure for selected startups. 150 of MENAs best startups will exhibit.",
            "To showcase as many startups as possible",
            "Your Summit experience does not stop at the exhibition space. We want startups to get the most out of the Summit, through attending workshops, talks and panels, participating in the RiseUp Pitch Competition, engaging in networking, mentorship, investor matchmaking, and many more activities."
          ]
        }
      ]
    }
  },
  Activities: {
    matchmaking: {
      banner: {
        title: "ACTIVITIES",
        bgOpacity: 0.22,
        width: 30
      },
      feature: {
        title: "December 8th, 2018",
        openText: "Deadline to apply for these opportunities is October 28",
        details:
          "This year we’re taking a hands-on approach to connecting your business to the specific resources it needs by offering one of our favorite features: matchmaking.",
        products: [
          {
            // title: "Mentorship",
            details:
              "As a senior professional, you will have an exclusive opportunity to meet the founders of Egypt’s most promising startups in one place and learn the ins and outs of the entrepreneurship scene in Egypt from the biggest leaders in the game."
          },
          {
            // title: "Investor Matchmaking",
            details:
              "As a startup founder, regardless whether you are an early-stage startup seeking experts to join you as co-founders or board members, or if you are in the expansion-stage or you’re well-funded and need executives in key positions, this event is your destination."
          }
        ]
      }
    },
    contentSupermarket: {
      banner: {
        title: "ACTIVITIES",
        bgOpacity: 0.22,
        width: 30
      },
      feature: {
        title: "THE STARTUP MANIFESTO",
        openText: "Deadline to apply for these opportunities is October 28",
        details:
          "This one is for all the startups that have faced obstacles while trying to aim for the stars. The Startup Manifesto is a collective impact initiative made by the ecosystem, for the ecosystem. This means that those working on making changes, are the stakeholders of the network themselves. The Startup Manifesto isn’t just a document that’s an accumulation of all the things that could or do go wrong, but it’s an action plan with the aim of creating the required plan for a more harmonious enabled ecosystem. The first edition of The Startup Manifesto is now officially available, so make sure to get your copy at the Summit!",
        products: [
          {
            title: "WHAT?",
            huge: "Investor — Startup Matchmaking:",
            details: "If That’s Your Next Investor, Swipe Right."
          },
          {
            huge: "Startup Mentoring:",
            details: "Get custom advice from leading experts. "
          }
        ]
      }
    },
    networking: {
      banner: {
        title: "ACTIVITIES",
        bgOpacity: 0.22,
        width: 30
      },
      feature: {
        title: "MEET THE RIGHT PEOPLE & COMPANIES",
        details:
          "Networking is core to the growth of your business. The bigger and stronger your network, the more visible you become and the more likely you are to connect with valuable resources."
      }
    },
    entertainment: {
      banner: {
        title: "ACTIVITIES",
        bgOpacity: 0.22,
        width: 30
      },
      feature: {
        title: "IMMERSE YOUR SENSES",
        details:
          "Our parties and pop-up stages will host some of the most talented artists in MENA. Whether your ideal Thursday night is filled with binge watching Project Runway, jamming to a yet-to-be-discovered band, or appreciating the latest in art & dance, we’ve got you covered. Get ready for a one-of-a-kind visual and audio experience!"
      },
      feature1: {
        title: "FOODCOURT",
        details:
          "With everything happening at RiseUp18, you’ll need some fuel. We’re bringing you one of the newest editions to our hustling capital city: UberEats. They will be waiting to take your order and bringing you your biggest cravings right then and there; all you have to do is look at the menu and take your pick!"
      }
    },
    food: {
      banner: {
        title: "ACTIVITIES",
        bgOpacity: 0.22,
        width: 30
      },
      feature: {
        title: "STARTUP FOODCOURT",
        details:
          "we’re connecting exciting entrepreneurs with industry experts and investors to discuss their specific startup challenges. We’re bringing together an inspiring number of mentors that will dedicate their time and effort to guide the startups in the right direction through addressing their questions and concerns across different industries and functions. Mentors are all of top caliber from across the MENA region with expertise covering the whole startup cycle. We have 2 types of mentoring session that are structured around the startup needs assessment. The first is in the form of a round table where entrepreneurs and mentors are matched in breakout groups of 8 -10 based on skills and challenges ( 2-3 mentors & 6-8 entrepreneurs). Mentors and entrepreneurs typically rotate, to get the maximum benefit out of the sessions. These are three sessions of one hour each followed by an open networking session. Later stage startups get the chance to be matched with a mentor for a deep dive one on one session tailoring to their specific needs."
      }
    },
    investorOH: {
      banner: {
        title: "ACTIVITIES",
        bgOpacity: 0.22,
        width: 30
      },
      feature: {
        title: "December 7th, 2018",
        details:
          "Algebra Ventures want to meet the founders of exciting technology startups. They’re happy to give advice, answer your questions, discuss your ideas, and listen to your pitches. ",
        products: [
          {
            title: "",
            huge: "",
            details:
              "-   15 minute one-on-one sessions with pre-selected startups"
          },
          {
            huge: "",
            details:
              "-   Startups will apply and if accepted, will receive confirmation with more information and a Calendly link to book their time-slot"
          }
        ]
      }
    },
    ubuer: {
      banner: {
        title: "ACTIVITIES",
        bgOpacity: 0.22,
        width: 30
      },
      feature: {
        title: "UBER Pitch n’Ride",
        details:
          "For the 4th year running our friends and Main Partners Uber are organizing the original Pitch n' Ride! 10 lucky entrepreneurs will get in a special car with an established investor and have 10 minutes to pitch their idea. Investors will either choose to invest in your startup or provide free consultation.",
        link: "https://riseupco.typeform.com/to/E5m5De",
        products: [
          {
            title: "",
            huge: "",
            details: ""
          }
        ]
      }
    },
    AllSummitLong: {
      banner: {
        title: "ACTIVITIES",
        bgOpacity: 0.22,
        width: 30
      },
      feature: {
        title: "All Summit Long!",
        details:
          "Networking, Exposure, Knowledge, and Celebration. This is what #RiseUp18 is all about! We've partnered with our friends at Brainy Squad to hold 3 fantastic activities to get you engaging with as many attendees as possible, learn all about the different startups participating at the Summit, and share your RiseUp experience with your crowd.",
        link:
          "https://indd.adobe.com/view/83adbb11-dae3-4340-b1ee-0cf274a6b77c",
        products: [
          {
            details:
              "All while competing to win free tickets to RiseUp Summit 2019! How cool is that!"
          },
          {
            title: "RiseUp Collector",
            huge: "",
            details:
              " We know you’re a networking star and you’re coming to #RiseUp18 to connect with the crowd. Play this special networking game and win a free ticket to the next Summit."
          },
          {
            title: "Rise & Riddle",
            huge: "",
            details:
              "180 startups at #RiseUp18, all waiting to get to know you! Crack the codes, meet the startups, and win a free ticket to the next Summit."
          },
          {
            title: "Digital Bingo",
            huge: "",
            details:
              "Show your crowd how much fun you’re having at #RiseUp18! Play Digital Bingo and post about your experience. We're sharing the best on RiseUp & Brainy Squad pages."
          },

          {
            title: "",
            huge: "",
            details:
              "Visit the Brainy Squad booth at the Greek Campus to start playing! "
          }
        ]
      }
    },

    threeDayAcceleratorProgram: {
      feature: {
        title: "December 7-9th, 2018",
        details: "",
        link:
          "https://flat6labs.wufoo.com/forms/flat6labs-cairo-pitchathon-dec-9-riseup-2018/",
        products: [
          {
            title: "Office Hours with Flat6Labs Team:",
            subTitle: "11:00 - 12:00",
            details:
              " December 7: Pitch Practice with Ramez El-Serafy (CEO of Flat6Labs)"
          },
          {
            details:
              "December 8: Digital Strategy with Fawzi Rahal (Managing Director of Flat6Labs Beirut)"
          },
          {
            details:
              "December 9: Financial Modeling with Marie Therese Fam (Managing Partner at Flat6Labs Cairo)"
          },
          {
            title: "Group Mentoring Sessions:",
            subTitle: "12:00-13:00",
            details:
              "December 7: Scalability & Expansion with Hadeer Walid (Careem)"
          },
          {
            details: "December 8: Customer Aquisition Ahmed Galal (Taskty)"
          },
          {
            details: "December 9: Growth Hacking Ashraf Tawakol (Forty Nine)"
          },
          {
            subTitle: "16:00-17:00 ",
            details:
              "December 7: Operational Expansions with Ahmed Hammouda (Uber)"
          },
          { details: "December 8: Pivoting your startup Amr Saleh (Elkrem)" },

          { details: "December 9: Investments & Fundraising Amal Enan (EAEF)" },
          {
            title: "20 Min Info-Sessions:",
            subTitle: "15:00-16:00",
            details:
              "These are introductory sessions for each of our active accelerators and programs across the region:"
          },
          {
            details: "December 7: Flat6Labs Cairo, StartEgypt & Flat6Labs Tunis"
          },
          {
            details:
              "December 8: Flat6Labs Cairo, StartEgypt & Flat6Labs Bahrain"
          },
          {
            details:
              "December 9: Flat6Labs Cairo, StartEgypt & Flat6Labs Beirut"
          },
          {
            title: "Flat6Labs Cairo Pitchathon: ",
            details:
              "Startups will sign up to get a chance to pitch to us and ultimately get a chance to be invited to our upcoming Flat6Labs Cairo Bootcamp Qualifier event."
          }
        ]
      }
    },
    UXClinicConsultancy: {
      banner: {
        title: "December 7th, 2018",
        bgOpacity: 0.22,
        width: 30
      },
      feature: {
        title: "December 7th, 2018",
        details:
          "The Product and UX Design firm will be sharing the expertise they’ve gathered throughout their 8 years of seamless digital experiences! Their providing you with UX Clinic Consultancy so you can get advice on your startup, app or website! Make sure to apply to get your slot today as spaces are limited.",
        link: "https://riseupco.typeform.com/to/uMxJ8D",
        products: [{}]
      }
    },
    BESTARABICPOSTERS: {
      banner: {
        title: "December 7th, 2018",
        bgOpacity: 0.22,
        width: 30
      },
      feature: {
        title: "December 7-9th, 2018",
        details:
          "With everything going on in the Summit, you’re going to need a place to sit down, absorb all the knowledge, and generate some smash output. To help you get away from the hustle and bustle to close those deals and finish up your pitches, book a meeting room or relax in our co-working areas!",
        link: ""
      }
    },
    FastTrack: {
      banner: {
        title: "December 7th, 2018",
        bgOpacity: 0.22,
        width: 30
      },
      feature: {
        title: "December 7-9th, 2018",
        details:
          "These sessions are open for all Fintech startups. If you’re looking for targeted advice from Fintech experts, you definitely don’t want to miss this one! They’re looking for startups specializing in:",
        link: "https://bit.ly/2KSfSSg",
        products: [
          { huge: "• Payments", details: " " },
          { huge: "• Investments", details: " " },
          { huge: "• Insurance", details: " " },
          { huge: "• Capital Markets", details: " " },
          { huge: "• SME Lending", details: "  " },
          { huge: "• Financial Inclusion", details: " " },
          { huge: "• Identity Authentication", details: " " },
          { huge: "• Blockchain", details: " " }
        ]
      },
      openText:
        "Startups joining the program will receive immediate feedback from experienced entrepreneurs, their management team, and will be able to expand their network meeting other startup founders."
    },
    CareerCoaching: {
      banner: {
        title: "December 7th, 2018",
        bgOpacity: 0.22,
        width: 30
      },
      feature: {
        title: "December 9th, 2018",
        details:
          "Wuzzuf is providing young startups founders with the how-to on how to build their teams and how to choose the best fit calibers. They’re also providing juniors and non-managers professionals with the top-notch advice tips for job search strategies & tools and interviewing tips.",
        link: "",
        products: [
          {
            details: " ",
            huge: "Founders with tips and tricks to build your teams"
          },
          {
            details: " ",
            huge: "Managers with tips and tricks to choose the best fit"
          },
          {
            details: " ",
            huge: "Juniors with tips and tricks to find your dream job"
          },
          {
            details: " ",
            huge: "Job seekers with tips and tricks to ace the interview"
          }
        ]
      }
    },
    ImpactInvestmentRoundtable: {
      banner: {
        title: "December 7th, 2018",
        bgOpacity: 0.22,
        width: 30
      },
      feature: {
        title: "December 8th, 2018",
        details:
          'The event is organized by an active pool of supporting organizations together with The Startup Manifesto under the working group of "Impact Investment & Purpose-driven Organizations". ',
        link:
          "https://www.eventbrite.com/e/impact-investment-roundtable-riseup-satellite-event-tickets-53347502817",
        products: [
          {
            title:
              "The roundtable speakers will dive deep into the following questions:",
            details: " - What's Impact Investment vs. traditional investment?"
          },
          {
            details:
              " - The feasibility of implementing it in Egypt and the region."
          },
          {
            details:
              " - Next steps to take it further, in terms of policy and practice."
          },
          {
            title: "Among the guest speakers are",
            details: " - Shehab El Nawawi, CEO at Giza Systems (Private Sector)"
          },
          {
            details:
              " - Dr. Aisha Saad, Yala Research Scholar in Law (Policies & Academia)"
          },
          {
            details: " - Amr AboDraiaa, Rology Startup (Impact Enterprise)"
          },
          {
            details: " - Ayman Shehata, UNDP / Elre7la (Moderator)"
          }
        ]
      },
      openText:
        "Participants will have an opportunity to interact with the experts in fields of impact investing and purpose-driven ventures and learn from their experiences."
    },

    coworking: {
      banner: {
        title: "ACTIVITIES",
        bgOpacity: 0.22,
        width: 30
      },
      feature: {
        title: "MEET, (CO)WORK, REPEAT!",
        details:
          "With everything going on in the Summit, you’re going to need a place to sit down, absorb all the knowledge, and generate some smash output. To help you get away from the hustle and bustle to close those deals and finish up your pitches, book a meeting room or relax in our co-working areas!"
      }
    }
  },
  Interestes: [
    "e-commerce",
    "fintech",
    "cleantech",
    "creative",
    "science",
    "f&b",
    "travel"
  ],
  Motivations: [
    "connect",
    "hire",
    "invest",
    "raise",
    "sell",
    "exposure",
    "learn"
  ]
}
