import React from 'react'
import PropTypes from 'prop-types'
import Banner from '../commons/banner'

const SpeakerDetails = ({speaker}) => {
	return(
		<div className="single--speaker">
			<Banner
        title={`${speaker.name}`}
        opacity={.5}
        width={85}
        bgurl={require('../../assets/images/homepage.jpg')}
        strength={200}
      />
		</div>
	)
}

SpeakerDetails.propTypes = {
  speaker: PropTypes.object.isRequired
}

export default SpeakerDetails
