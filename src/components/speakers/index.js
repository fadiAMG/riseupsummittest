import React from "react"
// import Proptypes from "prop-types"
import DocumentTitle from "react-document-title"
import Modal from "react-modal"

import FeaturePost from "../commons/FeaturePost"
import HeaderSlider from "../commons/HeaderSlider"
import QuotesCard from "../commons/QuotesCard"

import SpeakersList17 from "./SpeakersList17"
import Search from "./Search"
import SpeakerModalContent from "./SpeakerModal"
import { connect } from "react-redux"
import {
  fetchSpeakers17,
  fetchSpeaker,
  fetchLastSpeakers
} from "../../redux/actions/SpeakersAction"

import { strings } from "../strings"
import $ from "jquery"
class SpeakersComponent extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      searchTerm: "",
      modalIsOpen: false,
      id: null,
      slide: 1,
      speakerLoading: null,
      speaker: null
    }

    this.speakerSearch = this.speakerSearch.bind(this)
  }

  speakerSearch(term) {
    this.setState({ searchTerm: term })
  }

  openModal(id) {
    this.setState({ modalIsOpen: true, id })
  }

  afterOpenModal(id) {
    this.props.fetchSpeaker(this.state.id).then(res => {
      this.setState({
        speaker: res.payload.data.speaker,
        speakerLoading: res.payload.status
      })
    })
  }
  closeModal() {
    this.setState({ modalIsOpen: false, speaker: null, speakerLoading: null })
  }
  componentDidMount() {
    this.props.fetchSpeakers17()
    this.props.fetchLastSpeakers()
    if (!this.props.HomepageView)
      $(window).scrollTop($(".feature--post").offset().top - 100)
  }
  render() {
    const HomepageView = this.props.HomepageView
    const _self = this

    let { speakers17 } = this.props

    let homeSpeakers = speakers17.filter(item => {
      return item.homepage_speaker
    })
    speakers17 = speakers17.filter(item => {
      return item.new_status
    })
    let term = this.state.searchTerm

    if (this.state.searchTerm !== "") {
      speakers17 = speakers17.filter(
        item =>
          (item.name && item.name.toLowerCase().includes(term.toLowerCase())) ||
          (item.job_title &&
            item.job_title.toLowerCase().includes(term.toLowerCase())) ||
          (item.entity_name &&
            item.entity_name.toLowerCase().includes(term.toLowerCase()))
      )
    }

    function renderSpeakers17() {
      if (speakers17) {
        return (
          <SpeakersList17
            speakersList={HomepageView ? homeSpeakers : speakers17}
            ClickSpeaker={id => _self.openModal(id)}
            HomepageView={HomepageView}
          />
        )
      } else {
        return null
      }
    }
    function RenderSpeakerContent() {
      if (_self.props.speaker) {
        return (
          <SpeakerModalContent
            close={() => _self.setState({ modalIsOpen: false })}
            speakerData={_self.state.speaker}
            status={_self.state.speakerLoading}
          />
        )
      } else {
        return null
      }
    }
    function RenderHomepageView() {
      if (HomepageView) {
        return null
      } else {
        return (
          <div>
            <DocumentTitle title="RiseUp Summit - Speakers" />
            <HeaderSlider slider={false} />

            <FeaturePost
              title={strings.Speakers.feature.title}
              details={strings.Speakers.feature.details}
              img={require("../../assets/images/v2/speakers.png")}
              BtnTitle="APPLY TO SPEAK"
              Url="/"
              strength={300}
              gray={true}
              openText=""
              noLink={true}
            />
            <QuotesCard
              quote={
                '"RISEUP IS THE BIGGEST ENTREPRENEURSHIP STARTUP EVENT IN THE REGION"'
              }
              quoteBy={"KINDA IBRAHIM, PARTNERSHIPS DIRECTOR MENA, TWITTER"}
              BackgroundColor={"#2a428c"}
            />
          </div>
        )
      }
    }
    return (
      <div className="SlideUp">
        {RenderHomepageView()}
        <section className="speakers--list current">
          <h3
            className="speakers--head"
            style={{ marginBottom: HomepageView ? 50 : 0 }}
          >
            PREVIOUS SPEAKERS
          </h3>
          <div style={{ display: HomepageView ? "none" : "block" }}>
            <Search onSearchSpeaker={term => this.speakerSearch(term)} />
          </div>
          {renderSpeakers17()}
          <div id="tickets" />
        </section>
        <Modal
          isOpen={this.state.modalIsOpen}
          onAfterOpen={this.afterOpenModal.bind(this)}
          onRequestClose={this.closeModal.bind(this)}
          contentLabel=""
          className={"SpeakersModal"}
        >
          {RenderSpeakerContent()}
        </Modal>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    speakers17: state.Speakers.speakers17,
    SpeakersLast: state.SpeakersLast.SpeakersLast,
    speaker: state.Speaker.speaker
  }
}

SpeakersComponent.defaultProps = {
  HomepageView: false,
  moreButton: false
}
export default connect(
  mapStateToProps,
  { fetchSpeakers17, fetchSpeaker, fetchLastSpeakers }
)(SpeakersComponent)
