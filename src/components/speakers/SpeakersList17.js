import React from "react"
import PropTypes from "prop-types"
import SpeakerItem from "./SpeakerItem"
const SpeakersList17 = ({ speakersList, ClickSpeaker, HomepageView }) => {
  function RenderSpeakers() {
    return speakersList.map((speaker, i) => {
      if (speaker.status || speaker.new_status) {
        return (
          <SpeakerItem
            speaker={speaker}
            key={i}
            ClickSpeaker={id => ClickSpeaker(id)}
          />
        )
      } else {
        return null
      }
    })
  }
  return (
    <div className="speakers--list-current">
      {RenderSpeakers()}
      {HomepageView ? (
        <div className="speaker--item apply">
          <div className="content">
            <a className="see-button" href="/speakers">
              <div>
                <img
                  className="plus"
                  src="https://riseupsummit.com/wp-content/uploads/2017/08/plusicon.svg"
                  alt=""
                />
              </div>
              <div>
                <p>
                  SEE MORE
                  <img
                    src={require("../../assets/arrow-right.svg")}
                    alt="arrow"
                  />
                </p>
              </div>
            </a>
          </div>
        </div>
      ) : null}
    </div>
  )
}

SpeakersList17.propTypes = {
  speakersList: PropTypes.array.isRequired
}

export default SpeakersList17
