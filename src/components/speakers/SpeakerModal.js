import React from "react"

const SpeakerModalContent = ({ speakerData, close, status }) => {
  if (status !== null) {
    return (
      <div className="speaker-modal--preview">
        <div className="speaker--img">
          <img src={`${speakerData.imgUrl}`} alt="" />
        </div>
        <div className="speaker--details">
          <button className="close" onClick={close} />
          <div className="content">
            <h3>
              {speakerData.fname} {speakerData.lname}
            </h3>
            <h5>
              {speakerData.job_title}
              {undefined !== speakerData.entity_name &&
              speakerData.entity_name !== ""
                ? " AT " + speakerData.entity_name
                : null}
            </h5>
            <p>{speakerData.bio}</p>

            {/* <p>{speakerData.session.details}</p> */}
          </div>
          {/* <div className="session--wrap">
											<h4>{speakerData.session.name}</h4>
											<ul>
												<li>{speakerData.session.date}</li>
												<li>{speakerData.session.start} - {speakerData.session.end}</li>
												<li>{speakerData.session.location}</li>
											</ul>
										</div> */}
        </div>
      </div>
    )
  } else {
    return (
      <div className="speaker-modal--preview">
        <div className="speaker--img">
          <img src={require("../../assets/loading-speaker.jpg")} alt="" />
        </div>
        <div className="speaker--details">
          <div className="content">
            <h3>Loading</h3>
            {/* <p>{speakerData.session.details}</p> */}
          </div>
          {/* <div className="session--wrap">
											<h4>{speakerData.session.name}</h4>
											<ul>
												<li>{speakerData.session.date}</li>
												<li>{speakerData.session.start} - {speakerData.session.end}</li>
												<li>{speakerData.session.location}</li>
											</ul>
										</div> */}
        </div>
      </div>
    )
  }
}

export default SpeakerModalContent
