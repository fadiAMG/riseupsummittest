import React from 'react'
import Banner from '../../commons/banner'
import Dropzone from 'react-dropzone'
import {strings} from '../../strings'
class SpeakerDashboard extends React.Component{
	constructor(props) {
	  super(props);
	  this.state = {
	  	login: true
		};
		this.onDrop = this.onDrop.bind(this);
	}
	componentDidMount() {
    let ele = document.querySelector('.fullwidth--seprator');
    let windowHeight = window.innerHeight - 37;
    ele.style.paddingTop = windowHeight / 1.5 +"px";
	}
	onDrop(files) {
    this.setState({
      files
		});
  }
	render(){
		
		return(
			<div className="Speaker--dashboard">
				<Banner
	        title="Speakers Form"
	        opacity={strings.Homepage.banner.bgOpacity}
	        width={strings.Homepage.banner.width}
	        bgurl={require('../../../assets/images/banner.jpg')}
	        strength={200}
	      />
				<div className="fullwidth--seprator"></div>

	      <div className="form--wrap clearfix">			
					<div className="parllex-fix">
						<Dropzone onDrop={this.onDrop.bind(this)}>
							<p>Upload Image</p>
						</Dropzone>
  	      </div>
	      </div>
			</div>
		)
	}
}

export default SpeakerDashboard