import React from 'react';

const Search = (props) => {
  function onInputChange(event){
    let term = event.target.value;
    props.onSearchSpeaker(term);
  }
  return(
    <div className="speakers--search">
      <div className="input--wrap">
        <input
          placeholder={props.placeholder || "Search Name, Job Title or Entity Name..."}
          className="input"
          onChange={(event) => onInputChange(event)}
        />
        <img src={require('../../assets/search.svg')} alt=""/>
      </div>
    </div>
  )
}
export default Search
