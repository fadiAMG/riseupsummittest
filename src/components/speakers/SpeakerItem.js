import React from "react"
import PropTypes from "prop-types"
const SpeakerItem = ({ speaker, ClickSpeaker}) => {
  return (
    <div  key={speaker._id} className="speaker--item" onClick={id => ClickSpeaker(speaker._id)}>
      <div
        className="speaker--avatar"
        style={{background:`url(${ speaker.imgUrl }) 50% 50% no-repeat`,
        WebkitFilter:"grayscale(100%)",
        filter:"grayscale(100%)",
        backgroundPosition:"top"
      }}

      />
      <div className="speaker--info">
        <div className="content">
          <h3 onClick={id => ClickSpeaker(speaker._id)}>
            {speaker.fname} {speaker.lname}
          </h3>
          <h5>
            {speaker.job_title}{" "}
            {speaker.entity_name !== "" && speaker.entity_name ? "AT " : null}
            {speaker.entity_name}
          </h5>
        </div>
      </div>
      {/* <a className="speaker--link">
        <span />
      </a> */}
    </div>
  )
}

SpeakerItem.propTypes = {
  speaker: PropTypes.object.isRequired,
  ClickSpeaker: PropTypes.func.isRequired
}
export default SpeakerItem
