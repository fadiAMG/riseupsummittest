import React from "react"
import PropTypes from "prop-types"
import DocumentTitle from "react-document-title"
import Banner from "../commons/banner"

class SpeakerForm extends React.Component {
  render() {
    return (
      <div className="apply-to-speak">
        <DocumentTitle title="RiseUp Summit - Apply To Speak" />
        <Banner
          title="APPLY TO SPEAK"
          opacity={0.5}
          width={85}
          bgurl={require("../../assets/images/speakers2.jpg")}
          strength={200}
        />
        <div className="fullwidth--seprator" />
        <div className="form--wrap">
          <iframe
            title="SpeakerForm"
            id="typeform-full"
            width="100%"
            height="400px"
            frameBorder="0"
            src="https://riseupsummit2017.typeform.com/to/WmM3Oo"
          />
        </div>
      </div>
    )
  }
}

export default SpeakerForm
