import React from 'react'
import {connect} from 'react-redux'
import {fetchSpeaker} from '../../redux/actions/SpeakersAction'

import SpeakerDetails from './SpeakerDetails'
class SingleSpeaker extends React.Component{
	constructor(props) {
    super(props);
    this.state = {
      speaker: this.props.speaker
    }
  }
  componentWillMount() {
    this.props.fetchSpeaker(this.props.match.params.id);
  }

	render(){
		let { speaker} = this.props
		function renderSpeaker(){
      if(speaker){
        return(
          <SpeakerDetails speaker={speaker}/>
        )
      }else{
        return null;
      }
    }
		return(
			<div>
				{renderSpeaker()}
			</div>
		)
	}	
}
function mapStateToProps(state){
  return{
    speaker: state.Speaker.speaker
  }
}
export default connect(mapStateToProps,{fetchSpeaker})(SingleSpeaker)
