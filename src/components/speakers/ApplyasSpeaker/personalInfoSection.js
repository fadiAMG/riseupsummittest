import React, { Component } from 'react'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AppBar from 'material-ui/AppBar';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';


const styles = {
    background : 'white',
    padding: '20px'
}

export class PersonalInfo extends Component {
    constructor(){
      super()
      this.fname = ""
      this.lname = ""
      this.email = ""
      this.MOBNUM = ""
      this.BIO = ""
      this.PASS= ""
    }
    continue = e => {
        e.preventDefault();
        // console.log(this.fname , this.lname);
        this.props.collectData({
          fname: this.fname,
          lname: this.lname,
          email: this.email,
          mobile_number: this.MOBNUM,
          bio: this.BIO,
          password: this.PASS
        });
        this.props.nextStep();
      };
    
      render() {
        const { values, handleChange , collectData } = this.props;
        return (
          <MuiThemeProvider>
            <div style={styles}>
              <AppBar title="Enter Your Personal Info" />
              <TextField
                hintText="Enter Your First Name"
                floatingLabelText="First Name"
                onChange={e => this.fname = e.target.value}
                defaultValue={this.props.fname}
              />
              
              <TextField
                hintText="Enter Your Last Name"
                floatingLabelText="Last Name"
                onChange={e => this.lname = e.target.value}
                defaultValue={values.lastName}
              />
              
              <TextField
                hintText="Enter Your Email"
                floatingLabelText="Email"
                onChange={e => this.email = e.target.value}
                defaultValue={values.email}
              />
              <br/>
              <TextField
                hintText="Enter Your Phone Number"
                floatingLabelText="Mobile Numer"
                onChange={e => this.MOBNUM = e.target.value}
                defaultValue={values.mobile}
              />
              
              <TextField
                type = "password"
                hintText="At least 8 characters"
                floatingLabelText="Enter Your Password"
                onChange={e => this.PASS = e.target.value}
                defaultValue={values.password}
              />
              <TextField
                fullWidth
                hintText="Tell us more about you"
                floatingLabelText="Bio"
                onChange={e => this.BIO = e.target.value}
                defaultValue={values.bio}
              />
              <br/>
              <RaisedButton
                label="Continue"
                primary={true}
                //style={styles.button}
                onClick={this.continue}
              />
            </div>
          </MuiThemeProvider>
        );
      }
}

export default PersonalInfo
