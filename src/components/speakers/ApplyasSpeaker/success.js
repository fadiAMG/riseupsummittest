import React, { Component } from "react"
import MuiThemeProvider from "material-ui/styles/MuiThemeProvider"
import AppBar from "material-ui/AppBar"
import TextField from "material-ui/TextField"
import RaisedButton from "material-ui/RaisedButton"

const styles = {
  background: "white",
  padding: "20px"
}

export class Success extends Component {
  continue = e => {
    e.preventDefault()
    // console.log(this.fname , this.lname);
    this.props.nextStep()
    this.props.sendform();
  }

  back = e => {
    e.preventDefault()
    this.props.prevStep()
  }
  render() {
    const { values, handleChange, collectData } = this.props
    return (
      <MuiThemeProvider>
        <div style={styles}>
          <AppBar title="Final Step" />

          <br />
          <RaisedButton
            label="Continue&Submit"
            primary={true}
            //style={styles.button}
            onClick={this.continue}
          />
          <RaisedButton
            label="Back"
            primary={false}
            //style={styles.button}
            onClick={this.back}
          />
        </div>
      </MuiThemeProvider>
    )
  }
}

export default Success
