import React, { Component } from "react"
import MuiThemeProvider from "material-ui/styles/MuiThemeProvider"
import AppBar from "material-ui/AppBar"
import TextField from "material-ui/TextField"
import RaisedButton from "material-ui/RaisedButton"
const styles = {
  background: "white",
  padding: "20px"
}

export class SessionINFO extends Component {
  state = {
    Talk: false,
    FireSide: false,
    Panel: false,
    WorkShop101: false,
    DeepDive: false
  }

  toggleTalk = () => {
    this.setState(prevState => ({
      Talk: !prevState.Talk
    }))
  }
  toggleFireSide = () => {
    this.setState(prevState => ({
      FireSide: !prevState.FireSide
    }))
  }
  togglePanel = () => {
    this.setState(prevState => ({
      Panel: !prevState.Panel
    }))
  }
  toggleWorkShop101 = () => {
    this.setState(prevState => ({
      WorkShop101: !prevState.WorkShop101
    }))
  }
  toggleDeepDive = () => {
    this.setState(prevState => ({
      DeepDive: !prevState.DeepDive
    }))
  }

  continue = e => {
    e.preventDefault()
    let arr = []
    for (var key in this.state) {
      if (this.state[key] === true) {
        arr.push(key)
      }
    }

    this.props.collectData({
      session__Type: arr
    })
    this.props.nextStep()
  }

  back = e => {
    e.preventDefault()
    this.props.prevStep()
  }

  render() {
    const { values, handleChange, label, session, attend } = this.props
    return (
      <MuiThemeProvider>
        <div style={styles}>
          <AppBar title="Tell Us More About Your Session" />

          <br />
          <div className="group">
            <label for="PreviousAtten" className="label">
              Have you attended RiseUp Summit before?
            </label>
            <select id="PreviousAtten">
              <option value="Yes"> YES </option>
              <option value="No"> NO </option>
            </select>
          </div>

          <br />

          <div className="group">
            <label className="label">What are your areas of expertise?</label>{" "}
            <br />
            <input
              type="checkbox"
              className="input"
              value={session.A}
              checked={this.state.Talk}
              onChange={this.toggleTalk}
            ></input>{" "}
            {session.A} <br />
            <input
              type="checkbox"
              className="input"
              value={session.B}
              checked={this.state.FireSide}
              onChange={this.toggleFireSide}
            ></input>{" "}
            {session.B} <br />
            <input
              type="checkbox"
              className="input"
              value={session.C}
              checked={this.state.Panel}
              onChange={this.togglePanel}
            ></input>{" "}
            {session.C} <br />
            <input
              type="checkbox"
              className="input"
              value={session.D}
              checked={this.state.WorkShop101}
              onChange={this.toggleWorkShop101}
            ></input>{" "}
            {session.D} <br />
            <input
              type="checkbox"
              className="input"
              value={session.E}
              checked={this.state.DeepDive}
              onChange={this.toggleDeepDive}
            ></input>{" "}
            {session.E} <br />
          </div>
          <br />

          <RaisedButton
            label="Continue"
            primary={true}
            //style={styles.button}
            onClick={this.continue}
          />
          <RaisedButton
            label="Back"
            primary={false}
            //style={styles.button}
            onClick={this.back}
          />
        </div>
      </MuiThemeProvider>
    )
  }
}

export default SessionINFO
