import React, { Component } from "react"
import MuiThemeProvider from "material-ui/styles/MuiThemeProvider"
import AppBar from "material-ui/AppBar"
import TextField from "material-ui/TextField"
import RaisedButton from "material-ui/RaisedButton"
import { Checkbox } from "semantic-ui-react"

const styles = {
  background: "white",
  padding: "20px"
}

export class ExpertiseField extends Component {
  
  state = {
    fintech: false,
    TechForHumans: false,
    EmergingTech: false,
    CreativeCulture: false,
    CreativeEconomics: false,
    SmartCapital: false,
    GrowthHacking: false
  }

  toggleFinTech = () => {
    this.setState(prevState => ({
      fintech: !prevState.fintech
    }))
  }
  toggleTechForHumans = () => {
    this.setState(prevState => ({
      TechForHumans: !prevState.TechForHumans
    }))
  }
  toggleEmergingTech = () => {
    this.setState(prevState => ({
      EmergingTech: !prevState.EmergingTech
    }))
  }
  toggleCreativeCulture = () => {
    this.setState(prevState => ({
      CreativeCulture: !prevState.CreativeCulture
    }))
  }
  toggleCreativeEconomics = () => {
    this.setState(prevState => ({
      CreativeEconomics: !prevState.CreativeEconomics
    }))
  }
  toggleSmartCapital = () => {
    this.setState(prevState => ({
      SmartCapital: !prevState.SmartCapital
    }))
  }
  toggleGrowthHacking = () => {
    this.setState(prevState => ({
      GrowthHacking: !prevState.GrowthHacking
    }))
  }
  continue = e => {
    e.preventDefault()
    let arr = []
    for (var key in this.state) {
      if (this.state[key] === true) {
        arr.push(key)
      }
    }

    this.props.collectData({
      expertise: arr
    })

    this.props.nextStep()
  }

  back = e => {
    e.preventDefault()
    this.props.prevStep()
  }

  render() {
    const { values, handleChange, label } = this.props
    return (
      <MuiThemeProvider>
        <div style={styles}>
          <AppBar title="Tell Us More About Your Expertise" />

          <br />

          <div className="group">
            <label className="label">What are your areas of expertise?</label>{" "}
            <br />
            <input
              type="checkbox"
              className="input"
              value={label.A}
              checked={this.state.fintech}
              onChange={this.toggleFinTech}
            ></input>{" "}
            {label.A} <br />
            <input
              type="checkbox"
              className="input"
              value={label.B}
              checked={this.state.TechForHumans}
              onChange={this.toggleTechForHumans}
            ></input>{" "}
            {label.B} <br />
            <input
              type="checkbox"
              className="input"
              value={label.C}
              checked={this.state.EmergingTech}
              onChange={this.toggleEmergingTech}
            ></input>{" "}
            {label.C} <br />
            <input
              type="checkbox"
              className="input"
              value={label.D}
              checked={this.state.CreativeCulture}
              onChange={this.toggleCreativeCulture}
            ></input>{" "}
            {label.D} <br />
            <input
              type="checkbox"
              className="input"
              value={label.E}
              checked={this.state.CreativeEconomics}
              onChange={this.toggleCreativeEconomics}
            ></input>{" "}
            {label.E} <br />
            <input
              type="checkbox"
              className="input"
              value={label.F}
              checked={this.state.SmartCapital}
              onChange={this.toggleSmartCapital}
            ></input>{" "}
            {label.F} <br />
            <input
              type="checkbox"
              className="input"
              value={label.G}
              checked={this.state.GrowthHacking}
              onChange={this.toggleGrowthHacking}
            ></input>{" "}
            {label.G} <br />
          </div>
          <br />

          <RaisedButton
            label="Continue"
            primary={true}
            //style={styles.button}
            onClick={this.continue}
          />
          <RaisedButton
            label="Back"
            primary={false}
            //style={styles.button}
            onClick={this.back}
          />
        </div>
      </MuiThemeProvider>
    )
  }
}

export default ExpertiseField
