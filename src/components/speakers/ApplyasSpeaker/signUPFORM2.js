import React, { Component } from 'react'
import PersonalInfo from './personalInfoSection';
import ExperienceInfo from './experienceSection';
import ExpertiseField from './expertiseField';
import SessionINFO from './sessionInfo';
import Success from './success';
import SentSuccess from './sent';
import axios from 'axios';



export class SignUPForm_2 extends Component {
    constructor(props){
        super(props)
        this.state = {
            step: 1, 
            firstName: '',
            lastName: '',
            email: '',
            mobile:'',
            jobTitle:'',
            entity:'',
            entityfb:'',
            last_year: [
                { key: 1, text: 'Choice 1', value: 1 },
                { key: 2, text: 'Choice 2', value: 2 },
                { key: 3, text: 'Choice 3', value: 3 },
              ],
            password: '',
            ExpertiseField : {
                A: "FinTech ( Startups, Banking Sector, Government, all are Welcome)",
                B: "Tech for Humans (Edtech,Healthtech,Medtech, Natural language generation , all Technology enhancing the Human Experience)",
                C: "Emerging Tech ( AI,Blockchain, DLTs, Big Data, Robotics, Nanotechnology, 3D printing, 5G, AR/VR)",
                D: "Creative Culture (Film, Fashion, Music, Design, Art, Marketing, Branding, Gaming, Design Thinking, UI/UX, creative entrepreneurs, and creating authentic local content.)",
                E: "Creative Economies ( Business skills for creatives, monetizing creativity)",
                F: "Smart Capital (Raising Funds, Content for Startups & Investors)",
                G: "Growth Hacking (All the tips and Tricks to grow a business, generate leads, retain your customers)"
            },
            twitter: '',
            linkedIn: '',
            bio: '',
            sessionType : {
                A: "Talk (Insightful, inspirational, informative presentations)",
                B: "Fireside Chat (One-on-one. Speakers are asked questions about their personal journey and experiences as an entrepreneur by a fellow speaker)",
                C: "Panel (Discussions and solutions: 3-4 speakers)",
                D: "101 Workshops (Intro session: 1-1.5 hour(s) and up to 100 attendees)",
                E: "Deep Dive Workshops (In-depth knowhow: 2-4 hours and up to 20 attendees)"
            },
            sessionName: '',
            session_desc: '',
            formData: {}
        }
    }
    
    //Append to formData Object at each stage
    appendData = inputData  =>{
        
        this.setState({
            formData:{
                ...this.state.formData,
                ...inputData
            }
            
        });
        
        
    }
    
    //Proceed to next Step 
    nextStep = () => {
        const { step } = this.state;
        this.setState({
            step: step + 1
        });

        console.log(this.state);
    }
    //Back to last  Step 
    prevStep = () => {
        const { step } = this.state;
        this.setState({
            step: step - 1
        });
    }
    // handle Fields Change
    handleChange = input => e => {
        this.setState({
            [input]: e.target.value
        });
    }
    submitAPI = () => {
        axios.post('https://bdev.riseupsummit.com/api/v2/speakers', this.state.formData)
              .then(res => console.log(res));
    }





    render() {
        const {step} = this.state;

        const { firstName , lastName , email , mobile , password , bio , 
                jobTitle , entity , entityfb , twitter , linkedIn , sessionName , session_desc } = this.state;

        const experienceField = this.state.ExpertiseField; 
        const seesionTYPE = this.state.sessionType;
        const attendLastYear = this.state.lastName;

        const personalInfo = { firstName , lastName , email , mobile , password , bio , sessionName , session_desc }
        const experienceInfo = {jobTitle , entity , entityfb , twitter , linkedIn}
        const expertiseField = experienceField
        const attendLY = attendLastYear;
        const session_Type = seesionTYPE;

        switch(step){
            case 1:
                return(
                    <PersonalInfo
                        nextStep={this.nextStep}
                        prevStep={this.prevStep}
                        handleChange ={this.handleChange}
                        values = {personalInfo}
                        collectData = {this.appendData}
                    />
                );
            case 2:
                return(
                    <ExperienceInfo 
                        nextStep={this.nextStep}
                        prevStep={this.prevStep}
                        handleChange ={this.handleChange}
                        values = {experienceInfo}
                        collectData = {this.appendData}
                    />
                );
                
            case 3:
                return(
                    <ExpertiseField 
                        nextStep={this.nextStep}
                        prevStep={this.prevStep}
                        handleChange ={this.handleChange}
                        values = {expertiseField}
                        label = {expertiseField}
                        collectData = {this.appendData}
                    />
                );
            case 4:
                return(
                    <SessionINFO 
                        nextStep={this.nextStep}
                        prevStep={this.prevStep}
                        handleChange ={this.handleChange}
                        attend = {attendLY}
                        session = {session_Type}
                        values = {personalInfo}
                        collectData = {this.appendData}
                    />
                );
            case 5:
                return(
                    <Success
                    nextStep={this.nextStep}
                    prevStep={this.prevStep}
                    handleChange ={this.handleChange}
                    sendform = {this.submitAPI}
                    />
                )
            default:
                return(
                    <SentSuccess
                    />
                );
                
        }
    }
}

export default SignUPForm_2
