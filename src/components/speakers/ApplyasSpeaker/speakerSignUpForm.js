import React from 'react';

const formStyle ={
    background: 'gold',
    opacity:         '0.8',
    height:         '70vh',
    width:          '70vw',
    display:        'flex',
    flexWrap:      'wrap',
    paddingTop:     '10px'
}
const ExpertiseField = {
    A: "FinTech ( Startups, Banking Sector, Government, all are Welcome)",
    B: "Tech for Humans (Edtech,Healthtech,Medtech, Natural language generation , all Technology enhancing the Human Experience)",
    C: "Emerging Tech ( AI,Blockchain, DLTs, Big Data, Robotics, Nanotechnology, 3D printing, 5G, AR/VR)",
    D: "Creative Culture (Film, Fashion, Music, Design, Art, Marketing, Branding, Gaming, Design Thinking, UI/UX, creative entrepreneurs, and creating authentic local content.)",
    E: "Creative Economies ( Business skills for creatives, monetizing creativity)",
    F: "Smart Capital (Raising Funds, Content for Startups & Investors)",
    G: "Growth Hacking (All the tips and Tricks to grow a business, generate leads, retain your customers)"
}

const sessionType = {
    A: "Talk (Insightful, inspirational, informative presentations)",
    B: "Fireside Chat (One-on-one. Speakers are asked questions about their personal journey and experiences as an entrepreneur by a fellow speaker)",
    C: "Panel (Discussions and solutions: 3-4 speakers)",
    D: "101 Workshops (Intro session: 1-1.5 hour(s) and up to 100 attendees)",
    E: "Deep Dive Workshops (In-depth knowhow: 2-4 hours and up to 20 attendees)"
}

const SignUpForm = () => {
    return ( 
    <form style={formStyle}>
        <div className="group">
            <label for="Fname" className="label">First Name</label>
            <input id="Fname" type="text" className="input" placeholder="Enter Your First Name"></input>
        </div>
        <div className="group">
            <label for="Lname" className="label">Last Name</label>
            <input id="Lname" type="text" className="input" placeholder="Enter Your Last Name"></input>
        </div>
        <div className="group">
            <label for="email" className="label">Email Address</label>
            <input id="email" type="email" className="input"  placeholder="Enter Your E-mail"></input>
        </div>
        <div className="group">
            <label for="phoneNo" className="label">Phone Number</label>
            <input id="phoneNo" type="number" className="input"  placeholder="Enter Your E-mail"></input>
        </div>
        <div className="group">
            <label for="pass" class="label">Password</label>
            <input id="pass" type="password" class="input" data-type="password" placeholder="Enter Your Password"></input>
        </div>

        {/* <div className="group">
            <label  className="label">What are your areas of expertise?</label> <br></br>
            <input  type="checkbox" className="input" 
            value = {ExpertiseField.A} >
            </input> {ExpertiseField.A} <br></br>
            <input  type="checkbox" className="input" 
            value = {ExpertiseField.B} >
            </input> {ExpertiseField.B} <br></br>
            <input  type="checkbox" className="input" 
            value = {ExpertiseField.C} >
            </input> {ExpertiseField.C} <br></br>
            <input  type="checkbox" className="input" 
            value = {ExpertiseField.D} >
            </input> {ExpertiseField.D} <br></br>
            <input  type="checkbox" className="input" 
            value = {ExpertiseField.E} >
            </input> {ExpertiseField.E} <br></br>
            <input  type="checkbox" className="input" 
            value = {ExpertiseField.F} >
            </input> {ExpertiseField.F} <br></br>
            <input  type="checkbox" className="input" 
            value = {ExpertiseField.G} >
            </input> {ExpertiseField.G} <br></br>
        </div> */}

        <div className="group">
            <label for="Jtitle" className="label">Job Title</label>
            <input id="Jtitle" type="text" className="input" placeholder="Enter Your Job Title"></input>
        </div>
        <div className="group">
            <label for="Work" className="label">Work </label>
            <input id="Work" type="text" className="input"  placeholder="Where Do you Currently Work"></input>
        </div>
        <div className="group">
            <label for="WorkURL" className="label">Work Website</label>
            <input id="WorkURL" type="url" className="input"  placeholder="Enter Your Work Website if Available"></input>
        </div>
        <div className="group">
            <label for="twitterURL" className="label">Twitter</label>
            <input id="twitterURL" type="url" className="input"  placeholder="Enter Your Twitter if Available"></input>
        </div>
        <div className="group">
            <label for="LinkedInURL" className="label">LinkedIn</label>
            <input id="LinkedInURL" type="url" className="input"  placeholder="Enter Your LinkedIn if Available"></input>
        </div>
        <div className="group">
            <label for="PreviousAtten" className="label">Have you attended RiseUp Summit before?</label>
            <select id = "PreviousAtten">
                <option value = "Yes"> YES </option>
                <option value = "No"> NO </option>
            </select> 
        </div>
        <div className="group">
            <label for="BIO" className="label">About You</label>
            <input id="BIO" type="text" className="input" placeholder="Tell Us More About You"></input>
        </div>
        {/* <div className="group">
            <label  className="label">What format do you want for your session</label> <br></br>
            <input  type="checkbox" className="input" 
            value = {sessionType.A} >
            </input> {sessionType.A} <br></br>
            <input  type="checkbox" className="input" 
            value = {sessionType.B} >
            </input> {sessionType.B} <br></br>
            <input  type="checkbox" className="input" 
            value = {sessionType.C} >
            </input> {sessionType.C} <br></br>
            <input  type="checkbox" className="input" 
            value = {sessionType.D} >
            </input> {sessionType.D} <br></br>
            <input  type="checkbox" className="input" 
            value = {sessionType.E} >
            </input> {sessionType.E} <br></br>
        </div> */}
        
        
        
    </form> );
}
 
export default SignUpForm;