import React, { Component } from 'react'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AppBar from 'material-ui/AppBar';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';


const styles = {
    background : 'white',
    padding: '20px'
}

export class ExperienceInfo extends Component {
  constructor(props){
    super(props)
    this.jobTitle = ""
    this.company = ""
    this.EntityFB = ""
    this.Twitter = ""
    this.LinkedIN = ""
  }
    continue = e => {
        e.preventDefault();
        this.props.collectData({
          job: this.jobTitle,
          entity: this.company,
          entity_fb: this.EntityFB,
          twitter: this.Twitter,
          linkedin: this.LinkedIN,
        });
        this.props.nextStep();
      };


    back = e => {
        e.preventDefault();
        this.props.prevStep();
      };
    
      render() {
        const { values, handleChange } = this.props;
        return (
          <MuiThemeProvider>
            <div style={styles}>
              <AppBar title="Tell Us More About What You Do" />
              
              <TextField
                hintText="What Is your Job Title"
                floatingLabelText="Job"
                onChange={e => this.jobTitle = e.target.value}
                defaultValue={values.job}
              />
              <TextField
                hintText="Where Do you currently Work"
                floatingLabelText="Work Company"
                onChange={e => this.company = e.target.value}
                defaultValue={values.entity}
              />
              
              <TextField
                hintText="Company's Facebook Account"
                floatingLabelText="Company's Facebook"
                onChange={e => this.EntityFB = e.target.value}
                defaultValue={values.entityfb}
              />
              <br/>
              <TextField
                hintText="Your Twitter"
                floatingLabelText="Twitter Profile"
                onChange={e => this.Twitter = e.target.value}
                defaultValue={values.twitter}
              />
              <TextField
                hintText="Your LinkedIn"
                floatingLabelText="LinkedIn Profile"
                onChange={e => this.LinkedIN = e.target.value}
                defaultValue={values.linkedIn}
              />
              <br/>
              <RaisedButton
                label="Continue"
                primary={true}
                //style={styles.button}
                onClick={this.continue}
              />
              <RaisedButton
                label="Back"
                primary={false}
                //style={styles.button}
                onClick={this.back}
              />
            </div>
          </MuiThemeProvider>
        );
      }
}

export default ExperienceInfo
