import React, {Component} from 'react'
import {Redirect, NavLink} from 'react-router-dom'
import {connect} from 'react-redux'
import FeaturePost from '../commons/FeaturePost'
import Sessions from './Sessions'
import Steps from './steps'
import $ from 'jquery'
class MyExperience extends Component{
  componentDidMount () {
    $('.main--menu').addClass('v2');
    $("html, body").animate({scrollTop: 0})
  }
  componentWillUnmount(){
    $('.main--menu').removeClass('v2');
  }
  
  render(){
    const {user, isAuth, isLoading} = this.props;
    return(
      <div>
        {
          isAuth && isLoading ? (
            <div className="experience--wrap" style={{marginTop:50}}>
               {/* <FeaturePost 
                title   = {user.name.toUpperCase()}
                details = {user.details}
                img     = ""
                BtnTitle = "COMPLETE PROFILE "
                Url="/edit-my-experience"
                strength={300}
                slog={`${user.City}, ${user.country}`}
                seprator={false}
                openText="FOR RISEUP TO RECOMMEND THE MOST RELEVANT EVENTS TO YOU"
                img={user.avater}
                job={`${user.job_title} @ ${user.Name_of_Entity}`}
              />  */}
              <div className="banner--wrap">
                <h1>Hi, {user.fname ? user.fname.toUpperCase() : null}</h1>
                <p>FOR RISEUP TO RECOMMEND THE MOST RELEVANT EVENTS TO YOU</p>
                <NavLink className="link" to="/edit-my-experience" style={{marginBottom:10}}>
                  Complete your profile 
                  <img src={require('../../assets/arrow-right.svg')} alt=""/>
                </NavLink> 
                <NavLink className="link" to="/my-sessions" style={{marginLeft:10, marginBottom:10}}>
                  MY workshops
                  <img src={require('../../assets/arrow-right.svg')} alt=""/>
                </NavLink>
                
              </div>
               {/* <section className="section--wrap">
                <h3>RISEUP RECOMMENDS</h3>
                <Sessions/>
              </section> */}

              <section className="section--wrap">
                <h3>NEXT STEPS</h3>
                <Steps/>
              </section> 
            </div>
          ) : (
            <Redirect to="/login-with-tickets"/>
          )
        }
      </div>
    )
  }
}

function mapStateToProps(state){
  return {
    user: state.authTicket.user,
    isAuth: state.authTicket.isAuth,
    isLoading: state.authTicket.isLoading
  }
}

export default connect(mapStateToProps)(MyExperience);