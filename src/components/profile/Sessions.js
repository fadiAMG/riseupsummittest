import React from 'react';
import axios from 'axios';
import {connect} from 'react-redux';
import Modal from 'react-modal'
// import OwlCarousel from 'react-owl-carousel2'
import {fetchSessions} from '../../redux/actions/sessions/SessionsActions';

const BOOK_SESSION_API = "https://bdev.riseupsummit.com/api/sessionsdata"


class Sessions extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      sessions:[],
      modalIsOpen: false,
      type:'',
      item:{},
      success101:false,
      successMsg:'',
      successDeep:false,
      iframeUrl:''
    }
  }
  
  componentWillMount () {
    console.log('asdasd');
    this.props.fetchSessions().then(res => this.setState({sessions:res.payload.data.sessions}))
  }
  openModal(id, type) {
    this.setState({modalIsOpen: true, id, type: type});
  }

  afterOpenModal() {
    this.state.sessions.map(item => {
      if(item._id === this.state.id){
        this.setState({item:item})
      }
    });
  }
  closeModal(){
    this.setState({
      modalIsOpen: false,
      item:{}, 
      type:'', 
      id:null, 
      successDeep:false,
      iframeUrl:'', 
      loading:false,
      success101:false
    })
  }

  bookSession101(id){
    let _self = this;
    let token = localStorage.token;
    axios.defaults.headers.common['Authorization'] = token;
    axios.post(`${BOOK_SESSION_API}/${id}/book`).then(res => {
      this.setState({success101: res.data.success, successMsg:res.data.message})
      setTimeout(function(){
        _self.setState({modalIsOpen:false})
      },5500)
    }).catch((err) => {
      // alert(err.response.data.error)
    })
  }

  bookSessionDeep(id){
    let _self = this;
    let token = localStorage.token;
    axios.defaults.headers.common['Authorization'] = token;
    this.setState({loading:true})
    axios.post(`${BOOK_SESSION_API}/${id}/book`).then(res => {
      this.setState({successDeep: res.data.success, iframeUrl:res.data.typeFormUrl, loading:false})
    }).catch((err) => {
      // alert(err.response.data.error)
    })
  }

  
  render(){
    let _self = this;
    function RenderCorpModalContent(){
      if(_self.state.type === '101s'){
        return(
          <div className="speaker-modal--preview session--preview session--details">
            <div className="speaker--details profile--session">
              <button className="close" onClick={() => _self.closeModal()}></button>
              <div className="content">
                {
                  !_self.state.success101 ? (
                    <div>
                        <h2 className="msg--text">YOUR'RE STEP AWAY TO REGISTER TO THIS SESSION PLEASE CHECK BELOW DETAILS AND THEN CLICK CONFIREM</h2>
                        <h3>{_self.state.item.title} <br/></h3>
                        <h5>{_self.state.item.subTitle}</h5>
                        <ul className="main--list">
                          <li><i className="fa fa-calendar"></i> {_self.state.item.date} DAY</li>
                          <li><i className="fa fa-clock-o"></i> {_self.state.item.start_time} - {_self.state.item.end_time}</li>
                          <li><i className="fa fa-map-marker"></i> {_self.state.item.venue} {_self.state.item.subVenue? '-':''} {_self.state.item.subVenue}</li>
                        </ul>

                        <p className="details">
                          {_self.state.item.description}
                        </p>
                        <a onClick={() => _self.bookSession101(_self.state.item._id)} className="apply--session-button">Register</a>
                      </div>
                  ) : (
                    <div className="success--session--content">
                      <span className="fa fa-check-circle-o"></span>
                      <h3>{_self.state.successMsg} SESSION</h3>
                      <h6>For any questions, applicants should contact tamara@riseup.co</h6>
                    </div>
                  )
                }
              </div>
            </div>
          </div>
        )
      }else{
        // DEEP DIVE
        return(
          <div className="speaker-modal--preview session--preview session--details">
            <div className="speaker--details profile--session">
              <button className="close" onClick={() => _self.closeModal()}></button>
              <div className="content">
                {
                  _self.state.loading ? (
                    <div className='LoadingComponent'>
                      <img width="80" src={require('../../assets/loading.svg')} alt='Loading...'/>
                    </div>
                  ) : null
                }
                {
                  !_self.state.successDeep ? (
                    <div>
                        <h2 className="msg--text">YOUR'RE STEP AWAY TO REGISTER TO THIS SESSION PLEASE CHECK BELOW DETAILS AND THEN CLICK CONFIREM</h2>
                        <h3>{_self.state.item.title} <br/></h3>
                        <h5>{_self.state.item.subTitle}</h5>
                        <ul className="main--list">
                          <li><i className="fa fa-calendar"></i> {_self.state.item.date} DAY</li>
                          <li><i className="fa fa-clock-o"></i> {_self.state.item.start_time} - {_self.state.item.end_time}</li>
                          <li><i className="fa fa-map-marker"></i> {_self.state.item.venue} {_self.state.item.subVenue? '-':''} {_self.state.item.subVenue}</li>
                        </ul>

                        <p className="details">
                          {_self.state.item.description}
                        </p>
                        <a onClick={() => _self.bookSessionDeep(_self.state.item._id)} className="apply--session-button">APPLY</a>
                      </div>
                  ) : (
                    <div className="success--session--content">
                      <div>
                        <iframe id="typeform-full" width="100%" height="400px" frameBorder="0" src={_self.state.iframeUrl}></iframe>
                      </div>
                    </div>
                  )
                }
              </div>
            </div>
          </div>
        )
      }
    }
    function renderSessionItem(){
      if(true){
        return _self.state.sessions.map((item, i) => {
          if(item.session_types.includes('Deep Dives') || item.session_types.includes('101s')){
            return(
              <div className="item">
                <div className="banner">
                  <div className="content">
                    <p className="date">{item.date} DAY</p>
                    <h4>{item.title.substring(0,30)}...</h4>
                    {/* <h5>BY JESSICA HECK</h5> */}
                  </div>
                </div>
                <div className="content--info">
                  <ul>
                    {
                      item.session_types.map(list => <li>{list}</li>)
                    }
                  </ul>
              
                  {
                    item.bookedByMe == false ? (
                      item.bookingStatus != 'Completed' ? (
                        item.session_types.includes('101s') ? (
                          <button 
                            onClick={() => _self.openModal(item._id,'101s') } 
                            className="btn btn-default">
                            APPLY NOW
                          </button>
                        ) : (
                          <button 
                            onClick={() => _self.openModal(item._id,'deep')} 
                            className="btn btn-default">
                            Register
                          </button>
                        ) 
                      ) : (
                        <p>closed</p>
                      )

                    ) : (
                      <button 
                        className="btn btn-default">
                        {item.bookedByMe}
                      </button>
                    )
                  }
                  
                  
                </div>
              </div>
            )
          }
        })
      }else{
        return null
      }
    }

    return(
      <div className="sessions--list my-experience">
        {renderSessionItem()}
        <Modal
            isOpen={this.state.modalIsOpen}
            onAfterOpen={this.afterOpenModal.bind(this)}
            onRequestClose={this.closeModal.bind(this)}
            contentLabel=""
            className={{
              base: ''
            }}
          >
          {RenderCorpModalContent()}
        </Modal>
      </div>
    )
  }
}


export default connect(null,{fetchSessions})(Sessions)

{/* <OwlCarousel ref="car" options={options}>
  {renderSliderItem()}
</OwlCarousel> */}