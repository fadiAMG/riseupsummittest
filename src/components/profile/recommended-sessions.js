// import React from 'react';
// import {connect} from 'react-redux';
// import OwlCarousel from 'react-owl-carousel2'
// import {fetchSessions} from '../../redux/actions/sessions/SessionsActions';

// const options = {
//     dots:false,
//     autoplay: true,
//     nav:true,
//     navText:['<i class="fa fa-chevron-left"></i>', '<i class="fa fa-chevron-right"></i>'],
//     responsive:{
//       1200:{
//         items:4
//       },
//       768:{
//           items:2
//       },
//       360:{
//           items:1
//       }
//   }
// }
// let RecommendedSessionsArray = [
//   {
//     title:"MEET THE TALENT:HIRE AND GET HIRED"
//   },
//   {
//     title:"MEET THE TALENT:HIRE AND GET HIRED"
//   },
//   {
//     title:"MEET THE TALENT:HIRE AND GET HIRED"
//   },
//   {
//     title:"MEET THE TALENT:HIRE AND GET HIRED"
//   },
//   {
//     title:"MEET THE TALENT:HIRE AND GET HIRED"
//   }
// ]

// class RecommendedSessions extends React.Component{
//   constructor(props){
//     super(props);
//     this.state = {
//       slider:false
//     }
//   }
  
//   componentWillMount () {
//     this.props.fetchSessions().then(res => this.setState({sessions:res.payload.data.sessions}))
//   }
  
//   componentDidMount () {
//     this.setState({slider:true})
//   }
  
//   render(){
//     function renderSliderItem(){
//       if(true){
//         return RecommendedSessionsArray.map((item, i) => {
//           return(
//             <div className="item">
//               <a href="">
//                 <div className="img-wrap">
//                   <img src="https://2016.riseupsummit.com/wp-content/uploads/2017/10/IMG_8882.png" alt={item.title}/>
//                 </div>
//                 <div className="content">
//                   <p>{item.title}</p>
//                 </div>
//                 </a>
//             </div>
//           )
//         })
//       }else{
//         return null
//       }
//     }

//     return(
//       <div className="recommended-sessions">
//         {/* {
//           this.state.slider ? (
//             {renderSliderItem()}
//           ) : null
//         } */}
//         {renderSliderItem()}
//       </div>
//     )
//   }
// }


// export default connect(null,{fetchSessions})(RecommendedSessions)

// {/* <OwlCarousel ref="car" options={options}>
//   {renderSliderItem()}
// </OwlCarousel> */}