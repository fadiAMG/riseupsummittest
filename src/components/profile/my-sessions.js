import React, { Component } from "react"
import { Redirect } from "react-router-dom"
import axios from "axios"
import { connect } from "react-redux"
import $ from "jquery"
const MY_SESSIONS =
  "https://bdev.riseupsummit.com/api/newsessionsdata/mysessions"
const UNBOOK_SESSION = "https://bdev.riseupsummit.com/api/newsessionsdata"
class MySessionsList extends Component {
  constructor(props) {
    super(props)
    this.state = {
      sessions: []
    }
  }
  componentDidMount() {
    $("html, body").animate({ scrollTop: 0 })
    if (this.props.isAuth && this.props.isLoading) {
      this.getSessions()
    }
    $(".main--menu").addClass("v2")
  }
  componentWillUnmount() {
    $(".main--menu").removeClass("v2")
  }
  getSessions() {
    axios.get(MY_SESSIONS).then(res => {
      this.setState({ sessions: res.data.sessions })
    })
  }
  UnBookSession(id) {
    let token = localStorage.token
    axios.defaults.headers.common["Authorization"] = token
    axios.post(`${UNBOOK_SESSION}/${id}/unbook`).then(res => {
      this.getSessions()
    })
  }
  render() {
    const { isAuth, isLoading } = this.props
    return (
      <div>
        {isAuth && isLoading ? (
          <div className="experience--wrap" style={{ marginTop: 50 }}>
            <section className="section--wrap">
              <h3>YOUR REGISTERED WORKSHOPS</h3>
              <div className="table-responsive container">
                <table className="table table-bordered">
                  <thead>
                    <tr>
                      <th>status</th>
                      <th>Session Title</th>
                      <th>Date</th>
                      <th>Start Time</th>
                      <th>End Time</th>
                      <th>Venue</th>
                      <th>#</th>
                    </tr>
                  </thead>
                  <tbody>
                    {this.state.sessions.map(item => {
                      return (
                        <tr>
                          <td>{item.status}</td>
                          <td>{item.title}</td>
                          <td>{item.date}</td>
                          <td>{item.start_time}</td>
                          <td>{item.end_time}</td>
                          <td>{item.venue}</td>
                          <td>
                            <a
                              className="btn btn-danger"
                              onClick={() => this.UnBookSession(item.id)}
                            >
                              UNREGISTER
                            </a>
                          </td>
                        </tr>
                      )
                    })}
                  </tbody>
                </table>
              </div>
            </section>
          </div>
        ) : (
          <Redirect to="/login-with-tickets" />
        )}
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    isAuth: state.authTicket.isAuth,
    isLoading: state.authTicket.isLoading
  }
}
export default connect(mapStateToProps)(MySessionsList)
