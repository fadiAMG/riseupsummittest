import React, { Component } from 'react'
import USER_PROFILE_PAGE from './userInfo';
import Modal from 'react-modal';


const customStyles = {
    content : {
      top                   : '50%',
      left                  : '50%',
      right                 : 'auto',
      background            : 'white',
      bottom                : 'auto',
      marginRight           : '-50%',
      transform             : 'translate(-50%, -50%)',
      height               : '65vh'
    },
    
  }; 

export class USER_INFO_MODAL extends Component {

    constructor() {
        super();
    
        this.state = {
          modalIsOpen: false
        };
    
        this.openModal = this.openModal.bind(this);
        this.afterOpenModal = this.afterOpenModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
      }
      openModal() {
        this.setState({modalIsOpen: true});
      }
    
      afterOpenModal() {
        // references are now sync'd and can be accessed.
        //this.subtitle.style.color = '#f00';
      }
    
      closeModal() {
        this.setState({modalIsOpen: false});
      }
    render() {
        return (
            <div>
            <img
             src= 'https://react.semantic-ui.com/images/avatar/large/matthew.png'
             onClick={this.openModal} 
             style={{color: 'white' , width: '40px' , borderRadius: '50%'}}
             />

            <Modal
              isOpen={this.state.modalIsOpen}
              onAfterOpen={this.afterOpenModal}
              onRequestClose={this.closeModal}
              style={customStyles}
            >
                <USER_PROFILE_PAGE/>
            </Modal>
          </div>
        )
    }
}

export default USER_INFO_MODAL
