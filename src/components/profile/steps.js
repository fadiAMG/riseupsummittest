import React from 'react';
import {NavLink} from 'react-router-dom'
const Steps = () => {
  return(
    <div className="steps--wrap">
      <div className="item">
        <h4>WORKSHOPS</h4>
        <ul className="content">
          <li><NavLink to="/workshops">REGISTER TO 101S OR APPLY TO DEEP DIVES</NavLink></li>
        </ul>
      </div>
      <div className="item">
        <h4>NETWORK</h4>
        <ul className="content">
          <li><button disabled >EXPLORE ATTENDEES (Coming Soon!)</button></li>
          <li><button disabled>RESERVE MEETING SPACE (Coming Soon!)</button></li>
        </ul>
      </div>
      <div className="item">
        <h4>PROFILES</h4>
        <ul className="content">
          <li><NavLink to="/edit-my-experience">COMPLETE YOUR PROFILE</NavLink></li>
          <li><NavLink to="/signup">CREATE INVESTOR PROFILE</NavLink></li>
        </ul>
      </div>
      <div className="item">
        <h4>MORE</h4>
        <ul className="content">
          <li><NavLink to="/discover-egypt">BOOK HOTEL/DISCOVER EGYPT</NavLink></li>
          <li><a target="_blank" href="https://docs.google.com/forms/d/e/1FAIpQLSfDvaSAJsyq88isGUHZwCV0tjrviuazXAAfDgAiG7rn5dDgkA/viewform">KIDS CORNER REGISTRATION</a></li>
        </ul>
      </div>
      
    </div>
  )
}
export default Steps