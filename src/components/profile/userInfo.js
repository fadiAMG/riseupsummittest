import React, { Component } from 'react'
import axios from 'axios';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import TextField from 'material-ui/TextField';
import { Label, Button } from 'semantic-ui-react';
import { RaisedButton } from 'material-ui';
// import { NavLink } from "react-router-dom"
import Modal from "react-modal"
import { Redirect } from "react-router-dom"
import { connect } from "react-redux"
import {
  LoginActionWithTickets,
  LoginSpeaker,
  GetCurrentUserWithTickets
} from "../../redux/actions/myExperience/authActions"
import ValidateLogin from "../../validation/LoginValidation"


const styles = {
    userCard: {
        width: '32rem',
        backgroundColor: '#FCE100',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        margin: '2rem',
        boxShadow: '.5rem .8rem 3rem rgba(0,0,0,.3)',
        borderRadius:  '15px', 
        flexWrap: 'wrap',
        alignContent: 'center'
    }, 
    userCardDetails: {
        backgroundColor: '#FCE100',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        margin: '2rem',
        boxShadow: '.5rem .8rem 3rem rgba(0,0,0,.3)',
        borderRadius:  '15px', 
        alignContent: 'center',
        padding: '25px'
    }, 
    cardImg : {
        width: '100%',
        height: '26rem',
        objectFit: 'cover',
        webkitClipPath: 'polygon(50% 0%, 100% 0, 100% 60%, 75% 100%, 25% 100%, 0% 60%, 0 0)',
        clipPath: 'polygon(50% 0%, 100% 0, 100% 60%, 75% 100%, 25% 100%, 0% 60%, 0 0)',
    },
    inputfield : {
        border: '0',
        textAlign: 'center',
        background: 'rgba(0,0,0,0)',
        '&::placeholder':{
            textOverflow: 'ellipsis !important',
            color: 'blue'
        }
    },
    

}

export class USER_PROFILE_PAGE extends Component {
    constructor(props){
        super(props);
        this.state = {
            userPhoto: 'https://react.semantic-ui.com/images/avatar/large/steve.jpg',
            username: '',
            email:    '',
            facebook: '',
            twitter:  '',
            city:     '',
            phoneNum: '',
            tags:     ['TECH' , 'SOFTWARE DEVELOPMENT' , 'FREELANCER'],
            position: '',
            company:  '',
            role:     'ATTENDEE',
            country:  '',
            userID:   '',
        }
    }
    componentDidMount(){
        this.getUserInfo()
    }
    getUserInfo = () =>{
        let token = localStorage.getItem('token');
        // console.log(token);
        axios.get('https://bdev.riseupsummit.com/api/my-profile', { headers: { Authorization: token } })
        .then(response => {
            // If request is good...
            console.log(response.data);
                
                this.setState({
                    username: `${response.data.profile.fname} ${response.data.profile.lname}`,
                    email: `${response.data.profile.email}`,
                    userID: `${response.data.profile._id}`
                    
                })
            
        })
        .catch((error) => {
            console.log('error ' + error);
        });
    }

    handleChange(e, field) {
        this.setState({
          [field]: e.target.value
        });
      }


    // onUpdate = () => {
    //     let token = localStorage.getItem('token');
    //     axios("https://bdev.riseupsummit.com/api/v2/user", {
    //     method: "patch",
    //     headers: {Authorization: token},
    //     data: {
    //         country: this.state.country,
    //         phone: this.state.phoneNum,
    //         city: this.state.city,
    //         facebook_link: this.state.facebook,
    //         job_title: this.state.position
    //     }, 
        
    //     withCredentials: true
    //     })
    // }




    /********************************************************************** */
    onUpdate() {
        let _self = this
        let token = localStorage.token
        
          axios.defaults.headers.common["Authorization"] = token
          axios
            .patch('https://bdev.riseupsummit.com/api/v2/user', {
                country: "sdasdasdds"
            })
            
            .catch(err => {
              console.log(err, "ERORR");
            })
        
      }

    /************************** */

    render() {
        return (
            <React.Fragment>
            <div style = {{display: 'flex' , flexDirection: 'row' , flexWrap: 'wrap'}}>
                <div styles= {{display: 'flex', alignItems: 'center', alignContent: 'center', flexDirection: 'column'}}>
                    <div style= {{background: 'blue'}} style = {styles.userCard}>
                        <img style = {styles.cardImg} src = {this.state.userPhoto}  alt="user-photo" />
                        <h2 className="user-email"> {this.state.username} </h2>
                        {/* <input 
                        type = 'text' 
                        placeholder = {this.state.username} 
                        style = {styles.inputfield} 
                        onChange={ e => this.handleChange(e,"username")}/> */}
                        <h4 className="user-email"> {this.state.email} </h4>
                        <h4 className="user-email"> {this.state.role} </h4>
                    </div>

                </div>
                <div style = {{display: 'flex' , flexDirection: 'column'}}>
                    <div styles= {{display: 'flex', alignItems: 'center', alignContent: 'center', flexDirection: 'column'}}>
                        <div style= {{background: 'blue'}} style = {styles.userCardDetails}>
                        <MuiThemeProvider>
                            <div>
                            <Label> Phone Number : </Label>
                            <TextField
                             hintText="Update Your Phone Number"
                             floatingLabelText={this.state.phoneNum}
                             onChange={e => this.handleChange(e,"phoneNum")}
                             style = {styles.inputfield}
                              />
                              <Label> Job Title : </Label>
                            <TextField
                             hintText="Update Your Job Title"
                             floatingLabelText={this.state.position}
                             onChange={e => this.handleChange(e,"position")}
                             style = {styles.inputfield}
                              />
                              <br/>
                            <Label> Country : </Label>
                            <TextField
                             hintText="Change Your Country"
                             floatingLabelText={this.state.country}
                             onChange={e => this.handleChange(e,"country")}
                             style = {styles.inputfield}
                              />
                            <Label> City : </Label>
                            <TextField
                             hintText="Change Your City"
                             floatingLabelText={this.state.city}
                             onChange={e => this.handleChange(e,"city")}
                             style = {styles.inputfield}
                              />
                              <br/>
                            <Label> Company : </Label>
                            <TextField
                             hintText="Change Your Company"
                             floatingLabelText={this.state.company}
                             onChange={e => this.handleChange(e,"company")}
                             style = {styles.inputfield}
                              />
                            <Label> Facebook : </Label>
                            <TextField
                             type= "link"
                             hintText="Change Your FaceBook"
                             floatingLabelText={this.state.facebook}
                             onChange={e => this.handleChange(e,"facebook")}
                             style = {styles.inputfield}
                              />
                            
                            </div>
                            
                        </MuiThemeProvider>
                        </div>
                        
                    </div>
                    <div className="update-profile">
                        <button 
                        onClick = {this.onUpdate}
                        className="btn-red"
                        > Update Profile </button>
                    </div>
                </div>
                
                
            </div>
            </React.Fragment>
        )
    }
}

function mapStateToProps(state) {
    return {
      isAuth: state.authTicket.isAuth,
      isLoading: state.authTicket.isLoading
    }
  }

export default USER_PROFILE_PAGE
