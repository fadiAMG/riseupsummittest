import React from "react"
import PropTypes from "prop-types"

import classnames from "classnames"
const TextFieldGroup = ({
  field,
  value,
  label,
  error,
  type,
  onChange,
  placeholder,
  disabled
}) => {
  return (
    <div className={classnames("form-group", { "has-error": error })}>
      <label className="control-label">{label}</label>
      <input
        onChange={onChange}
        type={type}
        value={value}
        name={field}
        className="form-control"
        placeholder={placeholder}
        disabled={disabled}
      />
      {error && <div className="error-popover">{error}</div>}
    </div>
  )
}

TextFieldGroup.proptypes = {
  field: PropTypes.string.isRequired,
  // value: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  placeholder: PropTypes.string,
  error: PropTypes.string,
  disabled: PropTypes.bool,
  type: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired
}

TextFieldGroup.defaultProps = {
  type: "text"
}
export default TextFieldGroup
