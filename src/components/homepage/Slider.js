import React from "react"
// import { history } from "react-router-dom"
import Modal from "react-modal"

class Slider extends React.Component {
  handleClick(path) {
    this.history.push(`/${path}`)
  }
  render() {
    return (
      <div>
        <div id="carouselContainer" className="carousel slide">
          <div className="carousel-inner Homepage--slider">
            <a className="item active" href="/startup-station">
              <img
                alt=""
                className="slider-first"
                src={require("../../assets/images/StartupStation.jpg")}
              />
              <div
                className="slider--caption"
                // onClick={this.handleClick("startup-station").bind(this)}
              >
                <p className="title">STARTUP OPPORTUNITIES</p>
                <p className="sub-title">STARTUP STATION</p>
                <p className="description">
                  The Summit is fantastic for walking around and networking, but
                  nothing is as impressive as having your own space. Take a
                  booth, pimp it out, and exhibit at the Startup Station
                </p>
              </div>
            </a>
            <a className="item" href="/mena-hipos">
              <img
                alt=""
                className="slider-second"
                src={require("../../assets/images/SatelliteEvents.jpg")}
              />
              <div
                className="slider--caption"
                // onClick={this.handleClick("mena-hipos").bind(this)}
              >
                <p className="title">STARTUP OPPORTUNITIES</p>
                <p className="sub-title">HIPO STARTUPS</p>
                <p className="description">
                  Join this premium startup exposure program exclusive to HIgh
                  POtential startups! Last year’s lineup of future unicorns
                  included Instabug, Vezeeta, Mumzworld, and Bayzat
                </p>
              </div>
            </a>
            <a className="item" href="/pitch-competition">
              <img
                alt=""
                className="slider-third"
                src={require("../../assets/images/PitchCompetition.jpg")}
              />
              <div
                className="slider--caption"
                // onClick={this.handleClick("pitch-competition").bind(this)}
              >
                <p className="title">STARTUP OPPORTUNITIES</p>
                <p className="sub-title">PITCH COMPETITION</p>
                <p className="description">
                  We Know your idea is worth it and that your startup can make
                  it big, it&apos;s time to prove it! Compete in the #RiseUp18
                  Pitch Competition and show the world what you&apos;ve got.
                </p>
              </div>
            </a>
          </div>

          <a
            className="left carousel-control"
            href="#carouselContainer"
            data-slide="prev"
          >
            <span className="glyphicon glyphicon-chevron-left" />
            <span className="sr-only">Previous</span>
          </a>
          <a
            className="right carousel-control"
            href="#carouselContainer"
            data-slide="next"
          >
            <span className="glyphicon glyphicon-chevron-right" />
            <span className="sr-only">Next</span>
          </a>
        </div>
      </div>
    )
  }
}

export default Slider
