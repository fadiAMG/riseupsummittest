import React from "react"

import ReversedFeaturePostWithBlockQuote from "../commons/ReversedFeaturePostWithBlockQuote"

class bottomSlider extends React.Component {
  componentDidMount() {}
  render() {
    return (
      <div>
        <div
          id="featureCarousel"
          className="carousel slide"
          data-ride="carousel"
          data-pause="hover"
        >
          <div className="carousel-inner feature--slider">
            <div className="item active">
              <ReversedFeaturePostWithBlockQuote
                title="SPEAK AT THE SUMMIT"
                blockQuote="You'd be hard-pressed to come to an event like RiseUp and not say there's something interesting going on here"
                quote="Sarah Heck, Head of Entrepreneurship at Stripe, speaking after her talk at Summit 17."
                details="Ready to step onto one of MENA's biggest stages and share your thoughts with the world? Apply to speak at #Summit18"
                img={require("../../assets/images/applySpeaker.jpg")}
                BtnTitle="APPLY AS A SPEAKER"
                Url="https://riseupco.typeform.com/to/d7o6da"
                strength={300}
                slog={""}
                bgColor={"#FCE100"}
              />
            </div>

            <div className="item">
              <ReversedFeaturePostWithBlockQuote
                title="BE OUR PARTNER"
                blockQuote="RiseUp2017 has been our most engaged event so far! The  RiseUp Summit Event App reached 95% engagement rate."
                quote="Published by Eventtus Team, the developers of the Summit App"
                details="Every year, we collaborate with around 100 orgainzations to build the Summit. From small creative startups to large multinational corporations, Summit is our partners' as much as it is our own. Apply to become a partner at #Summit18"
                img={require("../../assets/images/applyPartner.jpg")}
                BtnTitle="APPLY AS A PARTNER"
                Url="https://riseupco.typeform.com/to/ciWzvP"
                strength={300}
                slog={""}
                bgColor={"#FCE100"}
              />
            </div>
            <div className="item" style={{ minWidth: "550px" }}>
              <ReversedFeaturePostWithBlockQuote
                title="STARTUP OPPORTUNITIES"
                description="Expose your idea to hundreds of investors by participating at the Summit! You can take a booth at the Startup Station or pitch at the RiseUp Pitch Competition."
                img={require("../../assets/images/Startup_Opps_Image.jpg")}
                BtnTitle="Apply as a startup"
                Url="http://eepurl.com/dHVY7z"
                strength={300}
                slog={""}
                bgColor={"#FCE100"}
              />
            </div>
            <a
              className="left carousel-control"
              href="#featureCarousel"
              data-slide="prev"
              style={{ width: "4%" }}
            >
              <span className="glyphicon glyphicon-chevron-left" />
              <span className="sr-only">Previous</span>
            </a>
            <a
              className="right carousel-control"
              href="#featureCarousel"
              data-slide="next"
            >
              <span className="glyphicon glyphicon-chevron-right" />
              <span className="sr-only">Next</span>
            </a>
          </div>
        </div>
      </div>
    )
  }
}

export default bottomSlider
