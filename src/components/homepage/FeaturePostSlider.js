import React from "react"

import ReversedFeaturePostWithBlockQuote from "../commons/ReversedFeaturePostWithBlockQuote"

class FeaturePostSlider extends React.Component {
  render() {
    return (
      <div>
        <div
          id="featureCarousel"
          className="carousel slide"
          data-ride="carousel"
          data-pause="hover"
        >
          <div className="carousel-inner feature--slider">
            <div className="item active">
              <ReversedFeaturePostWithBlockQuote
                title="HOST YOUR OWN SATELLITE EVENT"
                description="Satellites bring the Summit to your space. By harnessing the spirit and momentum of the Summit, Satellite Events are a great way to shine a spotlight on your organization while contributing to the value offered to Summit attendees. Interested in hosting but unsure what to do? Get in touch with our team and we’ll design a Satellite perfect for you!"
                img={require("../../assets/images/satelliteHeader.png")}
                BtnTitle="HOST YOUR OWN"
                Url="https://riseupco.typeform.com/to/D0gtvZ"
                strength={300}
                bgColor={"#FCE100"}
                subTitle={"WHY HOST?  "}
                style={{
                  titleFontSize: "35px",
                  subTitleFontSize: "25px",
                  descriptionFontSize: "15px"
                }}
              />
            </div>

            <div className="item">
              <ReversedFeaturePostWithBlockQuote
                title="HIPO STARTUPS"
                // blockQuote="RiseUp2017 has been our most engaged event so far! The  RiseUp Summit Event App reached 95% engagement rate."
                // quote="Published by Eventtus Team, the developers of the Summit App"
                // details="Every year, we collaborate with around 100 orgainzations to build the Summit. From small creative startups to large multinational corporations, Summit is our partners' as much as it is our own. Apply to become a partner at #Summit18"
                description="Join this premium startup exposure program exclusive to HIgh POtential startups! Last year’s lineup of future unicorns included Instabug, Vezeeta, Mumzworld, and Bayzat."
                img={require("../../assets/images/HIPOS.png")}
                BtnTitle="APPLY NOW"
                Url="https://riseupco.typeform.com/to/yA53jQ"
                strength={300}
                slog={""}
                bgColor={"#FCE100"}
                style={{
                  titleFontSize: "40px",
                  subTitleFontSize: "25px",
                  descriptionFontSize: "20px"
                }}
              />
            </div>
            {/* <div className="item">
              <ReversedFeaturePostWithBlockQuote
                title="EXHIBIT AT RISEUP SUMMIT ’18"
                description="Showcase your startup to the ecosystem! The best of MENA’s startups exhibit at RiseUp’s Startup Station. This year, 150 startups will exhibit at RiseUp Summit, each getting ONE day to show us what they’ve got!"
                img={require("../../assets/images/STARTUP_STATION_HEADER-min.png")}
                BtnTitle="APPLY AS AN EXHIBITOR"
                Url="https://riseupco.typeform.com/to/m59t7V"
                strength={300}
                slog={""}
                bgColor={"#FCE100"}
              />
            </div>

            <div className="item">
              <ReversedFeaturePostWithBlockQuote
                title="PITCH ON THE MENA REGION’S BIGGEST STAGE"
                description="Showcase your startup to the ecosystem! The best of MENA’s startups exhibit at RiseUp’s Startup Station. This year, 150 startups will exhibit at RiseUp Summit, each getting ONE day to show us what they’ve got!"
                img={require("../../assets/images/PitchCompetition.jpg")}
                BtnTitle="APPLY TO PITCH"
                Url="https://riseupco.typeform.com/to/m59t7V"
                strength={300}
                slog={""}
                bgColor={"#FCE100"}
              />
            </div>

            <div className="item">
              <ReversedFeaturePostWithBlockQuote
                title="JOIN OUR HIPOs PROGRAM"
                description="For the 2nd year running, the Summit shines a spotlight on the region’s best. A HIPO is a unique startup that has been identified as having HIgh POtential growth, a thirst for disruption and unparalleled resilience. Last year’s lineup of future unicorns included Instabug, Vezeeta, Mumzworld, and Bayzat."
                img={require("../../assets/images/HIPOS.png")}
                BtnTitle="APPLY"
                Url="https://riseupco.typeform.com/to/yA53jQ"
                strength={300}
                slog={""}
                bgColor={"#FCE100"}
              />
            </div> */}
            <a
              className="left carousel-control"
              href="#featureCarousel"
              data-slide="prev"
              style={{ width: "4%" }}
            >
              <span className="glyphicon glyphicon-chevron-left" />
              <span className="sr-only">Previous</span>
            </a>
            <a
              className="right carousel-control"
              href="#featureCarousel"
              data-slide="next"
            >
              <span className="glyphicon glyphicon-chevron-right" />
              <span className="sr-only">Next</span>
            </a>
          </div>
        </div>
      </div>
    )
  }
}

export default FeaturePostSlider
