import React from "react"
import DocumentTitle from "react-document-title"
import FeaturePost from "../commons/FeaturePost"
import ReversedFeaturePost from "../commons/ReversedFeaturePost"
import HeaderSlider from "../commons/HeaderSlider"
import Slider from "./Slider"
import SpeakersComponent from "../speakers/index"
import Partnerships from "../crowds/partnerships"
import Tickets from "../Tickets/index"
import BottomSlider from "./bottomSlider"
import { connect } from "react-redux"
import {
  fetchPartners,
  fetchPartner
} from "../../redux/actions/partners/PartnersAction"

let demo = true

class HomeComponent extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      demo,
      home: false,
      screens: false,
      itemNumber: 0,
      mainPartners: [],
      modalIsOpen: false
    }
  }

  componentDidMount() {
    let mainPartners = []
    this.props.fetchPartners().then(res => {
      res.payload.data.collections.map((item, i) => {
        if (item.status) {
          if (item.type_of_partner === "Main partner") {
            mainPartners.push(item)
          }
          return item
        }
        return item
      })
      this.setState({ mainPartners: mainPartners })
    })

    this.setState({ demo: true })
  }

  RenderHome() {
    return (
      <div className="Homepage--component">
        <HeaderSlider slider={true} />

        <FeaturePost
          title="#RISEUP18"
          details="90 seconds of electric Summit vibes. Check out the Official #RiseUp18 Aftermovie for a taste of the energy. Can you feel it?? Are you pumped?! This is YOUR year."
          img={null}
          BtnTitle="LEARN MORE"
          Url="/programs"
          strength={300}
          slog="HERE'S WHAT HAPPENED AT"
          video={true}
          videoSrc="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Friseupsummit%2Fvideos%2F993823650812361%2F&show_text=0&width=560"
          noLink={true}
        />

 
    
      <BottomSlider />
  
  



      {/*}  <ReversedFeaturePost
          title="GROUNDED EXPERIENCES"
          details="#RiseUp18 is going back to the roots, reminding us to keep both feet on the ground. This year, it’s all about being real with what we want to create as an ecosystem, how to reach resources more easily, and taking better risks. Grounded Experiences brings back to mind that yes, entrepreneurship means if you fall, you’ll fall hard and if you rise, you’ll soar; that’s why you’ve always got to aim high, but stay grounded."
          img={require("../../assets/images/ThemePart.jpg")}
          BtnTitle="LEARN MORE"
          Url="/programs"
          strength={300}
          slog={`THIS YEAR'S THEME`}
          bgColor={"#FCE100"}
          noLink={true}
        />
*/}
        <Slider />
        <SpeakersComponent HomepageView={true} />
        <div id="tickets" />
        <Tickets NoSpaceHomepage={true} HomepageView={true} />
        <Partnerships homePageView={true} />
        { /*<FeaurePostSlider />*/ }
      </div>
    )
  }

  render() {
    return (
      <div>
        <DocumentTitle title="RiseUp Summit - Home" />
        {this.RenderHome()}
      </div>
    )
  }
}
function mapStateToProps(state) {
  return {
    partner: state.Partner.partner
  }
}

export default connect(
  mapStateToProps,
  { fetchPartners, fetchPartner }
)(HomeComponent)
