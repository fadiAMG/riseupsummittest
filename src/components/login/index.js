import React from "react"
// import { NavLink } from "react-router-dom"
import Modal from "react-modal"
import { Redirect } from "react-router-dom"
import { connect } from "react-redux"
import {
  LoginActionWithTickets,
  LoginSpeaker,
  GetCurrentUserWithTickets
} from "../../redux/actions/myExperience/authActions"
import ValidateLogin from "../../validation/LoginValidation"
import { Button } from 'semantic-ui-react'
import axios from 'axios';




class LoginWidthTicket extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      username: "",
      password: "",
      /* This state attribute manages which login should be showed */
      toggle: 2,
      isLoading: false,
      modalIsOpen: false,
      sendEmailSuccess: "",
      sendEmailErr: "",
      authErorr: '',
      StepsModal: false,
      StepsCase: false
    }
    this.onChange = this.onChange.bind(this)
    this.onSubmitForm = this.onSubmitForm.bind(this)
  }



  componentDidMount() {}

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value })
  }

  isValid() {
    const { errors, isValid } = ValidateLogin(this.state)
    if (!isValid) {
      this.setState({ errors, erorrsExit: true })
    }
    return isValid
  }

  onSubmitForm(e) {
    e.preventDefault()
    if (this.isValid()) {
      this.setState({ isLoading: true })
      this.props.LoginActionWithTickets(this.state).then(res => {
        if (res.payload.data.success) {
          const token = res.payload.data.access_token
          // const isLoading = res.payload.data.success
          localStorage.setItem("token", token) 
          this.setState({ isLoading: false })
          // this.props.GetCurrentUserWithTickets(token)
          this.props.history.push(`/`)
          window.location.reload()
        } else {
          this.setState({
            authErr: true,
            isLoading: false,
            authErorr: res.payload.data.message
          })
        }
      })
      this.setState({ errors: {}, erorrsExit: false, submit: true })
    }
  }



  onSignIn = event => {
    event.preventDefault();
    axios.post('https://bdev.riseupsummit.com/api/auth/token', {
      username: this.state.username,
      password: this.state.password
    }, {
      withCredentials: true
    })
    .then( response => {
      console.log(response.data.session); 
      if(response.data.session){
        const token = response.data.access_token
        localStorage.setItem("token", token) 
        this.setState({isLoading: false})
        //window.location.href = 'http://localhost:3000/my-sessions';
         this.props.history.push(`/`)
         window.location.reload()
      }
      else{
        this.setState({
          authErr: true,
          isLoading: false,
          authErorr: 'Login Error'
        })
      }
      
    })
    .catch(error => {
      console.log(error);
    });

    //  fetch('https://bdev.riseupsummit.com/api/auth/token',{
    //   method: 'POST',
    //   body: JSON.stringify(this.state),
    //   headers: {
    //     'Content-Type':'application/json'
    //   }
    // })
    // .then(res => {
    //   res.json().then(body => {
    //     if(res.status === 200){
    //       console.log("Signed IN");
    //       console.log(body.access_token);
    //     }
    //     else{
    //       console.log("Error")
    //     }
    //   })
    // })
    // .catch(err => {
    //   console.error(err);
    //   alert("Error Login In Please Try Again");
    // })
    
  }


  RenderLoading() {
    if (this.state.isLoading) {
      return (
        <img
          alt=""
          width="60"
          className="loading"
          src={require("../../assets/loading.gif")}
        />
      )
    }
  }

  RenderErorrs() {
    if (this.state.erorrsExit) {
      return (
        <ul className="errors login">
          <li>{this.state.errors.email}</li>
          <li>{this.state.errors.password}</li>
        </ul>
      )
    } else if (this.state.authErr) {
      return (
        <ul className="errors login">
          <li>{this.state.authErorr}</li>
        </ul>
      )
    } else {
      return null
    }
  }

  openStepsModal(e) {
    e.preventDefault()
    this.setState({ StepsModal: true, StepsCase: true })
  }

  closeStepsModal() {
    this.setState({ StepsModal: false, StepsCase: false })
  }
  // Login Modal
  openLoginModal() {
    this.setState({ loginModal: true, loginCase: true })
  }

  closeLoginModal() {
    this.setState({
      loginModal: false,
      username: "",
      password: "",
      errors: {},
      LoginerorrExist: false
    })
  }

  changeLogin = num => {
    this.setState({
      toggle: num
    })
  }

  render() {

    switch(this.state.toggle){
      case 1:
        return (
          <div>
            {this.props.isAuth ? (
              <Redirect to="/" />
            ) : (
              <div className="login--withId-wrap">
                <div className="image-wrap" />      
                <div className="form--wrap">
                
                  <form className="login--form" onSubmit={this.onSubmitForm}>
                  {this.RenderLoading()}
                            <div className="right--side">
                              <h4 onClick={() => this.changeLogin(2)} className="btn btn-red"> CLICK HERE TO SIGN IN AS SPEAKER </h4> 
                            </div>
                    <div className="inner-form">
                      <h3>RISEUP SUMMIT <span style ={{color: '#FCE100', fontFamily: "inherit" , fontSize: 'inherit' , fontWeight: "bold"}} >ATTENDEE'S</span> LOGIN</h3>
                      <p>{""}</p>
                      <div className="form--wrap">
                        <div className="row">
                          <div className="col-md-12">{this.RenderErorrs()}</div>
                          <div className="col-md-12">
                            <input
                              name="username"
                              className="form-control"
                              placeholder="Email Address"
                              onChange={this.onChange}
                            />
                          </div>
                          <div className="col-md-12">
                            <input
                              type="text"
                              name="password"
                              className="form-control"
                              placeholder="Ticket Confirmation Code "
                              onChange={this.onChange}
                              style={{ textTransform: "uppercase" }}
                            />
                          </div>
                          <div className="col-md-12">
                            {this.RenderLoading()}
                            <div className="right--side">
                              <button className="btn btn-red">SIGN IN</button>
                            </div>
                          </div>
    
                          
    
                          <div className="col-md-12">
                            <div className="send-id">
                              <h4>
                                Please Use Your Email And Ticket Confirmation Code.
                                <br />
                                <br />
                                <h3
                                  onClick={e => {
                                    this.openStepsModal(e)
                                  }}
                                  style={{
                                    fontSize: "14px",
                                    fontWeight: "bold",
                                    cursor: "pointer"
                                  }}
                                >
                                  here is how you can find your Confirmation code
                                </h3>
                              </h4>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            )}
    
            <Modal
              isOpen={this.state.StepsModal}
              onRequestClose={this.closeStepsModal.bind(this)}
              contentLabel=""
              className={"workshops-popup"}
            >
              <div className="speaker-modal--preview session--preview session--details">
                <div
                  className="speaker--details profile--session loginIMG"
                  style={{
                    height: "100%",
                    overflowY: "scroll",
                    maxHeight: "500px"
                  }}
                >
                  <img
                    alt=""
                    src={require("../../assets/images/ConfirmationCodeSteps.jpeg")}
                    onClick={e => {
                      this.closeStepsModal()
                    }}
                  />
                </div>
              </div>
            </Modal>
          </div>
        )
        case 2:
        return (
          <div>
            {this.props.isAuth ? (
              <Redirect to="/" />
            ) : (
              <div className="login--withId-wrap">
                <div className="image-wrap" />      
                <div className="form--wrap">
                  <form className="login--form" onSubmit={this.onSignIn}>
                  {this.RenderLoading()}
                    <div className="right--side">
                        <h4 onClick={() => this.changeLogin(1)} className="btn btn-red"> CLICK HERE TO SIGN IN AS ATTENDEE </h4>
                      </div>
                    <div className="inner-form">
                    
                      <h3>RISEUP SUMMIT <span style ={{color: '#FCE100', fontFamily: "inherit" , fontSize: 'inherit' , fontWeight: "bold"}} >SPEAKER'S</span>  LOGIN</h3>
                      <p>{""}</p>
                      <div className="form--wrap">
                        <div className="row">
                          <div className="col-md-12">{this.RenderErorrs()}</div>
                          <div className="col-md-12">
                            <input
                              name="username"
                              className="form-control"
                              placeholder="Email Address"
                              onChange={this.onChange}
                            />
                          </div>
                          <div className="col-md-12">
                            <input
                              type="password"
                              name="password"
                              className="form-control"
                              placeholder="Password "
                              onChange={this.onChange}
                            />
                          </div>
                          <div className="col-md-12">
                            {this.RenderLoading()}
                            <div className="right--side">
                              <button className="btn btn-red">SIGN IN</button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            )}
          </div>
        )
    }
  }
}

function mapStateToProps(state) {
  return {
    isAuth: state.authTicket.isAuth
  }
}
export default connect(
  mapStateToProps,
  { LoginActionWithTickets, GetCurrentUserWithTickets }
)(LoginWidthTicket)
