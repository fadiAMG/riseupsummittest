import React from "react"

const Locations = () => {
  return (
    <div className="location--wrap">
      <ol>
        <li>
          <div className="item">
            <h5>
              <i>Greek Campus</i> CAPITAL STAGE
            </h5>
          </div>
        </li>
        <li>
          <div className="item">
            <h5>
              <i>AUC Campus</i> CREATIVE STAGE
            </h5>
          </div>
        </li>
        <li>
          <div className="item">
            <h5>
              <i>AUC Campus</i> TECH STAGE
            </h5>
          </div>
        </li>
      </ol>
    </div>
  )
}

export default Locations
