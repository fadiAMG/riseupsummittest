let ActiveDay = {}

export let factor = 120

function setTime(data) {
  let arr = []
  let TimeSlotsObject = {}
  data.forEach((item, i) => {
    if (!item.not_general_agenda && !item.other_activities && !item.session_101)
      arr.push(Number(item.start_time.replace(":", "")))
  })
  arr = arr.sort((a, b) => a - b)
  let k = 0

  arr.forEach((item, i) => {
    if (TimeSlotsObject[item.toString()] === undefined) {
      TimeSlotsObject[item.toString()] = i === 0 ? 0 : k * factor
      k++
    }
  })

  ActiveDay = TimeSlotsObject
}

export function getConatinerHeight(data) {
  setTime(data)
  let val = 0
  Object.entries(ActiveDay).forEach(([key, value], i) => {
    if (value > val || i === 0) val = value
  })
  return val
}

export function getY(i, data) {
  setTime(data)
  return ActiveDay[i]
}
