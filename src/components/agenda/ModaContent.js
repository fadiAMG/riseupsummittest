import React from "react"

const SpeakerModalContent = ({ data, close, status, speakers }) => {
  let spks = data.speakers
  let filterdArray = []
  speakers.filter(itemOfArray => {
    return spks.map((itemOfFilter, i) => {
      if (itemOfArray._id === itemOfFilter) {
        filterdArray.push(itemOfArray)
      }
    })
  })
  if (status !== null) {
    return (
      <div className="speaker-modal--preview session--preview session--details">
        <div className="speaker--details">
          <button className="close" onClick={close} />
          <div className="content">
            <h3>
              {data.title} <br />
            </h3>
            {/* <h5>{data.subTitle}</h5> */}
            <ul className="tags">
              {data.user_tags
                ? data.user_tags
                    .split(",")
                    .map((item, i) => <li key={i}>{item}</li>)
                : null}
            </ul>
            <ul className="main--list">
              <li>
                <i className="fa fa-calendar" /> {data.date} DAY
              </li>
              <li>
                <i className="fa fa-clock-o" /> {data.start_time} -{" "}
                {data.end_time}
              </li>
              <li>
                <i className="fa fa-map-marker" /> {data.venue}{" "}
                {data.subVenue ? "-" : ""} {data.subVenue}
              </li>
            </ul>
            {filterdArray.length > 0 ? (
              <h6 className="session--speakers--list">
                {filterdArray.length === 1
                  ? "Session Speaker"
                  : "Session Speakers"}
              </h6>
            ) : null}

            <ul className="speakers--list--wrap">
              {filterdArray.map(item => {
                return (
                  <li>
                    <div className="speaker--image">
                      <img src={item.imgUrl} alt="" />
                    </div>
                    <div className="speaker--details--wrap">
                      <h4>
                        {item.fname} {item.lname}
                      </h4>
                      <p>
                        {item.job_title} {item.entity_name ? "AT" : null}{" "}
                        {item.entity_name}
                      </p>
                    </div>
                  </li>
                )
              })}
            </ul>
            <p className="details">
              {data.description === "NA" ? "" : data.description}
            </p>
          </div>
          {/* <div className="session--wrap">
						<h4>{data.title}</h4>
						<ul>
							<li>{data.date}</li>
							<li>{data.start} - {data.end}</li>
							<li>{data.location}</li>
						</ul>
					</div>  */}
        </div>
      </div>
    )
  } else {
    return (
      <div className="speaker-modal--preview">
        <div className="speaker--img">
          <img src={require("../../assets/loading-speaker.jpg")} alt="" />
        </div>
        <div className="speaker--details">
          <div className="content">
            <h3>Loading</h3>
            {/*<p>{speakerData.session.details}</p>*/}
          </div>
          {/*<div className="session--wrap">
											<h4>{speakerData.session.name}</h4>
											<ul>
												<li>{speakerData.session.date}</li>
												<li>{speakerData.session.start} - {speakerData.session.end}</li>
												<li>{speakerData.session.location}</li>
											</ul>
										</div>*/}
        </div>
      </div>
    )
  }
}

export default SpeakerModalContent
