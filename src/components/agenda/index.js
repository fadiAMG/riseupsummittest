import React, { Component } from "react"
import { connect } from "react-redux"
import Modal from "react-modal"
import Locations from "./locations"
import AgendaDay from "./AgendaDay"
import ModaContent from "./ModaContent"
import { fetchSpeakers17 } from "../../redux/actions/SpeakersAction"
import { fetchAgenda } from "../../redux/actions/agenda/agendaAction"
import $ from "jquery"
class Agenda extends Component {
  constructor(props) {
    super(props)
    this.state = {
      ActiveTab: "1",
      modalIsOpen: false,
      status: false,
      session: {}
    }
    this.openModal = this.openModal.bind(this)
    this.sort = this.sort.bind(this)
  }
  componentDidMount() {
    this.props.fetchSpeakers17()
    this.props.fetchAgenda().then(_ => {
      this.setState({
        status:
          this.props.agenda.success === undefined
            ? false
            : this.props.agenda.success,
        data:
          this.props.agenda.items === undefined ? [] : this.props.agenda.items
      })
    })
  }

  openModal(id, day) {
    this.setState({ modalIsOpen: true, id })
    let session = this.state.data[day].filter(item => {
      return item._id === id
    })
    this.setState({ session: session[0] })
  }
  afterOpenModal(id) {}
  closeModal() {
    this.setState({ modalIsOpen: false, speaker: null, speakerLoading: null })
  }
  sort(item) {
    let arr = []
    let times = []
    item.forEach(element => {
      times.push({
        start_time: element.start_time,
        end_time: element.end_time,
        position: ""
      })
      element.order = Number(
        element.start_time.substring(0, 2) + element.start_time.substring(3, 5)
      )
      arr.push(element)
    })

    return arr.sort((a, b) => a.order - b.order)
  }
  render() {
    let speakers = this.props.speakers17
    let _self = this
    let DayInMonth =
      this.state.ActiveTab === "1"
        ? "7"
        : this.state.ActiveTab === "2"
        ? "8"
        : "9"
    let DayCountInWord =
      this.state.ActiveTab === "1"
        ? "one"
        : this.state.ActiveTab === "2"
        ? "two"
        : "three"
    function RenderSpeakerContent() {
      if (_self.state.session) {
        return (
          <ModaContent
            close={() => _self.setState({ modalIsOpen: false })}
            data={_self.state.session}
            speakers={speakers}
          />
        )
      } else {
        return null
      }
    }
    return (
      <div className="agenda--content" id="Agenda">
        <div className="agenda--main-banner">
          <h1>AGENDA</h1>
        </div>
        <div className="agenda--tabs--wrapper">
          <ul className="agenda--tabs">
            <li className={this.state.ActiveTab === "1" ? "active" : null}>
              <button onClick={() => this.setState({ ActiveTab: "1" })}>
                <p className="day">
                  1 <span>st</span> <i> DAY</i>
                </p>
              </button>
            </li>
            <li className={this.state.ActiveTab === "2" ? "active" : null}>
              <button onClick={() => this.setState({ ActiveTab: "2" })}>
                <p className="day">
                  2 <span>nd</span> <i> DAY</i>
                </p>
              </button>
            </li>
            <li className={this.state.ActiveTab === "3" ? "active" : null}>
              <button onClick={() => this.setState({ ActiveTab: "3" })}>
                <p className="day">
                  3 <span>rd</span> <i> DAY</i>
                </p>
              </button>
            </li>
          </ul>
        </div>
        <div className="agenda--wrap">
          {!this.state.status ? (
            <div className="session--loading">
              <img
                width="90"
                height="90"
                src={require("../../assets/spinning.svg")}
                alt=""
              />
            </div>
          ) : null}
          <div className="agenda--dayBanner">
            {`Day ${DayCountInWord} - dec ${DayInMonth}`}
          </div>
          <Locations />
          <div className="agenda--grid">
            {this.state.status && this.state.ActiveTab === "1" ? (
              <AgendaDay
                data={this.sort(this.state.data.day1)}
                onClick={id => this.openModal(id, "day1")}
              />
            ) : null}
            {this.state.status && this.state.ActiveTab === "2" ? (
              <AgendaDay
                data={this.sort(this.state.data.day2)}
                onClick={id => this.openModal(id, "day2")}
              />
            ) : null}
            {this.state.status && this.state.ActiveTab === "3" ? (
              <AgendaDay
                data={this.sort(this.state.data.day3)}
                onClick={id => this.openModal(id, "day3")}
              />
            ) : null}
          </div>
          <Modal
            isOpen={this.state.modalIsOpen}
            onAfterOpen={this.afterOpenModal.bind(this)}
            onRequestClose={this.closeModal.bind(this)}
            contentLabel=""
            className={"SpeakersModal"}
          >
            {RenderSpeakerContent()}
          </Modal>
        </div>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    speakers17: state.Speakers.speakers17,
    agenda: state.agenda.agenda
  }
}
export default connect(
  mapStateToProps,
  { fetchSpeakers17, fetchAgenda }
)(Agenda)
