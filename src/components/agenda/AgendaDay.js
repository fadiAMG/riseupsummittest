import React, { Component } from "react"
import $ from "jquery"
import { getY, getConatinerHeight, factor } from "./FindYCoordinates"

class AgendaDay extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }
  componentDidMount() {
    $(window).scroll(function() {
      var scroll = $(window).scrollTop()
      let workshopBannerPosition = document.querySelector(".workshops-banner")
        ? document.querySelector(".workshops-banner").getBoundingClientRect()
            .top
        : 0

      if (scroll >= 445) $(".location--wrap").addClass("fixed")
      else $(".location--wrap").removeClass("fixed")

      if (250 >= workshopBannerPosition) {
        $(".location--wrap").removeClass("fixed")
        // $(".workshops-banner").addClass("fixed")
      }
      // if (workshopBannerPosition > 0) {
      //   $(".workshops-banner").removeClass("fixed")
      // }
    })
  }

  render() {
    let { data, onClick } = this.props
    let dayInNumbers = 1
    if (data[0]) {
      dayInNumbers = data[0].date === "1st" ? 1 : data[0].date === "2nd" ? 2 : 3
    }
    function renderItems() {
      return (
        <div
          className="location_container"
          style={{
            minHeight: `${Number(getConatinerHeight(data)) + factor}px`
          }}
        >
          <ul>
            {data.map((item, i) => {
              let panel = item.talk
                ? "TALK"
                : item.panel
                ? "PANEL"
                : item.session_break
                ? "BREAK"
                : item.fireside_chat
                ? "FIRESIDE CHAT"
                : item.opening_ceremony
                ? "OPENING CEREMONY"
                : "LAUNCHPAD"

              if (
                !item.not_general_agenda &&
                !item.other_activities &&
                !item.session_101 &&
                item.venue.includes("Capital Stage")
              ) {
                return (
                  <li
                    key={i}
                    className={`item ${item.y}`}
                    onClick={
                      item.session_break || item.launchpad
                        ? null
                        : id => onClick(item._id)
                    }
                    style={{
                      opacity: `${item.opacity}`,
                      zIndex: item.display !== "vr" ? 20 : 10,
                      cursor:
                        item.session_break || item.launchpad
                          ? "default"
                          : "pointer",

                      top: getY(item.start_time.replace(":", ""), data) + "px",
                      display: isNaN(Number(item.start_time.replace(":", "")))
                        ? "none"
                        : "auto"
                    }}
                  >
                    {item.arabic ? (
                      <React.Fragment>
                        <div className="item--type">
                          {panel}
                          <i className="item--type--AR">{"AR"}</i>
                        </div>
                      </React.Fragment>
                    ) : (
                      <div className="item--type">{panel}</div>
                    )}
                    <h3>{item.start_time + " - " + item.end_time}</h3>

                    <h5>
                      {item.title.length > 75
                        ? item.title.substring(0, 74) + "..."
                        : item.title === "Break" || item.title === "Launchpad"
                        ? ""
                        : item.title}
                    </h5>

                    <h4>{handleSpeakersName(item.speakersData)}</h4>
                    {/* <h4>{item.speakersData}</h4> */}
                  </li>
                )
              }
              // return <li style={{ height: "50px" }} className="item hidden" />
              else return null
            })}
          </ul>

          <ul className="">
            {data.map((item, i) => {
              let panel = item.talk
                ? "TALK"
                : item.panel
                ? "PANEL"
                : item.session_break
                ? "BREAK"
                : item.fireside_chat
                ? "FIRESIDE CHAT"
                : item.opening_ceremony
                ? "OPENING CEREMONY"
                : "LAUNCHPAD"

              if (
                !item.not_general_agenda &&
                !item.other_activities &&
                !item.session_101 &&
                item.venue.includes("Creative Stage")
              ) {
                return (
                  <li
                    key={i}
                    className="item"
                    onClick={
                      item.session_break || item.launchpad
                        ? null
                        : id => onClick(item._id)
                    }
                    style={{
                      opacity: `${item.opacity}`,
                      zIndex: item.display !== "vr" ? 20 : 10,
                      cursor:
                        item.session_break || item.launchpad
                          ? "default"
                          : "pointer",
                      top: getY(item.start_time.replace(":", ""), data) + "px",
                      display: isNaN(Number(item.start_time.replace(":", "")))
                        ? "none"
                        : "auto"
                      // paddingBottom: "50px"
                    }}
                  >
                    {item.arabic ? (
                      <React.Fragment>
                        <div className="item--type">
                          {panel}
                          <i className="item--type--AR">{"AR"}</i>
                        </div>
                      </React.Fragment>
                    ) : (
                      <div className="item--type">{panel}</div>
                    )}
                    <h3>{item.start_time + " - " + item.end_time}</h3>

                    <h5>
                      {item.title.length > 75
                        ? item.title.substring(0, 74) + "..."
                        : item.title === "Break" || item.title === "Launchpad"
                        ? ""
                        : item.title}
                    </h5>

                    <h4>{handleSpeakersName(item.speakersData)}</h4>
                    {/* <h4>{item.speakersData}</h4> */}
                  </li>
                )
              }
              // return <li style={{ height: "50px" }} className="item hidden" />
              else return null
            })}
          </ul>

          <ul className="">
            {data.map((item, i) => {
              let panel = item.talk
                ? "TALK"
                : item.panel
                ? "PANEL"
                : item.session_break
                ? "BREAK"
                : item.fireside_chat
                ? "FIRESIDE CHAT"
                : item.opening_ceremony
                ? "OPENING CEREMONY"
                : "LAUNCHPAD"

              if (
                !item.not_general_agenda &&
                !item.other_activities &&
                !item.session_101 &&
                item.venue.includes("Tech Stage")
              ) {
                return (
                  <li
                    key={i}
                    className="item"
                    onClick={
                      item.session_break || item.launchpad
                        ? null
                        : id => onClick(item._id)
                    }
                    style={{
                      opacity: `${item.opacity}`,
                      zIndex: item.display !== "vr" ? 20 : 10,
                      cursor:
                        item.session_break || item.launchpad
                          ? "default"
                          : "pointer",

                      top: getY(item.start_time.replace(":", ""), data) + "px",
                      display: isNaN(Number(item.start_time.replace(":", "")))
                        ? "none"
                        : "auto"
                    }}
                  >
                    {item.arabic ? (
                      <React.Fragment>
                        <div className="item--type">
                          {panel}
                          <i className="item--type--AR">{"AR"}</i>
                        </div>
                      </React.Fragment>
                    ) : (
                      <div className="item--type">{panel}</div>
                    )}
                    <h3>{item.start_time + " - " + item.end_time}</h3>

                    <h5>
                      {item.title.length > 75
                        ? item.title.substring(0, 74) + "..."
                        : item.title === "Break" || item.title === "Launchpad"
                        ? ""
                        : item.title}
                    </h5>

                    <h4>{handleSpeakersName(item.speakersData)}</h4>

                    {/* <h4>{item.speakersData}</h4> */}
                  </li>
                )
              }
              // return <li style={{ height: "50px" }} className="item hidden" />
              else return null
            })}
          </ul>
        </div>
      )
    }
    function render101Workshops() {
      if (data.some(i => i.session_101))
        return (
          <div className="workshops_container">
            <div className="workshops-banner"> {"101 WORKSHOPS"}</div>
            <ul className="item workshops left">
              {data.map((item, i) => {
                let venue = item.venue.includes("AUC 101s(Hill House)")
                  ? "Hill House -AUC"
                  : item.venue.includes("AUC 101s(Oriental Hall)")
                  ? "Oriental Hall - AUC"
                  : item.venue.includes("Greek 101s(Falak)")
                  ? "Falak Startups - Greek"
                  : "Jameel Auditorium - Greek"

                if (item.session_101 && !item.not_general_agenda) {
                  if (item.venue.includes("Greek"))
                    return (
                      <li key={i} onClick={_ => onClick(item._id)} className="">
                        <div className="item--type">{venue}</div>
                        <h3>{item.start_time + " - " + item.end_time}</h3>

                        <h5>
                          {item.title.length > 100
                            ? item.title.substring(0, 99) + "..."
                            : item.title === "Break" ||
                              item.title === "Launchpad"
                            ? ""
                            : item.title}
                        </h5>
                        <h4>{handleSpeakersName(item.speakersData)}</h4>
                        {/* <h4>{item.speakersData}</h4> */}
                      </li>
                    )
                  else return null
                } else {
                  return null
                }
              })}
            </ul>

            <ul className="item workshops right">
              {data.map((item, i) => {
                let venue = item.venue.includes("AUC 101s(Hill House)")
                  ? "Hill House -AUC"
                  : item.venue.includes("AUC 101s(Oriental Hall)")
                  ? "Oriental Hall - AUC"
                  : item.venue.includes("Greek 101s(Falak)")
                  ? "Falak Startups - Greek"
                  : "Jameel Auditorium - Greek"

                if (item.session_101 && !item.not_general_agenda) {
                  if (item.venue.includes("AUC"))
                    return (
                      <li key={i} onClick={_ => onClick(item._id)} className="">
                        <div className="item--type">{venue}</div>
                        <h3>{item.start_time + " - " + item.end_time}</h3>

                        <h5>
                          {item.title.length > 100
                            ? item.title.substring(0, 99) + "..."
                            : item.title === "Break" ||
                              item.title === "Launchpad"
                            ? ""
                            : item.title}
                        </h5>
                        <h4>{handleSpeakersName(item.speakersData)}</h4>
                        {/* <h4>{item.speakersData}</h4> */}
                      </li>
                    )
                  else return null
                } else {
                  return null
                }
              })}
            </ul>
          </div>
        )
      else return <div className="workshops" />
    }
    function renderOtherActivites() {
      if (data.some(i => i.other_activities))
        return (
          <React.Fragment>
            <div className="other_activities-banner">
              {" "}
              {"OTHER ACTIVITIES - DAY " + dayInNumbers}
            </div>
            <div className="item other_activities ">
              {data.map((item, i) => {
                if (item.other_activities && !item.not_general_agenda) {
                  return (
                    <li key={i} onClick={id => onClick(item._id)}>
                      <h3>{item.start_time + " - " + item.end_time}</h3>

                      <h5>
                        {item.title.length > 100
                          ? item.title.substring(0, 99) + "..."
                          : item.title === "Break" || item.title === "Launchpad"
                          ? ""
                          : item.title}
                      </h5>
                    </li>
                  )
                } else {
                  return null
                }
              })}
            </div>
          </React.Fragment>
        )
    }

    function handleSpeakersName(speakersData) {
      var outPutArr = []
      if (speakersData)
        if (speakersData.length > 0) {
          speakersData.map((speaker, i) => {
            if (speaker && speaker.fname && speaker.lname) {
              if (speakersData.length - 2 === i)
                outPutArr.push(
                  speaker.fname.toLowerCase() +
                    " " +
                    speaker.lname.toLowerCase() +
                    " & "
                )
              else if (speakersData.length - 1 === i)
                outPutArr.push(
                  speaker.fname.toLowerCase() +
                    " " +
                    speaker.lname.toLowerCase() +
                    ""
                )
              else
                outPutArr.push(
                  speaker.fname.toLowerCase() +
                    " " +
                    speaker.lname.toLowerCase() +
                    ", "
                )
            }
            return null
          })
        }
      return outPutArr.join("")
    }

    return (
      <div className="sessions--wrap">
        {renderItems()}
        {render101Workshops()}
        {renderOtherActivites()}
      </div>
    )
  }
}

export default AgendaDay
