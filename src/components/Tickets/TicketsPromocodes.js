import React from 'react'
import DocumentTitle from 'react-document-title'
import Banner from '../commons/banner'
import TicketItem from './TicketItem'

import {strings} from '../strings'
import $ from 'jquery'

class TicketsPromo extends React.Component{
    constructor(props){
        super(props);
        this.state={
            promocode: ''
        }
    }
  componentDidMount() {
    let ele = document.querySelector('.fullwidth--seprator');
    let windowHeight = window.innerHeight - 37;
    ele.style.paddingTop = windowHeight+"px";




    // console.log("componentDidMount Payments");


    if (typeof  window.shouldRemoveButtonLoader !== 'undefined' && window.shouldRemoveButtonLoader == true) {
          var bookBtn = $('body').find('.widget_id.madfix');
          $('body').find('.widget_id').html('Buy now');
          bookBtn.removeClass('disabled');

    }   
  }
    shouldComponentUpdate(){
      return false;
    }
	render(){
    const RenderPackagesBol = this.props.RenderPackagesBol;
    function RenderPackages(){
      if(RenderPackagesBol){
        return null;
      }else{
        return(
         <div>
            <div className="tickets--package">
            <h3>TICKET PACKAGES</h3>
            <div className="TicketsWrap V2">
              <TicketItem
                type="STUDENT TICKET"
                value="300 L.E"
                leftTickets="STAY TUNED"
                img="price1"
                benefits={
                  [
                    '(must be enrolled in High School or University)'
                  ]
                }
              />
              <TicketItem
                type="GROUP TICKET"
                value="15% OFF ON THREE TICKETS OR MORE"
                leftTickets="STAY TUNED"
                img="price1"
                dateExist={false}
                available={true}
                eventId={21}
                madfix={true}
                disabled={true}
                benefits={
                  [
                    ''
                  ]
                }
              />
            </div>
          </div>
          {/* APPLY NOW > LINK TO INVESTOR PAGE investor */}
          <div className="tickets--package v2">
            <div className="TicketsWrap V2">
              <TicketItem
                type="INVESTOR ACCESS"
                img="price1"
                available={true}
                disabled={true}
                madfix={true}
                notTargetE7gzly={false}
                link="/investors"
                btnText="GET ACCESS"
                benefits={
                  [
                    ''
                  ]
                }
                benefits={
                  [
                    '(must be enrolled in High School or University)'
                  ]
                }
              />
              <TicketItem
                type="EXHIBITION SPACE"
                img="price1"
                available={true}
                disabled={true}
                madfix={true}
                notTargetE7gzly={false}
                link="/startup-station"
                benefits={
                  [
                    ''
                  ]
                }
              />
            </div>
          </div>
         </div>
        )
      }
    }
    function RenderBanner(){
      if(RenderPackagesBol){
        return null;
      }else{
        return(
          <div>
            <DocumentTitle title='RiseUp Summit - Tickets'></DocumentTitle>
            <Banner
              title={strings.Tickets.banner.title}
              opacity={strings.Tickets.banner.bgOpacity}
              width={strings.Tickets.banner.width}
              bgurl={require('../../assets/images/tickets.jpg')}
              strength={200}
            />
          </div>
        )
      }
    }
    let img;
    if(RenderPackagesBol){
      img = 'price4'
    }else{
      img = 'price1'
    }
    let url = this.props.location.search;
    let promocode = url.replace('?registrationcode=', '');
		return(
			<div >
        {RenderBanner()}
        <div className="fullwidth--seprator"></div>
        <div className="parllex-fix">
  				<div className="TicketsWrap V2">
            <TicketItem
              type="CROWD TICKET"
              value=" "
              eventId={21}
              // autoOpen={true}
              date="Deadline Nov25"
              // leftTicketsNum ={2500}
              // leftTicketsNum ={}
              // soldTickets = {}
              // leftTickets=""
              img="price2"
              imgExits={true}
              textLabel = 'GET TICKET'
              benefits={
                [
                  'Stage program',
                  '100+ talks, panels, fireside chats',
                  'Opening/closing parties',
                  'Reserve workshop spots',
                  'Attendee database',
                  'Satellite events ',
                  'Attend the pitch competition ',
                  'Coworking Areas',
                  'Everyday entertainment ',
                  'Delicious F&B ',
                  'Discover Egypt '
                ]
              }
              available={true}
              disabled={true}
              madfix={true}
              promocode={promocode}
              registrationExist={true}
            />
          </div>
        </div>
			</div>
		)
	}
}

TicketsPromo.defaultProps = {
  RenderPackagesBol: false
}

export default TicketsPromo
