import React from "react"
import DocumentTitle from "react-document-title"
import BannerV2 from "../commons/bannerv2"
import TicketItem from "./TicketItem"

import { strings } from "../strings"
import $ from "jquery"

class Payment extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      promocode: false
    }
  }

  componentDidMount() {
    if (
      typeof window.shouldRemoveButtonLoader !== "undefined" &&
      window.shouldRemoveButtonLoader === true
    ) {
      var bookBtn = $("body").find(".widget_id.madfix")
      $("body")
        .find(".widget_id")
        .html("Buy now")
      bookBtn.removeClass("disabled")
    }
  }
  shouldComponentUpdate() {
    return false
  }
  render() {
    const RenderPackagesBol = this.props.RenderPackagesBol
    function RenderPackages() {
      if (RenderPackagesBol) {
        return null
      } else {
        return (
          <div>
            <div className="tickets--package">
              <div className="TicketsWrap V2" style={{ marginTop: "60px" }}>
                <TicketItem
                  type="STUDENT TICKET"
                  value="300 EGP"
                  leftTickets="STAY TUNED"
                  img="price1"
                  benefits={[
                    "Must be enrolled in high school or university. Email a valid student ID or enrolment letter to crowdtickets@riseup.co"
                  ]}
                  available={false}
                  disabled={true}
                  madfix={false}
                  leftTicketsNum={0}
                  textLabel={"Closed"}
                />
                <TicketItem
                  type="GROUP TICKET"
                  value="15% OFF ON THREE TICKETS OR MORE"
                  leftTickets="STAY TUNED"
                  img="price1"
                  available={false}
                  disabled={true}
                  madfix={false}
                  leftTicketsNum={0}
                  textLabel={"Closed"}
                  benefits={[""]}
                />
              </div>
            </div>
            {/* APPLY NOW > LINK TO INVESTOR PAGE investor */}
            <div className="tickets--package v2">
              <div className="TicketsWrap V2">
                <TicketItem
                  type="INVESTOR ACCESS"
                  img="price1"
                  available={false}
                  disabled={true}
                  madfix={false}
                  leftTicketsNum={0}
                  textLabel={"Closed"}
                  benefits={["(must be enrolled in High School or University)"]}
                />
                <TicketItem
                  type="EXHIBITION SPACE"
                  img="price1"
                  available={false}
                  disabled={false}
                  madfix={true}
                  notTargetE7gzly={false}
                  link="/tickets"
                  benefits={[""]}
                />
              </div>
            </div>
          </div>
        )
      }
    }
    function RenderBanner() {
      if (RenderPackagesBol) {
        return null
      } else {
        return (
          <div>
            <DocumentTitle title="RiseUp Summit - Tickets" />

            <BannerV2
              title={strings.Tickets.banner.title}
              opacity={strings.Tickets.banner.bgOpacity}
              width={strings.Tickets.banner.width}
              bgurl={require("../../assets/images/tickets.jpg")}
              strength={200}
            />
          </div>
        )
      }
    }

    return (
      <div>
        {this.props.NoSpaceHomepage ? null : (
          <div>
            {RenderBanner()}
            <div className="fullwidth--seprator" />
          </div>
        )}
        <div className="parllex-fix">
          <h3>BE PART OF OUR STORY</h3>

          <div className="TicketsWrap V2">
            <TicketItem
              type="EARLY BIRD"
              value="550 EGP"
              date="Oct 10 - Nov 1"
              leftTicketsNum={0}
              soldTickets={100}
              eventId={21}
              leftTickets="1140 Tickets Left"
              img="price4_Grey"
              imgExits={true}
              btnText={"SOLD OUT"}
              textLabel={"SOLD OUT"}
              link="https://ticketing.eventtus.com/sssehl1vuuabm2vfhxijlxevn0p2dowi/tickets"
              benefits={[
                "Stage program",
                "100+ talks, panels, fireside chats",
                "Opening/closing parties",
                "Reserve workshop spots",
                "Attendee database",
                "Satellite events ",
                "Attend the pitch competition ",
                "Coworking Areas",
                "Everyday entertainment ",
                "Delicious F&B ",
                "Discover Egypt "
              ]}
              available={false}
              disabled={true}
              madfix={true}
              notTargetE7gzly={false}
              selfTarget={false}
              disableButton={true}
            />
            <TicketItem
              type="REGULAR TICKET"
              leftTickets="1140 Tickets Left"
              value="750 EGP"
              eventId={21}
              date="Nov 1- Dec 5"
              // leftTicketsNum ={2500}
              // leftTicketsNum ={2500}
              soldTickets={100}
              // leftTickets=" Tickets Left"
              img="price2_grey"
              imgExits={true}
              benefits={[
                "Stage program",
                "100+ talks, panels, fireside chats",
                "Opening/closing parties",
                "Reserve workshop spots",
                "Attendee database",
                "Satellite events ",
                "Attend the pitch competition ",
                "Coworking Areas",
                "Everyday entertainment ",
                "Delicious F&B ",
                "Discover Egypt "
              ]}
              available={false}
              disabled={true}
              madfix={false}
              leftTicketsNum={0}
              textLabel={"Closed"}
              btnText={"SOLD OUT"}
              studentButtonExist={false}
              notTargetE7gzly={false}
              disableButton={true}
              link="https://ticketing.eventtus.com/sssehl1vuuabm2vfhxijlxevn0p2dowi/tickets"
            />
            <TicketItem
              type="LATE OWL"
              value="1500 EGP"
              date="Dec 6- Dec 9"
              // leftTickets="stay tuned"
              img="price3"
              imgExits={true}
              btnText={"BUY NOW"}
              benefits={[
                "Stage program",
                "100+ talks, panels, fireside chats",
                "Opening/closing parties",
                "Reserve workshop spots",
                "Attendee database",
                "Satellite events ",
                "Attend the pitch competition ",
                "Coworking Areas",
                "Everyday entertainment ",
                "Delicious F&B ",
                "Discover Egypt "
              ]}
              eventId={21}
              leftTicketsNum={0}
              soldTickets={100}
              leftTickets="1140 Tickets Left"
              available={true}
              disabled={false}
              madfix={false}
              textLabel={"Closed"}
              notTargetE7gzly={false}
              disableButton={false}
              link="https://ticketing.eventtus.com/sssehl1vuuabm2vfhxijlxevn0p2dowi/tickets"

            />
          </div>

          <div className="TicketsWrap V2" style={{ marginTop: "300px" }}>
            <TicketItem
              type="STUDENT TICKET"
              leftTickets="1140 Tickets Left"
              value="375 EGP"
              eventId={21}
              date="Oct 10 - Dec 9"
              // leftTicketsNum ={2500}
              // leftTicketsNum ={2500}
              soldTickets={100}
              // leftTickets=" Tickets Left"
              img="StudentIcon"
              imgExits={true}
              benefits={[
                "Stage program",
                "100+ talks, panels, fireside chats",
                "Opening/closing parties",
                "Reserve workshop spots",
                "Attendee database",
                "Satellite events ",
                "Attend the pitch competition ",
                "Coworking Areas",
                "Everyday entertainment ",
                "Delicious F&B ",
                "Discover Egypt "
              ]}
              available={false}
              disabled={true}
              madfix={false}
              leftTicketsNum={0}
              textLabel={"Closed"}
              studentButtonExist={false}
              notTargetE7gzly={false}
              selfTarget={false}
              disableButton={true}
              btnText={"SOLD OUT"}
              link="https://ticketing.eventtus.com/sssehl1vuuabm2vfhxijlxevn0p2dowi/tickets"
            />

            <TicketItem
              type="GROUP LATE TICKET"
              value="1000 EGP"
              date="Nov 1 - Dec 1"
              // leftTickets="stay tuned"
              img="GroupTicket"
              imgExits={true}
              benefits={[
                "Stage program",
                "100+ talks, panels, fireside chats",
                "Opening/closing parties",
                "Reserve workshop spots",
                "Attendee database",
                "Satellite events ",
                "Attend the pitch competition ",
                "Coworking Areas",
                "Everyday entertainment ",
                "Delicious F&B ",
                "Discover Egypt "
              ]}
              eventId={21}
              leftTicketsNum={0}
              soldTickets={100}
              leftTickets="1140 Tickets Left"
              available={true}
              disabled={false}
              madfix={false}
              textLabel={"Closed"}
              notTargetE7gzly={false}
              selfTarget={false}
              link="https://ticketing.eventtus.com/sssehl1vuuabm2vfhxijlxevn0p2dowi/tickets"
              btnText={"BUY NOW"}
              benfitsTitle={
                "Looking for a good deal on tickets? Grab 2 or more of your friends and get #RiseUp18 passes for 20% off with our Group Tickets offer!:"
              }
            />

            <TicketItem
              type="LATE INVESTOR TICKET"
              value="5000 EGP"
              date="Oct 10 - Dec 9"
              // leftTickets="stay tuned"
              img="InvestorIconBlue"
              imgExits={true}
              benefits={[
                "Priority access to workshops",
                "Access to Investors' Lounge",
                "Complimentary catering at the Summit",
                "Invitation to Closing after-party ",
                "Access to Summit Startups database ",
                "Priority access to Satellite Events"
              ]}
              eventId={21}
              leftTicketsNum={0}
              soldTickets={100}
              leftTickets="1140 Tickets Left"
              available={true}
              disabled={false}
              madfix={false}
              textLabel={"Closed"}
              notTargetE7gzly={false}
              selfTarget={false}
              link="https://ticketing.eventtus.com/sssehl1vuuabm2vfhxijlxevn0p2dowi/tickets"
              btnText={"BUY NOW"}
              benfitsTitle={
                "Investor Ticket offers you everything a Regular Ticket does plus these awesome benefits:"
              }
            />
          </div>
          {this.props.HomepageView ? null : RenderPackages()}
        </div>
      </div>
    )
  }
}

Payment.defaultProps = {
  RenderPackagesBol: false,
  NoSpaceHomepage: false
}

export default Payment
