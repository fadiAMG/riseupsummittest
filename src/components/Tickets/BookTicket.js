// import React from 'react';
// import IntlTelInput from 'react-bootstrap-intl-tel-input'
// import HTML from 'html-parse-stringify';
// import {Checkbox, CheckboxGroup} from 'react-checkbox-group';
// import classnames from 'classnames';
// import {connect} from 'react-redux';
// import Banner from '../commons/banner'
// import TextFieldGroup from './commons/TextFieldGroup'
// import Loading from '../commons/Loading'

// import TicketDetails from './TicketDetails';
// import {requestTicket} from '../../redux/actions/TicketsAction'
// import TicketsValidation from '../../validation/TicketsValidation'
// import {strings} from '../strings'

// class BootTicket extends React.Component{
// 	constructor(props) {
// 	  super(props);
	
// 	  this.state = {
// 	  	interests: [],
// 	  	motivation:[],
//       firstName: '',
//       lastName:'',
//       email: '',
//       phone_no:'',
//       promocode:'',

//       phoneErorr:true,
//       phoneErorrMsg:'',
//       errors: {},
//       ArrayError:'',

//       isLoading: false,
//       iframe:'',
//       SuccessSubmit:false,
//       iframe:{},
//       submit:false,
//       BookTicketSubmit:false
//     }
//     this.onChange = this.onChange.bind(this);
//     this.onSubmit = this.onSubmit.bind(this);
// 	}
// 	componentDidMount() {
//       let ele = document.querySelector('.fullwidth--seprator');
//       let windowHeight = window.innerHeight - 37;
//       ele.style.paddingTop = windowHeight+"px";
//   }
// 	onChange(e) {
//     this.setState({ [e.target.name]: e.target.value });
//     // if(this.isValid()){
//     //   this.setState({ errors: {}, isLoading: true});
//     // }
//   }
//   InterestsChange(Interests){
//     this.setState({interests: Interests})
//   }
//   MotivationChange(Motivations){
//     this.setState({motivation: Motivations})
//   }
//   isValid(){
//     const {errors, isValid} = TicketsValidation(this.state);
//     if(!isValid){
//       this.setState({errors})
//     }
//     return isValid;
//   }
//   SubmitPayment(){
//   	this.setState({isLoading:true, BookTicketSubmit:true });
// 		this.props.requestTicket(this.state)
//   	.then((res) => {
//   		this.setState({
//   			isLoading: false,
//   			SuccessSubmit:true,
//   			iframe: res.payload.data
//   		})
//   	})
//   }
//   onSubmit(e){
// 		e.preventDefault();
//     if(this.isValid()){
//       this.setState({ errors: {}, submit:true});
//     }
//       	console.log(this.state);

//   }
//   onChangeHandler(data){
//   	console.log(data);
//   	this.setState({
// 			phoneErorr: data.valid,
// 			phoneErorrMsg: data.friendlyMessage,
// 			phone_no: data.intlPhoneNumber
//   	})
//   }
// 	render(){
// 		let _self = this;
// 		const { Type, Price } = this.props.match.params;
// 		const { errors } = this.state;

// 		const Interests = strings.Interestes.map(item => {
// 			return(
// 				<label><Checkbox value={item}/> {item}</label>
// 			)
// 		})
// 		const Motivations = strings.Motivations.map(item => {
// 			return(
// 				<label><Checkbox value={item}/> {item}</label>
// 			)
// 		})

// 		function renderIframe(){
// 			if(_self.state.SuccessSubmit){
// 				let Iframe = HTML.parse(_self.state.iframe.iframeHtml);
// 				return(
// 					<div>
// 						<h3 className="price--wrap">Final Price <b>{_self.state.iframe.finalPrice}</b></h3>
// 						<b>Please Wait After Submit Your Payment Details to get your Code to Access Your Profile.</b>
// 						<iframe src={Iframe[0].attrs.src} frameborder="0" width="100%" height="500"></iframe>
// 					</div>
// 				)
// 				console.log(_self.state.iframe)
// 			}
// 	  }
// 		return(
// 			<div className="single--ticket">
// 				<Banner
// 	        title={`Book Ticket As ${Type}`}
// 	        opacity={strings.Homepage.banner.bgOpacity}
// 	        width={strings.Homepage.banner.width}
// 	        bgurl={require('../../assets/images/homepage.jpg')}
// 	        strength={200}
// 	      />
// 	      			<div className="fullwidth--seprator"></div>

// 	     	<div className="form--wrap clearfix" style={{display: this.state.SuccessSubmit ? 'none' : 'block'}}>
// 	     		<div className="col-md-12 row">
// 						{
// 							this.state.isLoading && 
// 							<p className="help-block success--submit">
// 								<i className="fa fa-smile-o"> </i> 
// 								Thanks For Submit Your Details 
// 								<b>{` ${this.state.firstName}, `}</b>
// 								Request Pending Please Wait To Complete Your Payment Info.
// 								<Loading/>
// 							</p>
// 						}
// 					</div>
// 					<div className="col-md-12 row">
// 						{ 
// 							this.state.submit && 
// 							<TicketDetails 
// 								details={this.state} 
// 								Price={Price}
// 								SubmitPayment={this.SubmitPayment.bind(this)}
// 								BookTicketSubmit={this.state.BookTicketSubmit}
// 							/>
// 						}
// 					</div>
// 					{
// 						!this.state.submit &&
// 						<form className="row" onSubmit={this.onSubmit} disbaled>
// 							<div className="col-md-6">
// 								<TextFieldGroup
// 				          error={errors.firstName}
// 				          label="First Name"
// 				          onChange={this.onChange}
// 				          value={this.state.firstName}
// 				          field="firstName"
// 				          placeholder="First Name"
// 				        />
// 							</div>
// 							<div className="col-md-6">
// 								<TextFieldGroup
// 				          error={errors.lastName}
// 				          label="Last Name"
// 				          onChange={this.onChange}
// 				          value={this.state.lastName}
// 				          field="lastName"
// 				          placeholder="Last Name"
// 				        />
// 							</div>
// 							<div className="col-md-6">
// 								<TextFieldGroup
// 				          error={errors.email}
// 				          label="Email Address"
// 				          onChange={this.onChange}
// 				          value={this.state.email}
// 				          field="email"
// 				          placeholder="Email Address"
// 				          type="email"
// 				        />
// 							</div>
							
// 							<div className="col-md-6">
// 								<div className={classnames("form-group", { 'has-error': !this.state.phoneErorr || errors.phone_no  })}>
// 										{!this.state.phoneErorr && <div className="error-popover">{this.state.phoneErorrMsg}</div>}
// 										{errors.phone_no && <div className="error-popover">{errors.phone_no}</div>}
// 										<label className="control-label">Phone Number</label>
// 										<IntlTelInput
// 										  preferredCountries={['']}
// 										  defaultCountry={''}
// 										  onChange={(data) => this.onChangeHandler(data)}
// 										  name="phone_no"
// 										/>
// 								</div>
// 							</div>

// 							<div className="Interests clearfix">
// 								<div className="col-md-12">
// 									<div className={classnames("form-group", { 'has-error': errors.interests})}>
// 										{errors.interests && <div className="error-popover">{errors.interests}</div>}
// 										<label >Interests</label>
// 										<CheckboxGroup
// 			                className="checkList"
// 			                value={this.state.Interests}
// 			                onChange={this.InterestsChange.bind(this)}>

// 			                {Interests}
// 			              </CheckboxGroup>
// 		              </div>
// 								</div>

// 								<div className="col-md-12">
// 									<div className={classnames("form-group", { 'has-error': errors.motivation })}>
// 										{errors.motivation && <div className="error-popover">{errors.motivation}</div>}
// 										<label >Motivations</label>
// 										<CheckboxGroup
// 			                className="checkList"
// 			                value={this.state.motivation}
// 			                onChange={this.MotivationChange.bind(this)}>
// 			                {Motivations}
// 			              </CheckboxGroup>
// 									</div>
// 								</div>
// 							</div>
// 							<div className="col-md-4">
// 								<TextFieldGroup
// 				          error={errors.promocode}
// 				          label="Promo Code"
// 				          onChange={this.onChange}
// 				          value={this.state.promocode}
// 				          field="promocode"
// 				          placeholder="Promo Code"
// 				        />
// 							</div>
// 							<div className="col-md-12">
// 								<button className="btn btn-blue" type="submit" disabled={this.state.isLoading ? true : false}>Submit</button>
// 							</div>
// 			      </form>
// 			    }
// 	     	</div>
// 	     	<div className="form--wrap" style={{display: this.state.SuccessSubmit ? 'block' : 'none'}}>
// 	     		{renderIframe()}
// 	     		<div id="iframWrap"></div>
// 	     	</div>
// 			</div>
// 		)
// 	}
// }

// export default connect(null, {requestTicket})(BootTicket)