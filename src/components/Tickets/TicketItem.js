import React from "react"
import classNames from "classnames"
import axios from "axios"
import $ from "jquery"

class TicketItem extends React.Component {
  constructor() {
    super()
    this.state = {
      ticketsCount: 0,
      soldTickets: 0,
      mainSoldTickets: 0,
      stTimerInterval: 1000,
      ndTimerInterval: 0
    }
    this.timer = 0
    this.countDown = this.countDown.bind(this)
    this.countNdDown = this.countNdDown.bind(this)
    this.startStTimer = this.startStTimer.bind(this)
    this.startNdTimer = this.startNdTimer.bind(this)

    // console.log(this);
    /* if (this.props.fullTicketsCount > 0) {
          console.log("ytes");
        } */
  }
  startStTimer() {
    if (this.timer === 0) {
      // console.log("this.state.stTimerInterval");
      // console.log(this.state.stTimerInterval);
      this.timer = setInterval(this.countDown, this.state.stTimerInterval)
    }
  }
  startNdTimer() {
    if (this.timer === 0) {
      this.timer = setInterval(this.countNdDown, 50)
    }
  }
  countDown() {
    // Remove one second, set state so a re-render happens.
    let soldTickets = this.state.soldTickets
    let ticketsCount = this.state.ticketsCount
    // console.log(soldTickets);
    // console.log(parseInt(this.state.mainSoldTickets*.8));
    // console.log(soldTickets < parseInt(this.state.mainSoldTickets*.8))
    if (soldTickets < parseInt(this.state.mainSoldTickets * 0.95)) {
      clearInterval(this.timer)
      this.timer = 0
      this.startNdTimer()
    }
    this.setState({
      ticketsCount: ticketsCount - 3,
      soldTickets: soldTickets - 3
    })

    // Check if we're at zero.
  }
  countNdDown() {
    // Remove one second, set state so a re-render happens.
    let soldTickets = this.state.soldTickets
    let ticketsCount = this.state.ticketsCount
    if (this.state.soldTickets < 1) {
      clearInterval(this.timer)
    }
    this.setState({
      ticketsCount: ticketsCount - 3,
      soldTickets: soldTickets - 3
    })

    // Check if we're at zero.
  }

  componentDidUpdate() {
    if (window.shouldRemoveButtonLoader === true) {
      let bookBtn = $("body").find(".widget_id.madfix")
      $("body")
        .find(".widget_id")
        .each(function(el) {
          let element = $("body")
            .find(".widget_id")
            .eq(el)
          element.html(element.attr("data-text"))
        })
      bookBtn.removeClass("disabled")
    }
  }
  componentDidMount() {
    if (parseInt(this.props.leftTicketsNum) > 0) {
      axios
        .get(`https://e7gezly.tktd.co/api/orders/${this.props.eventId}/count`)
        .then(res => {
          let ticketsCount = this.props.leftTicketsNum
          this.setState({
            ticketsCount: ticketsCount,
            soldTickets: res.data.data.paid - 447,
            mainSoldTickets: res.data.data.paid - 447,
            stTimerInterval: 500 / (0.95 * res.data.data.paid)
          })
          this.startStTimer()
        })
    }
    if (parseInt(this.props.soldTickets) > 0) {
      let ticketsCount = this.props.leftTicketsNum
      this.setState({ ticketsCount: ticketsCount })
    }
  }
  render() {
    return (
      <div className={classNames("item", { available: this.props.available })}>
        >
        <div className="wow fadeInUp">
          {this.props.imgExits ? (
            <div className="icon">
              <img src={require(`../../assets/${this.props.img}.png`)} alt="" />
            </div>
          ) : null}

          <div className="Price-Table">
            <div className="price--head">
              <h5>{this.props.date}</h5>
              <h4>{this.props.type}</h4>
            </div>
            <div className="content">
              {this.props.dateExist ? (
                <div className="number--left">
                  {this.state.ticketsCount > 0 && this.state.ticketsCount}
                  {/* {this.props.leftTickets} */}
                </div>
              ) : null}

              <h5>{this.props.value}</h5>
              <ul>
                <h4>
                  {this.props.benfitsTitle ? this.props.benfitsTitle : ""}
                </h4>
                {this.props.benefits.map((item, i) => {
                  return <li key={i}>{item}</li>
                })}
              </ul>
            </div>

            {this.props.studentButtonExist ? (
              <a
                href="mailto:crowdtickets@riseup.co?Subject=Student%20Ticket"
                className="button purple"
              >
                EMAIL US
              </a>
            ) : (
              <a
                href="/"
                data-event-id={this.props.eventId}
                data-auto-open={this.props.autoOpen}
                data-registration-code={
                  this.props.promocode !== "" ? this.props.promocode : null
                }
                data-display-registration-code={!!this.props.registrationExist}
                style={{
                  display: this.props.notTargetE7gzly ? "inline-block" : "none"
                }}
                data-text={
                  this.props.textLabel ? this.props.textLabel : "Buy now"
                }
                className={classNames(
                  "button purple widget_id ",
                  { madfix: this.props.madfix },
                  { disabled: this.props.disabled }
                )}
              >
                <img src={require(`../../assets/loading.svg`)} alt="" />
              </a>
            )}
            {/* // null} */}
            {/* //this was changed from Link to a tag due to a problem with _blank routing */}
            {this.props.studentButtonExist ? null : (
              <a
                // to={this.props.disableButton ? "#" : this.props.link}
                disabled={true}
                href={this.props.disableButton ? "/" : this.props.link}
                target={this.props.selfTarget ? "_self" : "_blank"}
                rel={this.props.selfTarget ? "" : "noopener noreferrer"}
                data-event-id={this.props.eventId}
                style={{
                  display: !this.props.notTargetE7gzly
                    ? "inline-block"
                    : "none",
                  pointerEvents: this.props.disableButton ? "none" : "auto"
                }}
                className="button purple"
              >
                {this.props.btnText ? this.props.btnText : "AVAILABLE SOON"}
              </a>
            )}
          </div>
        </div>
      </div>
    )
  }
}
// const  = (this.props, active, available) => {
// }

TicketItem.defaultProps = {
  disabled: true,
  fullTicketsCount: 0,
  leftTicketsNum: "",
  eventId: false,
  notTargetE7gzly: true,
  link: "",
  imgExits: false,
  dateExist: true,
  autoOpen: false,
  studentButtonExist: false,
  registrationExist: false,
  disableButton: false,
  btnText: null
}

export default TicketItem
