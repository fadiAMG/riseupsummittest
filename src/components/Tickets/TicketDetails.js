import React from 'react'

const TicketDetails = ({details,Price, SubmitPayment, BookTicketSubmit }) => {
	const interests = details.interests.map(item => {
		return(
			<span> { item }, </span>
		)
	})
	const motivation = details.motivation.map(item => {
		return(
			<span> { item }, </span>
		)
	})
	function onClick(){
		SubmitPayment();
	}
	return(
		<div className="ticket-report">
			<ul>
				<li><b>Name :</b> {details.firstName} {details.lastName}</li>
				<li><b>Email :</b> {details.email}</li>
				<li><b>Interests :</b> {interests}</li>
				<li><b>Motivation :</b> {motivation}</li>
				<li><b>Discount:</b> 0 LE</li>
				<li><b>Final price :</b> {Price} LE</li>
			</ul>
			<button disabled={BookTicketSubmit} onClick={onClick} className="btn btn-blue">Proceed to payment</button>
		</div>
	)
}

TicketDetails.propTypes = {
	Price: React.PropTypes.string.isRequired,
	SubmitPayment: React.PropTypes.func.isRequired,
	BookTicketSubmit: React.PropTypes.bool.isRequired
}

export default TicketDetails