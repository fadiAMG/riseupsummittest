import React from 'react'
import DocumentTitle from 'react-document-title'
import Banner from '../commons/banner'

// Data
import {strings} from '../strings'

class NotFound extends React.Component{
  render(){
    let data = strings.Attendees;
    return(
      <div>
          <DocumentTitle title='Not Found'></DocumentTitle>
          <Banner
            title="Page Not Found"
            opacity={data.banner.bgOpacity}
            width={data.banner.width}
            bgurl={require('../../assets/images/attendees.jpg')}
            strength={100}
          />
      </div>
    );
  }
}
export default NotFound;

