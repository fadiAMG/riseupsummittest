import React from "react"

class QuotesCard extends React.Component {
  constructor(props) {
    super(props)

    this.state = {}
  }
  componentDidMount() {}
  render() {
    let props = this.props
    return (
      <div>
        <div
          className="QuotesCard_Container"
          style={{ backgroundColor: props.BackgroundColor }}
        >
          <h2>{props.quote}</h2>
          <h4>{props.quoteBy}</h4>
        </div>
      </div>
    )
  }
}

export default QuotesCard
