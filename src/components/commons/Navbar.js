import React from "react"
import { NavLink } from "react-router-dom"
import { connect } from "react-redux"
import { Logout } from "../../redux/actions/myExperience/authActions"
import $ from "jquery"
import USER_INFO_MODAL from "../profile/userInfoModal"

class Navbar extends React.Component {
  constructor(props) {
    super(props)

    this.state = {}
  }
  componentDidMount() {
    let windowH = window.innerHeight
    let menuEl = $(".main--menu")
    $(window).on("scroll", function(e) {
      let scroll = $(window).scrollTop()

      if (scroll >= windowH / 20) {
        if (!menuEl.hasClass("fixed--menu")) {
          menuEl.addClass("fixed--menu")
        }
      } else {
        menuEl.removeClass("fixed--menu")
      }
    })
    let menuClickableItem = $(".main--menu a")
    menuClickableItem.click(function() {
      $(".mobile--navigation").removeClass("visible")
    })
    $("#menu--trigger").click(function() {
      $(".mobile--navigation").toggleClass("visible")
    })
  }
  onLogout(e) {
    e.preventDefault()
    localStorage.removeItem("token")
    this.props.Logout()
    window.location.reload()
  }
  render() {
    return (
      <div>
        <div className="riseup--products">
          <ul>
            <li>
              <a
                target="_blank"
                rel="noopener noreferrer"
                href="https://riseup.co/"
              >
                RISEUP CO.
              </a>
            </li>
            <li>
              <a
                target="_blank"
                rel="noopener noreferrer"
                href="https://riseupconnect.com/"
              >
                RISEUP CONNECT
              </a>
            </li>
            <li>
              <a
                target="_blank"
                rel="noopener noreferrer"
                href="https://riseup.co/riseupteam/"
              >
                TEAM
              </a>
            </li>
          </ul>
        </div>
        <header className="main--menu">
          <nav className="menu--nav">
            <div className="menu--logo">
              <NavLink to="/">
                <img
                  src={require("../../assets/main-logo.svg")}
                  alt="RiseUp Summit"
                />
              </NavLink>
            </div>

            <div className="menu--items">
              <ul className="items">
                <li>
                  <NavLink to="/" replace>
                    Home
                  </NavLink>
                </li>
                <li>
                  <a>
                    CROWD <i className="fa fa-angle-down" />
                  </a>
                  <ul>
                    <li>
                      <NavLink to="/speakers" replace>
                        <span>Speakers</span>
                      </NavLink>
                    </li>

                    <li>
                      <NavLink to="/partners" replace>
                        <span>partners</span>
                      </NavLink>
                    </li>
                    <li>
                      <NavLink to="/startups" replace>
                        <span>Startups</span>
                      </NavLink>
                    </li>
                    <li>
                      <NavLink to="/volunteers" replace>
                        <span>VOLUNTEERS</span>
                      </NavLink>
                    </li>
                    <li>
                      <NavLink to="/media" replace>
                        <span>Media</span>
                      </NavLink>
                    </li>
                    <li>
                      <NavLink to="/mena-hipos" replace>
                        <span>MENA HiPOs</span>
                      </NavLink>
                    </li>
                  </ul>
                </li>
                <li>
                  <a>
                    summit 2018 <i className="fa fa-angle-down" />
                  </a>
                  <ul>
                    <li>
                      <NavLink to="/programs" replace>
                        <span>Tracks</span>
                      </NavLink>
                    </li>
                    <li>
                      <NavLink to="/agenda" replace>
                        <span>AGENDA</span>
                      </NavLink>
                    </li>
                    <li>
                      <NavLink to="/activities" replace>
                        <span>ACTIVITIES</span>
                      </NavLink>
                    </li>
                    <li>
                      <NavLink to="/pitch-competition" replace>
                        <span>pitch competition</span>
                      </NavLink>
                    </li>
                    <li>
                      <NavLink to="/workshops" replace>
                        {" "}
                        <span>Workshops</span>
                      </NavLink>
                    </li>
                    <li>
                      <NavLink to="/startup-station" replace>
                        <span>startup station</span>
                      </NavLink>
                    </li>
                    <li>
                      <NavLink to="/satelliteEvents" replace>
                        <span>Satellite Events</span>
                      </NavLink>
                    </li>
                    <li>
                      <NavLink to="/launchpad" replace>
                        <span>launchpad</span>
                      </NavLink>
                    </li>{" "}
                  </ul>
                </li>

                <li>
                  <NavLink to="/summit101" replace>
                    summit 101
                  </NavLink>
                </li>
                {this.props.isAuth || localStorage.getItem("token") ? (
                  <div style = {{display: 'contents', cursor: 'pointer'}}>
                    <li>
                      <NavLink to="/my-sessions">My Workshops</NavLink>
                      
                    </li>
                    <li>
                    <USER_INFO_MODAL/>
                    </li>
                  </div>
                ) : (
                  <li>
                    <NavLink to="/login-with-tickets" className="menu--btn">
                      Login
                    </NavLink>
                  </li>
                )}
                {this.props.isAuth ? (
                  <li>
                    <NavLink
                      className="menu--btn"
                      onClick={this.onLogout.bind(this)}
                      to=""
                      replace
                    >
                      {" "}
                      Logout
                    </NavLink>
                  </li>
                ) : null
                // <li>
                //   <a className="menu--btn" href="/home#tickets">
                //     BUY TICKETS
                //   </a>
                // </li>
                }
              </ul>
              <div className="mobile--navigation--trigger">
                <i id="menu--trigger" className="fa fa-align-right" />
              </div>
            </div>
            <div className="mobile--navigation">
              <ul className="items">
                <li>
                  <NavLink to="/" replace>
                    Home
                  </NavLink>
                </li>
                <div className="multi--level">
                  <li className="item">
                    <h5>CROWD</h5>
                    <ul>
                      <li>
                        <NavLink to="/speakers" replace>
                          <span>Speakers</span>
                        </NavLink>
                      </li>
                      <li>
                        <NavLink to="/partners" replace>
                          <span>Partners</span>
                        </NavLink>
                      </li>
                      <li>
                        <NavLink to="/startups" replace>
                          <span>Startups</span>
                        </NavLink>
                      </li>

                      <li>
                        <NavLink to="/volunteers" replace>
                          <span>VOLUNTEERS</span>
                        </NavLink>
                      </li>
                      <li>
                        <NavLink to="/media" replace>
                          <span>Media</span>
                        </NavLink>
                      </li>
                      <li>
                        <NavLink to="/mena-hipos" replace>
                          <span>MENA HiPOs</span>
                        </NavLink>
                      </li>
                    </ul>

                    <h5>Profile</h5>
                    {this.props.isAuth ? (
                      <ul>
                        <li>
                          <NavLink to="/my-sessions" replace>
                            <span>
                              <i className="fa fa-list" /> My Workshops
                            </span>
                          </NavLink>
                        </li>
                        <li>
                          <NavLink
                            onClick={this.onLogout.bind(this)}
                            to=""
                            replace
                          >
                            <span>
                              <i className="fa fa-sign-out" /> Logout
                            </span>
                          </NavLink>
                        </li>
                      </ul>
                    ) : (
                      <ul>
                        <li>
                          <NavLink to="/login-with-tickets" replace>
                            <span>
                              <i className="fa fa-user" /> Login With Tickets
                            </span>
                          </NavLink>
                        </li>
                      </ul>
                    )}
                  </li>
                  <li className="item">
                    <h5>summit 2018</h5>
                    <ul>
                      <li>
                        <NavLink to="/agenda" replace>
                          <span>AGENDA</span>
                        </NavLink>
                      </li>
                      <li>
                        <NavLink to="/programs" replace>
                          <span>Tracks</span>
                        </NavLink>
                      </li>
                      <li>
                        <NavLink to="/activities" replace>
                          <span>ACTIVITIES</span>
                        </NavLink>
                      </li>
                      <li>
                        <NavLink to="/pitch-competition" replace>
                          <span>pitch competition</span>
                        </NavLink>
                      </li>
                      <li>
                        <NavLink to="/workshops" replace>
                          {" "}
                          <span>Workshops</span>
                        </NavLink>
                      </li>
                      <li>
                        <NavLink to="/startup-station" replace>
                          <span>startup station</span>
                        </NavLink>
                      </li>
                      <li>
                        <NavLink to="/satelliteEvents" replace>
                          <span>Satellite Events</span>
                        </NavLink>
                      </li>
                      <li>
                        <NavLink to="/launchpad" replace>
                          <span>launchpad</span>
                        </NavLink>
                      </li>{" "}
                    </ul>
                  </li>
                </div>
                <li>
                  <NavLink to="/summit101" replace>
                    summit 101
                  </NavLink>
                </li>
              </ul>
            </div>
          </nav>
        </header>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    isAuth: state.authTicket.isAuth
  }
}
export default connect(
  mapStateToProps,
  { Logout }
)(Navbar)
