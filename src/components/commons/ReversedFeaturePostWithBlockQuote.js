import React from "react"
import classNames from "classnames"

class ReversedFeaturePostWithBlockQuote extends React.Component {
  render() {
    let props = this.props

    return (
      <div>
        <section className="reversed-feature-post feature--post">
          <div
            style={{ backgroundColor: props.bgColor }}
            className={classNames("right--side", {
              noParllex: props.noParallax
            })}
          >
            <h5 className="slog">{props.slog}</h5>
            <h3
              style={{
                color: "rgb(44,64,140)",
                fontSize: props.style.titleFontSize,
                marginBottom: "0px"
              }}
            >
              <span>{props.caption}</span>
              {props.title}
            </h3>

            <h4
              style={{
                color: "rgb(44,64,140)",
                fontSize: props.style.subTitleFontSize
              }}
            >
              {" "}
              {props.subTitle}
            </h4>
            {props.blockQuote ? (
              <blockquote
                style={{ fontWeight: "bold", color: "rgb(44,64,140)" }}
              >
                &#8220;
                {props.blockQuote}
                &#8221;
                <quote style={{ display: "block" }}>{props.quote}</quote>
              </blockquote>
            ) : (
              <blockquote
                style={{
                  fontWeight: "bold",
                  fontSize: props.style.descriptionFontSize,
                  color: "rgb(44,64,140)"
                }}
              >
                {props.description}
              </blockquote>
            )}

            <blockquote
              style={{
                color: "rgb(44,64,140)",
                display: "block",
                marginBottom: "10px",
                fontSize: "15px"
              }}
            >
              {props.details}
            </blockquote>
            {!props.noLink ? (
              <a
                className={classNames("link")}
                href={props.Url}
                target="_blank"
                rel="noopener noreferrer"
                style={!props.description ? { marginTop: "40px" } : {}}
              >
                {props.BtnTitle}
                <img
                  alt=""
                  className={classNames({ gray: props.gray })}
                  src={require("../../assets/arrow-right.svg")}
                />
              </a>
            ) : null}
          </div>
          <div className={classNames("left--side HomeHx")}>
            {props.img != null ? <img src={props.img} alt="" /> : <div />}
          </div>
        </section>
      </div>
    )
  }
}

ReversedFeaturePostWithBlockQuote.defaultProps = {
  gray: false,
  video: false,
  noLink: false,
  style: {
    titleFontSize: "auto",
    subTitleFontSize: "auto",
    descriptionFontSize: "15px"
  }
}
export default ReversedFeaturePostWithBlockQuote
