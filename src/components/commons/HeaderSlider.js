import React from "react"
class HeaderSlider extends React.Component {
  render() {
    return (
      <div>
        {this.props.slider ? (
          <div
            id="headerCarousel"
            className="carousel slide"
            data-ride="carousel"
            data-pause="false"
          >
            <div className="carousel-inner ">
              <div className="item active">
                <div className="new-banner first-header">
                  <div className="content">
                    <img
                      alt=""
                      style={{ width: "90%", marginTop: "-5%" }}
                      src={require("../../assets/images/HeaderText.png")}
                    />
                  </div>
                </div>
              </div>

              <div className="item">
                <div className="new-banner second-header headers-common">
                  <div className="content">
                    <p>RISEUP MOMENTS</p>
                    <p>
                      {" "}
                      &#8220;YOU GUYS ARE MORE INTELLIGENT TODAY THAN WE WERE AT
                      YOUR AGE 30 YEARS AGO! &#8221; NAGUIB SAWIRIS
                    </p>
                    <img
                      alt=""
                      style={{ width: "70%", marginTop: "-3%" }}
                      src={require("../../assets/images/downtown.png")}
                    />
                  </div>
                </div>
              </div>
              <div className="item ">
                <div className="new-banner third-header headers-common ">
                  <div className="content">
                    <p>RISEUP MOMENTS</p>
                    <p>
                      {" "}
                      &#8220; TO HAVE MORE THAN YOU HAVE, YOU&apos;VE GOT TO
                      BECOME MORE THAN YOU ARE &#8221; CHRIS DO
                    </p>
                    <img
                      alt=""
                      style={{ width: "70%", marginTop: "-3%" }}
                      src={require("../../assets/images/downtown.png")}
                    />
                  </div>
                </div>
              </div>
              <div className="item ">
                <div className="new-banner fourth-header headers-common ">
                  <div className="content">
                    <p>RISEUP MOMENTS</p>
                    <p>
                      {" "}
                      &#8220; ...THE ABILITY TO SPEAK TO ENTREPRENEURS, I
                      DON&apos;T THINK WE DO IT ENOUGH... &#8221; MAGNITT CEO,
                      PHILIP BAHOSHY
                    </p>
                    <img
                      alt=""
                      style={{ width: "70%", marginTop: "-3%" }}
                      src={require("../../assets/images/downtown.png")}
                    />
                  </div>
                </div>
              </div>
              <div className="item ">
                <div className="new-banner fifth-header headers-common ">
                  <div className="content">
                    <p>RISEUP MOMENTS</p>
                    <p>
                      {" "}
                      &#8220; IF YOU&apos;RE NOT READY TO BE WRONG, YOU WILL
                      NEVER CREATE ANYTHING ORIGINAL &#8221; ANGHAMI FOUNDER,
                      Elie HABIB
                    </p>
                    <img
                      alt=""
                      style={{ width: "70%", marginTop: "-3%" }}
                      src={require("../../assets/images/downtown.png")}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        ) : (
          <div className="new-banner first-header">
            <div className="content">
              <img
                alt=""
                style={{ width: "90%", marginTop: "-5%" }}
                src={require("../../assets/images/HeaderText.png")}
              />
            </div>
          </div>
        )}
      </div>
    )
  }
}

export default HeaderSlider
