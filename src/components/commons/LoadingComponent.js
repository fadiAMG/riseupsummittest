import React from 'react'
// const Loading = '/../../assets/icons/infinity.svg'
const LoadingComponent = () => {
  return(
    <div className='LoadingComponent'>
      <img src={require('../../../assets/loading.gif')} alt='test'/>
    </div>
  )
}

export default LoadingComponent
