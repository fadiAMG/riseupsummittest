import React from "react"
import { Link } from "react-router-dom"
import classNames from "classnames"
import PropTypes from "prop-types"

class FeaturePost extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      res: [],
      selected: "TECHNOLOGY",
      TabsList: [
        {
          title: "CREATIVE",
          slog: "#DriveHX"
        },
        {
          title: "CAPITAL",
          slog: "#RaiseHX"
        },
        {
          title: "TECHNOLOGY",
          slog: "#HackHX"
        }
      ]
    }
  }
  componentDidMount() {}
  componentWillUnmount() {}

  renderSperator() {
    if (this.props.seprator) {
      return <div className="fullwidth--seprator" />
    } else {
      return null
    }
  }
  render() {
    let props = this.props
    function renderProducts() {
      if (props.products) {
        return props.products.map((product, i) => {
          return (
            <div
              key={i}
              className={classNames(
                "product",
                { noListArrow: props.noArrows },
                { nested: product.nested }
              )}
            >
              {product.link ? (
                <Link to={product.link || ""}>
                  <h4>{product.title}</h4>
                </Link>
              ) : (
                <React.Fragment>
                  <h4>{product.title}</h4>
                  <h5>{product.subTitle}</h5>
                </React.Fragment>
              )}

              {product.details || product.subDetails ? (
                <React.Fragment>
                  <p
                    style={{
                      fontSize: props.style.productsFontSize
                    }}
                  >
                    <b>{product.huge}</b> {product.details}
                  </p>
                </React.Fragment>
              ) : null}
              <ul>
                {props.exist_list && product.list
                  ? product.list.map((item, i) => {
                      return <li key={i}>{item}</li>
                    })
                  : null}
              </ul>
            </div>
          )
        })
      } else {
        return null
      }
    }

    return (
      <div>
        {this.renderSperator()}
        <section className="feature--post">
          {!props.video ? (
            <div className={classNames("left--side HomeHx")}>
              {props.img != null ? (
                <img
                  src={props.img}
                  style={{ maxHeight: props.ImgMaxHeight }}
                  alt=""
                />
              ) : (
                <div />
              )}
            </div>
          ) : null}
          {props.video ? (
            <div className={classNames("videoFrame")}>
              <iframe
                title="Riseup17"
                src={props.videoSrc}
                width="100%"
                height="100%"
                style={{ border: "none", overflow: "hidden" }}
                scrolling="no"
                frameBorder="0"
                allowtransparency="true"
                allowFullScreen={true}
              />
            </div>
          ) : null}
          <div className={classNames("right--side")}>
            <h5
              className="slog"
              style={{
                color: props.style.slogColor,
                fontSize: props.style.slogFontSize,
                fontWeight: props.style.slogFontWeight
              }}
            >
              {props.slog}
            </h5>
            <h3
              style={{
                fontSize: props.style.titleFontSize
              }}
            >
              <span style={{ marginBottom: "20px" }}>
                {props.caption}{" "}
                {props.captionImage ? (
                  <img width="170" alt="" src={props.captionImage} />
                ) : null}
              </span>
              {props.title} <i style={{ marginTop: "5px" }}>{props.job}</i>{" "}
              <i>{props.subTitle}</i> <i>{props.location}</i>
            </h3>

            <p
              style={{
                fontSize: props.style.detailsFontSize
              }}
            >
              {props.details}
            </p>
            <div className="more--details">
              <i>{props.launchText}</i>
              <b>{props.progTitle}</b>
              {props.details2 ? <p>{props.details2}</p> : null}
              <div className="ul--list">
                <h5>{props.listTitle}</h5>
                <ul>
                  {props.listItems
                    ? props.listItems.map((item, i) => {
                        return <li key={i}>{item}</li>
                      })
                    : null}
                </ul>
              </div>
              <p className="hashtag">{props.hashTag}</p>
              {props.warnMsg ? (
                <div
                  className="alert"
                  style={{
                    backgroundColor: "#FCE100",
                    fontSize: 16,
                    color: "#254D9F",
                    fontFamily: "Proxima Nova semi"
                  }}
                >
                  {props.warnMsg}
                </div>
              ) : null}
            </div>
            <div className={classNames({ products_wrap: props.products })}>
              {renderProducts()}
            </div>
            {props.openText ? (
              <h6 className="open--text">{props.openText}</h6>
            ) : null}
            {props.openTextBlue ? (
              <h6 className="open--text--largeBlue">{props.openTextBlue}</h6>
            ) : null}
            <span className="email-text">{props.email}</span>

            {props.SelfLink ? (
              <a
                className={classNames(
                  "link",
                  { gray: props.gray },
                  { noLink: props.noLink }
                )}
                href={props.Url}
                target="_blank"
                rel="noopener noreferrer"
              >
                {props.BtnTitle}
                <img
                  alt=""
                  className={classNames({ gray: props.gray })}
                  src={require("../../assets/arrow-right.svg")}
                />
              </a>
            ) : (
              <Link
                className={classNames(
                  "link",
                  { gray: props.gray },
                  { noLink: props.noLink }
                )}
                to={props.Url}
              >
                {props.BtnTitle}
                <img
                  alt=""
                  className={classNames({ gray: props.gray })}
                  src={require("../../assets/arrow-right.svg")}
                />
              </Link>
            )}
          </div>
        </section>
      </div>
    )
  }
}

// {'typeform-share': props.popup },
// {'button': props.popup }

// data-mode="popup"
// target={props.selfTarget ? '_blank' : '_self'
FeaturePost.defaultProps = {
  subTitle: "",
  location: "",
  gray: false,
  Url: "",
  noParallax: false,
  noArrows: false,
  redTitle: false,
  seprator: true,
  popup: false,
  selfTarget: false,
  SelfLink: false,
  exist_list: false,
  video: false,
  noLink: false,
  style: {
    color: "auto",
    fontSize: "auto",
    fontWeight: "auto",
    productsFontSize: "auto"
  }
}
FeaturePost.propTypes = {
  seprator: PropTypes.bool
}

export default FeaturePost
