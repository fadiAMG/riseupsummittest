import React from "react"
import PropTypes from "prop-types"
import $ from "jquery"

const value1 = ["1", "2", "3", "o", "f", "D", "E", "C", "E", "M", "B", "E", "R"]
const value2 = ["d", "o", "w", "n", "t", "o", "w", "n", "c", "a", "i", "r", "o"]

class BannerV2 extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      scrolled: 1
    }
  }
  componentDidMount() {
    let _self = this
    let ele = document.querySelector(".banner--section .content")
    let windowHeight = window.innerHeight - 37
    ele.style.height = windowHeight + "px"

    $(window).scroll(function() {
      let scrollTopVal = $(window).scrollTop()
      if (scrollTopVal < 50) {
        _self.setState({ scrolled: 1 })
      } else if (scrollTopVal < 100) {
        _self.setState({ scrolled: 0.8 })
      } else if (scrollTopVal < 200) {
        _self.setState({ scrolled: 0.5 })
      } else if (scrollTopVal < 300) {
        _self.setState({ scrolled: 0.3 })
      } else if (scrollTopVal < 400) {
        _self.setState({ scrolled: 0 })
      }
    })
    if (this.props.yellow) {
      $("body").addClass("yellow-version")
    } else {
      $("body").removeClass("yellow-version")
    }
  }

  render() {
    let {
      title,
      width,
      strength,
      slog,
      noVerticalText,
      slog2,
      yellow,
      icoImageAbs
    } = this.props
    const val = value1.map(i => {
      return <span>{i}</span>
    })
    const val2 = value2.map(i => {
      return <span>{i}</span>
    })
    return (
      <div>
        {yellow ? (
          <div className="banner--section v2 yellow" strength={strength}>
            <div className="shape-wrap">
              <img
                src={require("../../assets/images/v2/shape-yellow.svg")}
                alt=""
              />
            </div>
            <div className={`content`} style={`background-color:#FFE200`}>
              <div
                className="container"
                style={{ position: "relative", zIndex: 10 }}
              >
                <p className="slog" style={`opacity: ${this.state.scrolled}`}>
                  {slog}
                </p>
                <h1
                  className="element"
                  style={`max-width:${width}%; margin:auto; opacity:${
                    this.state.scrolled
                  };`}
                >
                  <img
                    alt=""
                    className="abs-image"
                    width="120"
                    src={icoImageAbs}
                  />
                  {title}
                  <span>{slog2}</span>
                </h1>
              </div>
              <div
                style={{ visibility: noVerticalText ? "visable" : "hidden" }}
              >
                <div className="left--side">
                  <p>{val}</p>
                </div>
                <div className="right--side">
                  <p>{val2}</p>
                </div>
              </div>
            </div>
          </div>
        ) : (
          <div className="banner--section v2" strength={strength}>
            <div className="shape-wrap">
              <img src={require("../../assets/home-shape.svg")} alt="" />
            </div>
            <div
              className={`content`}
              style={`background-color:rgba(42, 66, 140, ${1})`}
            >
              <div
                className="container"
                style={{ position: "relative", zIndex: 10 }}
              >
                <p className="slog" style={`opacity: ${this.state.scrolled};`}>
                  {slog}
                </p>
                <h1
                  className="element"
                  style={`max-width:${width}%; margin:auto; opacity:${
                    this.state.scrolled
                  };`}
                >
                  <img
                    alt=""
                    className="abs-image"
                    width="120"
                    src={icoImageAbs}
                  />
                  {title} <span>{slog2}</span>
                </h1>
              </div>
              <div
                style={{ visibility: noVerticalText ? "visable" : "hidden" }}
              >
                <div className="left--side">
                  <p>{val}</p>
                </div>
                <div className="right--side">
                  <p>{val2}</p>
                </div>
              </div>
            </div>
          </div>
        )}
      </div>
    )
  }
}

BannerV2.proptypes = {
  title: PropTypes.string,
  opacity: PropTypes.number,
  width: PropTypes.number,
  bgurl: PropTypes.string
}

BannerV2.defaultProps = {
  noVerticalText: true,
  yellow: false,
  icoImageAbs: ""
}
export default BannerV2
