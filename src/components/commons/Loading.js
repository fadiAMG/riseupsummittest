import React from 'react'
const Loading = () => {
  return(
    <div className='LoadingComponent'>
      <img src={require('../../assets/loading.svg')} alt='Loading...'/>
    </div>
  )
}
export default Loading
