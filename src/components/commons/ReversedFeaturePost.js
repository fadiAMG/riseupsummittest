import React from "react"
import { Link } from "react-router-dom"
import classNames from "classnames"

class ReversedFeaturePost extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      res: [],
      selected: "TECHNOLOGY",
      TabsList: ["CREATIVE", "CAPITAL", "TECHNOLOGY"]
    }
  }

  render() {
    let props = this.props

    return (
      <div>
        <section className="reversed-feature-post feature--post ">
          <div
            style={{ backgroundColor: props.bgColor }}
            className={classNames("right--side", {
              noParllex: props.noParallax
            })}
          >
            <p
              className="slog"
              style={{
                color: "rgb(44,64,140)",
                fontFamily: "$xblack",
                letterSpacing: "10px"
              }}
            >
              {props.slog}
            </p>
            <h3 style={{ color: "rgb(44,64,140)" }}>
              <span>{props.caption}</span>
              GROUNDED <br /> EXPERIENCES
            </h3>

            <blockquote
              style={{
                color: "rgb(44,64,140)",
                display: "block",
                marginBottom: "10px",
                fontSize: "16px"
              }}
            >
              {props.details}
            </blockquote>

            {!props.noLink ? (
              <Link className={classNames("link")} to={props.Url}>
                {props.BtnTitle}
                <img
                  alt=""
                  className={classNames({ gray: props.gray })}
                  src={require("../../assets/arrow-right.svg")}
                />
              </Link>
            ) : null}
          </div>
          {!props.video ? (
            <div className={classNames("left--side HomeHx")}>
              {props.img != null ? (
                <img
                  style={{ width: "100%", height: "100%" }}
                  src={props.img}
                  alt=""
                />
              ) : (
                <div />
              )}
            </div>
          ) : null}

          {props.video ? (
            <div className={classNames("left--side HomeHx")}>
              <iframe
                title="mainVideo"
                src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Friseupsummit%2Fvideos%2F1556186567797605%2F&show_text=0&width=560"
                width="100%"
                height="450"
                style={{ border: "none", overflow: "hidden" }}
                scrolling="no"
                frameborder="0"
                allowTransparency="true"
                allowFullScreen="true"
              />
            </div>
          ) : null}
        </section>
      </div>
    )
  }
}

ReversedFeaturePost.defaultProps = {
  gray: false,
  video: false,
  noLink: false
}
export default ReversedFeaturePost
