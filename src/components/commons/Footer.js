import React from "react"
import CreationModal from '../speakers/ApplyasSpeaker/speakerCreationModal';

class Footer extends React.Component {
  componentDidMount() {
    // $(".menu--wrap ul li a").click(function () {
    //   $("html, body").animate({ scrollTop: 0 });
    // });
  }
  render() {
    return (
      <footer className="footer--component">
        <div className="content--wrap">
          <div className="subscribe--wrap">
            <a
              className="btn btn-newsletter"
              href="https://eepurl.com/bNEeTr"
              target="_blank"
              rel="noopener noreferrer"
            >
              SIGN UP TO OUR NEWSLETTER
            </a>

            <div className="copyrights--content">
              <span>STAY CONNECTED</span>
              <ul>
                <li>
                  <a
                    className="fb"
                    href="https://www.facebook.com/riseupsummit/"
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    <i className="fa fa-facebook fb" />
                  </a>
                </li>

                <li>
                  <a
                    className="tw"
                    href="https://twitter.com/riseupsummit/"
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    <i className="fa fa-twitter tw" />
                  </a>
                </li>
                <li>
                  <a
                    className="inst"
                    href="https://www.instagram.com/riseupsummit/"
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    <i className="fa fa-instagram" />
                  </a>
                </li>
              </ul>
            </div>
          </div>
          <div className="menu--wrap">
            <p style={{ color: "white", fontWeight: "bold" }}>QUICK LINKS</p>
            <ul>
              <li>
                <CreationModal />
              </li>
              <li>
                <a
                  target="_blank"
                  rel="noopener noreferrer"
                  href="https://riseupco.typeform.com/to/ciWzvP"
                >
                  Apply as a Partner
                </a>
              </li>
              <li>
                <a
                  target="_blank"
                  rel="noopener noreferrer"
                  href="https://riseupco.typeform.com/to/yA53jQ"
                >
                  Apply as a HIPO
                </a>
              </li>

              <li>
                <a
                  target="_blank"
                  rel="noopener noreferrer"
                  href="https://riseupco.typeform.com/to/m59t7V"
                >
                  Apply For Pitch Competition
                </a>
              </li>
              <li>
                <a
                  target="_blank"
                  rel="noopener noreferrer"
                  href="https://riseupco.typeform.com/to/m59t7V"
                >
                  Apply To Exhibit
                </a>
              </li>
              <li>
                <a
                  target="_blank"
                  rel="noopener noreferrer"
                  href="https://riseupco.typeform.com/to/D0gtvZ"
                >
                  apply to host a satellite event
                </a>
              </li>
            </ul>
          </div>
        </div>
        <h6>
          © 2018 RiseUp LLC. All rights reserved. CONNECTING STARTUPS TO THE
          MOST RELEVANT RESOURCES WORLDWIDE
        </h6>
      </footer>
    )
  }
}
export default Footer
