import React from "react"
import PropTypes from "prop-types"
import { Parallax } from "react-parallax"
import $ from "jquery"
const value1 = ["1", "2", "3", "o", "f", "D", "E", "C", "E", "M", "B", "E", "R"]
const value2 = ["d", "o", "w", "n", "t", "o", "w", "n", "c", "a", "i", "r", "o"]

class Banner extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      scrolled: 1
    }
  }
  componentDidMount() {
    let _self = this
    let ele = document.querySelector(".banner--section .content")
    let windowHeight = window.innerHeight - 37
    ele.style.height = windowHeight + "px"

    $(window).scroll(function() {
      let scrollTopVal = $(window).scrollTop()
      if (scrollTopVal < 50) {
        _self.setState({ scrolled: 1 })
      } else if (scrollTopVal < 100) {
        _self.setState({ scrolled: 0.8 })
      } else if (scrollTopVal < 200) {
        _self.setState({ scrolled: 0.5 })
      } else if (scrollTopVal < 300) {
        _self.setState({ scrolled: 0.3 })
      } else if (scrollTopVal < 400) {
        _self.setState({ scrolled: 0 })
      }
    })
    if (this.props.yellow) {
      $("body").addClass("yellow-version")
    } else {
      $("body").removeClass("yellow-version")
    }
  }
  render() {
    let {
      title,
      opacity,
      bgurl,
      width,
      strength,
      slog,
      noVerticalText,
      slog2,
      slog3
    } = this.props
    const val = value1.map(i => {
      return <span>{i}</span>
    })
    const val2 = value2.map(i => {
      return <span>{i}</span>
    })
    return (
      <Parallax className="banner--section" bgImage={bgurl} strength={strength}>
        <div
          className={`content`}
          style={{ backgroundColor: `rgba(0, 58, 190, ${opacity})` }}
        >
          <div className="container">
            <p className="slog" style={{ opacity: this.state.scrolled }}>
              {slog}
            </p>
            <h1
              className="element"
              style={{ maxWidth: width + "%", opacity: this.state.scrolled }}
            >
              {title} <span>{slog2}</span>
            </h1>
            {slog3 ? (
              <h1
                className="slog3"
                style={{ maxWidth: width + "%", opacity: this.state.scrolled }}
              >
                {" "}
                {slog3}{" "}
              </h1>
            ) : (
              ""
            )}
          </div>
          <div style={{ visibility: noVerticalText ? "visable" : "hidden" }}>
            <div className="left--side">
              <p>{val}</p>
            </div>
            <div className="right--side">
              <p>{val2}</p>
            </div>
          </div>
        </div>
      </Parallax>
    )
  }
}

Banner.proptypes = {
  title: PropTypes.string,
  opacity: PropTypes.number,
  width: PropTypes.number,
  bgurl: PropTypes.string
}

Banner.defaultProps = {
  noVerticalText: true
}
export default Banner
