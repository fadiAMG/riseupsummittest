import axios from 'axios';
export default function setAuthToken(token){
  if(token){
    localStorage.setItem('token', token)
    axios.defaults.headers.common['Authorization'] = `${token}`;
  }
  else{
    localStorage.removeItem('token', token)
    delete axios.defaults.headers.common['Authorization']
  }
}