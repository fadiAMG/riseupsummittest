import * as types from "../actions/ActionsTypes"

const INITIAL_STATE = {
  speaker: null
}

export default function(state = INITIAL_STATE, action) {
  switch (action.type) {
    case types.FETCH_SPEAKER:
      return {
        state,
        speaker:
          action.payload.data === undefined
            ? []
            : action.payload.data.speaker
      }
    // break;
    default:
      return state
  }
}
