import * as types from "../../actions/ActionsTypes"

const INITIAL_STATE = {
  startups: [],
  loading: 0
}

export default function(state = INITIAL_STATE, action) {
  switch (action.type) {
    case types.FETCH_STARTUPS:
      return {
        state,
        startups:
          action.payload.data === undefined
            ? []
            : action.payload.data.collections,
        loading:
          action.payload === undefined ? false : action.payload.status
      }
    default:
      return state
  }
}
