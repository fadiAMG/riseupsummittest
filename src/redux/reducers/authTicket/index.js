import * as types from "../../actions/ActionsTypes"
const INITIAL_STATE = {
  isAuth: false,
  isLoading: false,
  user: {}
}

export default function(state = INITIAL_STATE, action) {
  switch (action.type) {
    case types.SET_CURRENT_USER_TICKET:
      return {
        isAuth:
          action.payload.data === undefined
            ? false
            : action.payload.data.success,
        user:
          action.payload.data === undefined
            ? []
            : action.payload.data.profile,
        isLoading:
          action.payload.data === undefined
            ? false
            : action.payload.data.success
      }
    // break;
    default:
      return state
  }
}
