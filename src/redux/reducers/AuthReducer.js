import * as types from "../actions/ActionsTypes"
const INITIAL_STATE = {
  isAuth: false,
  isLoading: false,
  user: {}
}

export default function(state = INITIAL_STATE, action) {
  switch (action.type) {
    case types.EDIT_USER:
      return {
        isAuth:
          action.payload.data === undefined
            ? false
            : action.payload.data.success,
        user:
          action.payload.data === undefined
            ? []
            : action.payload.data,
        isLoading:
          !action.payload.data === undefined
            ? false
            : !action.payload.data.success
      }
    // break;
    default:
      return state
  }
}
