import * as types from "../../actions/ActionsTypes"

const INITIAL_STATE = {
  sessions: []
}

export default function(state = INITIAL_STATE, action) {
  switch (action.type) {
    case types.FETCH_SESSIONS:
      return {
        state,
        sessions:
          action.payload.data === undefined
            ? []
            : action.payload.data.sessions
      }
    default:
      return state
  }
}
