import * as types from "../actions/ActionsTypes"

const INITIAL_STATE = {
  speakers17: []
}

export default function(state = INITIAL_STATE, action) {
  switch (action.type) {
    case types.FETCH_SPEAKERS:
      // return {state, speakers17:action.payload.data.speekers}
      // console.log("fetching speakers ",action)
      return {
        state,
        speakers17:
          action.payload.data === undefined
            ? []
            : action.payload.data.speekers
      }

    default:
      return state
  }
}
