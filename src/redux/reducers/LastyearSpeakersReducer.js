import * as types from "../actions/ActionsTypes"

const INITIAL_STATE = {
  SpeakersLast: []
}

export default function(state = INITIAL_STATE, action) {
  switch (action.type) {
    case types.FETCH_SPEAKERSLAST:
      return {
        state,
        SpeakersLast:
          action.payload.data === undefined
            ? []
            : action.payload.data.speekers
      }
    default:
      return state
  }
}
