import { combineReducers } from "redux"
import SpeakersReducer from "./SpeakersReducer"
import SpeakerReducer from "./SpeakerReducer"
import LastyearSpeakersReducer from "./LastyearSpeakersReducer"
import PartnersReducer from "./partners/PartnersReducer"
import PartnerReducer from "./partners/PartnerReducer"
import InvestorsAuthReducer from "./InvestorsAuthReducer"
import AuthTicketReducer from "./authTicket/index"
import AuthReducer from "./AuthReducer"
import SessionsReducer from "./sessions/SessionsReducer"
import StartupsReducer from "./startups/StartupsReducer"
import AgendaReducer from "./agenda/AgendaReducer"

const reducers = combineReducers({
  Speakers: SpeakersReducer,
  SpeakersLast: LastyearSpeakersReducer,
  Speaker: SpeakerReducer,
  Partners: PartnersReducer,
  Partner: PartnerReducer,
  Investors: InvestorsAuthReducer,
  auth: AuthReducer,
  authTicket: AuthTicketReducer,
  sessions: SessionsReducer,
  startups: StartupsReducer,
  agenda: AgendaReducer
})

export default reducers
