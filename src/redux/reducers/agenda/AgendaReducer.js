import * as types from "../../actions/ActionsTypes"

const INITIAL_STATE = {
  agenda: []
}

export default function(state = INITIAL_STATE, action) {
  switch (action.type) {
    case types.FETCH_AGENDA:
      return {
        state,
        agenda: action.payload.data === undefined ? [] : action.payload.data
      }

    default:
      return state
  }
}
