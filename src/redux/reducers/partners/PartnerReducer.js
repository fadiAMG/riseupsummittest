import * as types from "../../actions/ActionsTypes"

const INITIAL_STATE = {
  partner: null
}

export default function(state = INITIAL_STATE, action) {
  switch (action.type) {
    case types.FETCH_PARTNER:
      return {
        state,
        partner:
          action.payload.data === undefined
            ? []
            : action.payload.data.collection
      }
    // break;
    default:
      return state
  }
}
