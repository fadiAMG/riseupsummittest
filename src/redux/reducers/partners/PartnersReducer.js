import * as types from "../../actions/ActionsTypes"

const INITIAL_STATE = {
  partners: []
}

export default function(state = INITIAL_STATE, action) {
  switch (action.type) {
    case types.FETCH_PARTNERS:
      return {
        state,
        partners:
          action.payload.data === undefined
            ? []
            : action.payload.data.collections
      }
    default:
      return state
  }
}
