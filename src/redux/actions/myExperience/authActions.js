import axios from 'axios';
import * as types from '../ActionsTypes'

const API_TOKEN= 'https://bdev.riseupsummit.com/api/v2/auth/ticket'
const GET_USER = 'https://bdev.riseupsummit.com/api/my-profile'
const GET_MY_EXP = 'https://bdev.riseupsummit.com/api/my-exp'
const CREATE_USER_API = 'https://bdev.riseupsummit.com/api/create-user'
const PROFILES_API = 'https://bdev.riseupsummit.com/api/profiles/'
const INV_PROFILE = 'https://bdev.riseupsummit.com/api/profileinvs/'


// - GET CURRENT USER
export function GetCurrentUserWithTickets(token){
  const request = axios.get(GET_USER , {
    headers: {
      'Authorization': token
    }
  })
  return {
    type: types.SET_CURRENT_USER_TICKET,
    payload: request
  }
}


// - GET CURRENT USER
export function GetMyExpData(token){
  const request = axios.get(GET_MY_EXP , {
    headers: {
      'Authorization': token
    }
  })
  return {
    type: types.SET_CURRENT_USER_TICKET,
    payload: request
  }
}

// LOGIN ATTENDEE USER
export function LoginActionWithTickets(data){
  const request = axios.post(API_TOKEN, data)
  return {
    type: types.LOGIN_TOKEN_TICKET,
    payload: request
  }
}

// LOGIN SPEAKER USER 
export function LoginSpeaker(data){
  const request = axios.post(API_TOKEN, data)
  return {
    type: types.LOGIN_TOKEN,
    payload: request
  }
}

// LOGOUT USER
export function Logout(){
  return dispatch => {
    localStorage.removeItem('token');
  }
}


// EDIT USER ACCOUNT

export function EditUserProfileWithTicket(id, user){
  const request = axios.patch(`${PROFILES_API+id}`, user)
  return{
    type: types.EDIT_USER,
    payload:request
  }
}
// EDIT my EXP

export function updateMyExp(token,data){
  const request = axios.post(GET_MY_EXP , data,{
      'Authorization': token
  });
  return{
    type: types.EDIT_USER,
    payload:request
  }
}