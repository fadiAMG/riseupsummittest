import * as types from "../ActionsTypes"
import axios from "axios"

const SESSION_VIEW_API =
  "https://bdev.riseupsummit.com/api/newsessionsdata/agendaview"

export function fetchAgenda() {
  const request = axios.get(SESSION_VIEW_API)
  return {
    type: types.FETCH_AGENDA,
    payload: request
  }
}
