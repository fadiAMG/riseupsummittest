import axios from 'axios';
import * as types from './ActionsTypes'
import setAuthToken from '../../utils/setAuthToken'

const API_TOKEN= 'https://bdev.riseupsummit.com/api/auth/token'
const GET_USER = 'https://bdev.riseupsummit.com/api/my-profile'
const CREATE_USER_API = 'https://bdev.riseupsummit.com/api/create-user'
const PROFILES_API = 'https://bdev.riseupsummit.com/api/profiles/'
const INV_PROFILE = 'https://bdev.riseupsummit.com/api/profileinvs/'


// - GET CURRENT USER
export function GetCurrentUser(token){
  const request = axios.get(GET_USER , {
    headers: {
      'Authorization': token
    }
  })
  return {
    type: types.SET_CURRENT_USER,
    payload: request
  }
}

// LOGIN USER
export function LoginAction(data){
  const request = axios.post(API_TOKEN, data)
  return {
    type: types.LOGIN_TOKEN,
    payload: request
  }
}


// LOGOUT USER
export function Logout(){
  return dispatch => {
    localStorage.removeItem('token');
    setAuthToken(false);
  }
}


// CREATE NEW USER ACCOUNT

export function CreateUser(user){
  const request = axios.post(CREATE_USER_API, user)
  return{
    type: types.CREATE_USER,
    payload:request
  }
}

// EDIT USER ACCOUNT

export function EditUserProfile(id, user){
  const request = axios.patch(`${PROFILES_API+id}`, user)
  return{
    type: types.EDIT_USER,
    payload:request
  }
}