// Speakers Consts
export const FETCH_SPEAKERS = "FETCH_SPEAKERS"
export const FETCH_SPEAKER = "FETCH_SPEAKER"
export const FETCH_SPEAKERSLAST = "FETCH_SPEAKERSLAST"

// Tickets Consts
export const REQUEST_TICKET = "REQUEST_TICKET"

// Partners Consts
export const FETCH_PARTNERS = "FETCH_PARTNERS"
export const FETCH_PARTNER = "FETCH_PARTNER"

// Create users
export const LOGIN_TOKEN = "LOGIN_TOKEN"
export const CREATE_USER = "CREATE_USER"
export const SET_CURRENT_USER = "SET_CURRENT_USER"

// Env Profile
export const SET_CURRENT_ENV = "SET_CURRENT_ENV"

// Startups
export const FETCH_STARTUPS = "FETCH_STARTUPS"
export const FETCH_STARTUP = "FETCH_STARTUP"

// Auth Ticket users
export const LOGIN_TOKEN_TICKET = "LOGIN_TOKEN_TICKET"
export const SET_CURRENT_USER_TICKET = "SET_CURRENT_USER_TICKET"

// Session
export const FETCH_SESSIONS = "FETCH_SESSIONS"
export const EDIT_USER = "EDIT_USER"

//Agenda
export const FETCH_AGENDA = "FETCH_AGENDA"
