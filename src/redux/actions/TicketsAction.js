import * as types from './ActionsTypes'
import axios from 'axios';

const TICKET_URL = "https://bdev.riseupsummit.com/api/ticketviaiframe"

export function requestTicket(data){
  const request = axios.post(`${TICKET_URL}`, data);
  return {
    type : types.REQUEST_TICKET,
    payload: request
  }
}

