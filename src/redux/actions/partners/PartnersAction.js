import * as types from '../ActionsTypes'
import axios from 'axios';

const PARTNERS_URL = "https://bdev.riseupsummit.com/api/partnerships/"

export function fetchPartners() {
  const request = axios.get(PARTNERS_URL);
  return{
    type: types.FETCH_PARTNERS,
    payload: request
  }
}

export function fetchPartner(id){
  const request = axios.get(`${PARTNERS_URL}${id}`)

  return {
    type: types.FETCH_PARTNER,
    payload: request
  }
}