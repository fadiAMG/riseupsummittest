import * as types from '../ActionsTypes'
import axios from 'axios';

const SESSION_URL = "https://bdev.riseupsummit.com/api/newsessionslist/"

export function fetchSessions() {
  const request = axios.get(SESSION_URL);
  return{
    type: types.FETCH_SESSIONS,
    payload: request
  }
}

export function fetchSession(id){
  const request = axios.get(`${SESSION_URL}${id}`)

  return {
    type: types.FETCH_PARTNER,
    payload: request
  }
}