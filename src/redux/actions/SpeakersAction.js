import * as types from './ActionsTypes'
import axios from 'axios';

const SPEAKERS_URL = "https://bdev.riseupsummit.com/api/speakers/"

export function fetchSpeakers17() {
  const request = axios.get(SPEAKERS_URL);
  return{
    type: types.FETCH_SPEAKERS,
    payload: request
  }
}

export function fetchSpeaker(id){
  const request = axios.get(`${SPEAKERS_URL}${id}`)

  return {
    type: types.FETCH_SPEAKER,
    payload: request
  }
}

export function fetchLastSpeakers() {
  const request = axios.get(`${SPEAKERS_URL}?last_year=1`);
  return{
    type: types.FETCH_SPEAKERSLAST,
    payload: request
  }
}