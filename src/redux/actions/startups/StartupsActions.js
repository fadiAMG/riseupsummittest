import * as types from '../ActionsTypes'
import axios from 'axios';

const STARTUPS_URL = "https://bdev.riseupsummit.com/api/newstartupships/"

export function fetchStartups() {
  const request = axios.get(STARTUPS_URL);
  return{
    type: types.FETCH_STARTUPS,
    payload: request
  }
}

export function fetchStartup(id){
  const request = axios.get(`${STARTUPS_URL}${id}`)

  return {
    type: types.FETCH_STARTUP,
    payload: request
  }
}
