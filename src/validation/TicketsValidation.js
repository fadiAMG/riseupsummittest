import Validator from 'validator'
import isEmpty from 'lodash/isEmpty'

export default function TicketsValidation(data) {
  let errors = {};

  if (Validator.isEmpty(data.firstName)) {
    errors.firstName = 'Firstname is Required'
  }
  if (Validator.isEmpty(data.lastName)) {
    errors.lastName = 'Lastname is Required'
  }
  if (!Validator.isEmail(data.email)) {
    errors.email = 'Email is invalid'
  }
  if (Validator.isEmpty(data.phone_no)) {
    errors.phone_no = 'Phone Number is Required'
  }
  if(data.interests.length === 0){
    errors.interests = 'You Must choose at least on Interest'
  }
  if(data.motivation.length === 0){
    errors.motivation = 'You Must choose at least on Motivations'
  }

  return {
    errors,
    isValid: isEmpty(errors)
  }
}