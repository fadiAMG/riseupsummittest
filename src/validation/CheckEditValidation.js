import Validator from 'validator'
import isEmpty from 'lodash/isEmpty'

export default function ValidateLogin(data) {
  let errors = {};
  if (Validator.isEmpty(data.fname)) {
    errors.fname = 'Firstname is Required'
  }
  if (Validator.isEmpty(data.lname)) {
    errors.lname = 'Lastname is Required'
  }
  if (Validator.isEmpty(data.gender)) {
    errors.gender = 'Gender is Required'
  }
  if (Validator.isEmpty(data.city)) {
    errors.city = 'City is Required'
  }
  if(data.iam.length === 0){
    errors.iam = 'Please Fill Data'
  }
  if(data.you_can_ask_me_about.length === 0){
    errors.you_can_ask_me_about = 'Please Fill Data'
  }
  if(data.iam_here_to.length === 0){
    errors.iam_here_to = 'Please Fill Data'
  }
  
  return {
    errors,
    isValid: isEmpty(errors)
  }
}
