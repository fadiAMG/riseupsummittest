import Validator from 'validator'
import isEmpty from 'lodash/isEmpty'

export default function CreateEntityValidation(data) {
  let Entityerrors = {};

  if (Validator.isEmpty(data.enitiy_name)) {
    Entityerrors.enitiy_name = 'Enitiy Name is Required'
  }
  if (Validator.isEmpty(data.job_title)) {
    Entityerrors.job_title = 'Job Title  is Required'
  }
  
  return {
    Entityerrors,
    isValid: isEmpty(Entityerrors)
  }
}
