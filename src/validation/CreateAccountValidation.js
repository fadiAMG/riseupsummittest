import Validator from 'validator'
import isEmpty from 'lodash/isEmpty'

export default function CreateAccountValidation(data) {
  let errors = {};

  if (Validator.isEmpty(data.password)) {
    errors.password = 'Confirmation Code is Required'
  }
  if (Validator.isEmpty(data.fname)) {
    errors.fname = 'First Name is Required'
  }
  if (Validator.isEmpty(data.lname)) {
    errors.lname = 'Last Name is Required'
  }
  if (!Validator.isEmail(data.email)) {
    errors.email = 'Email is invalid'
  }

  if (Validator.isEmpty(data.Repeatpassword)) {
    errors.Repeatpassword = 'Repeat Confirmation Code is Required'
  }
  if (data.Repeatpassword !== data.password) {
    errors.Notmatch = 'Confirmation Code Not Matched'
  }
  // if(data.interests.length === 0){
  //   errors.interests = 'You Must choose at least on Interest'
  // }
  // if(data.motivation.length === 0){
  //   errors.motivation = 'You Must choose at least on Motivations'
  // }

  return {
    errors,
    isValid: isEmpty(errors)
  }
}
