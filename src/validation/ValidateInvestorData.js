import Validator from 'validator'
import isEmpty from 'lodash/isEmpty'

export default function ValidateInvestorData(data) {
  let stepValidation = 1
  let errors = {}
  // if (!Validator.isEmail(data.username)) {
  //   errors.email = 'Email is invalid'
  // }

  if(stepValidation === 1){
    if(data.entity_type.length === 0){
      errors.entity_type = 'Entity Type is Required'
      stepValidation = 1
    }
    if (Validator.isEmpty(data.country)) {
      errors.country = 'Country is Required'
      stepValidation = 1
    }
    if (Validator.isEmpty(data.year_of_invest)) {
      errors.year_of_invest = 'Year of Invest is Required'
      stepValidation = 1
    }else{
      stepValidation = 2
    }
  }

  // if(stepValidation === 2){
  //   if (Validator.isEmpty(data.founder_fname)) {
  //     errors.founder_fname = 'Founder First Name is Required';
  //     stepValidation = 2
  //   }
  // }
  return {
    errors,
    isValid: isEmpty(errors),
    step: stepValidation
  }
}
