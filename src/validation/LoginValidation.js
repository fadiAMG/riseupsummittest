import Validator from 'validator'
import isEmpty from 'lodash/isEmpty'

export default function ValidateLogin(data, type) {
  let errors = {};
  if (!Validator.isEmail(data.username)) {
    errors.email = 'Email is invalid'
  }
  if (Validator.isEmpty(data.username)) {
    errors.email = 'Email is Required'
  }
  if (Validator.isEmpty(data.password)) {
    if(type === 'loginWithTicket'){
      errors.password = 'Ticket Confirmation Code is Required'
    }else{
      errors.password = 'Confirmation Code is Required'
    }
  }
  return {
    errors,
    isValid: isEmpty(errors)
  }
}
