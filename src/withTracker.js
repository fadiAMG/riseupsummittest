import React from "react"
import GoogleAnalytics from "react-ga"
import ReactPixel from "react-facebook-pixel"

GoogleAnalytics.initialize("UA-85514989-1")
ReactPixel.init("1866905303543941")

const withTracker = (WrappedComponent, options = {}) => {
  const trackPage = page => {
    GoogleAnalytics.set({
      page
      // ...options,
    })
    GoogleAnalytics.pageview(page)
    ReactPixel.pageView()
  }

  const HOC = class extends React.Component {
    componentDidMount() {
      const page = this.props.location.pathname
      trackPage(page)
    }

    componentWillReceiveProps(nextProps) {
      const currentPage = this.props.location.pathname
      const nextPage = nextProps.location.pathname

      if (currentPage !== nextPage) {
        trackPage(nextPage)
      }
    }

    render() {
      return <WrappedComponent {...this.props} />
    }
  }

  return HOC
}

export default withTracker
